//
//  LibraryViewController.m
//  Bovilisplan
//
//  Created by Admin on 4/27/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "ListingViewController.h"
#import "NotificationViewController.h"
#import "LibraryViewController.h"
#import "ProductMaster.h"
#import "HomeViewController.h"
#import "LoginViewController.h"
#import "WelcomeDetailViewController.h"
#import "MenuView.h"
#import "VaccineDetailViewController.h"
#import "DataController.h"
#import "JSON.h"
#import "BovilisApi.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "AboutUsViewController.h"
#import "GAITracker.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "AllAnimalsViewController.h"


@interface LibraryViewController ()
@property(nonatomic, weak)IBOutlet UILabel*lblMSD;
@property (strong,nonatomic)IBOutlet UITableView *tableView;
@property(nonatomic, weak)IBOutlet UILabel*lblTitle;
@property(nonatomic,weak) IBOutlet UIImageView *logoImg;

@end

@implementation LibraryViewController {
    
    UIView *transparentView ;
    MenuView *menuView;
    AppDelegate* appDelegate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
  self.logoImg.image = [UIImage imageNamed:[Utility setLocalizeLogo:@"logo"]];
    [_lblMSD setText:[appDelegate.dictCulture valueForKey:@"MsdAnimalHealthVaccines"]];
    NSString * lblMSD = [appDelegate.dictCulture valueForKey:@"MsdAnimalHealthVaccines"];
    if (lblMSD.length==0)
    {
        [_lblMSD setText:@"Msd Animal Health Vaccines"];
    }
    
    [_lblTitle setText:[appDelegate.dictCulture valueForKey:@"Library"]];
    NSString * lblTitle = [appDelegate.dictCulture valueForKey:@"Library"];
    if (lblTitle.length==0)
    {
        [_lblTitle setText:@"Library"];
    }
    
    [self loadMenuView];

}

-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Library";
    [transparentView setHidden:YES];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

#pragma mark === TableView Delegates/Datasource===
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appDelegate.productsArrayList.count;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:     simpleTableIdentifier];
    }
    
    ProductMaster * obj = [[ProductMaster alloc] init];
    obj =[appDelegate.productsArrayList objectAtIndex:indexPath.row];
    cell.textLabel.text= obj.vaccineNameL;
    cell.textLabel.font = [UIFont fontWithName:@"UNIVERS" size:15.0] ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.textColor =[UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1];
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    
    ProductMaster * obj1 = [[ProductMaster alloc] init];
    obj1 =[appDelegate.productsArrayList objectAtIndex:indexPath.row];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"Library_Tab_From_Home"
                                                           label:[NSString stringWithFormat:@"Product_Name_%@",obj1.vaccineNameL]
                                                           value:nil] build]];
    
    VaccineDetailViewController *vc = [VaccineDetailViewController new];
    vc.productObj = obj1;
    vc.titleTop =obj1.vaccineNameL;
    vc.dose =[obj1.dosageL capitalizedString];
    vc.admins=obj1.addministrationL;
    vc.cID =obj1.productIDL;
    vc.moreInfo = obj1.moreInformationL;
    vc.link = obj1.linksAndDownloadsL;
    vc.primary = obj1.primaryL;
    vc.secondaryOne = obj1.secondaryOneL;
    vc.secondaryTwo = obj1.secondaryTwoL;
    vc.secondaryThree = obj1.secondaryThreeL;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)loadMenuView
{
    transparentView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( transparentView.frame.size);
    UIGraphicsBeginImageContext( transparentView.frame.size);
    [[UIImage imageNamed:@"bg-Transparent"] drawInRect: transparentView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
   // transparentView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [self.view addSubview:transparentView];
    menuView= [[MenuView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-295, 0, self.view.frame.size.width, self.view.frame.size.height-0)];
    CGRect newFrame = CGRectMake(0, 0, menuView.frame.size.width,menuView.frame.size.height);
    menuView.innerView.frame = newFrame;
    
    [menuView.btnClose addTarget:self action:@selector(closeMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnDisclaimer addTarget:self action:@selector(disclaimerMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnPrivacy addTarget:self action:@selector(privacyMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnTerms addTarget:self action:@selector(termsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnSignout addTarget:self action:@selector(signoutMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnCalendar addTarget:self action:@selector(calendarMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnListing addTarget:self action:@selector(listingMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnLibrary addTarget:self action:@selector(libraryMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnNotification addTarget:self action:@selector(notificationMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [menuView.btnAboutUS addTarget:self action:@selector(aboutUsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
       [menuView.btnAnimals addTarget:self action:@selector(animalsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [transparentView addSubview:menuView];
}

-(void)showMenu
{
    [self loadMenuView];
    [transparentView setHidden:FALSE];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transparentView.layer addAnimation:transition forKey:nil];
}

-(void)closeMenuButtonPressed
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transparentView.layer addAnimation:transition forKey:nil];
    [transparentView setHidden:YES];
}

-(void)animalsMenuButtonPressed {
    AllAnimalsViewController *settingVC = [AllAnimalsViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)aboutUsMenuButtonPressed
{
    AboutUsViewController *settingVC = [AboutUsViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)notificationMenuButtonPressed
{
    NotificationViewController *settingVC = [NotificationViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)libraryMenuButtonPressed
{
    LibraryViewController *settingVC = [LibraryViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)listingMenuButtonPressed
{
    ListingViewController *settingVC = [ListingViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)calendarMenuButtonPressed
{
    HomeViewController *settingVC = [HomeViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)signoutMenuButtonPressed
{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"AllApiDataDictionary"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IsLogin"];
   // [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SaveUserEmail"];

    [[NSUserDefaults standardUserDefaults] synchronize];
    appDelegate.reloadCalendar=@"No";
    appDelegate.isLoadedNotifications=@"No";
    LoginViewController *loginVC = [LoginViewController new];
    [self.navigationController pushViewController:loginVC animated:YES];
}

-(void)privacyMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Privacy Policy";
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)disclaimerMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Disclaimer";
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)termsMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Terms and Conditions";
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark === UIButton Actions ===
-(IBAction)menuButtonPressed:(id)sender
{
    [self showMenu];
}

-(IBAction)homeButtonPressed:(id)sender
{
    HomeViewController *HomeVc = [HomeViewController new];
    [self.navigationController pushViewController:HomeVc animated:NO];
}

-(IBAction)listButtonPressed:(id)sender
{
    ListingViewController *listVc = [ListingViewController new];
    [self.navigationController pushViewController:listVc animated:NO];
}

-(IBAction)notificationButtonPressed:(id)sender
{
    NotificationViewController *notificationVc = [NotificationViewController new];
    [self.navigationController pushViewController:notificationVc animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
