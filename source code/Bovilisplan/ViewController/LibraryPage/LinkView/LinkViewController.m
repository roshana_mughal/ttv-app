//
//  LinkViewController.m
//  TTVApp
//
//  Created by Apple Macbook on 15/06/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "LinkViewController.h"
#import "AppDelegate.h"

@interface LinkViewController ()<UIWebViewDelegate>
{
    UIAlertController *active_alert;
    UIActivityIndicatorView *activityView;
    NSURL *requestURL ;
}

@property (strong,nonatomic)IBOutlet UIWebView *webView;
@end

@implementation LinkViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showLink];
}

-(void)showLink
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    
    activityView = [[UIActivityIndicatorView alloc]
                    initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityView.center = CGPointMake(160, 240);
    activityView.hidesWhenStopped = YES;
    [self.webView addSubview:activityView];
    [activityView startAnimating];
    [self performSelector:@selector(showWebView) withObject:nil afterDelay:0];
}

-(void)showWebView
{
    if ([_isFrom  isEqualToString:@"About"])
    {
        _link = @"http://wwww.msd-animal-health.com";
    }
    
    requestURL = [NSURL URLWithString:_link];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:requestURL];
    [ self.webView loadRequest:requestObj];
    
}

#pragma mark === UIWebview Delegate ===
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityView stopAnimating];
}

#pragma mark === UIButton Actions ===
-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
