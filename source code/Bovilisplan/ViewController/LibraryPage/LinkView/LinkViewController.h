//
//  LinkViewController.h
//  TTVApp
//
//  Created by Apple Macbook on 15/06/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkViewController : UIViewController
@property(nonatomic,strong) NSString *link;
@property(nonatomic,strong) NSString *isFrom;
@end
