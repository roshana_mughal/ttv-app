//
//  VaccineDetailViewController.m
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "VaccineDetailViewController.h"
#import "ProductMaster.h"
#import "BovilisApi.h"
#import "LinkViewController.h"
#import "AppDelegate.h"

@interface VaccineDetailViewController ()<UIScrollViewDelegate,UIWebViewDelegate>
{
    AppDelegate * appDelegate;
    NSURL *requestURL ;
    
}

@property(nonatomic,strong) IBOutlet UILabel *lblTitle;
@property(nonatomic,strong) IBOutlet UILabel *lblVaccineStatus;

@property(nonatomic,strong) IBOutlet UILabel *lbl1;
@property(nonatomic,strong) IBOutlet UILabel *lbl2;
@property(nonatomic,strong) IBOutlet UILabel *lbl3;
@property(nonatomic,strong) IBOutlet UIImageView *imgView1;
@property(nonatomic,strong) IBOutlet UIImageView *imgView2;
@property(nonatomic,strong) IBOutlet UIImageView *imgView3;
@property(nonatomic,strong) IBOutlet UILabel *lblDose;
@property(nonatomic,strong) IBOutlet UILabel *lblDoseHeading;
@property(nonatomic,strong) IBOutlet UILabel *lblMoreInfo;
@property(nonatomic,strong) IBOutlet UIScrollView * outerScrollView;
@property(nonatomic,strong) IBOutlet UITextView *tView;
@property(nonatomic,strong) IBOutlet UILabel *lblMoreInfoText;


@end

@implementation VaccineDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate= (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [self loadData];
}

-(void)loadData
{
    _imgView1.hidden=TRUE;
    _imgView2.hidden=TRUE;
    _imgView3.hidden=TRUE;
    
    _lbl1.hidden=FALSE;
    _lbl2.hidden=FALSE;
    _lbl3.hidden=FALSE;
    [self getVaccinesCountFromServer];
    
    [_tView flashScrollIndicators];
    
 
    NSString *status = [appDelegate.dictCulture valueForKey:@"VaccineStatus"];
    status = status.length == 0? @"":status;
    self.lblVaccineStatus.text = status;
    _lblTitle.text = [appDelegate.dictCulture valueForKey:@"VaccineDetail"];
    NSString * lblTitle = [appDelegate.dictCulture valueForKey:@"VaccineDetail"];
    if (lblTitle.length==0)
    {
        [_lblTitle setText:@"Vaccine Detail"];
    }
    
    _lblMoreInfo.text = [appDelegate.dictCulture valueForKey:@"MoreInformation"];
    NSString * moreInfoL = [appDelegate.dictCulture valueForKey:@"MoreInformation"];
    if (moreInfoL.length==0)
    {
        [_lblMoreInfo setText:@"More Schedule:"];
    }
    _lblMoreInfo.text = (moreInfoL.length==0)? @"More Schedule:": [NSString stringWithFormat:@"%@:",moreInfoL];
    
    _linksDownload.text = [appDelegate.dictCulture valueForKey:@"LinksAndDownloads"];
    NSString * linksDownload = [appDelegate.dictCulture valueForKey:@"LinksAndDownloads"];
    if (linksDownload.length==0)
    {
        [_linksDownload setText:@"Links And Downloads"];
    }
    
    _titlePVaccine.text = _titleTop;
    _tView.hidden=TRUE;
    
    NSString *ProductInfo = [appDelegate.dictCulture valueForKey:@"ProductInformation"];
    
    if(ProductInfo.length>0)
    {
        [_linkButton setTitle:ProductInfo forState:UIControlStateNormal];
    }
    else
    {
        
        [_linkButton setTitle:@"Product Information" forState:UIControlStateNormal];
    }
    
    _vaccineSchedule.text = [NSString stringWithFormat:@"%@:",[appDelegate.dictCulture valueForKey:@"VaccineSchedule"]] ;
    NSString * vaccineSchedule = [appDelegate.dictCulture valueForKey:@"VaccineSchedule"];
    if (vaccineSchedule.length==0)
    {
        _vaccineSchedule.text = @"Vaccine Schedule:" ;
    }
    //
    ProductMaster  *theObjectData =[[ProductMaster alloc] init];
    
    if (![_primary isEqualToString:@"NA"]  && _primary.length!= 0)
    {
        _imgView1.hidden = FALSE;
        NSString *newString = [_primary stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString* primary =  [appDelegate.dictCulture valueForKey:newString];
            _lbl1.text =  primary.length == 0 ? newString : primary;
             
    }
    
    if (![_secondaryOne isEqualToString:@"NA"] && _secondaryOne.length!= 0)
    {
        _imgView2.hidden = FALSE;
        NSString *newString = [_secondaryOne stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString* sec =  [appDelegate.dictCulture valueForKey:newString];
        _lbl2.text =  sec.length == 0 ? newString : sec;
         

    }
    
    if (![_secondaryTwo isEqualToString:@"NA"] && _secondaryTwo.length!= 0)
    {
        _imgView3.hidden=FALSE;
        NSString *newString = [_secondaryTwo stringByReplacingOccurrencesOfString:@" " withString:@""];
        _lbl3.text =   [appDelegate.dictCulture valueForKey:newString];
        NSString* third =  [appDelegate.dictCulture valueForKey:newString];

        _lbl3.text =  third.length == 0 ? newString : third;


    }
    NSString * finalDose = [appDelegate.dictCulture valueForKey:_dose];
    finalDose = finalDose.length == 0 ? _dose: finalDose;

    _lblDose.text= [NSString stringWithFormat:@"%@ %@",finalDose,_admins];
  
    NSString *doselblTitle = [appDelegate.dictCulture valueForKey:@"Dose"];
    
   _lblDoseHeading.text =  doselblTitle.length == 0 ? @"Dose:" : [NSString stringWithFormat:@"%@:",doselblTitle];
    if(![_moreInfo isEqualToString:@"NA"])
    {
        _tView.hidden=FALSE;
        
        self.tView.text = [appDelegate.dictCulture valueForKey:_moreInfo];

    }
    
    [_outerScrollView setContentSize:CGSizeMake( _outerScrollView.frame.size.width, 600)];
}

-(void)getVaccinesCountFromServer {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    BovilisApi * objN = [BovilisApi sharedInstance];
    
    [objN getVaccineCountDataWithID:self.productObj.productIDL WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        if(success) {
        
        NSDictionary *resultDic = [result objectForKey:@"results"];

        NSString *pastAnimalCount = [resultDic valueForKey:@"NumberOfPastVaccines"];
            
        NSString *past = [appDelegate.dictCulture valueForKey:@"0animalhavebeenvaccinated."];
            
        past = past.length == 0 ? @"animal have been vaccinated.": past;
            
        self.lblPast.text = [NSString stringWithFormat:@"%@ %@",pastAnimalCount,past];
            
        
        NSString *animal = [appDelegate.dictCulture valueForKey:@"animals"];

        NSString *upcoming = [appDelegate.dictCulture valueForKey:@"Upcomingvaccination"];
        NSString *upAnimalsCount = [resultDic valueForKey:@"NumberOfUpcomingVaccines"];
           upcoming = upcoming.length == 0 ? @"Upcomming vaccination":upcoming;
             animal = animal.length == 0 ? @"Animal(s)":animal;
            self.lblUpcoming.text = [NSString stringWithFormat:@"%@ %@ %@.",upcoming,upAnimalsCount,animal];
        
            dispatch_async(dispatch_get_main_queue(), ^{
                  [MBProgressHUD hideHUDForView:self.view animated:YES];

              });
        }
        else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];

        }

    } failure:^(NSError *error, NSString *message) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

#pragma mark === UIButton Actions ===
-(IBAction)linkButtonPressed
{
    
    NSURL *url = [NSURL URLWithString:_link];
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    
}

-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
