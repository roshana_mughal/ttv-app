//
//  VaccineDetailViewController.h
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductMaster.h"

@interface VaccineDetailViewController : UIViewController

@property(nonatomic,strong) IBOutlet UILabel *titlePVaccine;
@property(nonatomic,strong) IBOutlet UILabel * linksDownload;
@property(nonatomic,strong) IBOutlet UILabel * vaccineSchedule;
@property(nonatomic,strong) IBOutlet UILabel * lblUpcoming;
@property(nonatomic,strong) IBOutlet UILabel * lblPast;

@property(nonatomic,strong) IBOutlet UIButton * linkButton;
@property(nonatomic,strong) ProductMaster *productObj;


@property(nonatomic,strong) NSString *cID;
@property(nonatomic,strong) NSString *titleTop;
@property(nonatomic,strong) NSString *primaryV;
@property(nonatomic,strong) NSString *reVaccination;
@property(nonatomic,strong) NSString * primary;
@property(nonatomic,strong) NSString * secondaryOne;
@property(nonatomic,strong) NSString * secondaryTwo;
@property(nonatomic,strong) NSString * secondaryThree;
@property(nonatomic,strong) NSString *recordar;
@property(nonatomic,strong) NSString *dose;
@property(nonatomic,strong) NSString *link;
@property(nonatomic,strong) NSString *admins;
@property(nonatomic,strong) NSString *moreInfo;
@property(nonatomic) NSMutableArray *linksArray;
@end
