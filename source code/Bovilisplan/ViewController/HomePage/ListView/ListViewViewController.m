//
//  ListViewViewController.m
//  TTVApp
//
//  Created by apple on 17/12/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import "ListViewViewController.h"
#import "DateCell.h"
#import "AppDelegate.h"
#import "GetMappingDays.h"
#import "VaccineFarmCell.h"
#import "AnimalListViewCell.h"

@interface ListViewViewController () <UITableViewDataSource,UIAlertViewDelegate> {
    AppDelegate* appDelegate;
    NSMutableArray* sectionHeader;
    NSMutableArray* sectionHeaderDates;
    
    NSMutableDictionary* sectionDirectory ;
    NSMutableDictionary*   sortedNames;
    ListViewSelectedType selectedType;
    
}

@property(strong,nonatomic) IBOutlet UITableView *tableView;

@property(strong,nonatomic) IBOutlet UIButton *btnDate;
@property(strong,nonatomic) IBOutlet UIButton *btnFarm;
@property(strong,nonatomic) IBOutlet UIButton *btnAnimal;
@property(strong,nonatomic) IBOutlet UIButton *btnVaccine;



@end

@implementation ListViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
    
    
}

-(void)setUI {
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [self configureTableViewCell];
    selectedType = kDate;
    [self loadDataOfNotifications];
    [self.tableView reloadData];
    
}

#pragma mark === Configure TableView Cell===
-(void)configureTableViewCell {
    
    [_tableView registerNib:[UINib nibWithNibName:@"DateCell"
                                           bundle:nil] forCellReuseIdentifier:@"DateCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"VaccineFarmCell"
                                           bundle:nil] forCellReuseIdentifier:@"VaccineFarmCell"];
    
    [_tableView registerNib:[UINib nibWithNibName:@"AnimalListViewCell"
                                           bundle:nil] forCellReuseIdentifier:@"AnimalListViewCell"];
    
    
    
}


-(void)loadDataOfNotifications {
    
    sectionHeader = [[NSMutableArray alloc]init];
    sectionDirectory = [NSMutableDictionary new];
    sectionHeaderDates = [[NSMutableArray alloc]init];
    
    
    for (NSInteger i = 0; i < appDelegate.getScheduleVaccineArray.count; i++) {
        
        NSDictionary *tmp = appDelegate.getScheduleVaccineArray[i];
        NSString *str = [tmp valueForKey:@"addedDateTime2"];
        
        NSString *firstLetter = [[tmp valueForKey:@"addedDateTime2"] substringToIndex:7];
        if (![sectionDirectory valueForKey:firstLetter])
        {
            [sectionDirectory setObject:[NSMutableArray new] forKey:firstLetter];
            [sectionHeader addObject:firstLetter];
            [sectionHeaderDates addObject:str];
        }
        
        [[sectionDirectory objectForKey:firstLetter] addObject:tmp];
    }
    
    [self arrangeSortedDictionary];
}

-(void)arrangeSortedDictionary {
    
    sortedNames = [NSMutableDictionary dictionary];
    
    for(int characterIndex = 0; characterIndex < sectionHeader.count; characterIndex++)
    {
        NSString *date = [sectionHeader objectAtIndex:characterIndex];
        NSString *alphabetCharacter = [date substringWithRange:NSMakeRange(0, 7)];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"addedDateTime2 BEGINSWITH[c] %@",alphabetCharacter];
        NSArray *filteredNames =  (NSMutableArray *)[appDelegate.getScheduleVaccineArray  filteredArrayUsingPredicate:resultPredicate];
        [sortedNames setObject:filteredNames forKey:alphabetCharacter];
    }
}

#pragma mark === TableView Delegates/Datasource===
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [sectionHeader count];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index {
    
    return index;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 30);
    myLabel.font = [UIFont fontWithName:@"UNIVERS" size:18] ;
    myLabel.textColor = [UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1];

    myLabel.text =
    [NSString stringWithFormat:@"    %@",[self tableView:tableView titleForHeaderInSection:section]];
    myLabel.backgroundColor =[UIColor whiteColor];

    UIView *headerView = [[UIView alloc] init];
      [headerView addSubview:myLabel];
    UILabel *lblLine = [[UILabel alloc] init];
         lblLine.frame = CGRectMake(180, 15, self.tableView.frame.size.width, 1);
       
         lblLine.backgroundColor =[UIColor lightGrayColor];
    [headerView addSubview:lblLine];

        return headerView;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    return  [Utility setListViewDateFormatter:[sectionHeaderDates objectAtIndex:section]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionIndexTitle = [sectionHeader objectAtIndex:section];
    NSArray *sectionTitle = [sortedNames objectForKey:sectionIndexTitle];
    return [sectionTitle count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
    NSMutableArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
    GetMappingDays * obj = [sectionTitleList1 objectAtIndex:indexPath.row];
    if (selectedType == kDate) {
        
        NSString *CellIdentifier = @"DateCell";
        DateCell *cell = (DateCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        return  [self configureDateCell:cell withIndexPath:indexPath withObject:obj];
        
    }
    else if (selectedType == kAnimal) {
        
   NSString *CellIdentifier = @"AnimalListViewCell";
         AnimalListViewCell *cell = (AnimalListViewCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
         
         return  [self configureAnimalCell:cell withIndexPath:indexPath withObject:obj];
        
    }
    else if (selectedType == kVaccine) {
        
        NSString *CellIdentifier = @"VaccineFarmCell";
        VaccineFarmCell *cell = (VaccineFarmCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        return  [self configureVaccineCell:cell withIndexPath:indexPath withObject:obj];
        
    }
    else if (selectedType == kFarm) {
        
        NSString *CellIdentifier = @"VaccineFarmCell";
        VaccineFarmCell *cell = (VaccineFarmCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        return  [self configureFarmCell:cell withIndexPath:indexPath withObject:obj];
    }
    
    return  0;
}

-(AnimalListViewCell *)configureAnimalCell:(AnimalListViewCell*)cell withIndexPath:(NSIndexPath*)indexPath withObject:(GetMappingDays*)obj {
    
    cell.lblVaccineName.text= [NSString stringWithFormat:@"%@: %@",@"Vaccine Name",obj.scheduleVaccine];
    
    NSString *animalIDs = obj.NumberOfAnimals;
    animalIDs = [animalIDs stringByReplacingOccurrencesOfString:@"," withString:@", #"];
    animalIDs =  [NSString stringWithFormat:@"#%@",animalIDs];

    cell.lblAnimalIDs.text= [NSString stringWithFormat:@"%s: %@","Animals IDs",animalIDs];

    cell.lblDate.text=  [Utility getDayFromDate:obj.addedDateTime2];
    cell.lblDay.text=  [[Utility getDayNameFromDate:obj.addedDateTime2] uppercaseString];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return  cell;
}


-(VaccineFarmCell *)configureFarmCell:(VaccineFarmCell*)cell withIndexPath:(NSIndexPath*)indexPath withObject:(GetMappingDays*)obj {
    
    cell.lbl1.text= [NSString stringWithFormat:@"%@: %@",@"Farm Name",obj.farmName];
    cell.lbl2.text= [NSString stringWithFormat:@"%s: %lu %@","Animals",(unsigned long)[Utility getAnimalCountWithAnimal:obj.NumberOfAnimals],@"animal(s) selected"];
    cell.lbl3.text=[NSString stringWithFormat:@"%@: %@",@"Vaccine Name",obj.scheduleVaccine];

    cell.lblDate.text=  [Utility getDayFromDate:obj.addedDateTime2];
    cell.lblDay.text=  [[Utility getDayNameFromDate:obj.addedDateTime2] uppercaseString];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return  cell;
}

-(VaccineFarmCell *)configureVaccineCell:(VaccineFarmCell*)cell withIndexPath:(NSIndexPath*)indexPath withObject:(GetMappingDays*)obj {
    
    cell.lbl1.text = [NSString stringWithFormat:@"%@: %@",@"Vaccine Name",obj.scheduleVaccine];
    cell.lbl2.text = [NSString stringWithFormat:@"%@: %@",@"Farm Name",obj.farmName];
    cell.lbl3.text = [NSString stringWithFormat:@"%s: %lu %@","Animals",(unsigned long)[Utility getAnimalCountWithAnimal:obj.NumberOfAnimals],@"animal(s) selected"];
    
    cell.lblDate.text=  [Utility getDayFromDate:obj.addedDateTime2];
    cell.lblDay.text=  [[Utility getDayNameFromDate:obj.addedDateTime2] uppercaseString];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return  cell;
}

-(DateCell *)configureDateCell:(DateCell*)cell withIndexPath:(NSIndexPath*)indexPath withObject:(GetMappingDays*)obj {
    cell.lblVaccine.text= obj.scheduleVaccine;
    cell.lblFarm.text= obj.farmName;
    cell.lblDate.text=  [Utility getDayFromDate:obj.addedDateTime2];
    cell.lblDay.text=  [[Utility getDayNameFromDate:obj.addedDateTime2] uppercaseString];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return  cell;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 15;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  UITableViewAutomaticDimension;
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark === UIButton Actions ===

-(IBAction)tabButtonPressed:(id)sender {
    
    
    switch ([sender tag] ) {
        case 1:
            
            selectedType = kDate;
            self.btnDate.alpha = 1.0;
            self.btnFarm.alpha = 0.5;
            self.btnAnimal.alpha = 0.5;
            self.btnVaccine.alpha = 0.5;
            [self.tableView reloadData];
            
            break;
            
        case 2:
            
            selectedType = kFarm;
            self.btnDate.alpha = 0.5;
            self.btnFarm.alpha = 1.0;
            self.btnAnimal.alpha = 0.5;
            self.btnVaccine.alpha = 0.5;
            [self.tableView reloadData];
            
            
            break;
        case 3:
            
            selectedType = kAnimal;
            self.btnDate.alpha = 0.5;
            self.btnFarm.alpha = 0.5;
            self.btnAnimal.alpha = 1.0;
            self.btnVaccine.alpha = 0.5;
            [self.tableView reloadData];
            
            break;
        case 4:
            
            selectedType = kVaccine;
            self.btnDate.alpha = 0.5;
            self.btnFarm.alpha = 0.5;
            self.btnAnimal.alpha = 0.5;
            self.btnVaccine.alpha = 1.0;
            [self.tableView reloadData];
            
            break;
            
        default:
            break;
    }
    
}

-(IBAction)calendarViewButtonPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
