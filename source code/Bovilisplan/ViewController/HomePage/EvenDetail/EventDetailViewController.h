//
//  EventDetailViewController.h
//  Bovilisplan
//
//  Created by Admin on 5/22/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"

@protocol EventDetailViewControllerDelegate <NSObject>
@optional

- (void)isBackFromDetail:(NSString *)back ;

@end

@interface EventDetailViewController : UIViewController
@property(nonatomic,strong) NSString *farmName;
@property(nonatomic,strong) NSString *date;
@property(nonatomic,strong) NSString *region;
@property(nonatomic,strong) NSString *comments;
@property(nonatomic,strong) NSString *vaccineName;
@property(nonatomic,strong) NSString *vaccineType;
@property(nonatomic,strong) NSString *vaccineSchedule;
@property(nonatomic,strong) NSString *isFrom;
@property(nonatomic,strong) NSString *idForEdit;
@property(nonatomic,strong) NSString *image1;
@property(nonatomic,strong) NSString *selectedDate;
@property(nonatomic, assign) id <EventDetailViewControllerDelegate> delegate;
@property(nonatomic) BOOL isFromNotification;



@end
