//
//  EventDetailViewController.m
//  Bovilisplan
//
//  Created by Admin on 5/22/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//
#import "AppDelegate.h"
#import "UserVacineMaster.h"
#import "AddEventView.h"
#import "EventDetailViewController.h"
#import "BovilisApi.h"
#import "EditEventView.h"
#import "HomeViewController.h"
#import "GetMappingDays.h"
#import "GetDaysData.h"
#import "HomeViewController.h"
#import "AnimalsViewController.h"

@interface EventDetailViewController () {
    
    EventDetailViewController *obj;
    AppDelegate *appDelegate;
    NSData * imageDatatoShow;
    UIAlertController *active_alert;
    AddEventView *addEvent;
    EditEventView *editVaccineView;
    UIView *transparentView1;
    UIImage *decodeImage;
    NSString *decodeData;
    UIActivityIndicatorView *activityView;
}

@property(nonatomic,strong) IBOutlet UILabel*lblName;
@property(nonatomic,strong) IBOutlet UITextField*lblReg;
@property(nonatomic,strong) IBOutlet UILabel*lblVaccineName;
@property(nonatomic,strong) IBOutlet UILabel*lblVaccineType;
@property(nonatomic,strong) IBOutlet UILabel*lblVaccineSchedule;
@property(nonatomic,strong) IBOutlet UILabel*lblComments;
@property(nonatomic,strong) IBOutlet UILabel*lblDate;
@property(nonatomic,strong) IBOutlet UIButton *btnEdit;
@property(nonatomic,strong) IBOutlet UIButton *btnViewAnimal;

@property(nonatomic,strong) IBOutlet  UIImageView *imagView;
@property(nonatomic,strong) IBOutlet UILabel*lblTitle;
@property(nonatomic,strong) IBOutlet UIButton *btnViewImage;
@property(nonatomic,strong) IBOutlet UITextField *txt1;
@property(nonatomic,strong) IBOutlet UITextField *txt2;
@property(nonatomic,strong) IBOutlet UITextField *txt3;
@property(nonatomic,strong) IBOutlet UITextField *txt4;
@property(nonatomic,strong) IBOutlet UITextView *tView;

@end

@implementation EventDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    _btnViewImage.enabled=TRUE;
    _lblTitle.text = [appDelegate.dictCulture valueForKey:@"VaccineDetail"];
    
    NSString * lblTitle = [appDelegate.dictCulture valueForKey:@"VaccineDetail"];
    
    if (lblTitle.length==0)
    {
        _lblTitle.text = @"Vaccine Detail";
    }
    
    [self setUI];
}

-(void)viewWillLayoutSubviews {
    
    self.tView.contentInset = UIEdgeInsetsMake(-10.0,0.0,0,0.0);
    
}

-(void)setUI {
    
    NSString * viewAnimalText = [appDelegate.dictCulture valueForKey:@"ViewAnimals"] ;
     
    viewAnimalText = viewAnimalText.length == 0? @"View Animals":viewAnimalText;
     [self.btnViewAnimal setTitle:viewAnimalText forState:UIControlStateNormal];
    
    if([_isFrom isEqualToString:@"Notification"]) {
        _lblName.text=_farmName;
        _lblDate.text= _date;
        _lblReg.text=_region;
        
        self.tView.text=self.comments;
        
        _lblVaccineName.text = self.vaccineName;
        self.tView.text=_comments;
        
        _lblVaccineSchedule.text=_vaccineSchedule;
        NSString *  vaccinType = [appDelegate.dictCulture valueForKey:self.vaccineType];
        if (vaccinType.length ==0 )
        {
            if   ([vaccinType isEqualToString:@"Revaccination"]) {
                vaccinType =@"Revaccination";
            }
            
            else if   ([vaccinType isEqualToString:@"SecondaryVaccination"]) {
                vaccinType =@"Secondary Vaccination";
            }
            else if   ([vaccinType isEqualToString:@"PrimaryVaccination"]) {
                vaccinType =@"Primary Vaccination";
            }
            
        }
        _lblVaccineType.text = vaccinType.length == 0 ? self.vaccineType :vaccinType  ;
        
        
    }
    else if([_isFrom isEqualToString:@"History"]) {
        
        NSDateFormatter *dateFormatC = [[NSDateFormatter alloc] init];
        [dateFormatC setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        NSDate * serverDate = [dateFormatC dateFromString:_date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MMM-yyyy"];
        NSString * newDateString = [dateFormat stringFromDate:serverDate];
        _lblName.text=_farmName;
        _lblDate.text= [Utility setLocalDateFormatter:_date];
        _lblReg.text=_region;
        
        
        _lblVaccineName.text=[appDelegate.dictCulture valueForKey:_vaccineName];;
        self.tView.text=_comments;
        
        _lblVaccineSchedule.text=_vaccineSchedule;
        NSString *  vaccinType = [appDelegate.dictCulture valueForKey:appDelegate.vaccineType];
        if (vaccinType.length ==0 )
        {
            if   ([vaccinType isEqualToString:@"Revaccination"]) {
                vaccinType =@"Revaccination";
            }
            
            else if   ([vaccinType isEqualToString:@"SecondaryVaccination"]) {
                vaccinType =@"Secondary Vaccination";
            }
            else if   ([vaccinType isEqualToString:@"PrimaryVaccination"]) {
                vaccinType =@"Primary Vaccination";
            }
            
        }
        _lblVaccineType.text=vaccinType.length == 0 ? appDelegate.vaccineType :vaccinType  ;
        
        
        
        if([_vaccineName isEqualToString:@"NA"])
        {
            _lblVaccineName.text=@"";
        }
        
        if([_vaccineSchedule isEqualToString:@"NA"])
        {
            _lblVaccineSchedule.text=@"";
        }
        
        if([appDelegate.vaccineType isEqualToString:@"NA"])
        {
            _lblVaccineType.text=@"";
        }
        
        if([_region isEqualToString:@"NA"])
        {
            _lblReg.text=@"";
        }
        
        if([_comments isEqualToString:@"NA"])
        {
            self.tView.text=@"";
            
        }
    }
    else
    {
        NSDateFormatter *dateFormatC = [[NSDateFormatter alloc] init];
        [dateFormatC setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        NSDate * serverDate = [dateFormatC dateFromString:appDelegate.vaccineDate];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MMM-yyyy"];
        NSString * newDateString = [dateFormat stringFromDate:serverDate];
        _lblName.text=appDelegate.farmName;
        _lblDate.text = appDelegate.vaccineDate;
        _lblReg.text=appDelegate.region;
        _lblVaccineName.text=appDelegate.vaccineName;
        self.tView.text=appDelegate.vaccineComments;;
        
        _lblVaccineSchedule.text=appDelegate.scheduleVaccine;
        
        
        NSString *  vaccinType = [appDelegate.dictCulture valueForKey:appDelegate.vaccineType];
        if (vaccinType.length ==0 )
        {
            if   ([appDelegate.vaccineType isEqualToString:@"Revaccination"]) {
                vaccinType =@"Revaccination";
            }
            
            else if   ([appDelegate.vaccineType isEqualToString:@"SecondaryVaccination"]) {
                vaccinType =@"Secondary Vaccination";
            }
            else if   ([appDelegate.vaccineType isEqualToString:@"PrimaryVaccination"]) {
                vaccinType =@"Primary Vaccination";
            }
            
        }
        
        _lblVaccineType.text=vaccinType.length == 0 ? appDelegate.vaccineType :vaccinType  ;
        
        if([appDelegate.vaccineName isEqualToString:@"NA"])
        {
            _lblVaccineName.text=@"";
        }
        
        if([appDelegate.scheduleVaccine isEqualToString:@"NA"])
        {
            _lblVaccineSchedule.text=@"";
        }
        
        if([appDelegate.vaccineType isEqualToString:@"NA"])
        {
            _lblVaccineType.text=@"";
        }
        
        if([appDelegate.region isEqualToString:@"NA"])
        {
            _lblReg.text=@"";
        }
        
        if([appDelegate.vaccineComments isEqualToString:@"NA"])
        {
            self.tView.text=@"";
            
        }
    }
    decodeData =@"No";
    
    [_btnEdit setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    [_btnEdit setTitle:[appDelegate.dictCulture valueForKey:@"Edit"] forState:UIControlStateNormal];
    
    NSString * btnEdit = [appDelegate.dictCulture valueForKey:@"Edit"];
    
    if (btnEdit.length==0)
    {
        [_btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
    }
    
    [_btnViewImage setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnViewImage.layer.borderWidth = 1.0f;
    [_btnViewImage.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnViewImage setTitle:[appDelegate.dictCulture valueForKey:@"ViewImage"] forState:UIControlStateNormal];
    
    NSString * btnViewHistory = [appDelegate.dictCulture valueForKey:@"ViewImage"];
    
    if (btnViewHistory.length==0)
    {
        [_btnViewImage setTitle:@"View Image" forState:UIControlStateNormal];
    }
    
    NSString *vaccineTitle =[appDelegate.dictCulture valueForKey:@"VaccineTitle"];
    if ([vaccineTitle isEqualToString:@"NA"]  || (vaccineTitle.length == 0))
    {
        _txt1.text=@"Vaccine Title";
    }
    else
    {
        _txt1.text =vaccineTitle;
    }
    
    NSString *vaccineName =[appDelegate.dictCulture valueForKey:@"VaccineName"];
    if ([vaccineName isEqualToString:@"NA"]  || (vaccineName.length == 0))
    {
        _txt2.text=@"Vaccine Name";
    }
    else
    {
        _txt2.text =vaccineName;
    }
    
    NSString *vaccineType =[appDelegate.dictCulture valueForKey:@"VaccineType"];
    if ([vaccineType isEqualToString:@"NA"]  || (vaccineType.length == 0))
    {
        _txt3.text=@"Vaccine Type";
    }
    else
    {
        _txt3.text =vaccineType;
    }
    
    NSString *notes =[appDelegate.dictCulture valueForKey:@"Notes"];
    if ([notes isEqualToString:@"NA"]  || (notes.length == 0))
    {
        _txt4.text=@"Notes";
    }
    else
    {
        _txt4.text =notes;
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"EventDetailViewController"
                                                       object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loadImageFromserver {
    
    if([_isFrom isEqualToString:@"Notification"]) {
        if ([self.image1 isEqualToString:@"NA"]) {
            decodeImage = [self decodeBase64ToImage:self.image1];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self performSelector:@selector(showImageData) withObject:nil afterDelay:1.0];
                
            });
        }
        
        else {
            [self getPictureFromServer];
        }
        
    }
    
}

-(void)getPictureFromServer {
    
    NSString *message =[appDelegate.dictCulture valueForKey:@"LoadingPleaseWait"];
    
    if(message.length==0)
    {
        message =@"Loading, please wait...";
    }
    
    active_alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    NSString * okText =[appDelegate.dictCulture valueForKey:@"Ok"];
    [self presentViewController:active_alert animated:YES completion:nil];
    
    BovilisApi *boviObj =[BovilisApi sharedInstance];
    [boviObj requestImagedataFromServer:appDelegate.vaccineIDforEdit WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        if(success)
        {
            NSArray *resultDic = [result objectForKey:@"results"];
            NSString*data = @"NA";
            if ([result count] != 0)
            {
                for (int i = 0; i<[resultDic count]; i++)
                {
                    id item = [resultDic objectAtIndex:i];
                    NSDictionary *jsonDict = (NSDictionary *) item;
                    
                    GetMappingDays  *theObject =[[GetMappingDays alloc] init];
                    NSString*error = [jsonDict valueForKey:@"error"];
                    if(error != nil && ![error isEqualToString:@"No Record Found."])
                    {
                        
                        appDelegate.imageDataFromServer =[[NSMutableArray alloc]init];
                        data=   [jsonDict valueForKey:@"Image1"];
                        
                        [appDelegate.imageDataFromServer addObject:data];}
                    
                }
            }
            
            if(![data isEqualToString:@"NA"] ) {
                decodeData =@"Yes";
                decodeImage = [self decodeBase64ToImage:[appDelegate.imageDataFromServer objectAtIndex:0]];
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self performSelector:@selector(showImageData) withObject:nil afterDelay:1.0];
                
            });
        }
        
    } failure:^(NSError *error1, NSString *message2) {
        
    }];
}

-(void)showImageData {
    
    NSString * okText =[appDelegate.dictCulture valueForKey:@"Ok"];
    NSString *message1 =[appDelegate.dictCulture valueForKey:@"Imagenotfound"];
    if(message1.length==0)
    {
        message1 =@"Image not found.";
    }
    
    if(okText.length==0)
    {
        okText =@"Ok";
    }
    
    if([decodeData isEqualToString:@"No"] )
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
        UIAlertController *  active_alert1 = [UIAlertController alertControllerWithTitle:nil message:message1 preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
        [active_alert1 addAction:okButton];
        [self presentViewController:active_alert1 animated:YES completion:nil];
        
    }
    else
    {
        [appDelegate.imageDataFromServer removeAllObjects];
        _btnViewImage.enabled=FALSE;
        _imagView.image=decodeImage;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return [UIImage imageWithData:data];
}

-(IBAction)viewImageButtonPressed:(id)sender {
    
    [self loadImageFromserver];
}

-(IBAction)editButtonPressed:(id)sender {
    
    transparentView1 =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( transparentView1.frame.size);
    [[UIImage imageNamed:@"bg-Transparent.png"] drawInRect: transparentView1.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    transparentView1.backgroundColor = [UIColor colorWithPatternImage:image];
    [self.view addSubview:transparentView1];
    
    editVaccineView= [[EditEventView alloc]initWithFrame:CGRectMake(10, 20, 300, 230)];
    editVaccineView.center = [self.view convertPoint:self.view.center
                                            fromView:self.view.superview];
    editVaccineView.innerView.backgroundColor =[UIColor whiteColor];
    
    editVaccineView.lblVaccineName.text = _lblVaccineName.text;
    editVaccineView.txtNotes.text =self.tView.text;
    
    if([self.tView.text isEqualToString:@"NA"])
    {
        editVaccineView.txtNotes.text = @"";
    }
    
    [editVaccineView.btnCancel addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [editVaccineView.btnSave addTarget:self action:@selector(saveButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [transparentView1 addSubview:editVaccineView];
}

-(void)saveButtonPressed {
    
    [transparentView1 removeFromSuperview];
    BovilisApi *boviObj =[BovilisApi sharedInstance];
    
    if([_isFrom isEqualToString:@"History"]) {
        
        activityView = [[UIActivityIndicatorView alloc]
                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityView.center=self.view.center;
        [activityView startAnimating];
        [self.view addSubview:activityView];
        
        [boviObj updateEventWithID:_idForEdit withNotes:editVaccineView.txtNotes.text WithSuccessBlock:^(BOOL success, NSDictionary *result) {
            if(success)
            {
                BovilisApi * objN = [BovilisApi sharedInstance];
                //Changes for security
                [objN  getVaccineSchedule:@"NA" WithSuccessBlock:^(BOOL success1, NSDictionary *result1) {
                    
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    [userDefaults setObject:result1  forKey:@"ScheduleVaccinesList"];
                    [userDefaults synchronize];
                    
                    GetMappingDays *mapOBj=[[GetMappingDays alloc]init];
                    [mapOBj getScheduleVaccineFromServer];
                    
                } failure:^(NSError *error, NSString *message) {
                    
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [activityView stopAnimating];
                    [activityView setHidesWhenStopped:YES];
                    
                    UIAlertController * active_alertClose = [UIAlertController alertControllerWithTitle:nil message:[appDelegate.dictCulture valueForKey:@"NotesSaved"] preferredStyle:UIAlertControllerStyleAlert];
                    [self presentViewController:active_alertClose animated:YES completion:nil];
                    
                    [self performSelector:@selector(moveToCalenderView) withObject:nil afterDelay:0];
                });
            }
            
        } failure:^(NSError *error, NSString *message) {
            
        }];
    }
    else
    {
        activityView = [[UIActivityIndicatorView alloc]
                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityView.center=self.view.center;
        [activityView startAnimating];
        [self.view addSubview:activityView];
        
        [boviObj updateEventWithID:appDelegate.vaccineIDforEdit withNotes:editVaccineView.txtNotes.text WithSuccessBlock:^(BOOL success, NSDictionary *result) {
            
            if(success){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self loadingNotes];
                });
            }
            
        } failure:^(NSError *error, NSString *message) {
            
        }];
    }
}

-(void)loadingNotes
{
    BovilisApi * objN = [BovilisApi sharedInstance];
    
    [objN getVaccineSchedule:@"NA" WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        
        if(success) {
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:result  forKey:@"ScheduleVaccinesList"];
            [userDefaults synchronize];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [activityView stopAnimating];
                [activityView setHidesWhenStopped:YES];
                
                UIAlertController * active_alertClose = [UIAlertController alertControllerWithTitle:nil message:[appDelegate.dictCulture valueForKey:@"NotesSaved"] preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:active_alertClose animated:YES completion:nil];
                
                [self performSelector:@selector(moveToCalenderView) withObject:nil afterDelay:1.0];
            });
        }
        
    } failure:^(NSError *error, NSString *message) {
        
    }];
}

-(void)moveToCalenderView {
    
    appDelegate.isLoadedNotifications=@"No";
    self.tView.text =editVaccineView.txtNotes.text;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)cancelButtonPressed
{
    [transparentView1 removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark === UIButton Actions ===
-(IBAction)viewAnimalButtonPressed:(id)sender {
    
    
    AnimalsViewController *vc = [AnimalsViewController new];
    vc.isFrom = @"VaccineDetail";
    
    if([_isFrom isEqualToString:@"History"]) {
        vc.vaccineID = self.idForEdit;
        
    }
    else {
        vc.vaccineID = appDelegate.vaccineIDforEdit;
        
    }
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(IBAction)backButtonPressed:(id)sender {
    
    if([_isFrom isEqualToString:@"Notification"]) {
        self.isFrom = @"";
        HomeViewController *homeVC = [HomeViewController new];
        [self.navigationController pushViewController:homeVC animated:YES];
    }
    else {
        [_delegate isBackFromDetail:@"Yes"];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

@end
