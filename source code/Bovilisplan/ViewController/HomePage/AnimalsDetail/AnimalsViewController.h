//
//  AnimalsViewController.h
//  TTVApp
//
//  Created by Admin on 9/6/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AnimalsViewControllerDelegate <NSObject>

-(void)selectedNumberOfAnimalsCount:(NSString*)animalsCount withAnimalIDs:(NSString*)iDs;

@end

@interface AnimalsViewController : UIViewController

@property(nonatomic,assign)id<AnimalsViewControllerDelegate>delegate;
@property(strong,nonatomic) NSString *isFrom;
@property(strong,nonatomic) NSString *farmID;
@property(strong,nonatomic) NSString *animalIds;
@property(strong,nonatomic) NSString *userName;
@property(strong,nonatomic) NSString *vaccineID;





@end
