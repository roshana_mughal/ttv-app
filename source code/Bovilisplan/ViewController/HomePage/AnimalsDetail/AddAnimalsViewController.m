//
//  AddAnimalsViewController.m
//  TTVApp
//
//  Created by Macbook on 15/05/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import "AddAnimalsViewController.h"
#import "Reachability.h"
#import "BovilisApi.h"
#import "AppDelegate.h"
#import "AnimalMaster.h"
#import <NHNetworkTime/NHNetworkTime.h>

#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 "

@interface AddAnimalsViewController ()
{
    AppDelegate *appDelegate;
    NSArray *genderArr;
    NSString *selectedGender,*engSelectedGender;
    BOOL isDatePicker;
     UIDatePicker *datePicker;
    NSString *age, *gender;
}
//@property(nonatomic,strong) UIDatePicker *datePicker;
@property(nonatomic,strong) UIToolbar *dateToolBar;

@end

@implementation AddAnimalsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];

    self.btnSave.layer.borderWidth = 1.0f;
    [self.btnSave.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];

    self.btnCancel.layer.borderWidth = 1.0f;
    [self.btnCancel.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];


    // Do any additional setup after loading the view from its nib.
    if ([self.isFfrom isEqualToString:@"Edit"]) {
        self.txtAnimalID.userInteractionEnabled = false;
        
        NSString *editText = [appDelegate.dictCulture valueForKey:@"Edit"];
        editText = editText.length == 0 ? @"Edit":editText;
        self.lblHeader.text = editText;
        
        self.txtAge.text = self.animalObj.birthDate;
        self.txtGender.text = self.animalObj.gender;
        self.txtAnimalID.text = self.animalObj.animalNumber;
    }
    else {
    NSString *addAnimal = [appDelegate.dictCulture valueForKey:@"AddAnimal"];
        addAnimal=  addAnimal.length == 0? @"addAnimal":addAnimal;
        self.lblHeader.text = addAnimal;

    }
    [self showPickview];
    [self showDatePicker];


}

-(void)showPickview {
    
    self.pickerView = [[UIPickerView alloc]init];
                       
                       
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    self.pickerView.showsSelectionIndicator = YES;
    NSString *doneText =[appDelegate.dictCulture valueForKey:@"Done"];
    doneText = doneText.length == 0 ? @"Done":doneText;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:doneText style:UIBarButtonItemStyleDone
                                   target:self action:@selector(doneButtonPressed)];
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    self.txtGender.inputView = self.pickerView;
    self.txtGender.inputAccessoryView = toolBar;
    
    NSString *male =[appDelegate.dictCulture valueForKey:@"Male"];
    NSString *female =[appDelegate.dictCulture valueForKey:@"Female"];
    
    engSelectedGender =@"Male";
    if ([male isEqualToString:@"NA"]  || (male.length == 0) || [female isEqualToString:@"NA"]  || (female.length == 0))
    {
        genderArr = [[NSArray alloc] initWithObjects:@"Male",@"Female", nil];
    }
    else
    {
        genderArr = [[NSArray alloc] initWithObjects:male,female, nil];
    }
    
    NSString *tanimalID =[appDelegate.dictCulture valueForKey:@"AnimalID"];
    NSString *tage =[appDelegate.dictCulture valueForKey:@"BirthDate"];
    NSString *tGender =[appDelegate.dictCulture valueForKey:@"Gender"];
    if  (tanimalID.length == 0)
    {
        tanimalID =@"Animal ID";
        
    }
    if  (tage.length == 0) {
            tage =@"Birth Date";
        
    }
    if  (tGender.length == 0) {
        tGender =@"Gender";
    }
   
        [self.lblAnimalID setText:tanimalID];
        [self.lblDob setText:tage];
        [self.lblGender setText:tGender];

    
    NSString *save =[appDelegate.dictCulture valueForKey:@"Save"];
    NSString *cancel =[appDelegate.dictCulture valueForKey:@"Cancel"];
    if ([save isEqualToString:@"NA"]  || (save.length == 0) || [cancel isEqualToString:@"NA"]  || (cancel.length == 0))
    {
        [self.btnSave setTitle:@"Save" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    else
    {
        [self.btnSave setTitle:save forState:UIControlStateNormal];
        [self.btnCancel setTitle:cancel forState:UIControlStateNormal];
    }
    
    if ([self.isFfrom isEqualToString:@"Edit"]) {
         selectedGender = self.animalObj.gender;
    }
    else {
         selectedGender = [genderArr objectAtIndex:0];
    }

    [self.txtGender setText:selectedGender];
    

}

-(void)doneButtonPressed
{
    self.txtGender.text =selectedGender;
    [self.txtGender resignFirstResponder];
}

-(void)viewDidLayoutSubviews {
    
 
}

-(void)showDatePicker {
    

    datePicker=[[UIDatePicker alloc] init];
    datePicker.datePickerMode=UIDatePickerModeDate;
    datePicker.maximumDate = [NSDate networkDate];
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:
                        CGRectMake(0, 0, self.view.frame.size.width, 44)];
    NSString *doneText =[appDelegate.dictCulture valueForKey:@"Done"];

    if(doneText.length==0)
    {
        doneText =@"Done";
    }
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:doneText style:UIBarButtonItemStyleDone target:self action:@selector(doneDate:)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:doneBtn, nil]];
 
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    
  // NSArray *toolbarItems = [NSArray arrayWithObjects:    doneBtn, nil];
  ///  [toolBar setItems:toolbarItems];
    
    self.txtAge.inputAccessoryView = toolBar;
    [self.txtAge setInputView:datePicker];

}

-(IBAction)doneDate:(id)sender
{
    NSDateFormatter *dateFormatC=[[NSDateFormatter alloc]init];
    [dateFormatC setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    NSString* addedDateTime=[NSString stringWithFormat:@"%@",[dateFormatC  stringFromDate:datePicker.date]];
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:datePicker.date]];
    self.txtAge.text= str;
    //[Utility setLocalDateFormatter:str];
    [self.txtAge resignFirstResponder];


}

-(IBAction)genderButtonPressed:(id)sender {
    
    [self.txtAge resignFirstResponder];

}

-(IBAction)dobButtonPressed:(id)sender {
    
    [self.txtGender resignFirstResponder];
 
}

-(BOOL)checkAnimalIDIsExistence:(NSString *)animalID {

    for (AnimalMaster *obj in appDelegate.animalsArray) {
        if ([animalID isEqualToString:obj.animalNumber]){
            return false;
        }
    }

    return true;
}

-(IBAction)saveAnimalButtonPressed:(id)sender
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NSString *okKEY =[appDelegate.dictCulture valueForKey:@"Ok"];
    if ([okKEY isEqualToString:@"NA"]  || (okKEY.length == 0)) {
        okKEY = @"Ok";
    }
    NetworkStatus status = [reachability currentReachabilityStatus];
    if(status == NotReachable)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"No internet connection." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:okKEY style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        NSString *cancelKEY =[appDelegate.dictCulture valueForKey:@"Cancel"];
        if ([cancelKEY isEqualToString:@"NA"]  || (cancelKEY.length == 0)) {
            cancelKEY = @"Cancel";
        }
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        NSString *animalID = self.txtAnimalID.text; //stringByTrimmingCharactersInSet:whitespace];
        age = [self.txtAge.text stringByTrimmingCharactersInSet:whitespace];
       gender = [engSelectedGender stringByTrimmingCharactersInSet:whitespace];

        if (animalID.length == 0 || age.length == 0 || gender.length == 0) {

           
            NSString *message =[appDelegate.dictCulture valueForKey:@"PleaseProvideAllTheRequiredInformation"];
            
            if ([message isEqualToString:@"NA"]  || (message.length == 0)) {
                message = @"Please provide all the required information.";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:okKEY style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];

        }
        else  if ([self.isFfrom isEqualToString:@"Edit"]){
                 [self updateAnimal];
             }
        else  if (![self checkAnimalIDIsExistence:self.txtAnimalID.text])
        {
            NSString *message =[appDelegate.dictCulture valueForKey:@"AnimalAlreadyExists"];
        
            if ([message isEqualToString:@"NA"]  || (message.length == 0)) {
                message = @"Animal already exists.";
            }
          
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:okKEY style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.txtAnimalID.text =@"";
            }];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
     
        else
        
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            NSMutableDictionary *animalDict = [[NSMutableDictionary alloc] init];
            [animalDict setValue:animalID forKey:@"animalNumber"];
            [animalDict setValue:age forKey:@"birthDate"];
            [animalDict setValue:gender forKey:@"gender"];
            [animalDict setValue:self.farmID forKey:@"farmName"];
            BovilisApi * objN = [BovilisApi sharedInstance];
            [objN addAnimalsDataWithID:animalDict WithSuccessBlock:^(BOOL success, NSDictionary *result) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if(success)
                {
                    NSString *message = [result valueForKey:@"results"];
                
                    NSString *messageKEY =[appDelegate.dictCulture valueForKey:@"YourAnimalHasBeenAdded"];
                    if ([messageKEY isEqualToString:@"NA"]  || (messageKEY.length == 0)) {
                        messageKEY = @"Your animal has been added.";
                    }
                    
                    [self showAlert:messageKEY];

                }

            } failure:^(NSError *error, NSString *message) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
    }
}

-(void)updateAnimal {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *animalDict = [[NSMutableDictionary alloc] init];
    [animalDict setValue:self.animalObj.animalID forKey:@"ID"];
    [animalDict setValue:age forKey:@"birthDate"];
    [animalDict setValue:gender forKey:@"gender"];
    
    self.animalObj.birthDate = age;
    self.animalObj.gender = gender;
    BovilisApi * objN = [BovilisApi sharedInstance];
    
    [objN updateAnimalsDataWithID:animalDict WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if(success)
        {
            NSString *message = [result valueForKey:@"results"];
        
            NSString *messageKEY =[appDelegate.dictCulture valueForKey:@"AnimalUpdatedSuccessfully"];
            if ([messageKEY isEqualToString:@"NA"]  || (messageKEY.length == 0)) {
                messageKEY = @"Animal update successfull";
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            [self showAlert:messageKEY];

        }

    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}

-(void)showAlert:(NSString*)message {
    
   NSString *okKEY =[appDelegate.dictCulture valueForKey:@"Ok"];

    if ([okKEY isEqualToString:@"NA"]  || (okKEY.length == 0)) {
          okKEY = @"Ok";
      }
           
               

               UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
               
               UIAlertAction* ok = [UIAlertAction actionWithTitle:okKEY style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                   [self.delegate reloadAnimal:@"Yes"];
                   [self.navigationController popViewControllerAnimated:YES];
               }];
               [alert addAction:ok];
               
               [self presentViewController:alert animated:YES completion:nil];

    
}

-(IBAction)cancelButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.txtAge || textField == self.txtGender )
    {
        return  NO;
    }
    else {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    return  YES;
}

#pragma -mark PickerView-Delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return [genderArr count];
}

#pragma mark- Picker View Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component

{
    [self.txtGender setText:[genderArr objectAtIndex:row]];
  
    engSelectedGender = (row == 0) ? @"Male": @"Female";

    selectedGender=[genderArr objectAtIndex:row];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component
{
    return [genderArr objectAtIndex:row];
}


@end
