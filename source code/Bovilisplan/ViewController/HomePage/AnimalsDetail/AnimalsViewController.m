//
//  AnimalsViewController.m
//  TTVApp
//
//  Created by Admin on 9/6/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//


#import "AnimalsCell.h"
#import "AppDelegate.h"
#import "AnimalsViewController.h"
#import "AnimalMaster.h"
#import "BovilisApi.h"
#import "AddAnimalsViewController.h"
#import "ViewAnimalViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface AnimalsViewController ()<UITableViewDelegate,UITableViewDataSource,AddAnimalsViewControllerDelegate>

@property(nonatomic,strong) IBOutlet UIButton *btnAddAnimal;

@property(nonatomic,strong) IBOutlet UILabel*lblTop;
@property(nonatomic,strong) IBOutlet UIButton *btnSelectAll;
@property(strong,nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong) IBOutlet UILabel*lblHeader;


@end

@implementation AnimalsViewController
{
    NSMutableArray *selectedArray;
    NSMutableArray *animalsArray;
    AppDelegate *appDelegate;
    NSString * selectAll;
    UIActivityIndicatorView *activityView;
    AnimalMaster *animalObj;
    
    NSString * message, *cancelText,*proceedText ;
     
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    _btnSelectAll.selected = NO;

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [self configureTableViewCell];
    [self setUI];
    
    NSString *addAnimal =[appDelegate.dictCulture valueForKey:@"AddAnimal"];
    if ([addAnimal isEqualToString:@"NA"]  || (addAnimal.length == 0)) {
        [self.btnAddAnimal setTitle:@"Add Animal" forState:UIControlStateNormal];

    }
    else {
        [self.btnAddAnimal setTitle:addAnimal forState:UIControlStateNormal];

    }
    
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0];
    
    self.btnAddAnimal.layer.borderWidth = 1.0f;
    [self.btnAddAnimal.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
}

-(void)loadData
{
    [self selectedFrom];
}

-(void)setDisplay
{
    if(appDelegate.animalsArray.count>0)
    {
        _btnSelectAll.enabled=TRUE;
        _lblHeader.hidden=TRUE;
        self.tableView.hidden=FALSE;
        
        [self.tableView reloadData];
        
        selectAll = [appDelegate.dictCulture valueForKey:@"SelectAll"];
        NSString *deselect = [appDelegate.dictCulture valueForKey:@"Deselect"];
        
        if (selectAll.length==0)
        {
            selectAll = @"Select All";
        }
        
        if (deselect.length==0)
        {
            deselect = @"Deselect";
        }
        
        if (appDelegate.animalsSelectedArray.count == appDelegate.animalsArray.count)
        {
            _btnSelectAll.selected = YES;
            [_btnSelectAll setTitle:deselect forState:UIControlStateNormal];
        }
        
        else
        {
            _btnSelectAll.selected = NO;
            [_btnSelectAll setTitle:selectAll forState:UIControlStateNormal];
        }
    }
    else
    {
        
        selectAll = [appDelegate.dictCulture valueForKey:@"SelectAll"];

        if (selectAll.length==0) {
            selectAll = @"Select All";
        }
        
        [_btnSelectAll setTitle:selectAll forState:UIControlStateNormal];

        _btnSelectAll.enabled=FALSE;
        _lblHeader.hidden=FALSE;
        self.tableView.hidden=TRUE;
    }
   
   [MBProgressHUD hideHUDForView:self.view animated:YES];

}

-(void)loadAnimals {
    BovilisApi *boviObj=[BovilisApi sharedInstance];
    
    [boviObj getAnimalsDataWithID:_farmID  WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        if(success)
        {
            AnimalMaster * animalObj = [[AnimalMaster alloc] init];
            [animalObj getAnimals];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self performSelector:@selector(setDisplay) withObject:nil afterDelay:1.0];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            });
        }
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    }];
}
-(void)selectedFrom
{
    if([_isFrom isEqualToString:@"Listing"])
    {
        self.btnAddAnimal.hidden =false;
        [self loadAnimals];
    }
    
    if([_isFrom isEqualToString:@"Vaccine"])
    {
        self.btnAddAnimal.hidden =true;

        BovilisApi *boviObj=[BovilisApi sharedInstance];
        self.farmID = appDelegate.uniqueFarmID;
        [boviObj getAnimalsDataWithID:appDelegate.uniqueFarmID  WithSuccessBlock:^(BOOL success, NSDictionary *result) {
            if(success){
                AnimalMaster * animalObjct = [[AnimalMaster alloc] init];
                [animalObjct getAnimals];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSelector:@selector(setDisplay) withObject:nil afterDelay:1.0];
                });
            }
            
        } failure:^(NSError *error, NSString *message) {
            
        }];
    }
    
    if([_isFrom isEqualToString:@"Reschedule"])
    {
        self.btnAddAnimal.hidden =true;

        animalsArray =[[NSMutableArray alloc]init];
        selectedArray =[_animalIds componentsSeparatedByString:@","].mutableCopy;
                BovilisApi *boviObj=[BovilisApi sharedInstance];

        [boviObj getAnimalsDataWithID:_farmID  WithSuccessBlock:^(BOOL success, NSDictionary *result) {
            if(success)
            {
                AnimalMaster * aniObj = [[AnimalMaster alloc] init];
                [aniObj getAnimals];
                for (int i=0; i<appDelegate.animalsArray.count; i++)
                {
                    AnimalMaster *Obj =[appDelegate.animalsArray objectAtIndex:i];
                    
                    NSString *title = Obj.animalNumber;
                    if([selectedArray containsObject:title])
                    {
                        [animalsArray addObject:Obj];
                    }
                }
                
                [appDelegate.animalsArray removeAllObjects];
                [selectedArray removeAllObjects];
                [appDelegate.animalsArray addObjectsFromArray:animalsArray];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSelector:@selector(setDisplay) withObject:nil afterDelay:2.0];
                    
                });
            }
        } failure:^(NSError *error, NSString *message) {
            
        }];
    }
    
    if ([self.isFrom  isEqualToString:@"VaccineDetail"]) {
        self.btnAddAnimal.hidden =true;
        self.btnSelectAll.hidden =true;

        [self getVaccineScheduledAnimal];
        
    }
}

-(void)getVaccineScheduledAnimal {
   
     BovilisApi *boviObj=[BovilisApi sharedInstance];
       
       [boviObj getAnimalsDataWithVaccineID:_vaccineID  WithSuccessBlock:^(BOOL success, NSDictionary *result) {
           if(success)
           {
               AnimalMaster * anilObj = [[AnimalMaster alloc] init];
               [anilObj getAnimals];
               
               dispatch_async(dispatch_get_main_queue(), ^{
                   
                   [self performSelector:@selector(setDisplay) withObject:nil afterDelay:1.0];
                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                   
               });
           }
           else {
               
           }
           
       } failure:^(NSError *error, NSString *message) {
           [MBProgressHUD hideHUDForView:self.view animated:YES];

       }];
}


-(void)viewWillAppear:(BOOL)animated {
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [self.tableView reloadData];
}

-(void)reloadAnimal:(NSString *)status {
    if ([status isEqualToString:@"Yes"]) {
        [self loadAnimals];

    }
    
}

-(void)setUI {
    
    selectedArray =[[NSMutableArray alloc]init];
    
    NSString * header =[appDelegate.dictCulture valueForKey:@"Noanimalsfound."];
    if (header.length==0)
    {
        _lblHeader.text =@"No animals found.";
    }
    else
    {
        _lblHeader.text =header;
    }
    
    NSString * animals = [appDelegate.dictCulture valueForKey:@"Animals"];
    if (animals.length==0)
    {
        _lblTop.text = @"Animals";
    }
    
    else
    {
        _lblTop.text = animals;
    }
    selectAll = [appDelegate.dictCulture valueForKey:@"SelectAll"];
    if (selectAll.length==0)
    {
        _btnSelectAll.titleLabel.text = @"Select All";
    }
    else
    {
        _btnSelectAll.titleLabel.text = selectAll;
    }
    
    if([_isFrom isEqualToString:@"Listing"])
    {
        _btnSelectAll.hidden=TRUE;
        
    }
    else
    {
        _btnSelectAll.hidden=FALSE;
    }
    
     message= [appDelegate.dictCulture valueForKey:@"DeleteAnimalText"];
      cancelText =[appDelegate.dictCulture valueForKey:@"No"];
      proceedText =[appDelegate.dictCulture valueForKey:@"Proceed"];
     
     if (message.length==0)
     {
         message =@"Click “PROCEED” to delete the animal, OR “NO” to CANCEL.";
     }
     
     if (cancelText.length==0)
     {
         cancelText =@"NO";
     }
     
     if (proceedText.length==0)
     {
         proceedText =@"PROCEED";
     }
     
}

#pragma mark === Configure TableView Cell===
-(void)configureTableViewCell
{
    [_tableView registerNib:[UINib nibWithNibName:@"AnimalsCell"
                                           bundle:nil] forCellReuseIdentifier:@"AnimalsCell"];
}

#pragma mark === TableView Delegates/Datasource===

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return appDelegate.animalsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = @"AnimalsCell";
    AnimalsCell *cell = (AnimalsCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    AnimalMaster *aniObj =[appDelegate.animalsArray objectAtIndex:indexPath.row];
    NSString *title =[NSString stringWithFormat:@"ID:%@",aniObj.animalNumber ];
    
    
    NSString * gen = [appDelegate.dictCulture valueForKey:@"Gender"];
    gen= gen.length == 0 ? @"Gender" : gen;
  
    NSString * bd = [appDelegate.dictCulture valueForKey:@"BirthDate"];
     bd =  bd.length == 0 ? @"Birth Date" : bd;

    cell.lbl1.text=title;
    cell.lbl2.text=  [NSString stringWithFormat:@"%@:%@",bd,aniObj.birthDate];
    

     NSString *gender= [appDelegate.dictCulture valueForKey:aniObj.gender];
    if (gender.length ==0) {
        gender = aniObj.gender;
    }
    
   
    cell.lbl3.text= [NSString stringWithFormat:@"%@:%@",gen,gender];
 
    cell.textLabel.font = [UIFont fontWithName:@"UNIVERS" size:13.0] ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(![_isFrom isEqualToString:@"Listing"])
    {
        if([appDelegate.animalsSelectedArray containsObject:aniObj.animalNumber])
        {
            cell.imgView.hidden =FALSE;

        }
        else
        {
            cell.imgView.hidden =TRUE;

        }
    }
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([_isFrom isEqualToString:@"VaccineDetail"])  {
        
        return;
    }
   else if(![_isFrom isEqualToString:@"Listing"])
    {
        AnimalMaster *aniObj =[appDelegate.animalsArray objectAtIndex:indexPath.row];
        
        if([appDelegate.animalsSelectedArray  containsObject:aniObj.animalNumber])
        {
            [appDelegate.animalsSelectedArray  removeObject:aniObj.animalNumber];
        }
        else
        {
            [appDelegate.animalsSelectedArray  addObject:aniObj.animalNumber];
        }
        [self.tableView reloadData];
    }
    else {
        AnimalMaster *Obj =[appDelegate.animalsArray objectAtIndex:indexPath.row];

        ViewAnimalViewController *vc = [ViewAnimalViewController new];
        vc.animalObj = Obj;
        
        [self.navigationController pushViewController:vc animated:YES];

    }
}

-(void)setFarmID {
    
    NSString *farmID=@"";
    
    for(int i=0;i<appDelegate.animalsSelectedArray.count;i++) {
        
        if(i==0) {
            farmID =[appDelegate.animalsSelectedArray objectAtIndex:i];
        }
        else
        {
            farmID =[NSString stringWithFormat:@"%@,%@",farmID,[appDelegate.animalsSelectedArray objectAtIndex:i]];
        }
    }
    
    NSString *count =[NSString stringWithFormat:@"%lu",(unsigned long)appDelegate.animalsSelectedArray.count];
    [self.delegate selectedNumberOfAnimalsCount:count withAnimalIDs:farmID];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        AnimalMaster *Obj =[appDelegate.animalsArray objectAtIndex:indexPath.row];
        animalObj =Obj;
        [self deleteAnimalFromServer];
    }*/
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
       if([_isFrom isEqualToString:@"VaccineDetail"] || [_isFrom isEqualToString:@"Vaccine"])  {
           return UITableViewCellEditingStyleNone;
    }
    return 0;
 
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
       if([_isFrom isEqualToString:@"VaccineDetail"] || [_isFrom isEqualToString:@"Vaccine"])  {
          
     
          return 0;
      }
    
    else {
        
        NSString *editText =[appDelegate.dictCulture valueForKey:@"Edit"];
           
        editText  = editText.length == 0 ? @"Edit" :editText;
        
      
       UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:editText handler:^(UITableViewRowAction *action, NSIndexPath *indexxPath){
              AnimalMaster *Obj =[appDelegate.animalsArray objectAtIndex:indexPath.row];
          //insert your editAction here
              AddAnimalsViewController *vc = [AddAnimalsViewController new];
           vc.isFfrom =  @"Edit";
           vc.animalObj = Obj;
            
            [self.navigationController pushViewController:vc animated:YES];
       }];
    //here
        NSString *deleteText =[appDelegate.dictCulture valueForKey:@"Delete"];
           
        deleteText  = deleteText.length == 0 ? @"Delete" :deleteText;
    editAction.backgroundColor = [UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1];

       UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:deleteText handler:^(UITableViewRowAction *action, NSIndexPath *indexxPath){
          //insert your deleteAction here
           
           AnimalMaster *Obj =[appDelegate.animalsArray objectAtIndex:indexPath.row];
                animalObj =Obj;
                [self deleteAnimalFromServer];
       }];
       deleteAction.backgroundColor = [UIColor redColor];
        return @[deleteAction,editAction];
        
    }
    return 0;
}

-(void)deleteAnimalFromServer {
    
    
//userEmail:ttvtestuser@mailinator.com
//AnimalID:1212
   UIAlertController* delete_alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelText style:UIAlertActionStyleDefault handler:nil];
    [delete_alert addAction:cancel];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:proceedText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteAnimal];
    }];
    [delete_alert addAction:ok];
    [self presentViewController:delete_alert animated:YES completion:nil];
}

-(void)deleteAnimal {
   
    BovilisApi *obj1 = [BovilisApi sharedInstance];
    [appDelegate.animalsArray removeObject:animalObj];
    message= [appDelegate.dictCulture valueForKey:@"AnimalDeletedSuccessfully"];
    message = message.length == 0 ? @"Animal Deleted Successfully" : message;
    cancelText =[appDelegate.dictCulture valueForKey:@"Ok"];
    
    [obj1 deletenimalFromServerWithAnimalID:animalObj.animalNumber ViewController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        
        if (success) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertController* delete_alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction *ok = [UIAlertAction actionWithTitle:proceedText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                  [self.tableView  reloadData];

              }];
              [delete_alert addAction:ok];
              [self presentViewController:delete_alert animated:YES completion:nil];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *deleteText =[appDelegate.dictCulture valueForKey:@"Delete"];
    if(deleteText.length==0)
    {
        deleteText =@"Delete";
        
    }
    return deleteText;
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(IBAction)selectAllButtonPressed:(id)sender
{
    if (_btnSelectAll.selected)
    {
        selectAll = [appDelegate.dictCulture valueForKey:@"SelectAll"];
        if (selectAll.length==0)
        {
            selectAll = @"Select All";
        }
        
        _btnSelectAll.selected = NO;
        [_btnSelectAll setTitle:selectAll forState:UIControlStateNormal];
        [appDelegate.animalsSelectedArray removeAllObjects];
        [self.tableView reloadData];
    }
    else
    {
        NSString *deselect = [appDelegate.dictCulture valueForKey:@"Deselect"];
        if (deselect.length==0)
        {
            deselect = @"Deselect";
        }
        
        _btnSelectAll.selected = YES;
        [_btnSelectAll setTitle:deselect forState:UIControlStateNormal];
        
        
        [appDelegate.animalsSelectedArray removeAllObjects];
        if(appDelegate.animalsArray .count>0)
        {
            for(int i=0;i<appDelegate.animalsArray.count;i++)
            {
                AnimalMaster *animalObj =[appDelegate.animalsArray objectAtIndex:i];
                [appDelegate.animalsSelectedArray addObject:animalObj.animalNumber];
            }
            [self.tableView reloadData];
        }
    }
}

-(void)reScheduleFarmID
{
    NSString *farmID=@"";
    for(int i=0;i<appDelegate.animalsSelectedArray.count;i++)
    {
        if(i==0)
        {
            farmID =[appDelegate.animalsSelectedArray objectAtIndex:i];
            
        }
        else
        {
            farmID =[NSString stringWithFormat:@"%@,%@",farmID,[appDelegate.animalsSelectedArray objectAtIndex:i]];
        }
    }
    NSString *count =[NSString stringWithFormat:@"%lu",(unsigned long)appDelegate.animalsSelectedArray.count];
    [self.delegate selectedNumberOfAnimalsCount:count withAnimalIDs:farmID];
}

-(IBAction)addAnimalButtonTapped:(id)sender {
    AddAnimalsViewController *vc = [AddAnimalsViewController new];
    vc.farmID =  self.farmID;
    vc.delegate =self;
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)backButtonPressed:(id)sender
{
    if([_isFrom isEqualToString:@"Reschedule"])
    {
        [self reScheduleFarmID];
    }
    
    else if([_isFrom isEqualToString:@"Vaccine"])
    {
        [self setFarmID];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
