//
//  ViewAnimalViewController.m
//  TTVApp
//
//  Created by apple on 17/10/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import "ViewAnimalViewController.h"
#import "AnimalDisplayCell.h"
#import "AppDelegate.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "BovilisApi.h"
#import "AnimalMaster.h"


@interface ViewAnimalViewController () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) IBOutlet UIButton *btnCancel;
@property(strong,nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong) IBOutlet UILabel *lblAnimalID;
@property(nonatomic,strong) IBOutlet UILabel *lblAge;
@property(nonatomic,strong) IBOutlet UILabel *lblGender;
@property(nonatomic,strong) IBOutlet UILabel *lblHeading;

@property(nonatomic,strong) IBOutlet UILabel *lblVaccineSchedule;

@property(nonatomic,strong)  AnimalMaster *animalMaster;


@property(nonatomic,strong) IBOutlet UITextField *txtAnimalID;
@property(nonatomic,strong) IBOutlet UITextField *txtAge;
@property(nonatomic,strong) IBOutlet UITextField *txtGender;
@property(nonatomic,strong)  NSMutableArray *animalVaccines;



@end

@implementation ViewAnimalViewController {
    AppDelegate *appDelegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUI];
    [self loadValues];
    [self getAnimalsVaccinesFromServer];
}

-(void)setUI {
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [self configureTableViewCell];
    self.btnCancel.layer.borderWidth = 1.0f;
    [self.btnCancel.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    NSString *cancelText = [appDelegate.dictCulture valueForKey:@"Cancel"];
    cancelText= cancelText.length == 0? @"Cancel":cancelText;
         [self.btnCancel setTitle:cancelText forState:UIControlStateNormal];
    
 NSString *str = [appDelegate.dictCulture valueForKey:@"VaccineSchedule"];
    self.lblVaccineSchedule.text = str.length == 0 ? @"Vaccine Schedule":str;
    
    NSString *strHead = [appDelegate.dictCulture valueForKey:@"VaccineDetail"];
    self.lblHeading.text = strHead.length == 0 ? @"Vaccine Detail":strHead;
    
    
      NSString *animalID =[appDelegate.dictCulture valueForKey:@"AnimalID"];
      NSString *tage =[appDelegate.dictCulture valueForKey:@"BirthDate"];
      NSString *tGender =[appDelegate.dictCulture valueForKey:@"Gender"];
        self.lblAnimalID.text = animalID.length == 0 ? @"Animal ID":animalID;
        self.lblAge.text = tage.length == 0 ? @"Birth Date":tage;
        self.lblGender.text = tGender.length == 0 ? @"Gender":tGender;
    
}

-(void)getAnimalsVaccinesFromServer {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    BovilisApi * objN = [BovilisApi sharedInstance];
    
    [objN getAnimalsVaccinesDataWithID:self.animalObj.animalNumber WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        if(success) {
        
            AnimalMaster * animalObj = [[AnimalMaster alloc] init];
            self.animalVaccines = [animalObj getAnimalsVaccines:result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
              });
        }

    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}
-(void)loadValues {
    
    NSString *genderText =[appDelegate.dictCulture valueForKey:self.animalObj.gender];
        
    self.txtAge.text = self.animalObj.birthDate;
    self.txtGender.text = genderText.length == 0 ? self.animalObj.gender : genderText;
    self.txtAnimalID.text = self.animalObj.animalNumber;
      
}

#pragma mark === Configure TableView Cell===
-(void)configureTableViewCell {
    [_tableView registerNib:[UINib nibWithNibName:@"AnimalDisplayCell"
                                           bundle:nil] forCellReuseIdentifier:@"AnimalDisplayCell"];
}


#pragma mark === TableView Delegates/Datasource===

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  self.animalVaccines.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = @"AnimalDisplayCell";
    AnimalDisplayCell *cell = (AnimalDisplayCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    NSString *dateText = [appDelegate.dictCulture valueForKey:@"Date"];
       NSString *vNText = [appDelegate.dictCulture valueForKey:@"VaccineName"];
    AnimalMaster *animalObj =[self.animalVaccines objectAtIndex:indexPath.row];
    cell.lblDate.text = dateText.length == 0?    [NSString stringWithFormat:@"DATE:%@",animalObj.scheduleDate ]:
    [NSString stringWithFormat:@"%@:%@",dateText,animalObj.scheduleDate ];
    
    
    cell.lblVaccine.text = vNText.length == 0?
    [NSString stringWithFormat:@"Vaccine Name:%@",animalObj.scheduleVaccine ]:   [NSString stringWithFormat:@"%@:%@",vNText,animalObj.scheduleVaccine ] ;

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return UITableViewAutomaticDimension;

}

-(IBAction)cancelButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
