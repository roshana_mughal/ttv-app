//
//  AddAnimalsViewController.h
//  TTVApp
//
//  Created by Macbook on 15/05/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimalMaster.h"

@protocol AddAnimalsViewControllerDelegate <NSObject>

@optional
- (void)reloadAnimal:(NSString *_Nullable)status ;

@end

NS_ASSUME_NONNULL_BEGIN

@interface AddAnimalsViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, assign) id <AddAnimalsViewControllerDelegate> delegate;
@property(strong,nonatomic) NSString *farmID;
@property(strong,nonatomic) NSString *isFfrom;
@property(strong,nonatomic) AnimalMaster *animalObj;


@property(nonatomic,strong) IBOutlet UILabel *lblHeader;

@property(nonatomic,strong) IBOutlet UILabel *lblAnimalID;
@property(nonatomic,strong) IBOutlet UILabel *lblDob;
@property(nonatomic,strong) IBOutlet UILabel *lblGender;

@property(nonatomic,strong) IBOutlet UITextField *txtAnimalID;
@property(nonatomic,strong) IBOutlet UITextField *txtAge;
@property(nonatomic,strong) IBOutlet UITextField *txtGender;

@property(nonatomic,strong) IBOutlet UIButton *btnCancel;
@property(nonatomic,strong) IBOutlet UIButton *btnSave;

@property(nonatomic,strong) IBOutlet UIPickerView *pickerView;

@property(nonatomic,strong) UIToolbar *pickertoolBar;

@end

NS_ASSUME_NONNULL_END
