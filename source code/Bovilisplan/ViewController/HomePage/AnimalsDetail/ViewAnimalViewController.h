//
//  ViewAnimalViewController.h
//  TTVApp
//
//  Created by apple on 17/10/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimalMaster.h"

NS_ASSUME_NONNULL_BEGIN

@interface ViewAnimalViewController : UIViewController

@property(strong,nonatomic) AnimalMaster *animalObj;


@end

NS_ASSUME_NONNULL_END
