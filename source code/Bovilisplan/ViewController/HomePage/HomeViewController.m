//
//  HomeViewController.m
//  Bovilisplan
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//
#import <CoreGraphics/CoreGraphics.h>
#import "CKCalendarView.h"
#import "AppDelegate.h"
#import "AddEventView.h"
#import "EventView.h"
#import "ListingViewController.h"
#import "NotificationViewController.h"
#import "LibraryViewController.h"
#import "AddEventCell.h"
#import "LoginViewController.h"
#import "WelcomeDetailViewController.h"
#import "MenuView.h"
#import "BovilisApi.h"
#import "AppDelegate.h"
#import "EventDetailViewController.h"
#import "MainViewController.h"
#import "UserVacineMaster.h"
#import "HomeViewController.h"
#import "ProductMaster.h"
#import "GetDaysData.h"
#import "Utility.h"
#import "Reachability.h"
#import "AboutUsViewController.h"
#import "AnimalsViewController.h"
#import "GAITracker.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GetMappingDays.h"
#import "Utility.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "TranslationService.h"
#import "ListViewViewController.h"
#import "AllAnimalsViewController.h"

@import Firebase;

@interface HomeViewController ()

<CKCalendarDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AnimalsViewControllerDelegate,EventDetailViewControllerDelegate>
{
    NSString *selectedDate;
    NSDate *selectedDateForImage;
    UIView *transparentView ;
    UIView *transparentView1 ;
    
    EventView *eventView;
    AddEventView *addEvent;
    
    NSMutableArray *imagesArray;
    CKCalendarView *calendarObj;
    AppDelegate *appDelegate;
    MenuView *menuView;
    
    NSString *dateString;
    NSMutableArray *vacunaArray;
    NSString *image1;
    NSString *image2;
    NSString *image3;
    NSString *image4;
    NSString *image5;
    NSString *isReload;
    NSString *decodeImageData;
    NSData *dataImage;
    UIAlertController *saving_alert;
    UIAlertController *loading_alert;
    UIActivityIndicatorView *activityView;
    NSString *numerOfAnimals;
    NSString *dateFinal;
    NSString *vaccineType;
    NSString *addedDateTime;
    NSTimer *_timer;;
}

@property(nonatomic,weak) CKCalendarView *calendar;
@property(nonatomic,weak) IBOutlet UIView *calendarView;
@property(nonatomic,strong) NSDateFormatter *dateFormatter;
@property(nonatomic,strong) NSDate *minimumDate, *chosenDate;
@property(nonatomic,strong) NSArray *disabledDates;
@property(nonatomic,weak) IBOutlet UILabel*lblTitle;
@property(nonatomic,weak) IBOutlet UIImageView *logoImg;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    NSDate *date = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
       NSDateComponents *dateComponent = [calendar components:(NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
       NSLog(@"dateComponent:%@",dateComponent);
    
    self.logoImg.image = [UIImage imageNamed:[Utility setLocalizeLogo:@"logo"]];
    TranslationService *service = [TranslationService new];
    [service getCultures:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        _lblTitle.text = [NSString stringWithFormat:@"%@",[appDelegate.dictCulture valueForKey:@"Calendar"]];
        
        NSString * lblTitle = [appDelegate.dictCulture valueForKey:@"Calendar"];
        if (lblTitle.length==0)
        {
            _lblTitle.text = @"Calendar";
        }    } failure:^(NSError *error, NSString *message) {
            
        }];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@", @"TestingHome"],
                                     kFIRParameterItemName:@"TestingHome",
                                     
                                     }];
    // Facebook
    appDelegate =(AppDelegate * )[[UIApplication sharedApplication ]delegate];
    //  [_lblVaccinePlane setText:[appDelegate.dictCulture valueForKey:@"VaccinationPlanned"]];
    
    addEvent.btnAddPhotos.enabled=TRUE;
    appDelegate.animalsSelectedArray=[[NSMutableArray alloc]init];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self performSelector:@selector(allmethods) withObject:nil afterDelay:0];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    
    
}

-(void)allmethods {
    
    [self showCalendar];
    /* _timer=  [NSTimer scheduledTimerWithTimeInterval:3
     target:self selector:@selector(loadVaccineAsync:) userInfo:nil repeats:YES];*/
    NetworkStatus status = [Utility checkNetworkStatus];
    if(status != NotReachable)
    {
        
    }
    if(status != NotReachable)
    {
        //[self loadVaccineAsync];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //here
        
        [self getScheduledVaccineFromServer];
    }
    
    imagesArray = [[NSMutableArray alloc]init];
    calendarObj = [[CKCalendarView alloc]init];
    [self loadMenuView];
    [self loadProductDataFromServer];
    [eventView.tableView reloadData];
    
    [transparentView setHidden:YES];
    
}
-(void)viewWillLayoutSubviews {
    
}

-(void)loadVaccineAsync:(NSTimer *)timer {
    
    __weak __typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //main method
        [self getScheduledVaccineFromServer];
        
        __typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            GetMappingDays *mapOBj=[[GetMappingDays alloc]init];
            [mapOBj getScheduleVaccineFromServer];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self datesForCalander];
            });
        }
        
    });
    
}

-(void)getScheduledVaccineFromServer {
    
    //rosh if  ([appDelegate.reloadCalendar isEqualToString:@"No"]) {
    appDelegate.reloadCalendar= @"Yes";
    BovilisApi * objN = [BovilisApi sharedInstance];
    
    [objN getVaccineSchedule:@"NA" WithSuccessBlock:^(BOOL success, NSDictionary *result)
     {
         if(success)
         {
             // comment for async
             GetMappingDays *mapOBj=[[GetMappingDays alloc]init];
             [mapOBj getScheduleVaccineFromServer];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [self datesForCalander];
                 
                 [self performSelector:@selector(showCalendar) withObject:nil afterDelay:2.0];
             });
         }
         else {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
         }
         
     } failure:^(NSError *error, NSString *message)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
     }];
    /* }
     else
     {
     [self showCalendar];
     }*/
}

-(void)datesForCalander
{
    GetMappingDays  *theObjectDataDate =[[GetMappingDays alloc] init];
    appDelegate.allVaccinesWithDate =[[NSMutableArray alloc]init];
    for (int i=0; i<appDelegate.getScheduleVaccineArray.count; i++)
    {
        theObjectDataDate = [appDelegate.getScheduleVaccineArray objectAtIndex:i];
        
        NSDateFormatter *dateFormater= [[NSDateFormatter alloc] init];
        [dateFormater setDateFormat:@"yyyy-MM-dd"];
        NSDate *newDate = [dateFormater dateFromString:theObjectDataDate.addedDateTime];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSString *finaDate = [dateFormat stringFromDate:newDate];
        NSDate *disDate = [dateFormat dateFromString:finaDate];
        
        [appDelegate.allVaccinesWithDate addObject:newDate];
        
    }
  // self.disabledDates = appDelegate.allVaccinesWithDate;
    [self.calendar reloadData];
    
}

-(void)reloadCalander
{
    [self.calendar reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    self.screenName = @"Calendar";
    [transparentView setHidden:YES];
      [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification)
                                                 name:@"TestNotification"
                                               object:nil];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TestNotification" object:nil];
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
}

-(void)loadProductDataFromServer
{
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    NSString *email =userEmail;
    
    //get farm a
    BovilisApi *boviObj = [BovilisApi sharedInstance];
    [boviObj requestForGetUserFarmDataWithUserEmail:email];
    
    NSString *emailvaccine =email;
    //get library vaccines
    [boviObj requestGetProducts];
    [boviObj requestGetProductsList];
}

-(void)showCalendar
{
    
    CKCalendarView *toRemove;
    for (CKCalendarView *view in self.calendarView.subviews)
    {            if ([view isKindOfClass:[CKCalendarView class]])
    {
        
        toRemove = (CKCalendarView *)view;
        break;
    }
    }
    
    [toRemove removeFromSuperview];
    
    
    CKCalendarView *calendar = [[CKCalendarView alloc] initWithStartDay:startMonday];
    self.calendar = calendar;
    calendar.delegate = self;
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    
    calendar.frame = CGRectMake(0, 0, self.calendarView.frame.size.width, self.calendarView.frame.size.height);
    
    calendar.backgroundColor = [UIColor redColor];
    
    [self.calendarView addSubview:calendar];
    
    self.calendar.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)localeDidChange
{
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date
{
    for (NSDate *disabledDate in self.disabledDates)
    {
        if ([disabledDate isEqualToDate:date])
        {
            return YES;
        }
    }
    return NO;
}

#pragma mark - CKCalendarDelegate
- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date
{
    dateItem.backgroundColor = [UIColor whiteColor];
    if ([self dateIsDisabled:date])
    {
        dateItem.backgroundColor = [UIColor orangeColor];
        dateItem.textColor = [UIColor whiteColor];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date
{
    return ![self dateIsDisabled:date];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date
{
    self.chosenDate = date;
    selectedDate= [self.dateFormatter stringFromDate:date];
    selectedDateForImage =date;
    NSString *emailvaccine =@"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    dateString = [dateFormat stringFromDate:date];
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    dateFinal = [dateFormat1 stringFromDate:date];
    
    //**Get Vaccine Data with date***//
    GetDaysData *objDays =[GetDaysData new];
    [objDays getUserVaccineDataWithDate:dateFinal];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"Calendar_Tab_From_Home"
                                                           label:@"Main_Calendar_Date_Selection"
                                                           value:nil] build]];
    [self showEventView];
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date
{
    if ([date laterDate:self.minimumDate] == date)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark - ===First Pop up - Show Vaccine list===
-(void)showEventView
{
    transparentView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( transparentView.frame.size);
    [[UIImage imageNamed:@"bg-Transparent"] drawInRect: transparentView.bounds];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    transparentView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [self.view addSubview:transparentView];
    eventView= [[EventView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    CGRect newFrame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);
    eventView.innerView.frame = newFrame;
    eventView.textHeader = selectedDate;
    [ eventView.btnClose addTarget:self action:@selector(closeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    if ([Utility isOldeDate:selectedDate] == false) {
        eventView.btnAdd .userInteractionEnabled =false;
        eventView.btnAdd.alpha = 0.5;
    }
    else {
        [ eventView.btnAdd addTarget:self action:@selector(addButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
    }
    [transparentView addSubview:eventView];
}

-(void)isBackFromDetail:(NSString *)back
{
    if ([back isEqualToString:@"Yes"])
    {
        back =@"No";
        GetDaysData *objDays =[GetDaysData new];
        [objDays getUserVaccineDataWithDate:dateFinal];
        [eventView.tableView reloadData];
        [appDelegate.animalsSelectedArray removeAllObjects];
    }
}

- (void)receiveTestNotification
{
    EventDetailViewController *vc=[[EventDetailViewController alloc]init];
    vc.delegate=self;
    vc.selectedDate = dateFinal;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)navigateToDetailEventView
{
    [transparentView removeFromSuperview];
}

-(void)closeButtonPressed
{
    [imagesArray removeAllObjects];
    [transparentView removeFromSuperview];
}

-(void)cancelButtonPressed
{
    [imagesArray removeAllObjects];
    [appDelegate.animalsSelectedArray removeAllObjects];
    [transparentView1 removeFromSuperview];
}

#pragma mark - ===Second  Pop up --  Add/Schedule Vaccine ===

-(void)addButtonPressed
{
    if(imagesArray.count==0) {
        
        addEvent.btnAddPhotos.enabled=TRUE;
    }
    
 
        

    if(appDelegate.getUserFarmsFromServer.count>0)
    {
        transparentView1 =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        UIGraphicsBeginImageContext( transparentView1.frame.size);
        [[UIImage imageNamed:@"bg-Transparent.png"] drawInRect: transparentView1.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        transparentView1.backgroundColor = [UIColor colorWithPatternImage:image];
        [self.view addSubview:transparentView1];
        addEvent= [[AddEventView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        addEvent.innerView.backgroundColor =[UIColor whiteColor];
        
        
        addEvent.txtDate.text = [Utility setLocalDateFormatter:selectedDate];
        [addEvent.btnCancel addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [addEvent.btnAddPhotos addTarget:self action:@selector(selectPhoto) forControlEvents:UIControlEventTouchUpInside];
        [addEvent.btnDone addTarget:self action:@selector(saveButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [addEvent.btnAnimalID addTarget:self action:@selector(animalIDButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [transparentView1 addSubview:addEvent];
    }
    else
    {
        NSString * message =[appDelegate.dictCulture valueForKey:@"YouShouldAddAFarmFirst"];
        NSString * okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if (message.length == 0)
        {
            message =@"You should add a farm first";
        }
        
        if (okText.length==0)
        {
            okText =@"Ok";
        }
        
        UIAlertController*  alert = [UIAlertController alertControllerWithTitle:nil
                                                                        message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }

}

-(void)selectedNumberOfAnimalsCount:(NSString *)animalsCount withAnimalIDs:(NSString *)iDs
{
    numerOfAnimals =iDs;
    NSString *str = [Utility setLocalizeString:@"AnimalSelected"];
    
    addEvent.txtAnimalNo.text= str.length != 0 ? [NSString stringWithFormat:@"%@ %@",animalsCount,str] :[NSString stringWithFormat:@"%@ animal(s) selected",animalsCount];
    
}

-(void)animalIDButtonPressed
{
    if(addEvent.txtFarmName.text.length>0)
    {
        AnimalsViewController *animalVc=[AnimalsViewController new];
        animalVc.isFrom=@"Vaccine";
        animalVc.animalIds=appDelegate.vaccineNumberOfAnimals;
        animalVc.delegate = self;
        [self.navigationController pushViewController:animalVc animated:YES];
    }
    else
    {
        NSString * message =[appDelegate.dictCulture valueForKey:@"SelectAFarmFirst"];
        NSString * okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if (message.length == 0)
        {
            message =@"Select a farm first";
        }
        
        if (okText.length==0)
        {
            okText =@"Ok";
        }
        
        UIAlertController*  alert = [UIAlertController alertControllerWithTitle:nil
                                                                        message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

-(void)saveButtonPressed
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if(status != NotReachable)
    {
        [appDelegate.animalsSelectedArray removeAllObjects];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                              action:@"Calendar_Tab_From_Home"
                                                               label:@"Vaccine_Scheduled"
                                                               value:nil] build]];
        NSString * farmName = addEvent.txtFarmName.text;
        NSString * farmID = appDelegate.uniqueFarmID;
        NSString * date = addEvent.txtDate.text;
        if (addEvent.txtAnimalNo.text.length == 0 ) {
            numerOfAnimals =@"";
        }
        NSString * animalNumber = numerOfAnimals;
        NSString * scheduleVaccine=appDelegate.selectdVaccineName;
        NSString * vaccineName = [addEvent.txtVaccineName.text capitalizedString];
        NSString * numberOfVaccineCattle = addEvent.txtNoOfVaccine.text;
        NSString * notes = addEvent.txtNotes.text;
        NSString *vaccineID = appDelegate.vaccineID;
        
        BovilisApi * obj1  = [BovilisApi sharedInstance];
        
        if([numberOfVaccineCattle isEqualToString:@""])
        {
            numberOfVaccineCattle =@"NA";
        }
        
        if([notes isEqualToString:@""])
        {
            notes =@"NA";
        }
        
        if (appDelegate.selectedVaccineType.length>0 && [appDelegate.isSwitchOn isEqualToString:@"Yes"])
        {
            vaccineType = appDelegate.selectedVaccineType;
        }
        else
        {
            vaccineType = @"NA";
        }
        
        image1 = @"NA";
        image2 = @"0";
        image3 = @"0";
        image4 = @"0";
        image5 = @"0";
        
        if(addEvent.txtFarmName.text.length>0 && vaccineName.length>0 && animalNumber.length>0 && (![scheduleVaccine isEqualToString:@""] ) )
        {
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            BovilisApi * obj  = [BovilisApi sharedInstance];
            
            NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
            [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
            
            addedDateTime = [dateFormat1 stringFromDate:selectedDateForImage];
            NSString * userName = @"";
            
            NSString *message =[appDelegate.dictCulture valueForKey:@"Schedulingvaccine"];
            
            if(message.length==0)
            {
                message =@"Scheduling a vaccine...";
            }
            
            saving_alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
            [self presentViewController:saving_alert animated:YES completion:nil];
            
            NSString*image1WithDataImage = @"NA";
            if(imagesArray.count>0)
            {
                [dateFormat1 setDateFormat:@"yyyyMMddHHmmss"];
                NSString *formateDateTime = [dateFormat1 stringFromDate:selectedDateForImage];
                image1= [NSString stringWithFormat:@"%@%@%@",formateDateTime,farmName,scheduleVaccine];
                image1WithDataImage = decodeImageData;
            }
            
            [obj1 getVaccineMappingDaysWithVaccineID:vaccineID ViewController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
                
                if (success)
                {
                    BovilisApi * objN  = [BovilisApi sharedInstance];
                    
                    GetMappingDays *mapObj =[[GetMappingDays alloc]init];
                    if(appDelegate.getMappingDaysArray.count>0)
                    {
                        if([vaccineType isEqualToString:@"NA"])
                        {
                            NSDate * serverDate;
                            NSDate * newDate;
                            NSDate *date2;
                            NSString *dayIn;
                            for (int i=0; i<appDelegate.getMappingDaysArray.count; i++)
                            {
                                
                                mapObj =[appDelegate.getMappingDaysArray objectAtIndex:i];
                                vaccineType =mapObj.vaccinationType;
                                dayIn=mapObj.day;
                                
                                NSDateFormatter *dateFormatC = [[NSDateFormatter alloc] init];
                                [dateFormatC setDateFormat:@"yyyy-MM-dd"];
                                serverDate = [dateFormatC dateFromString:addedDateTime];
                                int dayInterV = [dayIn intValue];
                                newDate = [serverDate dateByAddingTimeInterval:60*60*24*dayInterV];
                                
                                NSString *dateStr1 = [dateFormatC stringFromDate:newDate];
                                
                                [objN sentNewUserVaccine:farmName withDate:date withAnimalNumber:animalNumber withScheduleVaccine:scheduleVaccine withVaccineType:vaccineType withNumberOfVaccineCattle:numberOfVaccineCattle withNotes:notes withImage1:image1WithDataImage withImage2:image2 withImage3:image3 withImage4:image4 withImage5:image5 withAddedDateTime:dateStr1 withUserName:userName withVaccineName:vaccineName withVaccineID:appDelegate.vaccineID withFarmID:farmID];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    
                                    BovilisApi * objN1 =[BovilisApi sharedInstance];
                                    
                                    [objN1 getVaccineSchedule:@"NA" WithSuccessBlock:^(BOOL success1, NSDictionary *result1) {
                                        [self performSelector:@selector(movetoHome) withObject:nil afterDelay:5.0];
                                    } failure:^(NSError *error, NSString *message1) {
                                        
                                    }];
                                });
                                
                            }
                        }
                        else
                        {
                            [objN sentNewUserVaccine:farmName withDate:date withAnimalNumber:animalNumber withScheduleVaccine:scheduleVaccine withVaccineType:vaccineType withNumberOfVaccineCattle:numberOfVaccineCattle withNotes:notes withImage1:image1WithDataImage withImage2:image2 withImage3:image3 withImage4:image4 withImage5:image5 withAddedDateTime:addedDateTime withUserName:userName withVaccineName:vaccineName withVaccineID:appDelegate.vaccineID withFarmID:farmID ];
                            
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                BovilisApi * objN1 =[BovilisApi sharedInstance];
                                [objN1 getVaccineSchedule:@"NA" WithSuccessBlock:^(BOOL success1, NSDictionary *result1) {
                                    [self performSelector:@selector(movetoHome) withObject:nil afterDelay:5.0];
                                } failure:^(NSError *error1, NSString *message1) {
                                    
                                }];
                            });
                        }
                    }
                }
                
            } failure:^(NSError *error, NSString *message1) {
                
            }];
        }
        
        else  if(addEvent.txtFarmName.text.length==0)
        {
            NSString*title =[appDelegate.dictCulture valueForKey:@"Alert"];
            NSString*message1 =[appDelegate.dictCulture valueForKey:@"PleaseSelectAFarmName"];
            NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            
            if (title.length==0)
            {
                title =@"Alert";
            }
            
            if (message1.length==0)
            {
                message1 =@"Please select a farm name";
            }
            
            if (okText.length==0)
            {
                okText =@"Ok";
            }
            
            UIAlertController*  alert = [UIAlertController alertControllerWithTitle:title
                                                                            message:message1 preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        
        else  if(vaccineName.length==0)
        {
            NSString*title =[appDelegate.dictCulture valueForKey:@"Alert"];
            NSString*message1 =[appDelegate.dictCulture valueForKey:@"AddTitle"];
            NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            
            if (title.length==0)
            {
                title =@"Alert";
            }
            
            if (message1.length==0)
            {
                message1 =@"Please add a title";
            }
            
            if (okText.length==0)
            {
                okText =@"Ok";
            }
            
            UIAlertController*  alert = [UIAlertController alertControllerWithTitle:title
                                                                            message:message1 preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        
        else  if(animalNumber.length==0)
        {
            NSString *   title =[appDelegate.dictCulture valueForKey:@"Alert"];
            NSString *   message1 =[appDelegate.dictCulture valueForKey:@"PleaseselectanimalID"];
            NSString *   okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            
            if (title.length==0)
            {
                title =@"Alert";
            }
            
            if (message1.length==0)
            {
                message1 =@"Please select animal ID";
            }
            
            if (okText.length==0)
            {
                okText =@"Ok";
            }
            
            UIAlertController*  alert = [UIAlertController alertControllerWithTitle:title
                                                                            message:message1 preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        
        else  if(([scheduleVaccine isEqualToString:@""]))
        {
            NSString *   title =[appDelegate.dictCulture valueForKey:@"Alert"];
            NSString *   message1 =[appDelegate.dictCulture valueForKey:@"PleaseScheduleAVaccine"];
            NSString *   okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            
            if (title.length==0)
            {
                title =@"Alert";
            }
            
            if (message1.length==0)
            {
                message1 =@"Please select a vaccine";
            }
            
            if (okText.length==0)
            {
                okText =@"Ok";
            }
            
            UIAlertController*  alert = [UIAlertController alertControllerWithTitle:title
                                                                            message:message1 preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}

-(void)movetoHome
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    appDelegate.reloadCalendar = @"No";
    appDelegate.isLoadedNotifications =@"No";
    
    GetMappingDays *mapOBj=[[GetMappingDays alloc]init];
    [mapOBj getScheduleVaccineFromServer];
    
    if(appDelegate.getScheduleVaccineArray.count>0)
    {
        [self datesForCalander];
    }
    
    [self.calendar reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [transparentView1 removeFromSuperview];
    [transparentView removeFromSuperview];
}

-(void)addPhotoFromLibrary
{
    [addEvent.scrollView setPagingEnabled:YES];
    [addEvent.scrollView setAlwaysBounceVertical:NO];
    CGFloat xOrigin = 0.0 ;
    for (int i = 0; i < [imagesArray count]; i++)
    {
        xOrigin = i * 80;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin, 0, 70, 80)];
        [imageView setImage:[imagesArray objectAtIndex:i]];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [addEvent.scrollView addSubview:imageView];
    }
    
    addEvent.btnAddPhotos.enabled=FALSE;
    [addEvent.scrollView setContentSize:CGSizeMake(xOrigin * [imagesArray count], addEvent.scrollView.frame.size.height)];
    
    decodeImageData = [self encodeToBase64String:[imagesArray objectAtIndex:0]];
}

- (NSString *)encodeToBase64String:(UIImage *)image
{
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

#pragma  mark === Add Photo from Library  ===
- (void)selectPhoto
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    NSString * capture;
    NSString * selectFromLibrary;
    NSString * cancel;
    
    capture =[appDelegate.dictCulture valueForKey:@"CaptureFromCamera"];
    selectFromLibrary =[appDelegate.dictCulture valueForKey:@"SelectFromLibrary"];
    cancel =[appDelegate.dictCulture valueForKey:@"Cancel"];
    
    if (capture.length == 0)
    {
        capture =@"Capture from camera";
    }
    
    if (selectFromLibrary.length ==0)
    {
        selectFromLibrary =@"Select From Library";
    }
    
    if (cancel.length ==0)
    {
        cancel =@"Cancel";
    }
    
    UIAlertAction* cameraButton = [UIAlertAction actionWithTitle:capture
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                       [self fromCamera];
                                   }];
    
    UIAlertAction* libarayButton = [UIAlertAction actionWithTitle:selectFromLibrary                                                     style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action){
                                                              
                                                              [self fromLibrary];
                                                          }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:cancel
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action){
                                                             
                                                         }];
    
    [alertController addAction:cameraButton];
    [alertController addAction:libarayButton];
    [alertController addAction:cancelButton];
    [self presentViewController:alertController animated:NO completion:nil];
}

-(void)fromLibrary
{
    if(imagesArray.count==0)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:NO completion:NULL];
    }
}

-(void)fromCamera
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertController*  alert = [UIAlertController alertControllerWithTitle:[appDelegate.dictCulture valueForKey:@"Error"]
                                     
                                                                        message:[appDelegate.dictCulture valueForKey:@"DeviceHasNoCamera"]
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[appDelegate.dictCulture valueForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        if(imagesArray.count==0)
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:NO completion:NULL];
        }
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    [imagesArray addObject:[Utility getNormalizedImage:chosenImage]];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [picker dismissViewControllerAnimated:YES completion:^{
            [self addPhotoFromLibrary];
        }];
    });
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)loadMenuView
{
    transparentView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( transparentView.frame.size);
    
    UIGraphicsBeginImageContext( transparentView.frame.size);
    [[UIImage imageNamed:@"bg-Transparent"] drawInRect: transparentView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // transparentView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [self.view addSubview:transparentView];
    
    menuView= [[MenuView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-295, 0, self.view.frame.size.width, self.view.frame.size.height-0)];
    CGRect newFrame = CGRectMake(0, 0, menuView.frame.size.width,menuView.frame.size.height);
    menuView.innerView.frame = newFrame;
    
    
    [menuView.btnClose addTarget:self action:@selector(closeMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnDisclaimer addTarget:self action:@selector(disclaimerMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnPrivacy addTarget:self action:@selector(privacyMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnTerms addTarget:self action:@selector(termsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnSignout addTarget:self action:@selector(signoutMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnCalendar addTarget:self action:@selector(calendarMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnListing addTarget:self action:@selector(listingMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnLibrary addTarget:self action:@selector(libraryMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnNotification addTarget:self action:@selector(notificationMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [menuView.btnAboutUS addTarget:self action:@selector(aboutUsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
      [menuView.btnAnimals addTarget:self action:@selector(animalsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [transparentView addSubview:menuView];
    
}
-(void)showMenu
{
    [self loadMenuView];
    [transparentView setHidden:FALSE];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transparentView.layer addAnimation:transition forKey:nil];
}

-(void)closeMenuButtonPressed
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transparentView.layer addAnimation:transition forKey:nil];
    [transparentView setHidden:YES];
}
-(void)animalsMenuButtonPressed {
    AllAnimalsViewController *settingVC = [AllAnimalsViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}


-(void)aboutUsMenuButtonPressed
{
    AboutUsViewController *settingVC = [AboutUsViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)notificationMenuButtonPressed
{
    NotificationViewController *settingVC = [NotificationViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)libraryMenuButtonPressed
{
    LibraryViewController *settingVC = [LibraryViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)listingMenuButtonPressed
{
    ListingViewController *settingVC = [ListingViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)calendarMenuButtonPressed
{
    HomeViewController *settingVC = [HomeViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)signoutMenuButtonPressed {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    [self sendTokenAtSever];
 
}

-(void)sendTokenAtSever {
    
    BovilisApi *obj = [BovilisApi new];
    [obj sendToken: @"DeleteToken"  WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        [MBProgressHUD hideHUDForView:self.view animated:false];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"AllApiDataDictionary"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IsLogin"];
       // [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SaveUserEmail"];

        
        
         [[NSUserDefaults standardUserDefaults] synchronize];
         appDelegate.reloadCalendar=@"No";
        appDelegate.isLoadedNotifications=@"No";

       LoginViewController *loginVC = [LoginViewController new];
                 [self.navigationController pushViewController:loginVC animated:YES];
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

-(void)privacyMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Privacy Policy";
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)disclaimerMenuButtonPressed {
    
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Disclaimer";
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)termsMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Terms and Conditions";
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark === UIButton Actions ===

-(IBAction)listViewButtonPressed:(id)sender
{
    ListViewViewController *listVc = [ListViewViewController new];
    [self.navigationController pushViewController:listVc animated:NO];
}

-(IBAction)menuButtonPressed:(id)sender
{
    [self showMenu];
}

-(IBAction)listButtonPressed:(id)sender
{
    ListingViewController *listVc = [ListingViewController new];
    [self.navigationController pushViewController:listVc animated:NO];
}

-(IBAction)notificationButtonPressed:(id)sender
{
    NotificationViewController *notificationVc = [NotificationViewController new];
    [self.navigationController pushViewController:notificationVc animated:NO];
}

-(IBAction)libraryButtonPressed:(id)sender
{
    LibraryViewController *libraryVc = [LibraryViewController new];
    [self.navigationController pushViewController:libraryVc animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
