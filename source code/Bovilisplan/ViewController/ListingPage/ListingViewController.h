//
//  ListingViewController.h
//  Bovilisplan
//
//  Created by Admin on 4/27/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ListingViewController : GAITrackedViewController

@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *subTitle;

@end
