//
//  ListingViewController.m
//  Bovilisplan
//
//  Created by Admin on 4/27/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//
#import "ListingViewController.h"
#import "NotificationViewController.h"
#import "LibraryViewController.h"
#import "HomeViewController.h"
#import "AddNewFarmView.h"
#import "ListingCell.h"
#import "LoginViewController.h"
#import "WelcomeDetailViewController.h"
#import "MenuView.h"
#import "AppDelegate.h"
#import "BovilisApi.h"
#import "UserFarmMasterModel.h"
#import "AboutUsViewController.h"
#import "ListingDetailViewController.h"
#import "GAITracker.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "Reachability.h"
#import "GetMappingDays.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <NHNetworkTime/NHNetworkTime.h>
#import "AllAnimalsViewController.h"

@import Firebase;

@interface ListingViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    UIView *transparentView ;
    AddNewFarmView *addNewFarmView;
    NSMutableDictionary * contactDirectory ;
    NSMutableDictionary * contactDirectoryFinal ;
    NSMutableDictionary *sortedNames;
    NSMutableArray *dataArray;
    NSMutableArray *subTitleArray;
    NSMutableArray *dataArray1;
    NSMutableArray*sectionHeader;
    NSArray *sectionListArray ;
    NSMutableArray*sectionArray;
    NSInteger rowCount;
    UserFarmMasterModel *obj;
    MenuView *menuView;
    NSString *alphabet;
    NSArray * filterArray;
    AppDelegate *appDelegate;
    NSMutableArray *sectionTitleList;
    UIAlertController*    delete_alert;
    UIAlertController*    success_alert;
    NSString *farmID;
    UserFarmMasterModel *farmObject;
    NSMutableArray *farmArray;
    UIActivityIndicatorView *activityView;
}

@property(nonatomic,strong) IBOutlet  UIButton *btnNewForm;
@property(nonatomic,strong) IBOutlet  UIButton *btnScheduleVaccine;
@property(strong,nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic) NSDictionary *alphabetizedDictionary;
@property(nonatomic) NSArray *sectionIndexTitles;
@property(nonatomic,strong) IBOutlet UILabel*lblTitle;
@property(nonatomic,weak) IBOutlet UIImageView *logoImg;

@end

@implementation ListingViewController
@synthesize name;


- (void)viewDidLoad
{
    [super viewDidLoad];
   
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication ]delegate];
  self.logoImg.image = [UIImage imageNamed:[Utility setLocalizeLogo:@"logo"]];
    obj = [[UserFarmMasterModel alloc]init];
    [self configureTableViewCell];
    
    if( appDelegate.getUserFarmsFromServer.count>0)
    {
        farmArray =appDelegate.getUserFarmsFromServer;
        [self setTableData];
        [self arrangeSortedDictionary];
    }
    
    [self setUI];
    [self loadMenuView];
    [transparentView setHidden:YES];
}

-(void)reloadData
{
    if(appDelegate.getUserFarmsFromServer.count>0 && ![appDelegate.getUserFarmsFromServer containsObject:@"No Record Found"] )
    {
        farmArray =appDelegate.getUserFarmsFromServer;
        [self setTableData];
        [self arrangeSortedDictionary];
        [self.tableView reloadData];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"My Farms";
    [transparentView setHidden:YES];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    if([appDelegate.reloadCalendar isEqualToString:@"No"])
    {
        [self reloadData];
    }
}


#pragma mark ===Set UI ===
-(void)setUI
{
    
    self.tableView.estimatedRowHeight = 50;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [_lblTitle setText:[appDelegate.dictCulture valueForKey:@"MyListing"]];
    NSString * lblTitle =[appDelegate.dictCulture valueForKey:@"MyListing"];
    if (lblTitle.length==0)
    {
        [_lblTitle setText:@"My Farms"];
    }
    
    [_btnNewForm setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnNewForm.layer.borderWidth = 1.0f;
    [_btnNewForm.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnNewForm setTitle:[appDelegate.dictCulture valueForKey:@"NewFarm"] forState:UIControlStateNormal];
    
    NSString * btnNewForm =[appDelegate.dictCulture valueForKey:@"NewFarm"];
    if (btnNewForm.length==0)
    {
        [_btnNewForm setTitle:@"NEW FARM" forState:UIControlStateNormal];
    }
    
    [_btnScheduleVaccine setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnScheduleVaccine.layer.cornerRadius= 5.0;
    _btnScheduleVaccine.layer.borderWidth = 1.0f;
    [_btnScheduleVaccine.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnScheduleVaccine setTitle:[appDelegate.dictCulture valueForKey:@"ScheduleAVaccine"] forState:UIControlStateNormal];
    
    NSString * btnScheduleVaccine =[appDelegate.dictCulture valueForKey:@"ScheduleAVaccine"];
    if(btnScheduleVaccine.length==0)
    {
        [_btnScheduleVaccine setTitle:@"Schedule A Vaccine" forState:UIControlStateNormal];
    }
}

#pragma mark ===Show side menu ===
-(void)showMenu
{
    [self loadMenuView];
    
    [transparentView setHidden:FALSE];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transparentView.layer addAnimation:transition forKey:nil];
}

#pragma mark ===Show Side Menu ===
-(void)loadMenuView
{
    transparentView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( transparentView.frame.size);
    
    UIGraphicsBeginImageContext( transparentView.frame.size);
    [[UIImage imageNamed:@"bg-Transparent"] drawInRect: transparentView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //transparentView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [self.view addSubview:transparentView];
    menuView= [[MenuView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-295, 0, self.view.frame.size.width, self.view.frame.size.height-0)];
    CGRect newFrame = CGRectMake(0, 0, menuView.frame.size.width,menuView.frame.size.height);
    menuView.innerView.frame = newFrame;
    [ menuView.btnClose addTarget:self action:@selector(closeMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [ menuView.btnDisclaimer addTarget:self action:@selector(disclaimerMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [ menuView.btnPrivacy addTarget:self action:@selector(privacyMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [ menuView.btnTerms addTarget:self action:@selector(termsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [ menuView.btnSignout addTarget:self action:@selector(signoutMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [ menuView.btnCalendar addTarget:self action:@selector(calendarMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [ menuView.btnListing addTarget:self action:@selector(listingMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [ menuView.btnLibrary addTarget:self action:@selector(libraryMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [ menuView.btnNotification addTarget:self action:@selector(notificationMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [ menuView.btnAboutUS addTarget:self action:@selector(aboutUsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
       [menuView.btnAnimals addTarget:self action:@selector(animalsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [transparentView addSubview:menuView];
}
-(void)closeMenuButtonPressed
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transparentView.layer addAnimation:transition forKey:nil];
    [transparentView setHidden:YES];
}

-(void)animalsMenuButtonPressed {
    AllAnimalsViewController *settingVC = [AllAnimalsViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)aboutUsMenuButtonPressed
{
    AboutUsViewController *settingVC = [AboutUsViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)notificationMenuButtonPressed
{
    NotificationViewController *settingVC = [NotificationViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)libraryMenuButtonPressed
{
    LibraryViewController *settingVC = [LibraryViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)listingMenuButtonPressed
{
    ListingViewController *settingVC = [ListingViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)calendarMenuButtonPressed
{
    HomeViewController *settingVC = [HomeViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)signoutMenuButtonPressed
{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"AllApiDataDictionary"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IsLogin"];
  //  [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SaveUserEmail"];

    [[NSUserDefaults standardUserDefaults] synchronize];
    appDelegate.reloadCalendar=@"No";
    appDelegate.isLoadedNotifications=@"No";
    LoginViewController *loginVC = [LoginViewController new];
    [self.navigationController pushViewController:loginVC animated:YES];
}

-(void)privacyMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Privacy Policy";
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)disclaimerMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Disclaimer";
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)termsMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Terms and Conditions";
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark === Alphabatic Order List Methods ===
-(void)setTableData
{
    sectionArray=[[NSMutableArray alloc]init];
    contactDirectory = [[NSMutableDictionary alloc] init];
    sectionListArray = [NSArray arrayWithObjects: @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z",@"#", nil];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"farmName" ascending:YES selector:@selector(localizedCompare:)];
    if (![appDelegate.getUserFarmsFromServer containsObject:@"No Record Found"] ){
        
        farmArray =(NSMutableArray *) [appDelegate.getUserFarmsFromServer sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        sectionHeader = [[NSMutableArray alloc]init];
        contactDirectory = [NSMutableDictionary new];
        
        for (NSInteger i = 0; i < appDelegate.getUserFarmsFromServer.count; i++)
        {
            NSDictionary *tmp = farmArray[i];
            NSString *firstLetter = [[tmp valueForKey:@"farmName"] substringToIndex:1];
            if (![sectionListArray containsObject:firstLetter])
            {
                firstLetter=@"#";
            }
            
            if (![contactDirectory valueForKey:firstLetter])
            {
                [contactDirectory setObject:[NSMutableArray new] forKey:firstLetter];
                [sectionHeader addObject:firstLetter];
                
            }
            [[contactDirectory objectForKey:firstLetter] addObject:tmp];
        }
    }
}

-(void)arrangeSortedDictionary
{
    alphabet = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    sortedNames = [NSMutableDictionary dictionary];
    for(int characterIndex = 0; characterIndex < 26; characterIndex++)
    {
        NSString *alphabetCharacter = [alphabet substringWithRange:NSMakeRange(characterIndex, 1)];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"farmName BEGINSWITH[c] %@",alphabetCharacter];
        NSArray *filteredNames =  (NSMutableArray *)[farmArray filteredArrayUsingPredicate:resultPredicate];
        [sortedNames setObject:filteredNames forKey:alphabetCharacter];
    }
}

#pragma mark === Show New Farm===
-(void)showNewFarmView
{
    transparentView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( transparentView.frame.size);
    [[UIImage imageNamed:@"bg-Transparent.png"] drawInRect: transparentView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //transparentView.backgroundColor = [UIColor colorWithPatternImage:image];
    [self.view addSubview:transparentView];
    
    addNewFarmView= [[AddNewFarmView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( addNewFarmView.innerView.frame.size);
    [[UIImage imageNamed:@"background-Popup"] drawInRect: addNewFarmView.innerView.bounds];
    UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    addNewFarmView.innerView.backgroundColor = [UIColor colorWithPatternImage:image1];
    [addNewFarmView.lblTop setText:[appDelegate.dictCulture valueForKey:@"NewFarm"]];
    NSString * addNewFarm =[appDelegate.dictCulture valueForKey:@"NewFarm"];
    if (addNewFarm.length==0)
    {
        [addNewFarmView.lblTop setText:@"NEW FARM"];
    }
    [addNewFarmView.btnSave addTarget:self action:@selector(sendNewFarmDataToServer) forControlEvents:UIControlEventTouchUpInside];
    [ addNewFarmView.btnCancel addTarget:self action:@selector(closeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [transparentView addSubview:addNewFarmView];
}

-(void)closeButtonPressed
{
    [appDelegate.animalsSelectedArray removeAllObjects];
    [transparentView removeFromSuperview];
}

#pragma mark ===Save new Farm Data on Server ===
-(void)sendNewFarmDataToServer
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    if(status == NotReachable)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"No internet connection." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else if( addNewFarmView.txtFarmName.text.length == 0) {
        NSString *message =[appDelegate.dictCulture valueForKey:@"PleaseProvideAllTheRequiredInformation"];
        
        if ([message isEqualToString:@"NA"]  || (message.length == 0)) {
            message = @"Please provide all the required information.";
        }
        NSString *okKEY =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if ([okKEY isEqualToString:@"NA"]  || (okKEY.length == 0)) {
            okKEY = @"Ok";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:okKEY style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            addNewFarmView.txtFarmName.text =@"";
        }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else  if (![self checkFarmIDIsExistence:addNewFarmView.txtFarmName.text])
        {
            NSString *message =[appDelegate.dictCulture valueForKey:@"FarmAlreadyExists"];
            
            if ([message isEqualToString:@"NA"]  || (message.length == 0)) {
                message = @"Farm already exists.";
            }
            NSString *okKEY =[appDelegate.dictCulture valueForKey:@"Ok"];

            if ([okKEY isEqualToString:@"NA"]  || (okKEY.length == 0)) {
                okKEY = @"Ok";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:okKEY style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
               addNewFarmView.txtFarmName.text =@"";
            }];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
 
    else
    {
        
        [FIRAnalytics logEventWithName:@"add_farm"
                            parameters:@{
                                         @"farm_name": addNewFarmView.txtFarmName.text
                                             }];
        [FBSDKAppEvents logEvent:@"Add Farm"];

        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                              action:@"MyFarms_Tab_From_Home"
                                                               label:@"New_Farm_Added"
                                                               value:nil] build]];
        
        activityView = [[UIActivityIndicatorView alloc]
                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityView.center=self.view.center;
        [activityView startAnimating];
        [self.view addSubview:activityView];
        
        NSDate *date =[NSDate networkDate];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSString*  dateString = [dateFormat stringFromDate:date];
        NSString *FarmName =[addNewFarmView.txtFarmName.text capitalizedString];
        NSString *region=addNewFarmView.txtReigon.text;
        NSString *contactP = addNewFarmView.txtContactPerson.text;
        NSString *address = addNewFarmView.txtAddress.text;
        NSString *phone =  addNewFarmView.txtPhone.text;
        NSString *notes =  addNewFarmView.txtComment.text;
        NSString *email =addNewFarmView.txtEmail.text;
        NSString * registrationNo = addNewFarmView.txtRegistration.text;
        NSString * animalNumber = addNewFarmView.txtAnimals.text;
        
        if ([animalNumber isEqualToString:@""])
        {
            animalNumber = @"NA";
        }
        
        if (region.length==0)
        {
            region = @"NA";
        }
        
        if ([contactP isEqualToString:@""])
        {
            contactP = @"NA";
        }
        
        if ([address isEqualToString:@""])
            
        {
            address = @"NA";
        }
        
        if ([phone isEqualToString:@""])
        {
            phone = @"NA";
        }
        
        if ([email isEqualToString:@""])
        {
            email = @"NA";
        }
        
        if ([notes isEqualToString:@""])
        {
            notes = @"NA";
        }
        
        if ([registrationNo isEqualToString:@""])
        {
            registrationNo = @"NA";
        }
        
        NSString * userName = @"";
        NSString * farmType1 = appDelegate.switchValue1;
        NSString * farmType2 = appDelegate.switchValue2;
        NSString * farmType3 =appDelegate.switchValue3;
        NSString * farmType4 =appDelegate.switchValue4;
        NSString * addedDateTime =dateString;
        
        if(FarmName.length>0)
        {
            appDelegate.reloadCalendar =@"No";
            BovilisApi *obj1 =[BovilisApi sharedInstance];
            
            [obj1 sentNewFarmDataToServerWithFarmName:FarmName withRegNO:registrationNo withRegion:region withContact:contactP withPhone:phone withNotes:notes withEmail:email withAnimalNumber:animalNumber withAddress:address withFarmType1:farmType1 withUserName:userName withAddedDateTime:addedDateTime withFarmType2:farmType2 withFarmType3:farmType3 withFarmType4:farmType4 WithSuccessBlock:^(BOOL success, NSDictionary *result) {
                
                if(success)
                {
                    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
                    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
                    [userDefaults1 synchronize];
                    
                    BovilisApi *boviObj = [BovilisApi sharedInstance];
                    [boviObj requestForGetUserFarmDataWithUserEmail:userEmail];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [activityView stopAnimating];
                        [activityView setHidesWhenStopped:YES];
                        
                        NSString *message= [appDelegate.dictCulture valueForKey:@"YourFarmHasBeenAdded"];
                        NSString *okText =[appDelegate.dictCulture valueForKey:@"Ok"];
                        
                        if (message.length==0)
                        {
                            message =@"Your farm has been added.";
                        }
                        
                        if (okText.length==0)
                        {
                            okText =@"Ok";
                        }
                        
                        UIAlertController*  alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                             {
                                                 [self performSelector:@selector(moveBackToFarListing) withObject:nil afterDelay:1.0];
                                             }];
                        
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                }
                
            } failure:^(NSError *error, NSString *message) {
                
            }];
        }
        else
        {
            [activityView stopAnimating];
            [activityView setHidesWhenStopped:YES];
            NSString *message =[appDelegate.dictCulture valueForKey:@"PleaseProvideFarmName"];
            NSString *okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            
            if (message.length==0)
            {
                
                message =@"Please provide farm name.";
            }
            
            if (okText.length==0)
            {
                
                okText =@"Ok";
            }
            
            UIAlertController*  alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(BOOL)checkFarmIDIsExistence:(NSString *)animalID {
    
    for (UserFarmMasterModel *fObj in appDelegate.getUserFarmsFromServer) {
        if ([animalID isEqualToString:fObj.farmName]){
            return false;
        }
    }
    return true;
}

-(void)moveBackToFarListing
{
    [self reloadData];
    [transparentView removeFromSuperview];
}

#pragma mark === Configure TableView Cell===
-(void)configureTableViewCell
{
    [_tableView registerNib:[UINib nibWithNibName:@"ListingCell"
                                           bundle:nil] forCellReuseIdentifier:@"ListingCell"];
}

#pragma mark === TableView Delegates/Datasource===
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sectionHeader.count;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return sectionListArray;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index
{
    return index;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"ListingCell";
    ListingCell *cell = (ListingCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
    NSMutableArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
    obj = [sectionTitleList1 objectAtIndex:indexPath.row];
    cell.lblTitle.text =obj.farmName;
    cell.lblSubTitle.text =obj.region;
    
    if([obj.region isEqualToString:@"NA"])
    {
        cell.lblSubTitle.text=@"";
    }
    
    cell.textLabel.numberOfLines=0;
    cell.lblTitle.textColor =[UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
    NSArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
    obj = [sectionTitleList1 objectAtIndex:indexPath.row];
    
    ListingDetailViewController *vc = [ListingDetailViewController new];
    vc.name =obj.farmName;
    vc.reigon =obj.region;
    vc.ownerName =obj.contactPerson;
    vc.address =obj.address;
    vc.phone =obj.phone;
    vc.email =obj.email;
    vc.comments =obj.Comments;
    vc.farmType1=obj.farmType1;
    vc.farmType2=obj.farmType2;
    vc.farmType3=obj.farmType3;
    vc.farmType4=obj.farmType4;
    vc.numberOfAnimals = obj.numberOfAnimals;
    vc.farmID = obj.farmID;
    vc.registration = obj.registrationNumber;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    return  UITableViewAutomaticDimension;

}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 30);
    myLabel.font = [UIFont fontWithName:@"UNIVERS-CONDENSED-BOLD" size:15] ;
    myLabel.text =
    [NSString stringWithFormat:@"    %@",[self tableView:tableView titleForHeaderInSection:section]];
    myLabel.backgroundColor =[UIColor colorWithRed:230/255.0 green:231/255.0 blue:232/255.0 alpha:1];
       myLabel.textColor =[UIColor colorWithRed:52/255.0 green:106/255.0 blue:68/255.0 alpha:1];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    return headerView;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [sectionHeader objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionIndexTitle = [sectionHeader objectAtIndex:section];
    NSArray *sectionTitle = [sortedNames objectForKey:sectionIndexTitle];
    return [sectionTitle count];
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
        NSArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
        obj = [sectionTitleList1 objectAtIndex:indexPath.row];
        farmID =obj.farmID;
        farmObject =obj;
        [self deleteFarmFromServer];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *deleteText =[appDelegate.dictCulture valueForKey:@"Delete"];
    deleteText = deleteText.length==0? @"Delete":deleteText;
   
    return deleteText;
    
}
#pragma mark === delete Farm===
-(void)deleteFarmFromServer
{
    NSString * message= [appDelegate.dictCulture valueForKey:@"DeleteFarmText"];
    NSString * cancelText =[appDelegate.dictCulture valueForKey:@"No"];
    NSString * proceedText =[appDelegate.dictCulture valueForKey:@"Proceed"];
    
    if (message.length==0)
    {
        message =@"Click “PROCEED” to delete the farm, OR “NO” to CANCEL.";
    }
    
    if (cancelText.length==0)
    {
        cancelText =@"NO";
    }
    
    if (proceedText.length==0)
    {
        proceedText =@"PROCEED";
    }
    
    delete_alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelText style:UIAlertActionStyleDefault handler:nil];
    [delete_alert addAction:cancel];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:proceedText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteFarm];
    }];
    [delete_alert addAction:ok];
    [self presentViewController:delete_alert animated:YES completion:nil];
}
-(void)deleteFarm {
    
   NSString* alertMessage= [appDelegate.dictCulture valueForKey:@"FarmDeletedSuccessfully"];
       alertMessage = alertMessage.length == 0 ? @"Farm Deleted Successfully" : alertMessage;
     NSString*   okText =[appDelegate.dictCulture valueForKey:@"Ok"];
    okText = okText.length == 0 ? @"OK" : okText;

    appDelegate.reloadCalendar=@"No";
    appDelegate.isLoadedNotifications =@"No";
    BovilisApi *obj1 = [BovilisApi sharedInstance];
   [appDelegate.getUserFarmsFromServer removeObject:farmObject];
    
    [obj1 deleteFarmFromServerWithFarmID:farmID ViewController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        
        if (success) {
            
            GetMappingDays *mapOBj=[[GetMappingDays alloc]init];
            [mapOBj getScheduleVaccineFromServer];
        }
    
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                      
                      UIAlertController* deleteAlert = [UIAlertController alertControllerWithTitle:nil message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self reloadFarmData];

                            [self.tableView  reloadData];

                        }];
                        [deleteAlert addAction:ok];
                        [self presentViewController:deleteAlert animated:YES completion:nil];
            
            
        });
        
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
   
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)reloadFarmData{

    farmArray =appDelegate.getUserFarmsFromServer;

        [self setTableData];
        [self arrangeSortedDictionary];

    [appDelegate.getUserFarmsFromServer removeObject:farmObject];
    [self.tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)getSubTitle:(NSString *)subTitle {
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@",subTitle];
    filterArray =  (NSMutableArray *)[appDelegate.getUserFarmsFromServer  filteredArrayUsingPredicate:resultPredicate];
}

#pragma mark === UIButton Actions ===
-(IBAction)menuButtonPressed:(id)sender
{
    //show side menu
    [self showMenu];
}

-(IBAction)newFarmButtonPressed:(id)sender
{
    //save user farm
    [self showNewFarmView];
}
-(IBAction)homeButtonPressed:(id)sender
{
    HomeViewController *HomeVc = [HomeViewController new];
    [self.navigationController pushViewController:HomeVc animated:NO];
}

-(IBAction)notificationButtonPressed:(id)sender
{
    NotificationViewController *notificationVc = [NotificationViewController new];
    [self.navigationController pushViewController:notificationVc animated:NO];
}

-(IBAction)libraryButtonPressed:(id)sender
{
    LibraryViewController *libraryVc = [LibraryViewController new];
    [self.navigationController pushViewController:libraryVc animated:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
