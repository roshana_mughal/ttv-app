//
//  ListingDetailViewController.m
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//


#import "BovilisApi.h"
#import "AddNewFarmView.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "AnimalsViewController.h"
#import "ListingDetailViewController.h"
#import "BovilisApi.h"
#import "GAITracker.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "UserFarmMasterModel.h"
#import "Reachability.h"
#import <NHNetworkTime/NHNetworkTime.h>

@interface ListingDetailViewController ()
{
    UIView *transparentView;
    AddNewFarmView *addNewFarmView;
    AppDelegate *appDelegate;
    UIAlertController*    delete_alert;
    UIAlertController*    success_alert;
    UIActivityIndicatorView *activityView;
    NSString * fType1;
    NSString * fType2 ;
    NSString * fType3 ;
    NSString * fType4;
}

@property(nonatomic,strong) IBOutlet  UIButton *btnEdit;
@property(nonatomic,strong) IBOutlet  UIButton *btnDelete;
@property(nonatomic,strong) IBOutlet  UIButton *btnViewAnimals;
@property(nonatomic,strong) IBOutlet UILabel *lblName;
@property(nonatomic,strong) IBOutlet UILabel *lblReigon;
@property(nonatomic,strong) IBOutlet UILabel *lblOwnerName;
@property(nonatomic,strong) IBOutlet UILabel *lblAddres;
@property(nonatomic,strong) IBOutlet UILabel *lblPhone;
@property(nonatomic,strong) IBOutlet UILabel *lblEmail;
@property(nonatomic,strong) IBOutlet UILabel *lblComments;
@property(nonatomic,strong) IBOutlet UILabel *lblHome;
@property(nonatomic,strong) IBOutlet UILabel*lblTitle;
@property(nonatomic,strong) IBOutlet UITextField *txt1;
@property(nonatomic,strong) IBOutlet UITextField *txt2;
@property(nonatomic,strong) IBOutlet UITextField *txt3;
@property(nonatomic,strong) IBOutlet UITextField *txt4;
@property(nonatomic,strong) IBOutlet UITextField *txt5;
@property(nonatomic,strong) IBOutlet UITextField *txt6;
@property(nonatomic,strong) IBOutlet UITextView *tView;

@end

@implementation ListingDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    [self setUI];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillLayoutSubviews {

    self.tView.contentInset = UIEdgeInsetsMake(-10.0,0.0,0,0.0);

}

-(void)setUI
{

    NSString *farmDetail =[appDelegate.dictCulture valueForKey:@"FarmDetail"];
    if ([farmDetail isEqualToString:@"NA"]  || (farmDetail.length == 0))
    {
        _lblTitle.text=@"Farm Detail";
    }
    else
    {
        _lblTitle.text =farmDetail;
    }
    
    NSString *farmType =[appDelegate.dictCulture valueForKey:@"FarmType"];
    if ([farmType isEqualToString:@"NA"]  || (farmType.length == 0))
    {
        _txt1.text=@"Farm Type";
    }
    else
    {
        _txt1.text =farmType;
    }
    
    NSString *contactPerson =[appDelegate.dictCulture valueForKey:@"ContactPerson"];
    if ([contactPerson isEqualToString:@"NA"]  || (contactPerson.length == 0))
    {
        _txt2.text=@"Contact Person";
    }
    else
    {
        _txt2.text =contactPerson;
    }
    
    NSString *address =[appDelegate.dictCulture valueForKey:@"Address"];
    if ([address isEqualToString:@"NA"]  || (address.length == 0))
    {
        _txt3.text=@"Address";
    }
    else
    {
        _txt3.text =address;
    }
    
    NSString *phone =[appDelegate.dictCulture valueForKey:@"Phone"];
    if ([phone isEqualToString:@"NA"]  || (phone.length == 0))
    {
        _txt4.text=@"Phone";
    }
    else
    {
        _txt4.text =phone;
    }
    
    NSString *emailAddress =[appDelegate.dictCulture valueForKey:@"EmailAddress"];
    if ([emailAddress isEqualToString:@"NA"]  || (emailAddress.length == 0))
    {
        _txt5.text=@"EmailAddress";
    }
    else
    {
        _txt5.text =emailAddress;
    }
    
    NSString *notes =[appDelegate.dictCulture valueForKey:@"Notes"];
    if ([notes isEqualToString:@"NA"]  || (notes.length == 0))
    {
        _txt6.text=@"Notes";
    }
    else
    {
        _txt6.text =notes;
    }
    
    _lblName.text =_name;
    _lblReigon.text=_reigon ;
    _lblOwnerName.text =_ownerName;
    _lblAddres.text=_address ;
    self.tView.text=_comments ;
    _lblPhone.text=_phone ;
    _lblEmail.text=_email ;
    
    
    if ([_name isEqualToString:@"NA"]  || (_name.length == 0))
    {
        _lblName.text =@"";
    }
    else
    {
        _lblName.text =_name;
    }
    
    if ([_reigon isEqualToString:@"NA"]  || (_reigon.length == 0))
    {
        _lblReigon.text =@"";
    }
    else
    {
        _lblReigon.text =_reigon;
    }
    
    if ([_ownerName isEqualToString:@"NA"]  || (_ownerName.length == 0))
    {
        _lblOwnerName.text =@"";
    }
    else
    {
        _lblOwnerName.text =_ownerName;
    }
    
    if ([_address isEqualToString:@"NA"]  || (_address.length == 0))
    {
        _lblAddres.text =@"";
    }
    else
    {
        _lblAddres.text =_address;
    }
    
    if ([_comments isEqualToString:@"NA"]  || (_comments.length == 0))
    {
        self.tView.text=@"" ;
        
    }
    else
    {
        self.tView.text=_comments ;
        
    }
    
    if ([_phone isEqualToString:@"NA"]  || (_phone.length == 0))
    {
        _lblPhone.text =@"";
    }
    else
    {
        _lblPhone.text =_phone;
    }
    
    if ([_email isEqualToString:@"NA"]  || (_email.length == 0))
    {
        _lblEmail.text =@"";
    }
    else
    {
        _lblEmail.text =_email;
    }
    
    NSString *milkCow =[appDelegate.dictCulture valueForKey:@"Dairy"];
    milkCow=  milkCow.length == 0 ?  @"Dairy": milkCow;
    
    NSString *beafSteak = [appDelegate.dictCulture valueForKey:@"BeefSlashVeal"];
    beafSteak=  beafSteak.length == 0 ?  @"Beaf / Veal": beafSteak;
    
    NSString *cebadero =[appDelegate.dictCulture valueForKey:@"CowCalf"];
    cebadero=  cebadero.length == 0 ?  @"Cow - Calf": cebadero;
    
    NSString *mixta =[appDelegate.dictCulture valueForKey:@"Mixed"];
    mixta=  mixta.length == 0 ?  @"Mixed": mixta;
    NSString *homeText=@"";
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init]; ;
    
    if([_farmType1 isEqualToString:@"True"])
    {
        [tempArray addObject:milkCow];
    }
    if([_farmType2 isEqualToString:@"True"])
    {
        [tempArray addObject:beafSteak];
    }
    if([_farmType3 isEqualToString:@"True"])
    {
        [tempArray addObject:cebadero];
    }
    
    if([_farmType4 isEqualToString:@"True"])
    {
        [tempArray addObject:mixta];
    }
    NSString *str;
    for (int i = 0; i<tempArray.count; i++) {
        
        if (i==0) {
            homeText = [tempArray objectAtIndex:i];
            
        }
        else{
            
            homeText =[NSString stringWithFormat:@"%@, %@",homeText, [tempArray objectAtIndex:i]];
            
        }
    }
    
    _lblHome.text=homeText;
    
    NSString *viewAnimals =[appDelegate.dictCulture valueForKey:@"AddViewAnimals"];
    viewAnimals = viewAnimals.length == 0? @"View/Add Animals":viewAnimals;
    
    if ([viewAnimals isEqualToString:@"NA"]  || (viewAnimals.length == 0))
    {
        [_btnViewAnimals setTitle:@"View/Add Animals" forState:UIControlStateNormal];
        
    }
    else
    {
        [_btnViewAnimals setTitle:viewAnimals forState:UIControlStateNormal];
    }
    NSString *edit =[appDelegate.dictCulture valueForKey:@"Edit"];
    if ([edit isEqualToString:@"NA"]  || (edit.length == 0))
    {
        [_btnEdit setTitle:@"EDIT" forState:UIControlStateNormal];
        
    }
    else
    {
        [_btnEdit setTitle:[edit uppercaseString] forState:UIControlStateNormal];
    }
    
    
    [_btnEdit setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnEdit.layer.borderWidth = 1.0f;
    [_btnEdit.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnDelete setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnDelete.layer.borderWidth = 1.0f;
    [_btnDelete.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
}

#pragma mark === Show New Farm===

-(void)showUpdateFarmView
{
    transparentView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( transparentView.frame.size);
    [[UIImage imageNamed:@"bg-Transparent.png"] drawInRect: transparentView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //transparentView.backgroundColor = [UIColor colorWithPatternImage:image];
    [self.view addSubview:transparentView];
    addNewFarmView= [[AddNewFarmView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( addNewFarmView.innerView.frame.size);
    [[UIImage imageNamed:@"background-Popup"] drawInRect: addNewFarmView.innerView.bounds];
    UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    addNewFarmView.innerView.backgroundColor = [UIColor colorWithPatternImage:image1];
    [self preloadDataInNewFarm];
    
    [ addNewFarmView.btnSave addTarget:self action:@selector(updateNewFarmDataToServer) forControlEvents:UIControlEventTouchUpInside];
    [ addNewFarmView.btnCancel addTarget:self action:@selector(closeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [transparentView addSubview:addNewFarmView];
}

-(void)preloadDataInNewFarm
{
    [addNewFarmView.lblTop setText:[appDelegate.dictCulture valueForKey:@"UpdateFarm"]];
    
    NSString * UpdateFarm =[appDelegate.dictCulture valueForKey:@"UpdateFarm"];
    if (UpdateFarm.length==0)
    {
        [addNewFarmView.lblTop setText:@"Update Farm"];
    }
    
    addNewFarmView.txtFarmName.text =_name;
    addNewFarmView.txtRegistration.text=_registration;
    addNewFarmView.txtReigon.text=_reigon;
    addNewFarmView.txtContactPerson.text=_ownerName;
    addNewFarmView.txtAddress.text=_address;
    addNewFarmView.txtPhone.text=_phone;
    addNewFarmView.txtComment.text=_comments;
    addNewFarmView.txtEmail.text=_email;
    addNewFarmView.txtAnimals.text = _numberOfAnimals;
    
    if ([_registration isEqualToString:@"NA"])
    {
        addNewFarmView.txtRegistration.text=@"";
        /*addNewFarmView.txtRegistration.placeholder =[appDelegate.dictCulture valueForKey:@"RegistrationNr"];
         
         NSString * RegistrationNr =[appDelegate.dictCulture valueForKey:@"RegistrationNr"];
         if (RegistrationNr.length==0)
         {
         addNewFarmView.txtRegistration.placeholder=@"Registration Nr.";
         }*/
    }
    
    if ([_ownerName isEqualToString:@"NA"])
    {
        addNewFarmView.txtContactPerson.text=@"";
        /* addNewFarmView.txtContactPerson.placeholder = [appDelegate.dictCulture valueForKey:@"ContactPerson"];
         
         NSString * SelectRegion =[appDelegate.dictCulture valueForKey:@"ContactPerson"];
         if (SelectRegion.length==0)
         {
         addNewFarmView.txtContactPerson.placeholder=@"Contact Person";
         }*/
    }
    
    if ([_numberOfAnimals isEqualToString:@"NA"])
    {
        addNewFarmView.txtAnimals.text=@"";
        /* addNewFarmView.txtAnimals.placeholder = [appDelegate.dictCulture valueForKey:@"AnimalNumber"];
         
         NSString * AnimalNumber =[appDelegate.dictCulture valueForKey:@"AnimalNumber"];
         if (AnimalNumber.length==0)
         {
         addNewFarmView.txtAnimals.placeholder =@"Animal Number";
         }*/
    }
    
    if ([_address isEqualToString:@"NA"])
    {
        addNewFarmView.txtAddress.text=@"";
        /*   addNewFarmView.txtAddress.placeholder =[appDelegate.dictCulture valueForKey:@"Address"];
         NSString * Address =[appDelegate.dictCulture valueForKey:@"Address"];
         if (Address.length==0)
         {
         addNewFarmView.txtAddress.placeholder =@"Address";
         }*/
    }
    
    if ([_phone isEqualToString:@"NA"])
    {
        addNewFarmView.txtPhone.text=@"";
        /*  addNewFarmView.txtPhone.placeholder = [appDelegate.dictCulture valueForKey:@"Phone"];
         
         NSString * Phone =[appDelegate.dictCulture valueForKey:@"Phone"];
         if (Phone.length==0)
         {
         addNewFarmView.txtPhone.placeholder =@"Phone";
         }*/
    }
    
    if ([_email isEqualToString:@"NA"])
    {
        addNewFarmView.txtEmail.text=@"";
        /* addNewFarmView.txtEmail.placeholder = [appDelegate.dictCulture valueForKey:@"EmailAddress"];
         NSString * EmailAddress =[appDelegate.dictCulture valueForKey:@"EmailAddress"];
         if (EmailAddress.length==0)
         {
         addNewFarmView.txtEmail.placeholder =@"Email Address";
         }*/
    }
    
    if ([_comments isEqualToString:@"NA"])
    {
        addNewFarmView.txtComment.text=@"";
        /*  addNewFarmView.txtComment.placeholder = [appDelegate.dictCulture valueForKey:@"Notes"];
         
         NSString * Notes =[appDelegate.dictCulture valueForKey:@"Notes"];
         if (Notes.length==0)
         {
         addNewFarmView.txtComment.placeholder =@"Notes";
         }*/
    }
    
    if ([_reigon isEqualToString:@"NA"])
    {
        addNewFarmView.txtReigon.text=@"";
        /* addNewFarmView.txtReigon.placeholder = [appDelegate.dictCulture valueForKey:@"SelectRegion"];
         
         NSString * Notes =[appDelegate.dictCulture valueForKey:@"Notes"];
         if (Notes.length==0)
         {
         addNewFarmView.txtComment.placeholder =@"Notes";
         }*/
    }
    
    if ([_farmType1 isEqualToString:@"True"])
    {
        [ addNewFarmView.switch1 setOn:YES animated:NO];
    }
    
    else
    {
        [ addNewFarmView.switch1 setOn:NO animated:NO];
    }
    
    if ([_farmType2 isEqualToString:@"True"])
    {
        [ addNewFarmView.switch2 setOn:YES animated:NO];
    }
    
    else
    {
        [ addNewFarmView.switch2 setOn:NO animated:NO];
    }
    
    if ([_farmType3 isEqualToString:@"True"])
    {
        [ addNewFarmView.switch3 setOn:YES animated:NO];
    }
    
    else
    {
        [ addNewFarmView.switch3 setOn:NO animated:NO];
    }
    
    if ([_farmType4 isEqualToString:@"True"])
    {
        [ addNewFarmView.switch4 setOn:YES animated:NO];
    }
    
    else
    {
        [ addNewFarmView.switch4 setOn:NO animated:NO];
    }
}

-(void)closeButtonPressed
{
    [transparentView removeFromSuperview];
}


#pragma mark ===Save new Farm Data on Server ===
-(void)updateNewFarmDataToServer
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    if(status == NotReachable)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"No internet connection." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    else if( addNewFarmView.txtFarmName.text.length == 0) {
        NSString *message =[appDelegate.dictCulture valueForKey:@"PleaseProvideAllTheRequiredInformation"];
        
        if ([message isEqualToString:@"NA"]  || (message.length == 0)) {
            message = @"Please provide all the required information.";
        }
        NSString *okKEY =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if ([okKEY isEqualToString:@"NA"]  || (okKEY.length == 0)) {
            okKEY = @"Ok";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:okKEY style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            addNewFarmView.txtFarmName.text =@"";
        }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                              action:@"MyFarms_Tab_From_Home"
                                                               label:@"Edit_Farm"
                                                               value:nil] build]];
        
        NSString *FarmName =
        addNewFarmView.txtFarmName.text;
        _reigon=addNewFarmView.txtReigon.text;
        NSString *region=addNewFarmView.txtReigon.text;
        NSString *contactP = addNewFarmView.txtContactPerson.text;
        NSString *address = addNewFarmView.txtAddress.text;
        NSString *phone =  addNewFarmView.txtPhone.text;
        NSString *notes =  addNewFarmView.txtComment.text;
        NSString *email =addNewFarmView.txtEmail.text;
        NSString * registrationNo = addNewFarmView.txtRegistration.text;
        NSString * animalNumber = addNewFarmView.txtAnimals.text;
        NSString * userName = @"";
        
        if ([animalNumber isEqualToString:@""])
        {
            animalNumber = @"NA";
        }
        
        if ([region isEqualToString:@""])
        {
            region = @"NA";
        }
        
        if ([contactP isEqualToString:@""])
        {
            contactP = @"NA";
        }
        
        if ([address isEqualToString:@""])
        {
            address = @"NA";
        }
        
        if ([phone isEqualToString:@""])
        {
            phone = @"NA";
        }
        
        if ([email isEqualToString:@""])
        {
            email = @"NA";
        }
        
        if ([notes isEqualToString:@""])
        {
            notes = @"NA";
        }
        
        if ([registrationNo isEqualToString:@""])
        {
            registrationNo = @"NA";
        }
        
        if( addNewFarmView.switch1.isOn)
        {
            fType1 = @"True";
        }
        else
        {
            fType1 = @"False";
        }
        
        if( addNewFarmView.switch2.isOn)
        {
            fType2 = @"True";
        }
        else
        {
            fType2 = @"False";
        }
        
        if( addNewFarmView.switch3.isOn)
        {
            fType3 = @"True";
        }
        
        else
        {
            fType3 = @"False";
        }
        
        if( addNewFarmView.switch4.isOn)
        {
            fType4 = @"True";
        }
        else
        {
            fType4 = @"False";
        }
        
        NSString * FarmID = _farmID;
        
        
        NSDate *date =[NSDate networkDate];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSString * addedDateTime = [dateFormat stringFromDate:date];
        appDelegate.reloadCalendar=@"No";
        appDelegate.isLoadedNotifications=@"No";
        
        //**Update user Farm**//
        BovilisApi *obj1 =[BovilisApi sharedInstance];
        [obj1 editNewFarmDataToServerWithFarmName:FarmName withRegNO:registrationNo withRegion:region withContact:contactP withPhone:phone withNotes:notes withEmail:email withAnimalNumber:animalNumber withAddress:address withFarmType1:fType1 withUserName:userName withAddedDateTime:addedDateTime withFarmType2:fType2 withFarmType3:fType3 withFarmType4:fType4 withFarmID:FarmID WithSuccessBlock:^(BOOL success, NSDictionary *result) {
            
            if(success)
            {
                NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
                NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
                [userDefaults1 synchronize];
                
                BovilisApi *boviObj = [BovilisApi sharedInstance];
                [boviObj requestForGetUserFarmDataWithUserEmail:userEmail];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [activityView stopAnimating];
                    [activityView setHidesWhenStopped:YES];
                    NSString*mesg  = [appDelegate.dictCulture valueForKey:@"Farmupdated"];
                    if (mesg.length == 0) {
                        mesg = @"Form updated successfully.";
                    }
                    UIAlertController * active_alertClose = [UIAlertController alertControllerWithTitle:nil message:mesg preferredStyle:UIAlertControllerStyleAlert];
                    [self presentViewController:active_alertClose animated:YES completion:nil];
                    
                    _name =  self.lblName.text;
                    _registration = addNewFarmView.txtRegistration.text;
                    _reigon = addNewFarmView.txtReigon.text;
                    _ownerName = addNewFarmView.txtContactPerson.text;
                    _address =  addNewFarmView.txtAddress.text;
                    _phone=addNewFarmView.txtPhone.text;
                    _comments =addNewFarmView.txtComment.text;
                    _email =addNewFarmView.txtEmail.text;
                    _numberOfAnimals = addNewFarmView.txtAnimals.text;
                    [self performSelector:@selector(reloadDataInNewFarm) withObject:nil afterDelay:0];
                    
                });
            }
            
        } failure:^(NSError *error, NSString *message) {
            
        }];
        
    }
}

-(BOOL)checkFarmIDIsExistence:(NSString *)animalID {
    
    for (UserFarmMasterModel *fObj in appDelegate.getUserFarmsFromServer) {
        if ([animalID isEqualToString:fObj.farmName]){
            return false;
        }
    }
    return true;
}
-(void)reloadDataInNewFarm {
    
    appDelegate.reloadCalendar =@"No";
    _lblName.text =addNewFarmView.txtFarmName.text;
    addNewFarmView.txtRegistration.text=addNewFarmView.txtRegistration.text;
    _lblReigon.text=addNewFarmView.txtReigon.text;
    
    _lblOwnerName.text= addNewFarmView.txtContactPerson.text;
    _lblAddres.text= addNewFarmView.txtAddress.text;
    _lblPhone.text=addNewFarmView.txtPhone.text;
    _tView.text=addNewFarmView.txtComment.text;
    _lblEmail.text=addNewFarmView.txtEmail.text;
    
    NSString *milkCow =[appDelegate.dictCulture valueForKey:@"Dairy"];
    milkCow=  milkCow.length == 0 ?  @"Dairy": milkCow;
    
    NSString *beafSteak = [appDelegate.dictCulture valueForKey:@"BeefSlashVeal"];
    beafSteak=  beafSteak.length == 0 ?  @"Beaf / Veal": beafSteak;
    
    NSString *cebadero =[appDelegate.dictCulture valueForKey:@"CowCalf"];
    cebadero=  cebadero.length == 0 ?  @"Cow - Calf": cebadero;
    
    NSString *mixta =[appDelegate.dictCulture valueForKey:@"Mixed"];
    mixta=  mixta.length == 0 ?  @"Mixed": mixta;
    NSString *homeText=@"";
    
    _farmType1 =fType1;
    _farmType2 =fType2;
    _farmType3 =fType3;
    _farmType4 =fType4;
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init]; ;
    
    if([_farmType1 isEqualToString:@"True"])
    {
        [tempArray addObject:milkCow];
    }
    if([_farmType2 isEqualToString:@"True"])
    {
        [tempArray addObject:beafSteak];
    }
    if([_farmType3 isEqualToString:@"True"])
    {
        [tempArray addObject:cebadero];
    }
    
    if([_farmType4 isEqualToString:@"True"])
    {
        [tempArray addObject:mixta];
    }
    NSString *str;
    for (int i = 0; i<tempArray.count; i++) {
        
        if (i==0) {
            homeText = [tempArray objectAtIndex:i];
            
        }
        else{
            
            homeText =[NSString stringWithFormat:@"%@, %@",homeText, [tempArray objectAtIndex:i]];
            
        }
        
    }
    
    _lblHome.text=homeText;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [transparentView removeFromSuperview];
    
}

#pragma mark === UIButton Actions ===

-(IBAction)viewAnimalsButtonPressed:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"MyFarms_Tab_From_Home"
                                                           label:@"View_Animal"
                                                           value:nil] build]];
    AnimalsViewController *animalVc= [AnimalsViewController new];
    animalVc.isFrom=@"Listing";
    animalVc.farmID=_farmID;
    [self.navigationController pushViewController:animalVc animated:YES];
    
}
-(IBAction)updateButtonPressed:(id)sender
{
    [self showUpdateFarmView];
}

-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
