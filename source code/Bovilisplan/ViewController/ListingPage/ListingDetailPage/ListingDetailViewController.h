//
//  ListingDetailViewController.h
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListingDetailViewController : UIViewController

@property(strong,nonatomic) NSString *gen_id;
@property(strong,nonatomic) NSString *name;
@property(strong,nonatomic) NSString *reigon;
@property(strong,nonatomic) NSString *ownerName;
@property(strong,nonatomic) NSString *address;
@property(strong,nonatomic) NSString *phone;
@property(strong,nonatomic) NSString *email;
@property(strong,nonatomic) NSString *comments;
@property(strong,nonatomic) NSString *numberOfAnimals;
@property(strong,nonatomic) NSString *farmType1;
@property(strong,nonatomic) NSString *farmType2;
@property(strong,nonatomic) NSString *farmType3;
@property(strong,nonatomic) NSString *farmType4;
@property(strong,nonatomic) NSString *registration;
@property(strong,nonatomic) NSString *farmID;
@property(strong,nonatomic) NSString *reloadData;

@end
