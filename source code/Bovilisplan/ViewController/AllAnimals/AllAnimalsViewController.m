//
//  AllAnimalsViewController.m
//  TTVApp
//
//  Created by apple on 18/12/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import "AllAnimalsViewController.h"
#import "AppDelegate.h"
#import "UserFarmMasterModel.h"
#import "AnimalMaster.h"
#import "BovilisApi.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "AnimalsCell.h"
#import "ViewAnimalViewController.h"
#import "AddAnimalsViewController.h"
#import "AddAnimalsViewController.h"

@interface AllAnimalsViewController () <UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate,UITableViewDataSource, AddAnimalsViewControllerDelegate>
@property(strong,nonatomic) UIPickerView *pickerView;
@property(strong,nonatomic)  UIToolbar *toolBar;
@property(strong,nonatomic)IBOutlet UITextField *lblFamrname;
@property(strong,nonatomic)IBOutlet UIView *contentView;
@property(strong,nonatomic)IBOutlet UITableView *tableView;


@end

@implementation AllAnimalsViewController {
    AppDelegate *appDelegate ;
    
    NSString* selectedfarmID;
    AnimalMaster *animalObj;
     NSString * message, *cancelText,*proceedText ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
}

-(void)reloadAnimal:(NSString *)status {
    
    if ([status isEqualToString:@"Yes"]) {
        [self loadAnimalsWithFarmID:selectedfarmID];

    }
    
}

-(void)setUI {
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
 
    [self showPickview];
    [self hidePickerview];
    [self configureTableViewCell];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    UserFarmMasterModel * obj  =[appDelegate.getUserFarmsFromServer objectAtIndex:0];
    selectedfarmID = obj.farmID;
    [self loadAnimalsWithFarmID:selectedfarmID];
    
        message = [Utility getTranslationkey:@"DeleteAnimalText" witchLanguageDict:appDelegate.dictCulture];
    
        message = message.length == 0 ? @"Click “PROCEED” to delete the animal, OR “NO” to CANCEL." : message;

       cancelText = [Utility getTranslationkey:@"No" witchLanguageDict:appDelegate.dictCulture];
       cancelText = cancelText.length == 0 ? @"NO" : cancelText;

       proceedText = [Utility getTranslationkey:@"Proceed" witchLanguageDict:appDelegate.dictCulture];
  
        proceedText = proceedText.length == 0 ? @"PROCEED" : proceedText;

}

-(void)viewWillAppear:(BOOL)animated {
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [self.tableView reloadData];
}

-(void)loadAnimalsWithFarmID:(NSString *)farmiD {
    
    BovilisApi *boviObj=[BovilisApi sharedInstance];
    
    [boviObj getAnimalsDataWithID:farmiD  WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        if(success)
        {
            AnimalMaster * obj = [[AnimalMaster alloc] init];
            [obj getAnimals];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            });
        }
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    }];
}

#pragma mark === Configure TableView Cell===
-(void)configureTableViewCell
{
    [_tableView registerNib:[UINib nibWithNibName:@"AnimalsCell"
                                           bundle:nil] forCellReuseIdentifier:@"AnimalsCell"];
}

#pragma mark === TableView Delegates/Datasource===

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return appDelegate.animalsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = @"AnimalsCell";
    AnimalsCell *cell = (AnimalsCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    AnimalMaster *aniObj =[appDelegate.animalsArray objectAtIndex:indexPath.row];
    NSString *title =[NSString stringWithFormat:@"ID:%@",aniObj.animalNumber ];
    
    
    NSString * gen = [Utility getTranslationkey:@"Gender" witchLanguageDict:appDelegate.dictCulture];

    gen= gen.length == 0 ? @"Gender" : gen;
  
    NSString * bd = [Utility getTranslationkey:@"BirthDate" witchLanguageDict:appDelegate.dictCulture];
     bd =  bd.length == 0 ? @"Birth Date" : bd;

    cell.lbl1.text=title;
    cell.lbl2.text=  [NSString stringWithFormat:@"%@:%@",bd,aniObj.birthDate];
    

     NSString *gender= [appDelegate.dictCulture valueForKey:aniObj.gender];
    if (gender.length ==0) {
        gender = aniObj.gender;
    }
   
    cell.lbl3.text= [NSString stringWithFormat:@"%@:%@",gen,gender];
 
    cell.textLabel.font = [UIFont fontWithName:@"UNIVERS" size:13.0] ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
 
    return  cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
        AnimalMaster *Obj =[appDelegate.animalsArray objectAtIndex:indexPath.row];

        ViewAnimalViewController *vc = [ViewAnimalViewController new];
        vc.animalObj = Obj;
        
        [self.navigationController pushViewController:vc animated:YES];

}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
        
        NSString *editText = [Utility getTranslationkey:@"Edit" witchLanguageDict:appDelegate.dictCulture];
           
        editText  = editText.length == 0 ? @"Edit" :editText;
        
      
       UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:editText handler:^(UITableViewRowAction *action, NSIndexPath *indexxPath){
              AnimalMaster *Obj =[appDelegate.animalsArray objectAtIndex:indexPath.row];
          //insert your editAction here
              AddAnimalsViewController *vc = [AddAnimalsViewController new];
           vc.isFfrom =  @"Edit";
           vc.animalObj = Obj;
            
            [self.navigationController pushViewController:vc animated:YES];
       }];
        NSString *deleteText = [Utility getTranslationkey:@"Delete" witchLanguageDict:appDelegate.dictCulture];

           
        deleteText  = deleteText.length == 0 ? @"Delete" :deleteText;
    editAction.backgroundColor = [UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1];

       UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:deleteText handler:^(UITableViewRowAction *action, NSIndexPath *indexxPath){
          //insert your deleteAction here
           
           AnimalMaster *Obj =[appDelegate.animalsArray objectAtIndex:indexPath.row];
                animalObj =Obj;
                [self deleteAnimalFromServer];
       }];
       deleteAction.backgroundColor = [UIColor redColor];
        return @[deleteAction,editAction];

}
-(void)deleteAnimalFromServer {
    
   UIAlertController* delete_alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelText style:UIAlertActionStyleDefault handler:nil];
    [delete_alert addAction:cancel];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:proceedText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteAnimal];
    }];
    [delete_alert addAction:ok];
    [self presentViewController:delete_alert animated:YES completion:nil];
}

-(void)deleteAnimal {
   
    BovilisApi *obj1 = [BovilisApi sharedInstance];
    [appDelegate.animalsArray removeObject:animalObj];
    message = [Utility getTranslationkey:@"AnimalDeletedSuccessfully" witchLanguageDict:appDelegate.dictCulture];
    
    message = message.length == 0 ? @"Animal Deleted Successfully" : message;
    cancelText =  [Utility getTranslationkey:@"Ok" witchLanguageDict:appDelegate.dictCulture];
    cancelText = cancelText.length == 0 ? @"OK" : cancelText;


    [obj1 deletenimalFromServerWithAnimalID:animalObj.animalNumber ViewController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        
        if (success) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertController* delete_alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction *ok = [UIAlertAction actionWithTitle:proceedText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                  [self.tableView  reloadData];

              }];
              [delete_alert addAction:ok];
              [self presentViewController:delete_alert animated:YES completion:nil];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)showPickview {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    _pickerView = [[UIPickerView alloc]initWithFrame:
                   CGRectMake(0,  screenHeight - 200, screenWidth
                              , 200)];
    
    _pickerView.dataSource = self;
    _pickerView.delegate = self;
    _pickerView.showsSelectionIndicator = YES;
    NSString *doneText = [Utility getTranslationkey:@"Done" witchLanguageDict:appDelegate.dictCulture];
    
    doneText = doneText.length == 0 ? @"Done" : doneText;

    _pickerView.backgroundColor = [UIColor lightGrayColor];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:doneText style:UIBarButtonItemStyleDone
                                   target:self action:@selector(done:)];
    
    _toolBar = [[UIToolbar alloc]initWithFrame:
                CGRectMake(0, screenHeight - 250, screenWidth, 50)];
    [_toolBar setBarStyle:UIBarStyleBlackOpaque];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [_toolBar setItems:toolbarItems];
    
    [self.view addSubview:_toolBar];
    [self.view addSubview:_pickerView];
}

-(void)hidePickerview {
    
    [_pickerView setHidden:YES];
    [_toolBar setHidden:YES];
}

#pragma mark === PickerView Delegates/Datasource===
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [appDelegate.getUserFarmsFromServer count];
  
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
   
    UserFarmMasterModel * obj  = [[UserFarmMasterModel alloc] init];
        obj =[appDelegate.getUserFarmsFromServer objectAtIndex:row];
        NSString * titleForRow = obj.farmName;
      //  _txtFarmName.text =  titleForRow;
        [appDelegate.animalsSelectedArray removeAllObjects];

}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *pickerLabel = (UILabel *)view;
    CGRect frame = CGRectMake(0,0,265,40);
    pickerLabel = [[UILabel alloc] initWithFrame:frame];
    pickerLabel.textAlignment = NSTextAlignmentCenter;;
    [pickerLabel setBackgroundColor:[UIColor clearColor]];
    [pickerLabel setFont:[UIFont fontWithName:@"Univers" size:14]];
    [pickerLabel setNumberOfLines:0];
    
 
        UserFarmMasterModel * obj  = [[UserFarmMasterModel alloc] init];
        obj =[appDelegate.getUserFarmsFromServer objectAtIndex:row];
        NSString * titleForRow = obj.farmName;
        self.lblFamrname.text = titleForRow;
        selectedfarmID = obj.farmID;
        [pickerLabel setText:titleForRow];
        return pickerLabel;
     
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
  
        UserFarmMasterModel * obj  = [[UserFarmMasterModel alloc] init];
        obj =[appDelegate.getUserFarmsFromServer objectAtIndex:row];
        NSString * titleForRow = obj.farmName;
        return titleForRow ;
   
}

#pragma mark ===UIButton Action Methods ===

-(IBAction)addAnimalButtonTapped:(id)sender {
    
    AddAnimalsViewController *vc = [AddAnimalsViewController new];
    vc.farmID =  selectedfarmID;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)farmButtonPressed:(id)sender {
    
    [_pickerView setHidden:NO];
    [_toolBar setHidden:NO];
    [_pickerView reloadAllComponents];
    
}

-(IBAction)done:(id)sender {
    
    [appDelegate.animalsArray removeAllObjects];

    [self hidePickerview];
    [MBProgressHUD showHUDAddedTo:self.view animated:true];

    [self loadAnimalsWithFarmID:selectedfarmID];
}

-(IBAction)backButtonPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
