//
//  MainViewController.m
//  Bovilisplan
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//


#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "MainViewController.h"
#import "AppDelegate.h"
#import "Utility.h"

@interface MainViewController ()
{
    AppDelegate * appDelegate;
}

@property(weak,nonatomic) IBOutlet UIButton *btnLogin;
@property(weak,nonatomic) IBOutlet UIButton *btnRegister;
@property(weak,nonatomic) IBOutlet UILabel *lblOne;
@property(weak,nonatomic) IBOutlet UILabel *lblTwo;
@property(weak,nonatomic) IBOutlet UIImageView *imgView;
@property(weak,nonatomic) IBOutlet UILabel *lblVersionNumb;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUI];
}

-(void)setUI
{
    
   
    

    appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
   // self.imgView.image = [UIImage imageNamed:[Utility setLocalizeBackground]];
        self.imgView.image = [UIImage imageNamed:[Utility setLocalizeLogo:@"background"]];
    _btnLogin.layer.borderWidth = 1.0f;
    [_btnLogin.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    _btnRegister.layer.borderWidth = 1.0f;
    [_btnRegister.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    self.lblVersionNumb.text = [appDelegate.dictCulture valueForKey:@"Version10"];
    [_btnLogin setTitle:[appDelegate.dictCulture valueForKey:@"Login"] forState:UIControlStateNormal];
    NSString * btnLogi = [appDelegate.dictCulture valueForKey:@"Login"];
    
    if (btnLogi.length==0)
    {
        [_btnLogin setTitle:@"LOGIN" forState:UIControlStateNormal];
    }
    
    [_btnRegister setTitle:[appDelegate.dictCulture valueForKey:@"Register"] forState:UIControlStateNormal];
    NSString * btnRegister = [appDelegate.dictCulture valueForKey:@"Register"];
    if (btnRegister.length==0)
    {
        [_btnRegister setTitle:@"REGISTER" forState:UIControlStateNormal];
    }
    
    [_lblOne setText:[appDelegate.dictCulture valueForKey:@"TimeToVaccinate"]];
    NSString * lblOne = [appDelegate.dictCulture valueForKey:@"TimeToVaccinate"];
    if (lblOne.length==0)
    {
        [_lblOne setText:@"Time To Vaccinate"];
    }
    
    [_lblTwo setText:[appDelegate.dictCulture valueForKey:@"DigitalVaccinationPlanningToolForRuminants"]];
    NSString * lblTwo = [appDelegate.dictCulture valueForKey:@"DigitalVaccinationPlanningToolForRuminants"];
    if (lblTwo.length==0)
    {
        [_lblTwo setText:@"Digital Vaccination Planning Tool For Ruminants"];
    }
  
}


-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

#pragma mark === UIButton Actions ====
-(IBAction)loginbuttonPressed:(id)sender
{
    LoginViewController*detailVc= [LoginViewController new];
    [self.navigationController pushViewController:detailVc animated:YES];
}

-(IBAction)registerBnuttonPressed:(id)sender
{
    RegisterViewController*detailVc= [RegisterViewController new];
    [self.navigationController pushViewController:detailVc animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
