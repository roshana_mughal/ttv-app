//
//  ForgotViewController.m
//  Bovilisplan
//
//  Created by Admin on 5/8/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotViewController.h"
#import "AppDelegate.h"

@interface ForgotViewController ()<UITextFieldDelegate,UIWebViewDelegate>
{
    NSString *html;
    NSURL *requestURL ;
    AppDelegate * appDelegate;
    NSString *urlString;
    UIAlertController *active_alert;
}

@property(strong,nonatomic) IBOutlet UITextField *txtEmail;
@property(strong,nonatomic) UIWebView *webView;
@property(nonatomic,strong) IBOutlet UIView*forgotView;
@property(strong,nonatomic) IBOutlet UIButton *btnCancel;
@property(strong,nonatomic) IBOutlet UIButton *btnRequest;
@property(strong,nonatomic) IBOutlet UILabel *lblForgot;
@property(strong,nonatomic) IBOutlet UILabel *lblOne;
@property(strong,nonatomic) IBOutlet UILabel *lblTwo;
@property(weak,nonatomic) IBOutlet UIImageView *imgView;

@property(strong,nonatomic) IBOutlet UILabel *lblErrorMessage;

@end

@implementation ForgotViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUI];
}

#pragma mark ==== SetUI  ====

-(void)setUI
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.imgView.image = [UIImage imageNamed:[Utility setLocalizeLogo:@"background"]];

    self.forgotView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.forgotView.layer.borderWidth = 1.0f;
    self.forgotView.clipsToBounds = YES;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtEmail.leftView = paddingView;
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    _txtEmail.layer.borderWidth = 1.0;
    _txtEmail.layer.borderColor = [UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1].CGColor;
    _txtEmail.clearsOnBeginEditing = NO;
    
    _btnRequest.layer.borderWidth = 1.0f;
    [_btnRequest.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    _btnCancel.layer.borderWidth = 1.0f;
    [_btnCancel.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnCancel setTitle:[appDelegate.dictCulture valueForKey:@"Cancel"] forState:UIControlStateNormal];
    NSString * btnCancel =[appDelegate.dictCulture valueForKey:@"Cancel"];
    if (btnCancel.length==0)
    {
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    NSString *erroMSg = [appDelegate.dictCulture valueForKey:@"Dontworrysimplyenteryourregisteredemailtorequestanewone."];
    
    self.lblErrorMessage.text = erroMSg.length == 0 ? @"Don't worry! simply enter your registered email to request a new one." :erroMSg;
    [_btnRequest setTitle:[appDelegate.dictCulture valueForKey:@"Request"] forState:UIControlStateNormal];
    NSString * btnRequest =[appDelegate.dictCulture valueForKey:@"Request"];
    if (btnRequest.length==0)
    {
        [_btnRequest setTitle:@"Request" forState:UIControlStateNormal];
    }
    
    [_lblForgot setText:[appDelegate.dictCulture valueForKey:@"ForgotPassword"]];
    NSString * lblForgot =[appDelegate.dictCulture valueForKey:@"ForgotPassword"];
    if (lblForgot.length==0)
    {
        [_lblForgot setText:@"Forgot Password?"];
    }
    
    [_lblOne setText:[appDelegate.dictCulture valueForKey:@"DidYouForgotYourPassword"]];
    NSString * lblOne =[appDelegate.dictCulture valueForKey:@"DidYouForgotYourPassword"];
    if (lblOne.length==0)
    {
        [_lblOne setText:@"Did you forgot your password?"];
    }

    NSString * textTwo =[appDelegate.dictCulture valueForKey:@"DontWorrySimplyEnterYourRegisteredEmailToRequestANewOne"];
    textTwo = textTwo.length == 0 ? @"Please enter your email address to request a new password.":textTwo;
    [_lblTwo setText:textTwo];
    
    _txtEmail.placeholder =[appDelegate.dictCulture valueForKey:@"EmailAddress"];
    NSString * txtEmail =[appDelegate.dictCulture valueForKey:@"EmailAddress"];
    if (txtEmail.length==0)
    {
        _txtEmail.placeholder = @"Email Address";
    }
}

-(void)loadForgotRequest
{
    [self.webView removeFromSuperview];
    self.webView = [[UIWebView alloc]init];
    self.webView.delegate=self;
    [self.webView removeFromSuperview];
    
    urlString= @"https://secure.merck-animal-health.com/LFW20/Pages/RequestPassword.asp?SiteID=573&st=SADDCSDAEENJEHCDKILEDBKOCOPAAHOG";
    NSString *urlRegEx =
    @"(?i)(http|https)(:\\/\\/)([^ .]+)(\\.)([^ \n]+)";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    BOOL status  = [urlTest evaluateWithObject:urlString];
    
    if( [urlTest evaluateWithObject:urlString]){
        if ([[UIApplication sharedApplication]
             canOpenURL:[NSURL URLWithString:[NSString  stringWithFormat:@"%@",urlString]]]){
            
            requestURL = [NSURL URLWithString:urlString];
            NSError *error;
            NSString *page = [NSString stringWithContentsOfURL:requestURL
                                                      encoding:NSASCIIStringEncoding
                                                         error:&error];
            [self.webView loadHTMLString:page baseURL:requestURL];
        }
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString * emailValue = _txtEmail.text;
    
    if (![Utility NSStringIsValidEmail:emailValue])
    {
        
        NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
        NSString*message= [appDelegate.dictCulture valueForKey:@"PleaseEnterAValidEmailAddress"];
        NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if (title.length==0)
        {
            title=@"Alert";
        }
        
        if (message.length==0)
        {
            message =@"Please enter a valid email address";
        }
        
        if (okText.length==0)
        {
            
            okText =@"Ok";
        }
        
        UIAlertController*  Empty_Error = [UIAlertController alertControllerWithTitle: title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
        
        [Empty_Error addAction:ok];
        
        [self presentViewController:Empty_Error animated:YES completion:nil];
    }

    else
    {
    
    NSString *javaScript = [NSString stringWithFormat:@"var textField = document.getElementById('lfw20VALID_EMAIL34698').value = '%@';",emailValue];
    [self.webView stringByEvaluatingJavaScriptFromString:javaScript];
    NSString *jsStat = @"document.forms[0].submit()";
    [webView stringByEvaluatingJavaScriptFromString:jsStat];
    html = [self.webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('html')[0].innerHTML"];
    
    NSString  * doc = [self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if([doc containsString:@"Thank you"])
    {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        self.webView=nil;
        self.webView.delegate=nil;
        
        NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
        NSString*message= [appDelegate.dictCulture valueForKey:@"ForgotPasswordText"];
        NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        if (title.length==0)
        {
            title=@"Alert";
        }
        if (message.length == 0)
        {
            message =@"Thank you. You will receive further instructions by email how to renew your password. If you do not receive this email, you either entered an incorrect email address or you are not the owner of that email address. You may also want to check your email program's spam folder.";
        }
        
        if (okText.length==0)
        {
            okText =@"Ok";
        }
        
        UIAlertController*  alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self stopLoading];
        }];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
        
    }
}

-(void)stopLoading
{
    LoginViewController *detailVc= [LoginViewController new];
    [self.navigationController pushViewController:detailVc animated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *URLString = [[request URL] absoluteString];
    if ([URLString hasPrefix:@"http"])
    {
    return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark=====UIButton Action Methods====
-(IBAction)requestButtonPressed:(id)sender
{
    if(_txtEmail.text.length>0)
    {
        if (![Utility NSStringIsValidEmail:_txtEmail.text])
        {
            
            NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
            NSString*message= [appDelegate.dictCulture valueForKey:@"PleaseEnterAValidEmailAddress"];
            NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            
            if (title.length==0)
            {
                title=@"Alert";
            }
            
            if (message.length==0)
            {
                message =@"Please enter a valid email address";
            }
            
            if (okText.length==0)
            {
                
                okText =@"Ok";
            }
            
            UIAlertController*  Empty_Error = [UIAlertController alertControllerWithTitle: title message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            
            [Empty_Error addAction:ok];
            
            [self presentViewController:Empty_Error animated:YES completion:nil];
        }
        
        else{
            
            active_alert = [UIAlertController alertControllerWithTitle:nil message:[appDelegate.dictCulture valueForKey:@"RequestingPleaseWait"] preferredStyle:UIAlertControllerStyleAlert];
            [self presentViewController:active_alert animated:YES completion:nil];
            [self performSelector:@selector(loadForgotRequest) withObject:nil afterDelay:0];
            
        }
    }
    else
    {
        UIAlertController*  alert = [UIAlertController alertControllerWithTitle:[appDelegate.dictCulture valueForKey:@"Alert"]
                                                                        message:[appDelegate.dictCulture valueForKey:@"PleaseProvideAllTheRequiredInformation"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[appDelegate.dictCulture valueForKey:@"Ok"]  style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

-(IBAction)cancelBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
