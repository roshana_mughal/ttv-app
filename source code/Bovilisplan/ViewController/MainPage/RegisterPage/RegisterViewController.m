//
//  RegisterViewController.m
//  Bovilisplan
//
//  Created by Admin on 5/8/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//


#import "MainViewController.h"
#import "RegisterViewController.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "WelcomeDetailViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "LoginViewController.h"

@import Firebase;

@interface RegisterViewController ()<UITextFieldDelegate,UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIWebViewDelegate>

@property(strong,nonatomic) IBOutlet UITextField *Name;
@property(strong,nonatomic) IBOutlet UITextField *surName;
@property(strong,nonatomic) IBOutlet UITextField *txtEmail;
@property(strong,nonatomic) IBOutlet UITextField *txtCountry;
@property(strong,nonatomic) IBOutlet UITextField *txtPassword;
@property(strong,nonatomic) IBOutlet UITextField *txtRePassword;
@property(strong,nonatomic) IBOutlet UIButton *btnCancel;
@property(strong,nonatomic) IBOutlet UIButton *btnRegister;
@property(nonatomic,strong) IBOutlet UIImageView *imgRememberMe;
@property(nonatomic,strong) IBOutlet UIButton *btnRememberMe;
@property(nonatomic,strong) IBOutlet UIView*regiserView;
@property(nonatomic,strong) IBOutlet UITextView *txtVTerms;
@property(nonatomic,strong) IBOutlet UILabel *lblTop;
@property(strong,nonatomic) IBOutlet UIWebView *webView;
@property(weak,nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation RegisterViewController
{
    UIPickerView *myPickerView;
    NSMutableArray *pickerArray;
    NSString *urlString;
    NSURL *requestURL ;
    NSString *html;
    AppDelegate * appDelegate;
    UIAlertController*active_alert;
    NSDictionary *country;
    NSMutableDictionary *countryDic;
    NSMutableArray *countryArray;
    NSMutableArray *countyIDArray;
    NSString *isValideEmail;
    NSString *isValidPassword;
    NSString *isPrivacyChecked;
    NSString *isPasswordSame;
    NSString *countryKey, *selectedCountry;
    UIActivityIndicatorView *activityView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imgView.image = [UIImage imageNamed:[Utility setLocalizeLogo:@"background"]];
    [self setUI];
    [self addPickerView];
}

-(void)addPickerView
{
    isPrivacyChecked=@"No";
    
    selectedCountry =@"Afghanistan";
    countryArray =[[NSMutableArray alloc]init];
    
    [ countryArray addObject:@"Afghanistan"];
    [ countryArray addObject:@"Aland Islands"];
    [ countryArray addObject:@"Albania"];
    [ countryArray addObject:@"Algeria"];
    [ countryArray addObject:@"American Samoa"];
    [ countryArray addObject:@"Andorra"];
    [ countryArray addObject:@"Angola"];
    [ countryArray addObject:@"Anguilla"];
    [ countryArray addObject:@"Antarctica"];
    [ countryArray addObject:@"Antigua and Barbud"];
    [ countryArray addObject:@"Argentina"];
    [ countryArray addObject:@"Armenia"];
    [ countryArray addObject:@"Aruba"];
    [ countryArray addObject:@"Australia"];
    [ countryArray addObject:@"Austria"];
    [ countryArray addObject:@"Azerbaijan"];
    [ countryArray addObject:@"Bahamas"];
    [ countryArray addObject:@"Bahrain"];
    [ countryArray addObject:@"Bangladesh"];
    [ countryArray addObject:@"Barbados"];
    [ countryArray addObject:@"Belarus"];
    [ countryArray addObject:@"Belgium"];
    [ countryArray addObject:@"Belize"];
    [ countryArray addObject:@"Benin"];
    [ countryArray addObject:@"Bermuda"];
    [ countryArray addObject:@"Bhutan"];
    [ countryArray addObject:@"Bolivia"];
    [ countryArray addObject:@"Bosnia and Herzegovina"];
    [ countryArray addObject:@"Botswana"];
    [ countryArray addObject:@"Bouvet Island"];
    [ countryArray addObject:@"Brazil"];
    [ countryArray addObject:@"British Indian Ocean Territory"];
    [ countryArray addObject:@"Brunei Darussalam"];
    [ countryArray addObject:@"Bulgaria"];
    [ countryArray addObject:@"Burkina Faso"];
    [ countryArray addObject:@"Burundi"];
    [ countryArray addObject:@"Cambodia"];
    [ countryArray addObject:@"Cameroon"];
    [ countryArray addObject:@"Canada"];
    [ countryArray addObject:@"Cape Verde"];
    [ countryArray addObject:@"Cayman Islands"];
    [ countryArray addObject:@"Central African Republic"];
    [ countryArray addObject:@"Chad"];
    [ countryArray addObject:@"Chile"];
    [ countryArray addObject:@"China"];
    [ countryArray addObject:@"Christmas Island"];
    [ countryArray addObject:@"Cocos (keeling) Islands"];
    [ countryArray addObject:@"Colombia"];
    [ countryArray addObject:@"Comoros"];
    [ countryArray addObject:@"Congo"];
    [ countryArray addObject:@"Congo, The Democratic Republic of the"];
    [ countryArray addObject:@"Cook Islands"];
    [ countryArray addObject:@"Costa Rica"];
    [ countryArray addObject:@"Côte d'Ivoire"];
    [ countryArray addObject:@"Croatia"];
    [ countryArray addObject:@"Cuba"];
    [ countryArray addObject:@"Cyprus"];
    [ countryArray addObject:@"Czech Republic"];
    [ countryArray addObject:@"Denmark"];
    [ countryArray addObject:@"Djibouti"];
    [ countryArray addObject:@"Dominica"];
    [ countryArray addObject:@"Dominican Republic"];
    [ countryArray addObject:@"Ecuador"];
    [ countryArray addObject:@"Egypt"];
    [ countryArray addObject:@"El Salvador"];
    [ countryArray addObject:@"Equatorial Guinea"];
    [ countryArray addObject:@"Eritrea"];
    [ countryArray addObject:@"Estonia"];
    [ countryArray addObject:@"Ethiopia"];
    [ countryArray addObject:@"Falkland Islands (Malvinas)"];
    [ countryArray addObject:@"Faroe Islands"];
    [ countryArray addObject:@"Fiji"];
    [ countryArray addObject:@"Finland"];
    [ countryArray addObject:@"France"];
    [ countryArray addObject:@"French Guiana"];
    [ countryArray addObject:@"French Polynesia"];
    [ countryArray addObject:@"French Southern Territories"];
    [ countryArray addObject:@"Gabon"];
    [ countryArray addObject:@"Gambia"];
    [ countryArray addObject:@"Georgia"];
    [ countryArray addObject:@"Germany"];
    [ countryArray addObject:@"Ghana"];
    [ countryArray addObject:@"Gibraltar"];
    [ countryArray addObject:@"Greece"];
    [ countryArray addObject:@"Greenland"];
    [ countryArray addObject:@"Grenada"];
    [ countryArray addObject:@"Guadeloupe"];
    [ countryArray addObject:@"Guam"];
    [ countryArray addObject:@"Guatemala"];
    [ countryArray addObject:@"Guinea"];
    [ countryArray addObject:@"Guinea-Bissau"];
    [ countryArray addObject:@"Guyana"];
    [ countryArray addObject:@"Haiti"];
    [ countryArray addObject:@"Heard Island and Mcdonald Islands"];
    [ countryArray addObject:@"Holy See (Vatican City State)"];
    [ countryArray addObject:@"Honduras"];
    [ countryArray addObject:@"Hong kong"];
    [ countryArray addObject:@"Hungary"];
    [ countryArray addObject:@"Iceland"];
    [ countryArray addObject:@"India"];
    [ countryArray addObject:@"Indonesia"];
    [ countryArray addObject:@"Iran, Islamic Republic of"];
    [ countryArray addObject:@"Iraq"];
    [ countryArray addObject:@"Ireland"];
    [ countryArray addObject:@"Israel"];
    [ countryArray addObject:@"Italy"];
    [ countryArray addObject:@"Jamaica"];
    [ countryArray addObject:@"Japan"];
    [ countryArray addObject:@"Jordan"];
    [ countryArray addObject:@"Kazakhstan"];
    [ countryArray addObject:@"Kenya"];
    [ countryArray addObject:@"Kiribati"];
    [ countryArray addObject:@"Korea, Democratic People's Republic of"];
    [ countryArray addObject:@"Korea, Republic of"];
    [ countryArray addObject:@"Kuwait"];
    [ countryArray addObject:@"Kyrgyzstan"];
    [ countryArray addObject:@"Lao People's Democratic Republic"];
    [ countryArray addObject:@"Letvia"];
    [ countryArray addObject:@"Lebanon"];
    [ countryArray addObject:@"Lesotho"];
    [ countryArray addObject:@"Liberia"];
    [ countryArray addObject:@"Libyan Arab Jamahiriya"];
    [ countryArray addObject:@"Liechtenstein"];
    [ countryArray addObject:@"Lithuania"];
    [ countryArray addObject:@"Luxembourg"];
    [ countryArray addObject:@"Macao"];
    [ countryArray addObject:@"Macedonia, Former Yugoslav Republic of"];
    [ countryArray addObject:@"Madagascar"];
    [ countryArray addObject:@"Malawi"];
    [ countryArray addObject:@"Malaysia"];
    [ countryArray addObject:@"Maldives"];
    [ countryArray addObject:@"Mali"];
    [ countryArray addObject:@"Malta"];
    [ countryArray addObject:@"Marshall Islands"];
    [ countryArray addObject:@"Martinique"];
    [ countryArray addObject:@"Mauritania"];
    [ countryArray addObject:@"Mauritius"];
    [ countryArray addObject:@"Mayotte"];
    [ countryArray addObject:@"Mexico"];
    [ countryArray addObject:@"Micronesia, Federated States of"];
    [ countryArray addObject:@"Moldova"];
    [ countryArray addObject:@"Monaco"];
    [ countryArray addObject:@"Mongolia"];
    [ countryArray addObject:@"Montserrat"];
    [ countryArray addObject:@"Morocco"];
    [ countryArray addObject:@"Mozambique"];
    [ countryArray addObject:@"Myanmar"];
    [ countryArray addObject:@"Namibia"];
    [ countryArray addObject:@"Nauru"];
    [ countryArray addObject:@"Nepal"];
    [ countryArray addObject:@"Netherlands"];
    [ countryArray addObject:@"Netherlands Antilles"];
    [ countryArray addObject:@"New Caledonia"];
    [ countryArray addObject:@"New Zealand"];
    [ countryArray addObject:@"Nicaragua"];
    [ countryArray addObject:@"Niger"];
    [ countryArray addObject:@"Nigeria"];
    [ countryArray addObject:@"Niue"];
    [ countryArray addObject:@"Norfolk Island"];
    [ countryArray addObject:@"Northern Mariana Islands"];
    [ countryArray addObject:@"Norway"];
    [ countryArray addObject:@"Oman"];
    [ countryArray addObject:@"Pakistan"];
    [ countryArray addObject:@"Palau"];
    [ countryArray addObject:@"Palestinian Territory"];
    [ countryArray addObject:@"Panama"];
    [ countryArray addObject:@"Papua New Guinea"];
    [ countryArray addObject:@"Paraguay"];
    [ countryArray addObject:@"Peru"];
    [ countryArray addObject:@"Philippines"];
    [ countryArray addObject:@"Pitcairn"];
    [ countryArray addObject:@"Poland"];
    [ countryArray addObject:@"Portugal"];
    [ countryArray addObject:@"Puerto Rico"];
    [ countryArray addObject:@"Qatar"];
    [ countryArray addObject:@"Réunion"];
    [ countryArray addObject:@"Romania"];
    [ countryArray addObject:@"Russian Federation"];
    [ countryArray addObject:@"Rwanda"];
    [ countryArray addObject:@"Saint Helena"];
    [ countryArray addObject:@"Saint Kitts and Nevis"];
    [ countryArray addObject:@"Saint Lucia"];
    [ countryArray addObject:@"Saint Pierre and Miquelon"];
    [ countryArray addObject:@"Saint Vincent and the Grenadines"];
    [ countryArray addObject:@"Samoa"];
    [ countryArray addObject:@"San Marino"];
    [ countryArray addObject:@"Sao Tome and Principe"];
    [ countryArray addObject:@"Saudi Arabia"];
    [ countryArray addObject:@"Senegal"];
    [ countryArray addObject:@"Serbia"];
    [ countryArray addObject:@"Seychelles"];
    [ countryArray addObject:@"Sierra Leone"];
    [ countryArray addObject:@"Singapore"];
    [ countryArray addObject:@"Slovakia"];
    [ countryArray addObject:@"Slovenia"];
    [ countryArray addObject:@"Solomon Islands"];
    [ countryArray addObject:@"Somalia"];
    [ countryArray addObject:@"South Africa"];
    [ countryArray addObject:@"South Georgia and the South Sandwich Islands"];
    [ countryArray addObject:@"Spain"];
    [ countryArray addObject:@"Sri Lanka"];
    [ countryArray addObject:@"Sudan"];
    [ countryArray addObject:@"Suriname"];
    [ countryArray addObject:@"Svalbard and Jan Mayen"];
    [ countryArray addObject:@"Swaziland"];
    [ countryArray addObject:@"Sweden"];
    [ countryArray addObject:@"Switzerland"];
    [ countryArray addObject:@"Syrian Arab Republic"];
    [ countryArray addObject:@"Taiwan"];
    [ countryArray addObject:@"Tajikistan"];
    [ countryArray addObject:@"Tanzania, United Republic of"];
    [ countryArray addObject:@"Thailand"];
    [ countryArray addObject:@"Timor-Leste"];
    [ countryArray addObject:@"Togo"];
    [ countryArray addObject:@"Tokelau"];
    [ countryArray addObject:@"Tonga"];
    [ countryArray addObject:@"Trinidad and Tobago"];
    [ countryArray addObject:@"Tunisia"];
    [ countryArray addObject:@"Turkey"];
    [ countryArray addObject:@"Turkmenistan"];
    [ countryArray addObject:@"Turks and Caicos Islands"];
    [ countryArray addObject:@"Tuvalu"];
    [ countryArray addObject:@"Uganda"];
    [ countryArray addObject:@"Ukraine"];
    [ countryArray addObject:@"United Arab Emirates"];
    [ countryArray addObject:@"United Kingdom"];
    [ countryArray addObject:@"United States of America"];
    [ countryArray addObject:@"United States minor outlying islands"];
    [ countryArray addObject:@"Uruguay"];
    [ countryArray addObject:@"Uzbekistan"];
    [ countryArray addObject:@"Vanuatu"];
    [ countryArray addObject:@"Venezuela"];
    [ countryArray addObject:@"Vietnam"];
    [ countryArray addObject:@"Virgin Islands, British"];
    [ countryArray addObject:@"Virgin Islands, U.S."];
    [ countryArray addObject:@"Wallis and Futuna"];
    [ countryArray addObject:@"Western Sahara"];
    [ countryArray addObject:@"Yemen"];
    [ countryArray addObject:@"Zambia"];
    [ countryArray addObject:@"Zimbabwe"];

    countryKey =@"AF";
    countyIDArray =[[NSMutableArray alloc]init];
    [countyIDArray addObject:@"AF"];
    [countyIDArray addObject:@"AX"];
    [countyIDArray addObject:@"AL"];
    [countyIDArray addObject:@"DZ"];
    [countyIDArray addObject:@"AS"];
    [countyIDArray addObject:@"AD"];
    [countyIDArray addObject:@"AO"];
    [countyIDArray addObject:@"AI"];
    [countyIDArray addObject:@"AQ"];
    [countyIDArray addObject:@"AG"];
    [countyIDArray addObject:@"AR"];
    [countyIDArray addObject:@"AM"];
    [countyIDArray addObject:@"AW"];
    [countyIDArray addObject:@"AU"];
    [countyIDArray addObject:@"AT"];
    [countyIDArray addObject:@"AZ"];
    [countyIDArray addObject:@"BS"];
    [countyIDArray addObject:@"BH"];
    [countyIDArray addObject:@"BD"];
    [countyIDArray addObject:@"BB"];
    [countyIDArray addObject:@"BY"];
    [countyIDArray addObject:@"BE"];
    [countyIDArray addObject:@"BZ"];
    [countyIDArray addObject:@"BJ"];
    [countyIDArray addObject:@"BM"];
    [countyIDArray addObject:@"BT"];
    [countyIDArray addObject:@"BO"];
    [countyIDArray addObject:@"BA"];
    [countyIDArray addObject:@"BW"];
    [countyIDArray addObject:@"BV"];
    [countyIDArray addObject:@"BR"];
    [countyIDArray addObject:@"IO"];
    [countyIDArray addObject:@"BN"];
    [countyIDArray addObject:@"BG"];
    [countyIDArray addObject:@"BF"];
    [countyIDArray addObject:@"BI"];
    [countyIDArray addObject:@"KH"];
    [countyIDArray addObject:@"CM"];
    [countyIDArray addObject:@"CA"];
    [countyIDArray addObject:@"CV"];
    [countyIDArray addObject:@"KY"];
    [countyIDArray addObject:@"CF"];
    [countyIDArray addObject:@"TD"];
    [countyIDArray addObject:@"CL"];
    [countyIDArray addObject:@"CN"];
    [countyIDArray addObject:@"CX"];
    [countyIDArray addObject:@"CC"];
    [countyIDArray addObject:@"CO"];
    [countyIDArray addObject:@"KM"];
    [countyIDArray addObject:@"CG"];
    [countyIDArray addObject:@"CD"];
    [countyIDArray addObject:@"CK"];
    [countyIDArray addObject:@"CR"];
    [countyIDArray addObject:@"CI"];
    [countyIDArray addObject:@"HR"];
    [countyIDArray addObject:@"CU"];
    [countyIDArray addObject:@"CY"];
    [countyIDArray addObject:@"CZ"];
    [countyIDArray addObject:@"DK"];
    [countyIDArray addObject:@"DJ"];
    [countyIDArray addObject:@"DM"];
    [countyIDArray addObject:@"DO"];
    [countyIDArray addObject:@"EC"];
    [countyIDArray addObject:@"EG"];
    [countyIDArray addObject:@"SV"];
    [countyIDArray addObject:@"GQ"];
    [countyIDArray addObject:@"ER"];
    [countyIDArray addObject:@"EE"];
    [countyIDArray addObject:@"ET"];
    [countyIDArray addObject:@"FK"];
    [countyIDArray addObject:@"FO"];
    [countyIDArray addObject:@"FJ"];
    [countyIDArray addObject:@"FI"];
    [countyIDArray addObject:@"FR"];
    [countyIDArray addObject:@"GF"];
    [countyIDArray addObject:@"PF"];
    [countyIDArray addObject:@"TF"];
    [countyIDArray addObject:@"GA"];
    [countyIDArray addObject:@"GM"];
    [countyIDArray addObject:@"GE"];
    [countyIDArray addObject:@"DE"];
    [countyIDArray addObject:@"GH"];
    [countyIDArray addObject:@"GI"];
    [countyIDArray addObject:@"GR"];
    [countyIDArray addObject:@"GL"];
    [countyIDArray addObject:@"GD"];
    [countyIDArray addObject:@"GP"];
    [countyIDArray addObject:@"GU"];
    [countyIDArray addObject:@"GT"];
    [countyIDArray addObject:@"GN"];
    [countyIDArray addObject:@"GW"];
    [countyIDArray addObject:@"GY"];
    [countyIDArray addObject:@"HT"];
    [countyIDArray addObject:@"HM"];
    [countyIDArray addObject:@"VA"];
    [countyIDArray addObject:@"HN"];
    [countyIDArray addObject:@"HK"];
    [countyIDArray addObject:@"HU"];
    [countyIDArray addObject:@"IS"];
    [countyIDArray addObject:@"IN"];
    [countyIDArray addObject:@"ID"];
    [countyIDArray addObject:@"IR"];
    [countyIDArray addObject:@"IQ"];
    [countyIDArray addObject:@"IE"];
    [countyIDArray addObject:@"IL"];
    [countyIDArray addObject:@"IT"];
    [countyIDArray addObject:@"JM"];
    [countyIDArray addObject:@"JP"];
    [countyIDArray addObject:@"JO"];
    [countyIDArray addObject:@"KZ"];
    [countyIDArray addObject:@"KE"];
    [countyIDArray addObject:@"KI"];
    [countyIDArray addObject:@"KP"];
    [countyIDArray addObject:@"KR"];
    [countyIDArray addObject:@"KW"];
    [countyIDArray addObject:@"KG"];
    [countyIDArray addObject:@"LA"];
    [countyIDArray addObject:@"LV"];
    [countyIDArray addObject:@"LB"];
    [countyIDArray addObject:@"LS"];
    [countyIDArray addObject:@"LR"];
    [countyIDArray addObject:@"LY"];
    [countyIDArray addObject:@"LI"];
    [countyIDArray addObject:@"LT"];
    [countyIDArray addObject:@"LU"];
    [countyIDArray addObject:@"MO"];
    [countyIDArray addObject:@"MK"];
    [countyIDArray addObject:@"MG"];
    [countyIDArray addObject:@"MW"];
    [countyIDArray addObject:@"MY"];
    [countyIDArray addObject:@"MV"];
    [countyIDArray addObject:@"ML"];
    [countyIDArray addObject:@"MT"];
    [countyIDArray addObject:@"MH"];
    [countyIDArray addObject:@"MQ"];
    [countyIDArray addObject:@"MR"];
    [countyIDArray addObject:@"MU"];
    [countyIDArray addObject:@"YT"];
    [countyIDArray addObject:@"MX"];
    [countyIDArray addObject:@"FM"];
    [countyIDArray addObject:@"MD"];
    [countyIDArray addObject:@"MC"];
    [countyIDArray addObject:@"MN"];
    [countyIDArray addObject:@"MS"];
    [countyIDArray addObject:@"MA"];
    [countyIDArray addObject:@"MZ"];
    [countyIDArray addObject:@"MM"];
    [countyIDArray addObject:@"NA"];
    [countyIDArray addObject:@"NR"];
    [countyIDArray addObject:@"NP"];
    [countyIDArray addObject:@"NL"];
    [countyIDArray addObject:@"AN"];
    [countyIDArray addObject:@"NC"];
    [countyIDArray addObject:@"NZ"];
    [countyIDArray addObject:@"NI"];
    [countyIDArray addObject:@"NE"];
    [countyIDArray addObject:@"NG"];
    [countyIDArray addObject:@"NU"];
    [countyIDArray addObject:@"NF"];
    [countyIDArray addObject:@"MP"];
    [countyIDArray addObject:@"NO"];
    [countyIDArray addObject:@"OM"];
    [countyIDArray addObject:@"PK"];
    [countyIDArray addObject:@"PW"];
    [countyIDArray addObject:@"PS"];
    [countyIDArray addObject:@"PA"];
    [countyIDArray addObject:@"PG"];
    [countyIDArray addObject:@"PY"];
    [countyIDArray addObject:@"PE"];
    [countyIDArray addObject:@"PH"];
    [countyIDArray addObject:@"PN"];
    [countyIDArray addObject:@"PL"];
    [countyIDArray addObject:@"PT"];
    [countyIDArray addObject:@"PR"];
    [countyIDArray addObject:@"QA"];
    [countyIDArray addObject:@"RE"];
    [countyIDArray addObject:@"RO"];
    [countyIDArray addObject:@"RU"];
    [countyIDArray addObject:@"RW"];
    [countyIDArray addObject:@"SH"];
    [countyIDArray addObject:@"KN"];
    [countyIDArray addObject:@"LC"];
    [countyIDArray addObject:@"PM"];
    [countyIDArray addObject:@"VC"];
    [countyIDArray addObject:@"WS"];
    [countyIDArray addObject:@"SM"];
    [countyIDArray addObject:@"ST"];
    [countyIDArray addObject:@"SA"];
    [countyIDArray addObject:@"SN"];
    [countyIDArray addObject:@"CS"];
    [countyIDArray addObject:@"SC"];
    [countyIDArray addObject:@"SL"];
    [countyIDArray addObject:@"SG"];
    [countyIDArray addObject:@"SK"];
    [countyIDArray addObject:@"SI"];
    [countyIDArray addObject:@"SB"];
    [countyIDArray addObject:@"SO"];
    [countyIDArray addObject:@"ZA"];
    [countyIDArray addObject:@"GS"];
    [countyIDArray addObject:@"ES"];
    [countyIDArray addObject:@"LK"];
    [countyIDArray addObject:@"SD"];
    [countyIDArray addObject:@"SR"];
    [countyIDArray addObject:@"SJ"];
    [countyIDArray addObject:@"SZ"];
    [countyIDArray addObject:@"SE"];
    [countyIDArray addObject:@"CH"];
    [countyIDArray addObject:@"SY"];
    [countyIDArray addObject:@"TW"];
    [countyIDArray addObject:@"TJ"];
    [countyIDArray addObject:@"TZ"];
    [countyIDArray addObject:@"TH"];
    [countyIDArray addObject:@"TL"];
    [countyIDArray addObject:@"TG"];
    [countyIDArray addObject:@"TK"];
    [countyIDArray addObject:@"TO"];
    [countyIDArray addObject:@"TT"];
    [countyIDArray addObject:@"TN"];
    [countyIDArray addObject:@"TR"];
    [countyIDArray addObject:@"TM"];
    [countyIDArray addObject:@"TC"];
    [countyIDArray addObject:@"TV"];
    [countyIDArray addObject:@"UG"];
    [countyIDArray addObject:@"UA"];
    [countyIDArray addObject:@"AE"];
    [countyIDArray addObject:@"GB"];
    [countyIDArray addObject:@"US"];
    [countyIDArray addObject:@"UM"];
    [countyIDArray addObject:@"UY"];
    [countyIDArray addObject:@"UZ"];
    [countyIDArray addObject:@"VU"];
    [countyIDArray addObject:@"VE"];
    [countyIDArray addObject:@"VN"];
    [countyIDArray addObject:@"VG"];
    [countyIDArray addObject:@"VI"];
    [countyIDArray addObject:@"WF"];
    [countyIDArray addObject:@"EH"];
    [countyIDArray addObject:@"YE"];
    [countyIDArray addObject:@"ZM"];
    [countyIDArray addObject:@"ZW"];
    
    myPickerView = [[UIPickerView alloc]init];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                   target:self action:@selector(done:)];
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, self.view.frame.size.height-
                                     myPickerView.frame.size.height-50, 320, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    _txtCountry.inputView = myPickerView;
    _txtCountry.inputAccessoryView = toolBar;
}

-(void)done:(id)sender
{
    _txtCountry.text =selectedCountry;
    [_txtCountry resignFirstResponder];
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [countryArray count];
}

#pragma mark- ====Picker View Delegate====

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    selectedCountry =[countryArray objectAtIndex:row];
  
    countryKey =[countyIDArray objectAtIndex:row];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [countryArray objectAtIndex:row];
}

#pragma mark- ====UITextfeild  Delegate====
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _Name)
    {
        [textField resignFirstResponder];
        [self.surName becomeFirstResponder];
    }
    else if (textField == _surName)
    {
        [textField resignFirstResponder];
        [self.txtEmail becomeFirstResponder];
    }
    else if (textField == _txtEmail)
    {
        [textField resignFirstResponder];
        [self.txtCountry becomeFirstResponder];
    }
    else if (textField == _txtCountry)
    {
        [textField resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
    }
    else if (textField == _txtPassword)
    {
        [textField resignFirstResponder];
        [self.txtRePassword becomeFirstResponder];
    }
    
    else if (textField == _txtRePassword)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _txtCountry)
    {
        return  NO;
    }
    // return  YES;
    
    NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    textField.text = updatedString;
    
    //Setting the cursor at the right place
    NSRange selectedRange = NSMakeRange(range.location + string.length, 0);
    UITextPosition* from = [textField positionFromPosition:textField.beginningOfDocument offset:selectedRange.location];
    UITextPosition* to = [textField positionFromPosition:from offset:selectedRange.length];
    textField.selectedTextRange = [textField textRangeFromPosition:from toPosition:to];
    
    //Sending an action
    [textField sendActionsForControlEvents:UIControlEventEditingChanged];
    
    return NO;
}

#pragma mark ==== SetUI  ====
-(void)setUI
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.regiserView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.regiserView.layer.borderWidth = 1.0f;
    self.regiserView.clipsToBounds = YES;
    
 
    [_txtPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_txtRePassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self setBorderTextField:_Name];
    [self setBorderTextField:_txtPassword];
    [self setBorderTextField:_txtEmail];
    [self setBorderTextField:_surName];
    [self setBorderTextField:_txtCountry];
    [self setBorderTextField:_txtRePassword];

    _btnCancel.layer.borderWidth = 1.0f;
    [_btnCancel.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    _btnRegister.layer.borderWidth = 1.0f;
    [_btnRegister.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    NSString *string1 = [appDelegate.dictCulture valueForKey:@"ILikeToSubscribeToTheNewsletterAndReceiveUpdatesFromMsdAnimalHealthIAcceptTheTermsAndConditionsAndPrivacyPolicy"];
    
    _txtVTerms.text = string1;
    
    if (string1.length==0)
    {
        _txtVTerms.text = @"I like To Subscribe To The Newsletter And Recieve Updates From Msd Aniaml Health.";
    }
    
    [_btnRegister setTitle:[appDelegate.dictCulture valueForKey:@"Register"] forState:UIControlStateNormal];
    
    NSString * btnRegister =[appDelegate.dictCulture valueForKey:@"Register"];
    if (btnRegister.length==0)
    {
        [_btnRegister setTitle:@"Register" forState:UIControlStateNormal];
    }
    
    [_btnCancel setTitle:[appDelegate.dictCulture valueForKey:@"Cancel"] forState:UIControlStateNormal];
    
    NSString * btnCancel =[appDelegate.dictCulture valueForKey:@"Cancel"];
    if (btnCancel.length==0)
    {
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    _Name.placeholder = [appDelegate.dictCulture valueForKey:@"Name"];
    NSString * Name =[appDelegate.dictCulture valueForKey:@"Name"];
    if (Name.length==0)
    {
        _Name.placeholder =@"Name";
    }
    
    _surName.placeholder =[appDelegate.dictCulture valueForKey:@"Surname"];
    NSString * surName =[appDelegate.dictCulture valueForKey:@"Surname"];
    if (surName.length==0)
    {
        _surName.placeholder =@"Surname";
    }
    
    _txtEmail.placeholder = [appDelegate.dictCulture valueForKey:@"EmailAddress"];
    NSString * txtEmail =[appDelegate.dictCulture valueForKey:@"EmailAddress"];
    if (txtEmail.length==0)
    {
        _txtEmail.placeholder =@"Email Address";
    }
    
    _txtCountry.placeholder = [appDelegate.dictCulture valueForKey:@"Country"];
    NSString * txtCountry =[appDelegate.dictCulture valueForKey:@"Country"];
    if (txtCountry.length==0)
    {
        _txtCountry.placeholder =@"Country";
    }
    
    self.txtPassword.placeholder = [appDelegate.dictCulture valueForKey:@"Password"];
    NSString * txtPassword =[appDelegate.dictCulture valueForKey:@"Password"];
    if (txtPassword.length==0)
    {
        self.txtPassword.placeholder =@"Password";
    }
    
    _txtRePassword.placeholder= [appDelegate.dictCulture valueForKey:@"RetypePassword"];
    NSString * txtRePassword =[appDelegate.dictCulture valueForKey:@"RetypePassword"];
    if (txtRePassword.length==0)
    {
        _txtRePassword.placeholder =@"Retype Password";
    }
    
    _lblTop.text = [appDelegate.dictCulture valueForKey:@"Register"];
    NSString * lblTop =[appDelegate.dictCulture valueForKey:@"Register"];
    if (lblTop.length==0)
    {
        [_lblTop  setText:@"Register"];
    }
}
-(void)setBorderTextField:(UITextField *)textfield
{
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textfield.leftView = paddingView;
    textfield.leftViewMode = UITextFieldViewModeAlways;
    
    textfield.layer.borderWidth = 1.0;
    textfield.layer.borderColor = [UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1].CGColor;
    textfield.clearsOnBeginEditing = NO;
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    NSString *page = URL.absoluteString;
    if ([page isEqualToString:@"Terms"])
    {
        WelcomeDetailViewController *detail =[WelcomeDetailViewController new];
        detail.isFrom =@"Terms and Conditions";
        [self.navigationController pushViewController:detail animated:YES];
        return NO;
    }
    else  if ([page isEqualToString:@"Privacy"])
    {
        WelcomeDetailViewController *detail =[WelcomeDetailViewController new];
        detail.isFrom =@"Privacy Policy";
        [self.navigationController pushViewController:detail animated:YES];
    }
    return YES;
}

#pragma mark====WebView ====
-(void)checkValidity
{
    if(_Name.text.length >0 && _surName.text.length >0 && _txtCountry.text.length >0 && _txtPassword.text.length >0 &&_txtRePassword.text.length >0)
    {
        if (![Utility NSStringIsValidEmail:_txtEmail.text])
        {
            isValideEmail =@"No";
            NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
            NSString*message= [appDelegate.dictCulture valueForKey:@"PleaseEnterAValidEmailAddress"];
            NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            
            if (title.length==0)
            {
                title=@"Alert";
            }
            
            if (message.length==0)
            {
                message =@"Please enter a valid email address";
            }
            
            if (okText.length==0)
            {
                
                okText =@"Ok";
            }
            
            UIAlertController*  Empty_Error = [UIAlertController alertControllerWithTitle: title message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            
            [Empty_Error addAction:ok];
            
            [self presentViewController:Empty_Error animated:YES completion:nil];
        }
        else
        {
            isValideEmail =@"Yes";
        }
        
        if (_txtPassword.text.length > 1) {
            NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
            NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                                   characterSetWithCharactersInString:specialCharacterString];
            NSString *specialNumberString = @"1234567890";
            NSCharacterSet *specialNumberSet = [NSCharacterSet
                                                characterSetWithCharactersInString:specialNumberString];
            
            NSString *CharacterString = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            NSCharacterSet *CharacterStringSet = [NSCharacterSet
                                                  characterSetWithCharactersInString:CharacterString];
            
            
            NSString*stringP=_txtPassword.text;
            
            if (!([stringP.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) || !([stringP.lowercaseString rangeOfCharacterFromSet:CharacterStringSet].length) || !([stringP.lowercaseString rangeOfCharacterFromSet:specialNumberSet].length) || (stringP.length<8) ) {
                isValidPassword =@"No";
                
                NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
                NSString*message= [appDelegate.dictCulture valueForKey:@"keyRequired"];
                NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
                
                if (title.length == 0)
                {
                    title=@"Alert";
                }
                
                if (message.length == 0)
                {
                    message =@"Your password should be a minimum of 8 letters, and should contain at least one upper case letter, one number and one special character.";
                    
                }
                
                if(okText.length == 0)
                {
                    okText =@"Ok";
                }
                
                UIAlertController*  Empty_Error = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
                [Empty_Error addAction:ok];
                [self presentViewController:Empty_Error animated:YES completion:nil];
            }
            
            else
            {
                isValidPassword=@"Yes";
                
            }
        }
        
        if(![_txtPassword.text isEqualToString:_txtRePassword.text])
        {
            isPasswordSame =@"No";
            NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
            NSString*message= [appDelegate.dictCulture valueForKey:@"PasswordDoNotMatch"];
            NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            if (title.length == 0)
            {
                title=@"Alert";
            }
            
            if (message.length==0)
            {
                message =@"Password do not match";
            }
            
            if (okText.length == 0)
            {
                okText =@"Ok";
            }
            
            UIAlertController*  Empty_Error = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            [Empty_Error addAction:ok];
            [self presentViewController:Empty_Error animated:YES completion:nil];
        }
        
        else{
        
          isPasswordSame=@"Yes";
        
        }
   
        if([isValideEmail isEqualToString:@"Yes"] && [isValidPassword isEqualToString:@"Yes"] &&  [isPasswordSame isEqualToString:@"Yes"])
        {
            NSString *  message= [appDelegate.dictCulture valueForKey:@"PleaseAccepTermsAndConditionsToProceedFurther"];
            
            if (message.length ==0)
            {
                message =@"Registering, please wait...";
            }
            
            active_alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
            activityView = [[UIActivityIndicatorView alloc]
                            initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            activityView.center=self.view.center;
            [activityView startAnimating];
            [activityView hidesWhenStopped];
            [self.view addSubview:activityView];
            [self performSelector:@selector(loadRegisterRequest) withObject:nil afterDelay:0];
        }
    }
    else
    {
        NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
        NSString*message= [appDelegate.dictCulture valueForKey:@"PleaseProvideAllTheRequiredInformation"];
        NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if (title.length == 0)
        {
            title=@"Alert";
        }
        
        if (message.length==0)
        {
            message =@"Please provide all the required information";
        }
        
        if (okText.length==0)
        {
            okText =@"Ok";
        }
        
        UIAlertController*  Empty_Error = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
        
        [Empty_Error addAction:ok];
        [self presentViewController:Empty_Error animated:YES completion:nil];
        
    }
}

#pragma mark ===LoadWebview Data ===
-(void)loadRegisterRequest {
    
     [self.webView removeFromSuperview];
    self.webView = [[UIWebView alloc]init];
    self.webView.delegate=self;
    
    [NSHTTPCookieStorage sharedHTTPCookieStorage].cookieAcceptPolicy =
    NSHTTPCookieAcceptPolicyAlways;
    [self logoutCall];
    
    urlString= @"http://lfwmobile.merck-animal-health.com/lfw20/pages/registeraccount.asp?lfwmobileapp=TTVApp";
    
    NSString *urlRegEx =
    @"(?i)(http|https)(:\\/\\/)([^ .]+)(\\.)([^ \n]+)";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    BOOL status  = [urlTest evaluateWithObject:urlString];
    
    
    if( [urlTest evaluateWithObject:urlString]){
        if ([[UIApplication sharedApplication]
             canOpenURL:[NSURL URLWithString:[NSString  stringWithFormat:@"%@",urlString]]]){
            
            requestURL = [NSURL URLWithString:urlString];
            NSError *error;
            NSString *page = [NSString stringWithContentsOfURL:requestURL
                                                      encoding:NSASCIIStringEncoding
                                                         error:&error];
            [self.webView loadHTMLString:page baseURL:requestURL];
        }
    }
}

-(void)logoutCall {

    urlString= @"http://lfwmobile.merck-animal-health.com/LFW20/pages/logout.asp";
    requestURL = [NSURL URLWithString:urlString];
    NSError *error;
    NSString *page = [NSString stringWithContentsOfURL:requestURL
                                              encoding:NSASCIIStringEncoding
                                                 error:&error];
    [self.webView loadHTMLString:page baseURL:requestURL];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self.txtPassword setSecureTextEntry:YES];
    [self.txtPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.txtRePassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.txtRePassword setSecureTextEntry:YES];
    
    NSString*occupassion=@"VET_PRACTICE";
    NSString *javaScript = [NSString stringWithFormat:@"var textField = document.getElementById('lfw20VALID_EMAIL34698').value = '%@';" "var textField1 = document.getElementById('lfw20VALID_PASSWORD34699').value = '%@';"  "var textField2 = document.getElementById('lfw20VALID_RPASSWORD34700').value = '%@';" "var textField3 = document.getElementById('lfw20SELECT34702').value = '%@';" "var textField4 = document.getElementById('lfw20TEXT34703').value = '%@';" "var textField5 = document.getElementById('lfw20TEXT34704').value = '%@';" "var textField6 = document.getElementById('lfw20TEXTAREA34705').value = '%@';" "var textField7 = document.getElementById('lfw20TEXT34711').value = '%@';" "var textField8 = document.getElementById('lfw20COUNTRY_SELECT34706').value = '%@';" "var textField9 = document.getElementById('lfw20SELECT34708').value = '%@';",[self.txtEmail text],[self.txtPassword text],[self.txtRePassword text],@"Mr",_Name.text,_surName.text,@"Address",@"City",countryKey,@"VET_PRACTICE"];
    
    [self.webView stringByEvaluatingJavaScriptFromString:javaScript];
    NSString *jsStat = @"document.forms[0].submit()";
    [self.webView stringByEvaluatingJavaScriptFromString:jsStat];
    html = [self.webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('html')[0].innerHTML"];
    NSString  * doc = [self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
    
    NSString  * error = [self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.innerHTML"];
    
    if([doc containsString:@"Thank you"])
    {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        self.webView=nil;
        self.webView.delegate=nil;
        [self stopLoading];
        [activityView stopAnimating];
    }
    if([doc containsString:@"email address already exists"]) {
            self.txtEmail.text =@"";
        [activityView stopAnimating];
        NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
        NSString*message= [appDelegate.dictCulture valueForKey:@"user_already_exist"];
        NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if (title.length==0)
        {
            title=@"";
        }
        
        if (message.length==0)
        {
            message =@"A registered user with this email address already exists";
        }
        if (okText.length==0)
        {
            
            okText =@"Ok";
        }
        
        UIAlertController*  Empty_Error = [UIAlertController alertControllerWithTitle: @"" message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
        
        [Empty_Error addAction:ok];
        
        [self presentViewController:Empty_Error animated:YES completion:nil];
    }
}

-(void)stopLoading
{
    [FBSDKAppEvents logEvent:FBSDKAppEventNameCompletedRegistration
                  parameters:@{
                               FBSDKAppEventParameterNameContentType :@"User Registration",
                               } ];
    
     [FIRAnalytics logEventWithName:kFIREventSignUp
     parameters:@{
     kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",@"User Registration"],
     kFIRParameterItemName:@"User_Registration",
     
     }];

    LoginViewController *detailVc= [[LoginViewController alloc]init];
    [self.navigationController pushViewController:detailVc animated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *URLString = [[request URL] absoluteString];
    if ([URLString hasPrefix:@"http"] || [URLString hasPrefix:@"https"])
     
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark=====UIButton Action Methods====
-(IBAction)rememberMeBtnPressed:(id)sender
{
    if (_btnRememberMe.selected)
    {
        isPrivacyChecked =@"No";
        _btnRememberMe.selected = NO;
        _imgRememberMe.image = [UIImage imageNamed:@"login-CheckBox"];
    }
    else
    {
        isPrivacyChecked =@"Yes";
        _btnRememberMe.selected = YES;
        _imgRememberMe.image = [UIImage imageNamed:@"login-CheckBox-Selected"];
    }
}

-(IBAction)cancelBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)registerBtnPressed
{
    [self.view endEditing:YES];
    [self performSelector:@selector(checkValidity) withObject:nil afterDelay:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
