//
//  LoginViewController.m
//  Bovilisplan
//
//  Created by Admin on 5/8/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//
#import "ForgotViewController.h"
#import "LoginViewController.h"
#import "IQKeyboardManager.h"
#import "HomeViewController.h"
#import "MainViewController.h"
#import "AppDelegate.h"
#import "BovilisApi.h"

@import Firebase;

@interface LoginViewController ()<UITextFieldDelegate,UIWebViewDelegate>

@property(strong,nonatomic)IBOutlet UITextField *txtEmail;
@property(strong,nonatomic)IBOutlet UITextField *txtPassword;
@property(strong,nonatomic)IBOutlet UIButton *btnConnect;
@property(nonatomic,strong)IBOutlet UIImageView *imgRememberMe;
@property(nonatomic,strong)IBOutlet UIButton *btnRememberMe;
@property(nonatomic,strong)IBOutlet UIView*loginView;
@property(strong,nonatomic)IBOutlet UIButton *btnCancel;
@property(strong,nonatomic)IBOutlet UIButton *btnForgot;
@property(strong,nonatomic)IBOutlet UIWebView *webView;
@property(strong,nonatomic)IBOutlet UILabel*lblRemmebrMe;
@property(strong,nonatomic)IBOutlet UILabel*lblLogin;
@property(weak,nonatomic) IBOutlet UIImageView *imgView;


@end

@implementation LoginViewController
{
    NSString *html;
    NSURL *requestURL ;
    NSString *urlString;
    UIAlertController *active_alert;
    AppDelegate * appDelegate;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.imgView.image = [UIImage imageNamed:[Utility setLocalizeLogo:@"background"]];
    [self setUI];
}

-(void)viewWillAppear:(BOOL)animated {
    
    self.txtEmail.text = @"";
    self.txtPassword.text = @"";
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *savedUserEmail = [userDefaults1   valueForKey:@"SaveUserEmail"];

    [userDefaults1 synchronize];
    
    if(savedUserEmail.length > 0) {
        self.txtEmail.text = savedUserEmail;
        _btnRememberMe.selected = YES;
    [self.btnRememberMe setImage: [UIImage imageNamed:@"login-CheckBox-Selected"] forState:UIControlStateNormal];
        
    }

}

#pragma mark ==== SetUI  ====
-(void)setUI {

    //_btnConnect.enabled=FALSE;
   // _btnConnect.alpha = 0.5;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtEmail.leftView = paddingView;
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    _txtEmail.layer.borderWidth = 1.0;
    _txtEmail.layer.borderColor = [UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1].CGColor;
    _txtEmail.clearsOnBeginEditing = NO;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtPassword.layer.borderWidth = 1.0;
    _txtPassword.layer.borderColor = [UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1].CGColor;
    _txtPassword.leftView = paddingView1;
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtPassword.clearsOnBeginEditing = NO;
    [_txtPassword setSecureTextEntry:TRUE];
    [_txtPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    

    
    _btnConnect.layer.borderWidth = 1.0f;
    [_btnConnect.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnConnect setTitle:[appDelegate.dictCulture valueForKey:@"Connect"] forState:UIControlStateNormal];
    NSString * btnConnect = [appDelegate.dictCulture valueForKey:@"Connect"] ;
    if (btnConnect.length==0)
    {
        [_btnConnect setTitle:@"CONNECT" forState:UIControlStateNormal];
    }
    
    _btnCancel.layer.borderWidth = 1.0f;
    [_btnCancel.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnCancel setTitle:[appDelegate.dictCulture valueForKey:@"Cancel"] forState:UIControlStateNormal];
    NSString * btnCancel = [appDelegate.dictCulture valueForKey:@"Cancel"] ;
    if (btnCancel.length==0)
    {
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    

    NSString * btnForgotText = [appDelegate.dictCulture valueForKey:@"ForgotPassword"] ;
    
    btnForgotText = btnForgotText.length == 0? @"Forgot Password?" :btnForgotText;
    [_btnForgot setTitle:btnForgotText forState:UIControlStateNormal];

    
    [_lblLogin setText:[appDelegate.dictCulture valueForKey:@"Login"]];
    NSString * lblLogin = [appDelegate.dictCulture valueForKey:@"Login"] ;
    if (lblLogin.length==0)
    {
        [_lblLogin setText:@"LOGIN"];
    }
    
    [_lblRemmebrMe setText:[appDelegate.dictCulture valueForKey:@"RememberMe"]];
    NSString * lblRemmebrMeText = [appDelegate.dictCulture valueForKey:@"RememberMe"] ;
    
    lblRemmebrMeText = lblRemmebrMeText.length == 0? @"  Remember Me" :[NSString stringWithFormat:@"  %@",lblRemmebrMeText];
    [self.btnRememberMe setTitle:lblRemmebrMeText forState:UIControlStateNormal];

    
    
    _txtEmail.placeholder =[appDelegate.dictCulture valueForKey:@"EmailAddress"];
    NSString * txtEmail = [appDelegate.dictCulture valueForKey:@"EmailAddress"] ;
    if (txtEmail.length==0)
    {
        _txtEmail.placeholder =@"Email Address" ;
    }
    
    _txtPassword.placeholder =[appDelegate.dictCulture valueForKey:@"Password"];;
    NSString * txtPassword = [appDelegate.dictCulture valueForKey:@"Password"] ;
    if (txtPassword.length==0)
    {
        _txtPassword.placeholder =@"Password" ;
    }
}

#pragma mark ===Load Webview Data ===
-(void)loadLoginRequest {
    
    [self.webView removeFromSuperview];
    self.webView = [[UIWebView alloc]init];
    self.webView.delegate=self;
    
    urlString= @"http://lfwmobile.merck-animal-health.com/logincheck.asp?lfwmobileapp=TTVApp";
    NSString *urlRegEx =
    @"(?i)(http|https)(:\\/\\/)([^ .]+)(\\.)([^ \n]+)";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    BOOL status  = [urlTest evaluateWithObject:urlString];
    
    if( [urlTest evaluateWithObject:urlString]){
        if ([[UIApplication sharedApplication]
             canOpenURL:[NSURL URLWithString:[NSString  stringWithFormat:@"%@",urlString]]]){
            
            requestURL = [NSURL URLWithString:urlString];
            NSError *error;
            NSString *page = [NSString stringWithContentsOfURL:requestURL
                                                      encoding:NSASCIIStringEncoding
                                                         error:&error];
            [self.webView loadHTMLString:page baseURL:requestURL];
        }
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *URLString = [[request URL] absoluteString];
    if ([URLString hasPrefix:@"http"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString * emailValue = _txtEmail.text;
    
    if (![Utility NSStringIsValidEmail:emailValue])
    {
        NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
        NSString*message= [appDelegate.dictCulture valueForKey:@"PleaseEnterAValidEmailAddress"];
        NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if (title.length==0)
        {
            title=@"Alert";
        }
        
        if (message.length==0)
        {
            message =@"Please enter a valid email address";
        }
        
        if (okText.length==0)
        {
            okText =@"Ok";
        }

        UIAlertController*  Empty_Error = [UIAlertController alertControllerWithTitle: title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
        
        [Empty_Error addAction:ok];
        [self presentViewController:Empty_Error animated:YES completion:nil];

    }
    
    else
    {
    
    [_txtPassword setSecureTextEntry:TRUE];
    [_txtPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
        
    NSString * passwordValue = [self.txtPassword text];
    NSString *javaScript = [NSString stringWithFormat:@"var textField = document.getElementById('lfw20VALID_EMAIL34698').value = '%@';" "var textField1 = document.getElementById('lfw20VALID_PASSWORD34699').value = '%@';",emailValue,passwordValue];
    
    [self.webView stringByEvaluatingJavaScriptFromString:javaScript];
    
    NSString *jsStat = @"document.getElementsByName('butLogin')[0].click()";
    [webView stringByEvaluatingJavaScriptFromString:jsStat];
    
    html = [self.webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('html')[0].innerHTML"];
      

        if ([html containsString:@"Too many unsuccessful login attempts"]) {
            [self dismissViewControllerAnimated:YES completion:nil];

            NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
            NSString*message= [appDelegate.dictCulture valueForKey:@"UnsuccessfulAttempts"];
            NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            
            if (title.length==0)
            {
                title=@"Error";
            }
            
            if (message.length==0)
            {
                message =@"Too many unsuccessful login attempts. Your login session was temporarily disabled for security reasons. Please try again in 1 minute. This message will be shown until you entered your valid password.";
            }
            
            if (okText.length==0)
            {
                okText =@"Ok";
            }
            
            UIAlertController*  alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        
        }
   else if([html containsString:@"LOGIN-SUCCESS=true"])
    {
      //here  [self dismissViewControllerAnimated:YES completion:nil];
        
        urlString= @"http://lfwmobile.merck-animal-health.com/LFW20/pages/logout.asp";
        requestURL = [NSURL URLWithString:urlString];
        NSError *error;
        NSString *page = [NSString stringWithContentsOfURL:requestURL
                                                  encoding:NSASCIIStringEncoding
                                                     error:&error];
        [self.webView loadHTMLString:page baseURL:requestURL];
       
        NSRange fName = [html rangeOfString:@"LFW_FirstName="];
        NSRange lName = [html rangeOfString:@"LFW_LastName="];
        NSRange region = [html rangeOfString:@"LFW_CountryOrigin="];

       
        NSRange rFname = NSMakeRange(fName.location + fName.length, lName.location - fName.location - fName.length);
         NSRange rLname = NSMakeRange(lName.location + lName.length, region.location - lName.location - lName.length);
        NSString *sub = [html substringWithRange:rFname];
        NSString *sub1 = [html substringWithRange:rLname];
        
        sub = [sub stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        sub1 = [sub1 stringByReplacingOccurrencesOfString:@"\n" withString:@""];


        if(_btnRememberMe.selected) {
            
            [[NSUserDefaults standardUserDefaults] setValue:_txtEmail.text forKey:@"SaveUserEmail"];
        }
        else {
           
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SaveUserEmail"];
        }
        
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",sub]  forKey:@"UserFirstName"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",sub1]  forKey:@"UserLastName"];

        
        [[NSUserDefaults standardUserDefaults] setValue:_txtEmail.text  forKey:@"UserEmail"];
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"IsLogin"];

        [[NSUserDefaults standardUserDefaults]  synchronize];
        
        [self sendTokenAtSever];
        
        dispatch_async(dispatch_get_main_queue(), ^{
                           [self stopLoading];
        });

      }
        
    else if([html containsString:@"Email address or password is incorrect."])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        self.webView =nil;
        
        NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
        NSString*message= [appDelegate.dictCulture valueForKey:@"YourEmailAndPasswordDoNotMatch"];
        NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if (title.length==0)
        {
            title=@"Error";
        }
        
        if (message.length==0)
        {
            message =@"Your email & password do not match";
        }
        
        if (okText.length==0)
        {
            okText =@"Ok";
        }
       
        UIAlertController*  alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    }
}

-(void)sendTokenAtSever {
    
    BovilisApi *obj = [BovilisApi new];
    [obj sendToken: appDelegate.fcmToken WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
-(void)stopLoading {
   [self dismissViewControllerAnimated:YES completion:nil];

    HomeViewController *detailVc= [HomeViewController new];
    [self.navigationController pushViewController:detailVc animated:YES];
}

#pragma mark=====UIButton Action Methods====
-(IBAction)rememberMeBtnPressed:(id)sender
{
    if (_btnRememberMe.selected)
    {
        _btnRememberMe.selected = NO;
      //  _imgRememberMe.image = [UIImage imageNamed:@"login-CheckBox"];
        [self.btnRememberMe setImage:[UIImage imageNamed:@"login-CheckBox"] forState:UIControlStateNormal];
    }
    else
    {
        _btnRememberMe.selected = YES;
      //  _imgRememberMe.image = [UIImage imageNamed:@"login-CheckBox-Selected"];
        [self.btnRememberMe setImage: [UIImage imageNamed:@"login-CheckBox-Selected"] forState:UIControlStateNormal];
    }
}

-(IBAction)forgotButtonPressed:(id)sender
{
    ForgotViewController *forgotVc= [ForgotViewController new];
    [self.navigationController pushViewController:forgotVc animated:YES];
}

-(IBAction)connectButtonPressed:(id)sender
{
    if(_txtEmail.text.length>0 &&  _txtPassword.text.length>0 )
    {
        if (![Utility NSStringIsValidEmail:_txtEmail.text])
        {
            NSString*title=[appDelegate.dictCulture valueForKey:@"Alert"];
            NSString*message= [appDelegate.dictCulture valueForKey:@"PleaseEnterAValidEmailAddress"];
            NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
            
            if (title.length==0)
            {
                title=@"Alert";
            }
            
            if (message.length==0)
            {
                message =@"Please enter a valid email address";
            }
            
            if (okText.length==0)
            {
                okText =@"Ok";
            }
            
            UIAlertController*  Empty_Error = [UIAlertController alertControllerWithTitle: title message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            
            [Empty_Error addAction:ok];

            [self presentViewController:Empty_Error animated:YES completion:nil];
        }

        else{
        NSString*message= [appDelegate.dictCulture valueForKey:@"SigningInPleaseWait"];
        NSString*okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        if (message.length == 0)
        {
            message =@"Signing in, please wait...";
        }
        if (okText.length == 0)
        {
            okText =@"Ok";
        }
        
        active_alert= [UIAlertController alertControllerWithTitle:nil message: message preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:active_alert animated:YES completion:nil];
        
     
        [self performSelector:@selector(loadLoginRequest) withObject:nil afterDelay:0];
        
        }
    }
    
    else
    {
        NSString* title=[appDelegate.dictCulture valueForKey:@"Alert"];
        NSString* message= [appDelegate.dictCulture valueForKey:@"PleaseProvideAllTheRequiredInformation"];
        NSString* okText =[appDelegate.dictCulture valueForKey:@"Ok"];
        
        if (title.length == 0)
        {
            title=@"Alert";
        }
        
        if (message.length == 0)
        {
            message =@"Please provide all the required information";
        }
        
        if (okText.length == 0)
        {
            okText =@"Ok";
        }
        
        UIAlertController*  alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(IBAction)cancelBtnPressed {
    
    MainViewController *forgotVc= [MainViewController new];
    [self.navigationController pushViewController:forgotVc animated:YES];
}

#pragma mark ==== UITextField Dlegates ====
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  

    
    //Setting the new text.
    NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    textField.text = updatedString;
    
    //Setting the cursor at the right place
    NSRange selectedRange = NSMakeRange(range.location + string.length, 0);
    UITextPosition* from = [textField positionFromPosition:textField.beginningOfDocument offset:selectedRange.location];
    UITextPosition* to = [textField positionFromPosition:from offset:selectedRange.length];
    textField.selectedTextRange = [textField textRangeFromPosition:from toPosition:to];
    
    //Sending an action
    [textField sendActionsForControlEvents:UIControlEventEditingChanged];
    
    return NO;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    [textField resignFirstResponder];
    return NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtEmail)
    {
        [textField resignFirstResponder];
        [_txtPassword becomeFirstResponder];
    }
    
    else if (textField == _txtPassword)
    {
        [textField resignFirstResponder];
    }
    
    return   [textField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
