//
//  WelcomeViewController.h
//  Bovilisplan
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController

-(IBAction)agreeButtonPressed:(id)sender;
-(void)nextButtonPreesed;

@end
