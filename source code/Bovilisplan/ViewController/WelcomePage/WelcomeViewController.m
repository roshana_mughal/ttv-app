//
//  WelcomeViewController.m
//  Bovilisplan
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//
#import "WelcomeDetailViewController.h"
#import "MainViewController.h"
#import "AppDelegate.h"
#import "WelcomeViewController.h"
#import "Utility.h"
#import "TranslationService.h"

@interface WelcomeViewController ()
@property(strong,nonatomic) IBOutlet UIButton *btnAgree;
@property(strong,nonatomic) IBOutlet UIButton *btnDisclaimer;
@property(strong,nonatomic) IBOutlet UIButton *btnPrivacy;
@property(strong,nonatomic) IBOutlet UIButton *btnTerms;
@property(strong,nonatomic) IBOutlet UILabel *lblTop;
@property(nonatomic,weak) IBOutlet UIImageView *logoImg;

@end
@implementation WelcomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}


-(void)setUI
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  
    self.logoImg.image = [UIImage imageNamed:[Utility setLocalizeLogo:@"logoG"]];
    _btnAgree.layer.borderWidth = 1.0f;
    _btnAgree.layer.cornerRadius= 0.0;
    [_btnAgree.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnAgree setTitle:[appDelegate.dictCulture valueForKey:@"IAgree"] forState:UIControlStateNormal];
    
    NSString* btnAgree=[appDelegate.dictCulture valueForKey:@"IAgree"];
    if (btnAgree.length == 0)
    {
        [_btnAgree setTitle:@"I AGREE" forState:UIControlStateNormal];
    }
    
    _btnDisclaimer.backgroundColor = [UIColor clearColor];
    
    [_btnDisclaimer setTitleColor:[UIColor colorWithRed:(0/255.0) green:(174/255.0) blue:(25/255.0) alpha:1] forState:UIControlStateNormal];
    
    _btnDisclaimer.layer.cornerRadius = 1.0;
    _btnDisclaimer.layer.borderColor = [UIColor colorWithRed:(0/255.0) green:(174/255.0) blue:(25/255.0) alpha:1] .CGColor;
    _btnDisclaimer.layer.borderWidth = 1.0f;
    _btnDisclaimer.clipsToBounds = YES;
    
    [_btnDisclaimer setTitle:[appDelegate.dictCulture valueForKey:@"Disclaimer"] forState:UIControlStateNormal];
    
    NSString* btnDisclaimer=[appDelegate.dictCulture valueForKey:@"Disclaimer"];
    if (btnDisclaimer.length == 0)
    {
        [_btnDisclaimer setTitle:@"Disclaimer" forState:UIControlStateNormal];
    }
    
    [_btnPrivacy setTitle:[appDelegate.dictCulture valueForKey:@"PrivacyPolicy"] forState:UIControlStateNormal];
    _btnPrivacy.backgroundColor = [UIColor clearColor];
    [_btnPrivacy setTitleColor:[UIColor colorWithRed:(0/255.0) green:(174/255.0) blue:(25/255.0) alpha:1] forState:UIControlStateNormal];
    
    _btnPrivacy.layer.cornerRadius = 1.0;
    _btnPrivacy.layer.borderColor = [UIColor colorWithRed:(0/255.0) green:(174/255.0) blue:(25/255.0) alpha:1] .CGColor;
    _btnPrivacy.layer.borderWidth = 1.0f;
    _btnPrivacy.clipsToBounds = YES;
    NSString* btnPrivacy=[appDelegate.dictCulture valueForKey:@"PrivacyPolicy"];
    if (btnPrivacy.length == 0)
    {
        [_btnPrivacy setTitle:@"Privacy Policy" forState:UIControlStateNormal];
    }
    
    [_btnTerms setTitle:[appDelegate.dictCulture valueForKey:@"TermsAndConditions"] forState:UIControlStateNormal];
    
    _btnTerms.backgroundColor = [UIColor clearColor];
    
    [_btnTerms setTitleColor:[UIColor colorWithRed:(0/255.0) green:(174/255.0) blue:(25/255.0) alpha:1] forState:UIControlStateNormal];
    
    _btnTerms.layer.cornerRadius = 0.0;
    _btnTerms.layer.borderColor = [UIColor colorWithRed:(0/255.0) green:(174/255.0) blue:(25/255.0) alpha:1] .CGColor;
    _btnTerms.layer.borderWidth = 1.0f;
    _btnTerms.clipsToBounds = YES;
    NSString* btnTerms=[appDelegate.dictCulture valueForKey:@"TermsAndConditions"];
    if (btnTerms.length == 0)
    {
        [_btnTerms setTitle:@"Terms And Conditions" forState:UIControlStateNormal];
    }
    
    [_lblTop setText:[appDelegate.dictCulture valueForKey:@"PleaseAcceptTheUserConditionsToContinue"]];
    
    NSString* lblTop=[appDelegate.dictCulture valueForKey:@"PleaseAcceptTheUserConditionsToContinue"];
    if (lblTop.length == 0)
    {
        [_lblTop setText:@"Please accept the user conditions to continue:"];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
   TranslationService *service = [TranslationService new];
    [service getCultures:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
            [self setUI];
        
    } failure:^(NSError *error, NSString *message) {
       
    }];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark === UIButton Actions ====
-(IBAction)privacyButtonPressed:(id)sender
{
    WelcomeDetailViewController *detailVc= [WelcomeDetailViewController new];
    detailVc.isFrom=@"Privacy Policy";
    [self.navigationController pushViewController:detailVc animated:YES];
}

-(IBAction)disclaimerButtonPressed:(id)sender
{
    WelcomeDetailViewController *detailVc= [WelcomeDetailViewController new];
    detailVc.isFrom=@"Disclaimer";
    [self.navigationController pushViewController:detailVc animated:YES];
}

-(IBAction)termsButtonPressed:(id)sender
{
    WelcomeDetailViewController *detailVc= [WelcomeDetailViewController new];
    detailVc.isFrom=@"Terms and Conditions";
    [self.navigationController pushViewController:detailVc animated:YES];
}

-(void)nextButtonPreesed
{
    MainViewController *detailVc= [MainViewController new];
    [self.navigationController pushViewController:detailVc animated:YES];
}

-(IBAction)agreeButtonPressed:(id)sender
{
    MainViewController *detailVc= [MainViewController new];
    [self.navigationController pushViewController:detailVc animated:YES];
}

@end
