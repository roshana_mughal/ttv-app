//
//  WelcomeDetailViewController.m
//  Bovilisplan
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "WelcomeDetailViewController.h"
#import "AppDelegate.h"
#import "TranslationService.h"
@interface WelcomeDetailViewController ()<UIWebViewDelegate>

@property(strong,nonatomic)IBOutlet UILabel *lblTitle;
@property(strong,nonatomic)IBOutlet UIWebView *webView;

@end

@implementation WelcomeDetailViewController
{
    NSURL *requestURL;
    UIAlertController *alert_loading;
    UIAlertController *active_alert;
    UIActivityIndicatorView *activityView;
    AppDelegate * appDelegate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    active_alert = [UIAlertController alertControllerWithTitle:nil message:@"Loading, please wait..." preferredStyle:UIAlertControllerStyleAlert];
    [self performSelector:@selector(loadWebviewData) withObject:nil afterDelay:0];
    
    _lblTitle.text =_isFrom;
    appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if([_isFrom isEqualToString:@"Privacy Policy"])
    {
        [_lblTitle setText:[appDelegate.dictCulture valueForKey:@"PrivacyPolicy"]];
        
        NSString * lblTitle = [appDelegate.dictCulture valueForKey:@"PrivacyPolicy"];
        if (lblTitle.length == 0)
        {
            [_lblTitle setText:@"Privacy Policy"];
        }
    }
    
    else if([_isFrom isEqualToString:@"Disclaimer"])
    {
        
        [_lblTitle setText:[appDelegate.dictCulture valueForKey:@"Disclaimer"]];
        
        NSString * lblTitle = [appDelegate.dictCulture valueForKey:@"Disclaimer"];
        if (lblTitle.length == 0)
        {
            [_lblTitle setText:@"Disclaimer"];
        }
    }
    
    else if([_isFrom isEqualToString:@"Terms and Conditions"])
    {
        [_lblTitle setText:[appDelegate.dictCulture valueForKey:@"TermsAndConditions"]];
        
        NSString * lblTitle = [appDelegate.dictCulture valueForKey:@"TermsAndConditions"];
        if (lblTitle.length == 0)
        {
            [_lblTitle setText:@"Terms And Conditions"];
            
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}


#pragma mark ===Load Webview Data ===

-(void)loadWebviewData{
    
    NSString *urlAddress ;
    if([_isFrom isEqualToString:@"Privacy Policy"]){
        
        NSString *Privacy = [TranslationService getDateForPrivacy];
        
        NSString *htmlString =
        [NSString stringWithFormat:@"<font face='UNIVERS' size='2' color = '2D2D2D'>%@", Privacy];
        [_webView loadHTMLString:htmlString baseURL:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    else if([_isFrom isEqualToString:@"Terms and Conditions"]){
        
        NSString *terms =[TranslationService getDateForTermsAndConditions];
        NSString *htmlString =
        [NSString stringWithFormat:@"<font face='UNIVERS' size='2' color = '2D2D2D' >%@", terms];
        [_webView loadHTMLString:htmlString baseURL:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    else
    {
        NSString *disclaimer = [TranslationService getDateForDisclaimer];
        
        NSString *htmlString =
        [NSString stringWithFormat:@"<font face='UNIVERS' size='2' color = '2D2D2D'>%@", disclaimer];
        [_webView loadHTMLString:htmlString baseURL:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark === UIButton Actions ====
-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
