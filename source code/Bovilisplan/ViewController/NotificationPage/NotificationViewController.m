//
//  NotificationViewController.m
//  Bovilisplan
//
//  Created by Admin on 4/27/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "ListingViewController.h"
#import "NotificationViewController.h"
#import "LibraryViewController.h"
#import "BovilisApi.h"
#import "LoginViewController.h"
#import "WelcomeDetailViewController.h"
#import "MenuView.h"
#import "GetDaysData.h"
#import "AppDelegate.h"
#import "NotificationCell.h"
#import "HomeViewController.h"
#import "NotificationDetailViewController.h"
#import "AboutUsViewController.h"
#import "OldNotificationCell.h"
#import "ReScheduleViewController.h"
#import "GetMappingDays.h"
#import "GAITracker.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "Utility.h"
#import <NHNetworkTime/NHNetworkTime.h>
#import "AllAnimalsViewController.h"



@interface NotificationViewController ()

<UITabBarDelegate,UITableViewDataSource>
{
    AppDelegate *appDelegate;
    NSMutableArray *sectionHeader;
    NSMutableDictionary * sectionDirectory ;
    NSMutableDictionary*   sortedNames;
    MenuView *menuView;
    UIView *transparentView ;
    UIAlertController *delete_alert;
    NSString *isUpcoming;
    NSMutableArray *notificationArray;
    NSIndexPath *indexx;
    UIActivityIndicatorView *activityView;
}
@property(strong,nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,weak) IBOutlet UILabel*lblTop;
@property(nonatomic,weak) IBOutlet UILabel*lblTitle;
@property(nonatomic,weak) IBOutlet UILabel*lblHeader;
@property(nonatomic,weak) IBOutlet UIView*innerView;
@property(nonatomic,weak) IBOutlet UIView*footerView;
@property(nonatomic,weak) IBOutlet UIView*outerView;
@property(nonatomic,weak) IBOutlet UIImageView*toggleImgView;
@property(nonatomic,weak) IBOutlet UIButton*btnPast;
@property(nonatomic,weak) IBOutlet UIButton*btnUpcoming;
@property(nonatomic,weak) IBOutlet UIImageView *logoImg;


@end

@implementation NotificationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate*) [[UIApplication sharedApplication]delegate];
    self.logoImg.image = [UIImage imageNamed:[Utility setLocalizeLogo:@"logo"]];

    [self loadMenuView];
    [transparentView setHidden:YES];
    self.lblHeader.hidden=TRUE;
    
    //UpdateIfAllSomeOrNoAnimalsWereVaccinated
    NSString *str = [Utility setLocalizeString:@"UpdateIfAllSomeOrNoAnimalsWereVaccinated"];
    
    self.lblHeader.text= str.length != 0 ? str : @"Update if all, some or no animals were vaccinated.";
    isUpcoming=@"Yes";
    
    activityView = [[UIActivityIndicatorView alloc]
                    initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.center=self.view.center;
    [activityView startAnimating];
    [self.view addSubview:activityView];
    [self configureTableViewCell];
    [self configureTableViewCell1];
    [self performSelector:@selector(allmethods) withObject:nil afterDelay:0];
}


-(void)allmethods
{
    if  ([appDelegate.isLoadedNotifications isEqualToString:@"No"] )
    {
        appDelegate.isLoadedNotifications =@"Yes";
        BovilisApi * objN =[BovilisApi sharedInstance];
        [objN  getVaccineSchedule:@"NA" WithSuccessBlock:^(BOOL success, NSDictionary *result) {
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:result  forKey:@"ScheduleVaccinesList"];
            [userDefaults synchronize];
            
            GetMappingDays *mapOBj=[[GetMappingDays alloc]init];
            [mapOBj getScheduleVaccineFromServer];
            
        } failure:^(NSError *error, NSString *message) {
            
        }];
    }
    else
    {
        GetMappingDays  *daysD =[[GetMappingDays alloc] init];
        [daysD getScheduleVaccineFromServer];
    }
    
    [_lblTitle setText:[appDelegate.dictCulture valueForKey:@"NoticeAndNotifications"]];
    NSString * lblTitle =[appDelegate.dictCulture valueForKey:@"NoticeAndNotifications"];
    
    if (lblTitle.length==0)
    {
        _lblTitle.text =@"Reminders";
    }
    
    [_lblTop setText:[appDelegate.dictCulture valueForKey:@"NotificationsHaveNotBeenFound"]];
    
    NSString * lblTop =[appDelegate.dictCulture valueForKey:@"NotificationsHaveNotBeenFound"];
    
    if (lblTop.length==0)
    {
        _lblTop.text =@"No reminders found.";
    }
    
    [transparentView setHidden:YES];
    [self arrangeData];
    [self loadDataOfNotifications];
    
    
    if(notificationArray.count>0 )
    {
        _tableView.hidden=FALSE;
        _lblTop.hidden=TRUE;
        [self loadDataOfNotifications];
        [activityView stopAnimating];
        [self.tableView reloadData];
    }
    else
    {
        _tableView.hidden=TRUE;
        _lblTop.hidden=FALSE;
        [activityView stopAnimating];
    }
}

-(void)arrangeData
{
    GetMappingDays  *theObjectDataDate =[[GetMappingDays alloc] init];
    notificationArray =[[NSMutableArray alloc]init];
    
    for (int i =0; i<appDelegate.getScheduleVaccineArray.count; i++)
    {
        theObjectDataDate = [appDelegate.getScheduleVaccineArray objectAtIndex:i];
        
        NSDateFormatter *dateFormater= [[NSDateFormatter alloc] init];
        //[dateFormater setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        [dateFormater setDateFormat:@"yyyy-MM-dd"];

         NSString *str =  theObjectDataDate.addedDateTime;
         str = [str substringToIndex:str.length-(str.length-10)];

        
        NSDate *today = [NSDate networkDate];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        
        NSString *todayStr = [dateFormat stringFromDate:today];
        NSDate *todayD = [dateFormat dateFromString:todayStr];
        
        NSDate *newDate = [dateFormater dateFromString:str];
        
        NSString *serverStr = [dateFormat stringFromDate:newDate];
        NSDate *serverD = [dateFormat dateFromString:serverStr];
        
        NSComparisonResult result;
        result = [todayD compare:serverD];

        if([isUpcoming isEqualToString:@"No"])
        {
            if(result==NSOrderedDescending && result != NSOrderedSame)
            {
                [notificationArray addObject:theObjectDataDate];
            }
        }
        
        else if([isUpcoming isEqualToString:@"Yes"])
        {
            if(result!=NSOrderedDescending  || result == NSOrderedSame){
                [notificationArray addObject:theObjectDataDate];
            }
        }
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    [transparentView setHidden:YES];
      [[self navigationController] setNavigationBarHidden:YES animated:YES];
    NSString *past =[Utility setLocalizeString:@"Past"];
    if (past.length != 0) {
        [self.btnPast setTitle:past forState:UIControlStateNormal];

    }
    NSString *upComing =[Utility setLocalizeString:@"Upcoming"];
    if (past.length != 0) {
        [self.btnUpcoming setTitle:upComing forState:UIControlStateNormal];
        
    }
    
    self.screenName = @"Reminders";
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    if([appDelegate.reloadCalendar isEqualToString:@"No"])
    {
        [self upcomingButtonPressed];
    }
}

-(void)loadDataOfNotifications
{
    sectionHeader = [[NSMutableArray alloc]init];
    sectionDirectory = [NSMutableDictionary new];
    
    for (NSInteger i = 0; i < notificationArray.count; i++)
    {
        NSDictionary *tmp = notificationArray[i];
      
         NSString *firstLetter = [[tmp valueForKey:@"addedDateTime2"] substringToIndex:10];
        if (![sectionDirectory valueForKey:firstLetter])
        {
            [sectionDirectory setObject:[NSMutableArray new] forKey:firstLetter];
            [sectionHeader addObject:firstLetter];
        }
        
        [[sectionDirectory objectForKey:firstLetter] addObject:tmp];
    }
    
    [self arrangeSortedDictionary];
}

-(void)arrangeSortedDictionary
{
    sortedNames = [NSMutableDictionary dictionary];
    
    for(int characterIndex = 0; characterIndex < sectionHeader.count; characterIndex++)
    {
        NSString *date = [sectionHeader objectAtIndex:characterIndex];
        NSString *alphabetCharacter = [date substringWithRange:NSMakeRange(0, 10)];
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"addedDateTime2 BEGINSWITH[c] %@",alphabetCharacter];
        NSArray *filteredNames =  (NSMutableArray *)[notificationArray  filteredArrayUsingPredicate:resultPredicate];
        [sortedNames setObject:filteredNames forKey:alphabetCharacter];
    }
}


#pragma mark === Configure TableView Cell===
-(void)configureTableViewCell
{
    [_tableView registerNib:[UINib nibWithNibName:@"NotificationCell"
                                           bundle:nil] forCellReuseIdentifier:@"NotificationCell"];
}

-(void)configureTableViewCell1
{
    [_tableView registerNib:[UINib nibWithNibName:@"OldNotificationCell"
                                           bundle:nil] forCellReuseIdentifier:@"OldNotificationCell"];
}


#pragma mark === TableView Delegates/Datasource===

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionHeader count];
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index
{
    return index;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 30);
    myLabel.font = [UIFont fontWithName:@"UNIVERS" size:15] ;
    myLabel.textColor = [UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1];
    myLabel.text =
    [NSString stringWithFormat:@"    %@",[self tableView:tableView titleForHeaderInSection:section]];
    myLabel.backgroundColor =[UIColor colorWithRed:230/255.0 green:231/255.0 blue:232/255.0 alpha:1];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    return headerView;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    
    return  [Utility setLocalDateFormatter:[sectionHeader objectAtIndex:section]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionIndexTitle = [sectionHeader objectAtIndex:section];
    NSArray *sectionTitle = [sortedNames objectForKey:sectionIndexTitle];
    return [sectionTitle count];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([isUpcoming isEqualToString:@"Yes"])
    {
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
            NSArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
            GetMappingDays*obj = [sectionTitleList1 objectAtIndex:indexPath.row];
            [self deleteSchedule:obj.scheduleIDPK withObj:obj];
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Detemine if it's in editing mode
    if([isUpcoming isEqualToString:@"Yes"])
    {
        return UITableViewCellEditingStyleDelete;
    }
    
    return UITableViewCellEditingStyleNone;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = @"NotificationCell";
    NSString *CellIdentifier1 = @"OldNotificationCell";
    
    if([isUpcoming isEqualToString:@"Yes"])
    {
        NotificationCell *cell = (NotificationCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        GetMappingDays *obj ;
        NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
        NSArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
        obj = [sectionTitleList1 objectAtIndex:indexPath.row];
        
        NSString * title = [appDelegate.dictCulture valueForKey:@"Title"];
        title=  title.length == 0 ? @"Title" : title;
        cell.lbl1.text=  [NSString stringWithFormat:@"%@:%@",title,obj.vaccineName];

        NSString * farmName = [appDelegate.dictCulture valueForKey:@"FarmName"];
        farmName=  farmName.length == 0 ? @"Farm Name" : farmName;
        cell.lbl2.text =[NSString stringWithFormat:@"%@:%@",farmName,obj.farmName];
        
        NSString * vaccineName = [appDelegate.dictCulture valueForKey:@"VaccineName"];
        vaccineName=  vaccineName.length == 0 ? @"Vaccine Name" : vaccineName;
        cell.lbl3.text = [NSString stringWithFormat:@"%@:%@",vaccineName,obj.scheduleVaccine];
        
      
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    else
    {
        OldNotificationCell *cell = (OldNotificationCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        GetMappingDays *obj ;
        NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
        NSArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
        obj = [sectionTitleList1 objectAtIndex:indexPath.row];
       // cell.lbl2.text = obj.vaccineName;
       // cell.lbl3.text = obj.farmName;
       // cell.lbl4.text = obj.scheduleVaccine;
        
        
        NSString * title = [appDelegate.dictCulture valueForKey:@"Title"];
        title=  title.length == 0 ? @"Title" : title;
        cell.lbl2.text=  [NSString stringWithFormat:@"%@:%@",title,obj.vaccineName];

        NSString * farmName = [appDelegate.dictCulture valueForKey:@"FarmName"];
        farmName=  farmName.length == 0 ? @"Farm Name" : farmName;
        cell.lbl3.text =[NSString stringWithFormat:@"%@:%@",farmName,obj.farmName];
        
        NSString * vaccineName = [appDelegate.dictCulture valueForKey:@"VaccineName"];
        vaccineName=  vaccineName.length == 0 ? @"Vaccine Name" : vaccineName;
        cell.lbl4.text = [NSString stringWithFormat:@"%@:%@",vaccineName,obj.scheduleVaccine];
        
        
        
        indexx = [self.tableView indexPathForCell:cell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.btnAll.tag= [indexPath row];
        cell.btnNone.tag= [indexPath row];
        cell.btnSome.tag= [indexPath row];
        
        [cell.btnAll addTarget:self action:@selector(allbuttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSome addTarget:self action:@selector(somebuttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnNone addTarget:self action:@selector(noneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
    NSArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
    GetMappingDays* obj = [sectionTitleList1 objectAtIndex:indexPath.row];
    NotificationDetailViewController *vc = [NotificationDetailViewController new];
    vc.farmName =obj.farmName;
    vc.date=obj.addedDateTime;
    vc.comments=obj.notes;
    vc.vaccineName=obj.vaccineName;
    vc.vaccineType=obj.vaccineType;
    vc.region =obj.Region;
    vc.comments =obj.notes;
    vc.scheduleType =  obj.scheduleVaccine;
    vc.vaccineID = obj.scheduleIDPK;
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return UITableViewAutomaticDimension;

}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *deleteText =[appDelegate.dictCulture valueForKey:@"Delete"];
    if(deleteText.length==0)
    {
        deleteText =@"Delete";
        
    }
    return deleteText;
    
}
-(void)noneButtonPressed:(id)sender
{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"Reminders_Tab_From_Home"
                                                           label:@"None_Animal_Vaccinated"
                                                           value:nil] build]];
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSInteger row=[sender tag];
    
    GetMappingDays *obj ;
    
    
    NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
    NSArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
    obj = [sectionTitleList1 objectAtIndex:indexPath.row
           ];
    
    
    ReScheduleViewController *reVaccineVc =[ReScheduleViewController new];
    reVaccineVc.isFrom=@"None";
    reVaccineVc.vaccineID =obj.scheduleIDPK;
    reVaccineVc.animalsIDs=obj.NumberOfAnimals;
    [self.navigationController pushViewController:reVaccineVc animated:YES];
}

-(void)somebuttonPressed:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"Reminders_Tab_From_Home"
                                                           label:@"Some_Animals_Vaccinated"
                                                           value:nil] build]];
    [appDelegate.animalsSelectedArray removeAllObjects];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSInteger row=[sender tag];
    
    GetMappingDays *obj ;
    NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
    NSArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
    obj = [sectionTitleList1 objectAtIndex:indexPath.row
           ];
    
    ReScheduleViewController *reVaccineVc =[ReScheduleViewController new];
    reVaccineVc.isFrom=@"Some";
    reVaccineVc.vaccineID =obj.scheduleIDPK;
    reVaccineVc.farmID =obj.FarmID;
    reVaccineVc.animalsIDs=obj.NumberOfAnimals;
    
    [self.navigationController pushViewController:reVaccineVc animated:YES];
}

-(void)allbuttonPressed:(id)sender
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"Reminders_Tab_From_Home"
                                                           label:@"All_Animals_Vaccinated"
                                                           value:nil] build]];
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSInteger row=[sender tag];
    
    GetMappingDays *obj ;
    NSString *sectionTitle = [sectionHeader objectAtIndex:indexPath.section];
    NSArray *sectionTitleList1 = [sortedNames objectForKey:sectionTitle];
    obj = [sectionTitleList1 objectAtIndex:indexPath.row
           ];
    NSString * messsage =[appDelegate.dictCulture valueForKey:@"ClearReminder"];
    NSString * cancelText =[appDelegate.dictCulture valueForKey:@"Cancel"];
    NSString * okText =[appDelegate.dictCulture valueForKey:@"Ok"];
    
    if (messsage.length == 0)
    {
        messsage =@"Clear Reminder";
    }
    
    if (cancelText.length == 0)
    {
        cancelText =@"cancel";
    }
    
    if (okText.length==0)
    {
        okText =@"OK";
    }
    
    UIAlertController*  alert = [UIAlertController alertControllerWithTitle:nil
                                                                    message:messsage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteScheduleWithID:obj.scheduleIDPK withObj:obj];
    }];
    
    [alert addAction:ok];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:cancelText style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark === Delete Schedule Vaccine Notification===
-(void)deleteSchedule:(NSString *)scheduleID withObj:(GetMappingDays*)object
{
    NSString * message= [appDelegate.dictCulture valueForKey:@"deleteNotificationText"];
    NSString * cancelText =[appDelegate.dictCulture valueForKey:@"No"];
    NSString * proceedText =[appDelegate.dictCulture valueForKey:@"Proceed"];
    
    if (message.length==0)
    {
        message =@"Click “PROCEED” to delete, OR “NO” to CANCEL.";
    }
    
    if (cancelText.length==0)
    {
        cancelText =@"NO";
    }
    
    if (proceedText.length==0)
    {
        proceedText =@"PROCEED";
    }
    
    delete_alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelText style:UIAlertActionStyleDefault handler:nil];
    [delete_alert addAction:cancel];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:proceedText style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteScheduleWithID:scheduleID withObj:object];
    }];
    [delete_alert addAction:ok];
    [self presentViewController:delete_alert animated:YES completion:nil];
}

-(void)deleteScheduleWithID:(NSString*)sID withObj:(GetMappingDays*)object
{
    BovilisApi *obj1 = [BovilisApi sharedInstance];
    appDelegate.reloadCalendar=@"No";
    appDelegate.isLoadedNotifications =@"No";
    [appDelegate.getScheduleVaccineArray removeObject:object];
    
    [obj1 deleteScheduleVaccineFromServerWithID:sID];
    [self arrangeData];
    [self loadDataOfNotifications];
    
    if([isUpcoming isEqualToString:@"No"])
    {
        if(notificationArray.count>0)
        {
            self.tableView.hidden=FALSE;
            _lblTop.hidden=TRUE;
            self.lblHeader.hidden=FALSE;
        }
        else
        {
            self.lblHeader.hidden=TRUE;
            self.tableView.hidden=TRUE;
            _lblTop.hidden=FALSE;
        }
    }
    [self.tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark ==Show Side Menu ==
-(void)loadMenuView
{
    transparentView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIGraphicsBeginImageContext( transparentView.frame.size);
    
    UIGraphicsBeginImageContext( transparentView.frame.size);
    [[UIImage imageNamed:@"bg-Transparent"] drawInRect: transparentView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
   // transparentView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [self.view addSubview:transparentView];
    
    menuView= [[MenuView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-295, 0, self.view.frame.size.width, self.view.frame.size.height-0)];
    CGRect newFrame = CGRectMake(0, 0, menuView.frame.size.width,menuView.frame.size.height);
    menuView.innerView.frame = newFrame;
    
    [menuView.btnClose addTarget:self action:@selector(closeMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnDisclaimer addTarget:self action:@selector(disclaimerMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnPrivacy addTarget:self action:@selector(privacyMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnTerms addTarget:self action:@selector(termsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnSignout addTarget:self action:@selector(signoutMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnCalendar addTarget:self action:@selector(calendarMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnListing addTarget:self action:@selector(listingMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnLibrary addTarget:self action:@selector(libraryMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [menuView.btnNotification addTarget:self action:@selector(notificationMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [menuView.btnAboutUS addTarget:self action:@selector(aboutUsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
       [menuView.btnAnimals addTarget:self action:@selector(animalsMenuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [transparentView addSubview:menuView];
    
}
-(void)showMenu
{
    [self loadMenuView];
    [transparentView setHidden:FALSE];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transparentView.layer addAnimation:transition forKey:nil];
}

-(void)closeMenuButtonPressed
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transparentView.layer addAnimation:transition forKey:nil];
    [transparentView setHidden:YES];
}

-(void)animalsMenuButtonPressed {
    AllAnimalsViewController *settingVC = [AllAnimalsViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}


-(void)aboutUsMenuButtonPressed
{
    AboutUsViewController *settingVC = [AboutUsViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}
-(void)notificationMenuButtonPressed
{
    NotificationViewController *settingVC = [NotificationViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)libraryMenuButtonPressed
{
    LibraryViewController *settingVC = [LibraryViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)listingMenuButtonPressed
{
    ListingViewController *settingVC = [ListingViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)calendarMenuButtonPressed
{
    HomeViewController *settingVC = [HomeViewController new];
    [self.navigationController pushViewController:settingVC animated:YES];
}

-(void)signoutMenuButtonPressed
{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"AllApiDataDictionary"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"IsLogin"];
   // [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SaveUserEmail"];

    [[NSUserDefaults standardUserDefaults] synchronize];
    appDelegate.reloadCalendar=@"No";
    appDelegate.isLoadedNotifications=@"No";
    LoginViewController *loginVC = [LoginViewController new];
    [self.navigationController pushViewController:loginVC animated:YES];
}

-(void)privacyMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Privacy Policy";
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)disclaimerMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Disclaimer";
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)termsMenuButtonPressed
{
    WelcomeDetailViewController *detailVC = [WelcomeDetailViewController new];
    detailVC.isFrom=@"Terms and Conditions";
    [self.navigationController pushViewController:detailVC animated:YES];
}
#pragma mark === UIButton Actions ===

-(IBAction)pastButtonPressed:(id)sender
{
    isUpcoming=@"No";
    [self arrangeData];
    [ _toggleImgView setImage:[UIImage imageNamed:@"toggle-Past"]];
  
    [_btnPast setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnUpcoming setTitleColor:[UIColor colorWithRed:0.0/255.0 green:174.0/255.0 blue:25.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    
    CGRect frame = _lblHeader.frame;
    self.tableView.frame = CGRectMake(0,frame.origin.y+_lblHeader.frame.size.height,self.view.frame.size.width,self.outerView.frame.size.height-_lblHeader.frame.size.height-_innerView.frame.size.height-10);
    [self loadDataOfNotifications];
    [self.tableView reloadData];
    
    if(notificationArray.count>0)
    {
        self.tableView.hidden=FALSE;
        _lblTop.hidden=TRUE;
        self.lblHeader.hidden=FALSE;
    }
    
    else
    {
        self.lblHeader.hidden=TRUE;
        self.tableView.hidden=TRUE;
        _lblTop.hidden=FALSE;
    }
}

-(IBAction)upcomingButtonPressed
{
    self.lblHeader.hidden=TRUE;
    isUpcoming=@"Yes";
    [self arrangeData];
    
    [ _toggleImgView setImage:[UIImage imageNamed:@"toggle-UpComing"]];
    [_btnUpcoming setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnPast setTitleColor:[UIColor colorWithRed:0.0/255.0 green:174.0/255.0 blue:25.0/255.0 alpha:1.0] forState:UIControlStateNormal];

    
    CGRect frame = _innerView.frame;
    self.tableView.frame = CGRectMake(0,frame.origin.y+_innerView.frame.size.height,self.view.frame.size.width,self.outerView.frame.size.height-_innerView.frame.size.height);
    
    if(notificationArray.count>0)
    {
        _tableView.hidden=FALSE;
        _lblTop.hidden=TRUE;
        [self loadDataOfNotifications];
        [self.tableView reloadData];
    }
    else
    {
        _tableView.hidden=TRUE;
        _lblTop.hidden=FALSE;
    }
}

-(IBAction)menuButtonPressed:(id)sender
{
    [self showMenu];
}

-(IBAction)homeButtonPressed:(id)sender
{
    HomeViewController *HomeVc = [HomeViewController new];
    [self.navigationController pushViewController:HomeVc animated:NO];
}

-(IBAction)listButtonPressed:(id)sender
{
    ListingViewController *listVc = [ListingViewController new];
    [self.navigationController pushViewController:listVc animated:NO];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(IBAction)libraryButtonPressed:(id)sender
{
    LibraryViewController *libraryVc = [LibraryViewController new];
    [self.navigationController pushViewController:libraryVc animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
