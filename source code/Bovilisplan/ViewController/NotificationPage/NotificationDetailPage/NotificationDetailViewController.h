//
//  NotificationDetailViewController.h
//  TTVApp
//
//  Created by Apple Macbook on 06/06/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationDetailViewController : UIViewController

@property(nonatomic,strong) NSString *farmName;
@property(nonatomic,strong) NSString *date;
@property(nonatomic,strong) NSString *region;
@property(nonatomic,strong) NSString *comments;
@property(nonatomic,strong) NSString *vaccineName;
@property(nonatomic,strong) NSString *vaccineType;
@property(nonatomic,strong) NSString *scheduleType;
@property(nonatomic,strong) NSString *vaccineID;



@end
