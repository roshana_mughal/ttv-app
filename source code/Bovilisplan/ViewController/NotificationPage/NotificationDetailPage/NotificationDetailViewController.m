//
//  NotificationDetailViewController.m
//  TTVApp
//
//  Created by Apple Macbook on 06/06/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "UserVacineMaster.h"
#import "NotificationDetailViewController.h"
#import "AppDelegate.h"
#import "AnimalsViewController.h"

@interface NotificationDetailViewController ()
{
    AppDelegate * appDelegate;
}

@property(nonatomic,weak) IBOutlet UILabel*lblName;
@property(nonatomic,weak) IBOutlet UITextField*lblReg;
@property(nonatomic,weak) IBOutlet UILabel*lblVaccineName;
@property(nonatomic,strong) IBOutlet UILabel*lblVaccineType;

@property(nonatomic,weak) IBOutlet UILabel*lblVaccineSchedule;
@property(nonatomic,weak) IBOutlet UITextField*lblComments;
@property(nonatomic,weak) IBOutlet UILabel*lblDate;
@property(nonatomic,weak) IBOutlet UIButton *btnViewHistory;
@property(nonatomic,weak) IBOutlet UILabel*lblTitle;
@property(nonatomic,weak) IBOutlet UILabel*lblFarmName;
@property(nonatomic,weak) IBOutlet UITextField *txt1;
@property(nonatomic,weak) IBOutlet UITextField *txt2;
@property(nonatomic,weak) IBOutlet UITextField *txt3;
@property(nonatomic,weak) IBOutlet UITextField *txt4;
@property(nonatomic,strong) IBOutlet UITextView *tView;
@property(nonatomic,strong) IBOutlet UIButton *btnViewAnimal;

@end

@implementation NotificationDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUI];
}

-(void)viewWillLayoutSubviews {

    self.tView.contentInset = UIEdgeInsetsMake(-10.0,0.0,0,0.0);

}

-(void)setUI {
    
   
    appDelegate  = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSString * viewAnimalText = [appDelegate.dictCulture valueForKey:@"ViewAnimals"] ;
     
    viewAnimalText = viewAnimalText.length == 0? @"View Animals":viewAnimalText;
     [self.btnViewAnimal setTitle:viewAnimalText forState:UIControlStateNormal];

    _lblTitle.text = [appDelegate.dictCulture valueForKey:@"NotificationDetail"];
    NSString * lblTitle = [appDelegate.dictCulture valueForKey:@"NotificationDetail"];
    if (lblTitle.length==0)
    {
        _lblTitle.text = @"Notification Detail";
    }
    
    NSDateFormatter *dateFormater= [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *newDate = [dateFormater dateFromString:_date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateFinal = [dateFormat stringFromDate:newDate];
    
    _lblName.text=_scheduleType;
    _lblDate.text= [Utility setLocalDateFormatter: dateFinal];
    _lblReg.text=_region;
    _lblFarmName.text=_farmName;
    _lblVaccineName.text=_vaccineName;
    self.tView.text = _comments;
    NSString *  vaccinType = [appDelegate.dictCulture valueForKey:_vaccineType];
    if (vaccinType.length ==0 )
    {
        if   ([_vaccineType isEqualToString:@"Revaccination"]) {
            vaccinType =@"Revaccination";
        }
        
        else if   ([_vaccineType isEqualToString:@"SecondaryVaccination"]) {
            vaccinType =@"Secondary Vaccination";
        }
        else if   ([_vaccineType isEqualToString:@"PrimaryVaccination"]) {
            vaccinType =@"Primary Vaccination";
        }
        
    }
        _lblVaccineType.text=vaccinType.length == 0 ? _vaccineType :vaccinType  ;
    //_lblVaccineSchedule.text=vaccinType;
    
    if([_comments isEqualToString:@"NA"])    {
        self.tView.text = @"";
        
    }
    if([_vaccineType isEqualToString:@"NA"])
    {
        _lblVaccineType.text=@"";
    }
    
    [_btnViewHistory setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnViewHistory.layer.borderWidth = 1.0f;
    [_btnViewHistory.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnViewHistory setTitle:[appDelegate.dictCulture valueForKey:@"ViewHistory"] forState:UIControlStateNormal];
    
    NSString * btnViewHistory = [appDelegate.dictCulture valueForKey:@"ViewHistory"];
    if (btnViewHistory.length==0)
    {
        [_btnViewHistory setTitle:@"View History" forState:UIControlStateNormal];
    }
    
    NSString *vaccineTitle =[appDelegate.dictCulture valueForKey:@"VaccineTitle"];
    if ([vaccineTitle isEqualToString:@"NA"]  || (vaccineTitle.length == 0))
    {
        _txt1.text=@"Vaccine Title";
    }
    else{
        _txt1.text =vaccineTitle;
    }
    
    NSString *vaccineName =[appDelegate.dictCulture valueForKey:@"VaccineName"];
    if ([vaccineName isEqualToString:@"NA"]  || (vaccineName.length == 0))
    {
        _txt2.text=@"Vaccine Name";
    }
    else
    {
        _txt2.text =vaccineName;
    }
    
    NSString *vaccineType =[appDelegate.dictCulture valueForKey:@"VaccineType"];
    if ([vaccineType isEqualToString:@"NA"]  || (vaccineType.length == 0))
    {
        _txt3.text=@"Vaccine Type";
    }
    else
    {
        _txt3.text =vaccineType;
    }
    
    NSString *notes =[appDelegate.dictCulture valueForKey:@"Notes"];
    if ([notes isEqualToString:@"NA"]  || (notes.length == 0))
    {
        _txt4.text=@"Notes";
    }
    else
    {
        _txt4.text =notes;
    }
}
#pragma mark === UIButton Actions ===
-(IBAction)viewAnimalButtonPressed:(id)sender {
    
    
    AnimalsViewController *vc = [AnimalsViewController new];
    vc.isFrom = @"VaccineDetail";
    vc.vaccineID = self.vaccineID;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
