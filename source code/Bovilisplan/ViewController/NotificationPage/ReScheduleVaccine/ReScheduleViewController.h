//
//  ReScheduleViewController.h
//  TTVApp
//
//  Created by Admin on 9/6/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReScheduleViewController : UIViewController

@property(strong,nonatomic) NSString *isFrom;
@property(strong,nonatomic) NSString *farmID;
@property(strong,nonatomic) NSString *animalsIDs;
@property(strong,nonatomic) NSString *vaccineID;

@end
