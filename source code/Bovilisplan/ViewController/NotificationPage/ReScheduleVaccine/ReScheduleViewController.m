//
//  ReScheduleViewController.m
//  TTVApp
//
//  Created by Admin on 9/6/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//


#import "AppDelegate.h"
#import "AnimalsViewController.h"
#import "ReScheduleViewController.h"
#import "GAITracker.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "BovilisApi.h"
#import "HomeViewController.h"
#import "GetMappingDays.h"
#import <NHNetworkTime/NHNetworkTime.h>


@interface ReScheduleViewController ()<AnimalsViewControllerDelegate>

@property(nonatomic,strong) IBOutlet UILabel*lblTop;
@property(nonatomic,strong) IBOutlet UITextField*lblAnimalID;
@property(nonatomic,strong) IBOutlet UIButton*btnReSchedule;
@property(nonatomic,strong) IBOutlet UIButton*btnSelectAnimal;
@property(nonatomic,strong) IBOutlet UITextField *txtAnimals;
@property(nonatomic,strong) IBOutlet UITextField *txtDate;
@property(nonatomic,strong) IBOutlet UIView *lineView;
@property(nonatomic,strong) IBOutlet UIImageView *imgView;
@property(nonatomic,strong) UIDatePicker *datePicker;
@property(nonatomic,strong) UIToolbar *toolBar;
@property(nonatomic,strong) IBOutlet UITextField *lblDate;


@end

@implementation ReScheduleViewController
{
    AppDelegate *appDelegate;
    NSString *animalsIDSome;
    NSString*addedDateTime;
    UIAlertController*saving_alert;
    UIActivityIndicatorView *activityView;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUI];
    
}
-(void)viewWillAppear:(BOOL)animated {
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)setUI
{
    appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [ self.btnReSchedule.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    if([_isFrom isEqualToString:@"None"])
    {
        _txtAnimals.hidden=TRUE;
        _lblAnimalID.hidden=TRUE;
        _btnSelectAnimal.enabled=FALSE;
        _lineView.hidden=TRUE;
        _imgView.hidden=TRUE;
    }
    
    NSString * dateText = [appDelegate.dictCulture valueForKey:@"SelectANewDate"];
    if (dateText.length==0)
    {
        _txtDate.text = @"Select a new date";
    }
    
    else
    {
        _txtDate.text = dateText;
    }
    NSString * animalText = [appDelegate.dictCulture valueForKey:@"SelectAnimalsToVaccinate"];
    if (animalText.length==0)
    {
        _txtAnimals.text = @"Select animals to vaccinate";
    }
    
    else
    {
        _txtAnimals.text = animalText;
    }
    
    NSString * topText = [appDelegate.dictCulture valueForKey:@"Reschedule"];
    if (topText.length==0)
    {
        _lblTop.text = @"Reschedule?";
        [_btnReSchedule setTitle:@"Reschedule" forState:UIControlStateNormal];

    }
    
    else
    {
        _lblTop.text = [NSString stringWithFormat:@"%@?", topText];
        [_btnReSchedule setTitle:topText forState:UIControlStateNormal];
    }
 
    NSString * animalID = [appDelegate.dictCulture valueForKey:@"AnimalID"];
    if (animalID.length==0)
    {
        _lblAnimalID.text = @"Animal ID";
    }
    
    else
    {
        _lblAnimalID.text = animalID;
    }
    
    NSString * date = [appDelegate.dictCulture valueForKey:@"Date"];
    _lblDate.text = (date.length==0)? @"Date" : date;
   
}

-(void)viewDidLayoutSubviews {
    
  [self showDatePicker];
    
  //  [_datePicker setHidden:YES];
   // [_toolBar setHidden:YES];
}

-(void)selectedNumberOfAnimalsCount:(NSString *)animalsCount
{
    
    NSString * animalSelectedText = [appDelegate.dictCulture valueForKey:@"AnimalSelected"];
    if (animalSelectedText.length==0)
    {
        animalSelectedText = @"animal(s) selected";
    }
    
    else
    {
        animalSelectedText = animalSelectedText;
    }
    
    _txtAnimals.text= [NSString stringWithFormat:@"%@ %@",animalsCount,animalSelectedText];
}

-(void)selectedNumberOfAnimalsCount:(NSString*)animalsCount withAnimalIDs:(NSString*)iDs
{
    
    if(![animalsCount isEqualToString:@"0"])
    {
        animalsIDSome =iDs;
    }
    NSString *str = [Utility setLocalizeString:@"AnimalSelected"];
    
    _txtAnimals.text= str.length != 0 ? [NSString stringWithFormat:@"%@ %@",animalsCount,str] :[NSString stringWithFormat:@"%@ animal(s) selected",animalsCount];
}

#pragma mark === UIButton Actions ===

-(IBAction)rescheduleButtonPressed:(id)sender
{
    NSString * okText =[appDelegate.dictCulture valueForKey:@"Ok"];
    if (okText.length==0)
    {
        okText =@"Ok";
    }
    appDelegate.reloadCalendar=@"No";
    appDelegate.isLoadedNotifications =@"No";
    
    if([_isFrom isEqualToString:@"None"])
    {
        if(addedDateTime.length>0)
        {
            NSString *message3 =[appDelegate.dictCulture valueForKey:@"Schedulingvaccine"];
            
            if(message3.length==0)
            {
                message3 =@"Scheduling a vaccine...";
            }
            
            saving_alert = [UIAlertController alertControllerWithTitle:nil message:message3 preferredStyle:UIAlertControllerStyleAlert];
            [self presentViewController:saving_alert animated:YES completion:nil];
            
            BovilisApi *boviObj=[BovilisApi sharedInstance];
            
            [boviObj updateScheduleVaccine:_vaccineID withNumberOfAnimals:_animalsIDs withAddedTime:addedDateTime   WithSuccessBlock:^(BOOL successVaccine, NSDictionary *resultVaccine) {
                
                if(successVaccine){
                    BovilisApi * objN = [BovilisApi sharedInstance];
                    [objN  getVaccineSchedule:@"NA" WithSuccessBlock:^(BOOL success, NSDictionary *result) {
                        if (success) {
                            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                            [userDefaults setObject:result  forKey:@"ScheduleVaccinesList"];
                            [userDefaults synchronize];
                            GetMappingDays *mapOBj=[[GetMappingDays alloc]init];
                            [mapOBj getScheduleVaccineFromServer];
                            [self loadingVaacines ];
                        }

                    } failure:^(NSError *error, NSString *message) {
                        
                    }];
                }
            } failure:^(NSError *error, NSString *message1) {
                
            }];
        }
        else
        {
            NSString *message =[appDelegate.dictCulture valueForKey:@"Pleaseselectdate"];
            if(message.length==0)
            {
                message =@"Please select date";
            }
            
            UIAlertController*   alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }
    else  if([_isFrom isEqualToString:@"Some"])
    {
        if(addedDateTime.length>0 && animalsIDSome.length>0){
            NSString *message2 =[appDelegate.dictCulture valueForKey:@"Schedulingvaccine"];
            
            if(message2.length==0)
            {
                message2 =@"Scheduling a vaccine...";
            }
            
            saving_alert = [UIAlertController alertControllerWithTitle:nil message:message2 preferredStyle:UIAlertControllerStyleAlert];
            [self presentViewController:saving_alert animated:YES completion:nil];
            
            BovilisApi *boviObj=[BovilisApi sharedInstance];
            [boviObj updateScheduleVaccine:_vaccineID withNumberOfAnimals:animalsIDSome withAddedTime:addedDateTime   WithSuccessBlock:^(BOOL successVaccine, NSDictionary *resultVaccine) {
                
                if(successVaccine)
                {
                    BovilisApi * objN = [BovilisApi sharedInstance];
                    [objN  getVaccineSchedule:@"NA" WithSuccessBlock:^(BOOL success, NSDictionary *result) {
                        if (success) {
                            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                            [userDefaults setObject:result  forKey:@"ScheduleVaccinesList"];
                            [userDefaults synchronize];
                            GetMappingDays *mapOBj=[[GetMappingDays alloc]init];
                            [mapOBj getScheduleVaccineFromServer];
                            [self loadingVaacines ];
                        }
                    } failure:^(NSError *error, NSString *message) {
                    }];
                }
                
            } failure:^(NSError *error, NSString *message1) {
                
            }];
        }
        else if(addedDateTime.length==0)
        {
            NSString *message =[appDelegate.dictCulture valueForKey:@"Pleaseselectdate"];
            if(message.length==0)
            {
                message =@"Please select date";
            }
            
            UIAlertController*   alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        else if(animalsIDSome.length==0)
        {
            NSString *message =[appDelegate.dictCulture valueForKey:@"Pleaseselectanimals"];
            
            if(message.length==0)
            {
                message =@"Please select animals";
            }
            
            UIAlertController*   alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
            
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(void)loadingVaacines
{
    BovilisApi * objN = [BovilisApi sharedInstance];
    
    [objN getVaccineSchedule:@"NA" WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        
        if(success)
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:result  forKey:@"DaysForScheduleVaccine"];
            [userDefaults synchronize];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [activityView stopAnimating];
                [activityView setHidesWhenStopped:YES];
                [self performSelector:@selector(movetoMain) withObject:nil afterDelay:1.0];
            });
        }
        
    } failure:^(NSError *error, NSString *message) {
        
    }];
}

-(void)movetoMain
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"Reminders_Tab_From_Home"
                                                           label:@"Vaccine_Rescheduled"
                                                           value:nil] build]];
    appDelegate.reloadCalendar =@"No";
    [self dismissViewControllerAnimated:YES completion:NULL];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)selectAnimalsButtonPressed:(id)sender
{
    AnimalsViewController *animalVC = [AnimalsViewController new];
    animalVC.delegate=self;
    animalVC.isFrom=@"Reschedule";
    animalVC.farmID=_farmID;
    animalVC.animalIds=_animalsIDs;
    [self.navigationController pushViewController:animalVC animated:YES];
}
-(void)showDatePicker {
    
    
    _datePicker=[[UIDatePicker alloc]init];
                 
    _datePicker.datePickerMode=UIDatePickerModeDate;
    _datePicker.minimumDate = [NSDate networkDate];
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:
                        CGRectMake(0, 0, self.view.frame.size.width, 44)];
    NSString *doneText =[appDelegate.dictCulture valueForKey:@"Done"];
    
    if(doneText.length==0)
    {
        doneText =@"Done";
    }
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:doneText style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:doneBtn, nil]];
    
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    
    // NSArray *toolbarItems = [NSArray arrayWithObjects:    doneBtn, nil];
    ///  [toolBar setItems:toolbarItems];
    
    self.txtDate.inputAccessoryView = toolBar;
    [self.txtDate setInputView:_datePicker];
    
}

-(IBAction)selectDateButtonPressed:(id)sender
{
    [_datePicker setHidden:NO];
    [_toolBar setHidden:NO];
}

-(IBAction)done:(id)sender
{
    NSDateFormatter *dateFormatC=[[NSDateFormatter alloc]init];
    [dateFormatC setDateFormat:@"yyyy-MM-dd"];
    addedDateTime=[NSString stringWithFormat:@"%@",[dateFormatC  stringFromDate:_datePicker.date]];
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:_datePicker.date]];
    
    _txtDate.text=    [Utility setLocalDateFormatter:str];
    [self.txtDate resignFirstResponder];

}
-(BOOL)checkAnimalIDIsExistence:(NSString *)animalID {
    
    return true;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
       return  NO;
   
}
@end
