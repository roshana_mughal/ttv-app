//
//  AboutUsViewController.h
//  TTVApp
//
//  Created by Apple Macbook on 20/07/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "GAITrackedViewController.h"

@interface AboutUsViewController : GAITrackedViewController <MFMailComposeViewControllerDelegate>
@property(nonatomic,retain) IBOutlet UIButton * emailButton;
-(IBAction)emailButtionAction;
@end
