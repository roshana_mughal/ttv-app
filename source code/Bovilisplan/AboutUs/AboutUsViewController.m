//
//  AboutUsViewController.m
//  TTVApp
//
//  Created by Apple Macbook on 20/07/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//
#import "AppDelegate.h"
#import "LinkViewController.h"
#import "AboutUsViewController.h"

@interface AboutUsViewController ()
{
    AppDelegate* appDelegate;
}
@property(nonatomic,strong) IBOutlet UILabel*lblTitle;
@property(nonatomic,strong) IBOutlet UILabel*lblAddress1;
@property(nonatomic,strong) IBOutlet UILabel*lblAddress2;
@property(nonatomic,strong) IBOutlet UILabel*lblAddress3;
@property(nonatomic,strong) IBOutlet UILabel*lblCompanyWebsite;
@property(nonatomic,strong) IBOutlet UILabel*lblCompanyName;
@end

@implementation AboutUsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [_lblTitle setText:[appDelegate.dictCulture valueForKey:@"AboutUs"]];
    NSString * lblTitle =[appDelegate.dictCulture valueForKey:@"AboutUs"];
    if (lblTitle.length==0)
    {
        _lblTitle.text=@"About Us";
    }
    
    [_lblCompanyName setText:[appDelegate.dictCulture valueForKey:@"CompanyName"]];
    NSString * lblCompanyName =[appDelegate.dictCulture valueForKey:@"CompanyName"];
    if (lblCompanyName.length==0)
    {
        _lblCompanyName.text=@"MSD Animal Health";
    }
    
    [_lblAddress1 setText:[appDelegate.dictCulture valueForKey:@"AddressLine1"]];
    NSString * lblAddress1 =[appDelegate.dictCulture valueForKey:@"AddressLine1"];
    if (lblAddress1.length==0)
    {
        _lblAddress1.text=@"2 Giralda Farms";
    }
    
    [_lblAddress2 setText:[appDelegate.dictCulture valueForKey:@"AddressLine2"]];
    NSString * lblAddress2 =[appDelegate.dictCulture valueForKey:@"AddressLine2"];
    if (lblAddress2.length==0)
    {
        _lblAddress2.text=@"Madison, NJ 07940";
    }
    
    [_lblAddress3 setText:[appDelegate.dictCulture valueForKey:@"AddressLine3"]];
    NSString * lblAddress3 =[appDelegate.dictCulture valueForKey:@"AddressLine3"];
    if (lblAddress3.length==0)
    {
        _lblAddress3.text=@"United States";
    }
   
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"About Us";
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(IBAction)webSiteButtonPressed:(id)sender
{
    LinkViewController *linkVC = [LinkViewController new];
    linkVC.isFrom=@"About";
    [self.navigationController pushViewController:linkVC animated:YES];
}

-(IBAction)emailButtionAction
{
    MFMailComposeViewController *emailBody;
    emailBody =[[MFMailComposeViewController alloc]init];
    emailBody.mailComposeDelegate = self;
    
    if ([MFMailComposeViewController canSendMail])
    {
        emailBody.mailComposeDelegate = self;
        [emailBody setSubject:@""];
        [emailBody setMessageBody:@"" isHTML:YES];
        NSArray *toRecipents = [NSArray arrayWithObject:@"animal-health-communications@merck.com"];
        [emailBody setToRecipients:toRecipents];
        [self presentViewController:emailBody animated:YES completion:NULL];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
