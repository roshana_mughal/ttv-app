//
//  NotificationCell.m
//  TTVApp
//
//  Created by Apple Macbook on 06/06/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//
#import "AppDelegate.h"
#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication ]delegate];
    
    NSString * Title=[appDelegate.dictCulture valueForKey:@"Title"];
    if (Title.length==0)
    {
        _lblTitle.text = [NSString stringWithFormat:@"%@:",@"Title"];
    }
    else
    {
        _lblTitle.text  = [NSString stringWithFormat:@"%@:",[appDelegate.dictCulture valueForKey:@"Title"]];
    }
    
    NSString * vaccineName=[appDelegate.dictCulture valueForKey:@"VaccineName"];
    if (vaccineName.length==0)
    {
        _lblVaccineName.text = [NSString stringWithFormat:@"%@:",@"Vaccine Name"];
        
    }
    else
    {
        _lblVaccineName.text  = [NSString stringWithFormat:@"%@:",[appDelegate.dictCulture valueForKey:@"VaccineName"]];
    }
    
    NSString * FarmName=[appDelegate.dictCulture valueForKey:@"FarmName"];
    if (FarmName.length==0)
    {
        _lblFarmName.text = [NSString stringWithFormat:@"%@:",@"Farm Name"];
    }
    else
    {
        _lblFarmName.text  = [NSString stringWithFormat:@"%@:",[appDelegate.dictCulture valueForKey:@"FarmName"]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
