//
//  AnimalFarmCell.h
//  TTVApp
//
//  Created by apple on 18/12/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VaccineFarmCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel* lbl1;
@property(nonatomic,weak)IBOutlet UILabel* lbl2;
@property(nonatomic,weak)IBOutlet UILabel* lbl3;

@property(nonatomic,weak)IBOutlet UILabel* lblDate;
@property(nonatomic,weak)IBOutlet UILabel* lblDay;

@end

NS_ASSUME_NONNULL_END
