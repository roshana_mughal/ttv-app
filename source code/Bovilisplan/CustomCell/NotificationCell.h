//
//  NotificationCell.h
//  TTVApp
//
//  Created by Apple Macbook on 06/06/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UILabel*lbl1;
@property(nonatomic,strong) IBOutlet UILabel*lbl2;
@property(nonatomic,strong) IBOutlet UILabel*lbl3;
@property(nonatomic,strong) IBOutlet UILabel*lblFarmName;
@property(nonatomic,strong) IBOutlet UILabel*lblTitle;
@property(nonatomic,strong) IBOutlet UILabel*lblVaccineName;

@end
