//
//  OldNotificationCell.h
//  TTVApp
//
//  Created by Admin on 9/6/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface OldNotificationCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UILabel*lbl1;
@property(nonatomic,strong) IBOutlet UILabel*lbl2;
@property(nonatomic,strong) IBOutlet UILabel*lbl3;
@property(nonatomic,strong) IBOutlet UILabel*lbl4;
@property(nonatomic,strong) IBOutlet UILabel*lblTitle;
@property(nonatomic,strong) IBOutlet UILabel*lblFarmName;
@property(nonatomic,strong) IBOutlet UILabel*lblDate;
@property(nonatomic,strong) IBOutlet UILabel*lblVaccineName;
@property(nonatomic,strong) IBOutlet UIView*buttonView;
@property(nonatomic,strong) IBOutlet UIButton*btnAll;
@property(nonatomic,strong) IBOutlet UIButton*btnSome;
@property(nonatomic,strong) IBOutlet UIButton*btnNone;

@end
