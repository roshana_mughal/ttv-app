//
//  MenuCell.h
//  Bovilisplan
//
//  Created by Admin on 5/12/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UILabel *lblTitle;
@property(nonatomic,strong) IBOutlet UIImageView *menuImage;

@end
