//
//  AnimalFarmCell.h
//  TTVApp
//
//  Created by apple on 18/12/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AnimalListViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel* lblVaccineName;
@property(nonatomic,weak)IBOutlet UILabel* lblAnimalIDs;

@property(nonatomic,weak)IBOutlet UILabel* lblDate;
@property(nonatomic,weak)IBOutlet UILabel* lblDay;

@end

NS_ASSUME_NONNULL_END
