//
//  OldNotificationCell.m
//  TTVApp
//
//  Created by Admin on 9/6/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//


#import "AppDelegate.h"
#import "OldNotificationCell.h"

@implementation OldNotificationCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    AppDelegate *appDelegate= (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    self.buttonView.layer.cornerRadius= 18.0;
    self.buttonView.layer.borderWidth = 1.0f;
    [ self.buttonView.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    NSString * some = [appDelegate.dictCulture valueForKey:@"Some"];
    if (some.length==0)
    {
        [self.btnSome setTitle:@"Some" forState:UIControlStateNormal];
    }
    else
    {
        [self.btnSome setTitle:some forState:UIControlStateNormal];

    }
    
    NSString * none = [appDelegate.dictCulture valueForKey:@"None"];
    if (none.length==0)
    {
        [self.btnNone setTitle:@"None" forState:UIControlStateNormal];

    }
    else
    {
        [self.btnNone setTitle:none forState:UIControlStateNormal];

    }
    
    NSString * all = [appDelegate.dictCulture valueForKey:@"All"];
    if (all.length==0)
    {
        [self.btnAll setTitle:@"All" forState:UIControlStateNormal];

    }
    else
    {
        _btnAll.titleLabel.text = all;
        [self.btnAll setTitle:all forState:UIControlStateNormal];

    }
    
    NSString * title = [appDelegate.dictCulture valueForKey:@"Title"];
    if (title.length==0)
    {
        _lblTitle.text = [NSString stringWithFormat:@"%@:",@"Title"];
    }
    else
    {
        _lblTitle.text =[NSString stringWithFormat:@"%@:",title];
    }
    
    NSString * farmName = [appDelegate.dictCulture valueForKey:@"FarmName"];
    if (farmName.length==0)
    {
        _lblFarmName.text = [NSString stringWithFormat:@"%@:",@"Farm Name"];
    }
    else
    {
        _lblFarmName.text = [NSString stringWithFormat:@"%@:",farmName];
    }
    
    NSString * vaccineName = [appDelegate.dictCulture valueForKey:@"VaccineName"];
    if (vaccineName.length==0)
    {
        _lblVaccineName.text =[NSString stringWithFormat:@"%@:",@"Vaccine Name"];
    }
    else
    {
        _lblVaccineName.text =[NSString stringWithFormat:@"%@:",vaccineName] ;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
