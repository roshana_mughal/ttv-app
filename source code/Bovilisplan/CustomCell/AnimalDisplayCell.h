//
//  AnimalDisplayCell.h
//  TTVApp
//
//  Created by apple on 17/10/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AnimalDisplayCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UILabel*lblDate;
@property(nonatomic,strong) IBOutlet UILabel*lblVaccine;


@end

NS_ASSUME_NONNULL_END
