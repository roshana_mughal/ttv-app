//
//  EventCell.h
//  Bovilisplan
//
//  Created by Admin on 5/22/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UILabel *lblTitle;
@property(nonatomic,strong) IBOutlet UILabel *lblSubTitle;
@property(nonatomic,strong) IBOutlet UIImageView *imgV;

@end
