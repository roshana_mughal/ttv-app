//
//  AddEventCell.m
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "AddEventCell.h"

@implementation AddEventCell
{
    NSMutableArray *dataAraay;
    AppDelegate *appDelegate ;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    _btnDD2.hidden=TRUE;
    _lblDD2.hidden=TRUE;
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.isSwitchOn=@"No";
    
    NSString * txtAddVaccine=[appDelegate.dictCulture valueForKey:@"AddVaccine"];
    if (txtAddVaccine.length==0)
    {
        _txtAddvaccine.placeholder = [NSString stringWithFormat:@"%@",@"Add Vaccine*"];
    }
    else
    {
        _txtAddvaccine.placeholder = [NSString stringWithFormat:@"%@*",[appDelegate.dictCulture valueForKey:@"AddVaccine"]];
    }
    
    
    NSString *manual =[appDelegate.dictCulture valueForKey:@"Manual"];
    if(manual.length==0)
    {
        manual =@"Manual";
    }
    self.lblManual.text =manual;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (IBAction)changeSwitch:(UISwitch*)sender
{
    if([sender isOn])
    {
        NSString * SelectASchedule = [appDelegate.dictCulture valueForKey:@"SelectASchedule"];
        if (SelectASchedule.length==0)
        {
            _lblDD2.text = @"Select A Schedule";
        }
        else
        {
            _lblDD2.text = [appDelegate.dictCulture valueForKey:@"SelectASchedule"];
        }
        _btnDD2.hidden=FALSE;
        _lblDD2.hidden=FALSE;
        appDelegate.isSwitchOn=@"Yes";
    }
    else
    {
        _btnDD2.hidden=TRUE;
        _lblDD2.hidden=TRUE;
        appDelegate.isSwitchOn=@"No";
    }
}

@end
