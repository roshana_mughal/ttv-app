//
//  DateCell.h
//  TTVApp
//
//  Created by apple on 17/12/2019.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DateCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel* lblVaccine;
@property(nonatomic,weak)IBOutlet UILabel* lblDate;
@property(nonatomic,weak)IBOutlet UILabel* lblFarm;
@property(nonatomic,weak)IBOutlet UILabel* lblDay;


@end

NS_ASSUME_NONNULL_END
