//
//  AnimalsCell.m
//  TTVApp
//
//  Created by Admin on 9/6/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "AnimalsCell.h"

@implementation AnimalsCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    AppDelegate *appDelegate= (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString * gender = [appDelegate.dictCulture valueForKey:@"Gender"];
    if (gender.length==0)
    {
        _lblGender.text = [NSString stringWithFormat:@"%@:",@"Gender"];
    }
    else
    {
        _lblGender.text =  [NSString stringWithFormat:@"%@:",gender];
    }
    
    NSString * birthDate = [appDelegate.dictCulture valueForKey:@"BirthDate"];
    if (birthDate.length==0)
    {
        _lblDate.text  = [NSString stringWithFormat:@"%@:",@"Birth Date"];
    }
    else
    {
        _lblDate.text = [NSString stringWithFormat:@"%@:",birthDate];
    }
}

@end
