//
//  AddEventCell.h
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddEventCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UITableView *subMenuTableView;
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(nonatomic,strong) IBOutlet UITextField *lblDD1;
@property(nonatomic,strong) IBOutlet UITextField *lblDD2;
@property(nonatomic,strong) IBOutlet UIButton *btnDD1;
@property(nonatomic,strong) IBOutlet UIButton *btnDD2;
@property(nonatomic,strong) IBOutlet UITextField *lblNoOfCattles;
@property(strong,nonatomic) IBOutlet UISwitch *switch1;
@property(strong,nonatomic) IBOutlet UITextField *txtAddvaccine;
@property(strong,nonatomic) IBOutlet UITextField *lblManual;
@end
