//
//  AnimalsCell.h
//  TTVApp
//
//  Created by Admin on 9/6/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnimalsCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UILabel*lbl1;
@property(nonatomic,strong) IBOutlet UILabel*lbl2;
@property(nonatomic,strong) IBOutlet UILabel*lbl3;
@property(nonatomic,strong) IBOutlet UILabel*lblID;
@property(nonatomic,strong) IBOutlet UILabel*lblDate;
@property(nonatomic,strong) IBOutlet UILabel*lblGender;
@property(nonatomic,strong) IBOutlet UIImageView *imgView;
@end
