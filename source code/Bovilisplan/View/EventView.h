//
//  EventView.h
//  Bovilisplan
//
//  Created by Admin on 4/25/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//




#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@protocol EventViewDelegate <NSObject>

@optional
- (void)selectedFromEventView:(NSString *)back ;
@end

@interface EventView : UIView<UITableViewDelegate,UITableViewDataSource>
{
    UILabel *subLabel;
    UIImageView *image1;
    UIImageView *image2;
    UIImageView *image3;
    float labelPadding;
}

@property(nonatomic, assign) id <EventViewDelegate> delegate;
@property(strong,nonatomic) NSString *textHeader;
@property(strong,nonatomic) IBOutlet UIView *innerView;
@property(strong,nonatomic) IBOutlet UIButton *btnClose;
@property(strong,nonatomic) IBOutlet UIButton *btnAdd;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property(nonatomic,strong) IBOutlet UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *dataArray;

@end
