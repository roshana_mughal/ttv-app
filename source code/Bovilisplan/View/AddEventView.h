//
//  AddEventView.h
//  Bovilisplan
//
//  Created by Admin on 4/25/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface AddEventView : UIView<UITableViewDelegate,UITableViewDataSource>
{
    UILabel *lblHeader;
    UILabel *subLabel;
    UIImageView *image1;
    UIImageView *image2;
    UIImageView *image3;
    float labelPadding;
    NSMutableArray  *arrayForBool;
    NSArray *sectionTitleArray;
}

@property(strong,nonatomic) IBOutlet UITextField *txtDate;
@property(strong,nonatomic) IBOutlet UITextField *txtFarmName;
@property(strong,nonatomic) IBOutlet UITextField *txtAnimalNo;
@property(strong,nonatomic) IBOutlet UITextField *txtAddvaccine;
@property(strong,nonatomic) IBOutlet UITextField *txtNoOfVaccine;
@property(strong,nonatomic) IBOutlet UITextField *txtNotes;
@property(strong,nonatomic) IBOutlet UITextField *txtPhotos;
@property(strong,nonatomic) IBOutlet UILabel *lblTop;
@property(strong,nonatomic) IBOutlet UITextField *txtSelectSchedule;
@property(strong,nonatomic) IBOutlet UITextField *txtVaccineName;
@property(strong,nonatomic) IBOutlet UISwitch *switch1;
@property(strong,nonatomic) NSString *textHeader;
@property(strong,nonatomic) UIButton *closeButton;
@property(strong,nonatomic) UIButton *addButton;
@property(strong,nonatomic) IBOutlet UIView *innerView;
@property(strong,nonatomic) IBOutlet UIButton *btnCancel;
@property(strong,nonatomic) IBOutlet UIButton *btnDone;
@property(strong,nonatomic) IBOutlet UIButton *btnAddPhotos;
@property(strong,nonatomic) IBOutlet UIScrollView *scrollView;
@property(strong,nonatomic) IBOutlet UIScrollView *outerScrollView;
@property(nonatomic,strong) IBOutlet UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(strong,nonatomic) IBOutlet UIButton *btnAnimalID;

@end
