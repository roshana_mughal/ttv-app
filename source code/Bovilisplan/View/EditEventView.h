//
//  EditEventView.h
//  TTVApp
//
//  Created by Admin on 8/16/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditEventView : UIView <UITextFieldDelegate>

@property(strong,nonatomic) IBOutlet UILabel *lblVaccineName;
@property(strong,nonatomic) IBOutlet UITextField *txtNotes;
@property(strong,nonatomic) IBOutlet UIView *innerView;
@property(strong,nonatomic) IBOutlet UIButton *btnCancel;
@property(strong,nonatomic) IBOutlet UIButton *btnSave;
@property(strong,nonatomic) IBOutlet UITextField *lblNotes;

@end
