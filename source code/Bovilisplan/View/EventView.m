//
//  EventView.m
//  Bovilisplan
//
//  Created by Admin on 4/25/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "EventView.h"
#import "EventDetailViewController.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "EventCell.h"
#import "BovilisApi.h"
#import "UserVacineMaster.h"
#import "GetDaysData.h"

@interface EventView()
{
    AppDelegate *appDelegate;
    EventDetailViewController *eventDetailViewController;
    HomeViewController *homeViewController;
    NSString*date;
}

@property(strong,nonatomic) IBOutlet UIView *contentView;

@end

@implementation EventView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initializeSubviews];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if(self)
    {
        [self initializeSubviews];
    }
    
    return self;
}

-(void)initializeSubviews
{
    appDelegate.removeView = @"False";
    
    [[NSBundle mainBundle] loadNibNamed:@"EventView" owner:self options:nil];
    self.contentView.frame =self.bounds;
    appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [_btnAdd setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnAdd.layer.borderWidth = 1.0f;
    [_btnAdd.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    [_btnClose setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnClose.layer.borderWidth = 1.0f;
    
    [_btnClose.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    [self configureTableViewCell];
    
    NSString * btnAdd =[appDelegate.dictCulture valueForKey:@"Cancel"];
    
    if (btnAdd.length==0)
    {
        [_btnAdd setTitle:@"Add" forState:UIControlStateNormal];
    }
    else
    {
        [_btnAdd setTitle:[appDelegate.dictCulture valueForKey:@"Add"] forState:UIControlStateNormal];
    }
    
    NSString * btnClose=[appDelegate.dictCulture valueForKey:@"Close"];
    if (btnClose.length==0)
    {
        [_btnClose setTitle:@"Close" forState:UIControlStateNormal];
    }
    
    else
    {
        [_btnClose setTitle:[appDelegate.dictCulture valueForKey:@"Close"] forState:UIControlStateNormal];
    }
    [self addSubview:self.contentView];
}

#pragma mark === Configure TableView Cell===
-(void)configureTableViewCell
{
    [_tableView registerNib:[UINib nibWithNibName:@"EventCell"
                                           bundle:nil] forCellReuseIdentifier:@"EventCell"];
}

#pragma mark === TableView Delegates/Datasource===
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appDelegate.getVaccinesWithDate.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"EventCell";
    EventCell *cell = (EventCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    GetDaysData *obj = [appDelegate.getVaccinesWithDate objectAtIndex:indexPath.row];
    cell.lblTitle.text = obj.farmName1;
    cell.lblSubTitle.text =obj.scheduleType1;
    
    if([obj.scheduleType1 isEqualToString:@"NA"])
    {
        cell.lblSubTitle.text=@"";
    }
    cell.lblTitle.font = [UIFont fontWithName:@"UNIVERS" size:16.0] ;
    cell.lblSubTitle.font = [UIFont fontWithName:@"UNIVERS" size:15.0] ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *title;
    NSString *subTitle;
    NSString *type;
    
    appDelegate= (AppDelegate*)[[UIApplication sharedApplication]delegate];
    GetDaysData *obj = [appDelegate.getVaccinesWithDate objectAtIndex:indexPath.row];
    appDelegate.vaccineName=obj.vaccineName1;
    appDelegate.vaccineType =obj.vaccinationType1;
    appDelegate.vaccineDate =date;
    appDelegate.vaccineRegion =obj.region1;
    appDelegate.vaccineComments =obj.notes1;
    appDelegate.farmName=obj.farmName1;
    appDelegate.scheduleVaccine=obj.scheduleType1;
    appDelegate.region=obj.region1;
    appDelegate.image1 =obj.image1;
    appDelegate.vaccineIDforEdit =obj.vaccineIDforEdit;
    appDelegate.vaccineNumberOfAnimals=obj.numberOfAnimals1;
    appDelegate.removeView = @"True";
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"TestNotification"
                                                       object:self];
}

-(void)setTextHeader:(NSString *)texteader
{
    date=[Utility setLocalDateFormatter:texteader];
    self.lblHeader.text =date;
}


@end
