//
//  AddNewFarmView.h
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewFarmView : UIView



@property(strong,nonatomic) IBOutlet UIButton *btnCancel;
@property(strong,nonatomic) IBOutlet UIButton *btnSave;
@property(strong ,nonatomic)IBOutlet UIView *innerView;


@property(nonatomic, strong) IBOutlet UILabel *lblTop;
@property(strong ,nonatomic)IBOutlet UITextField *txtFarmName;
@property(strong ,nonatomic)IBOutlet UITextField *txtRegistration;
@property(strong ,nonatomic)IBOutlet UITextField *txtReigon;
@property(strong ,nonatomic)IBOutlet UITextField *txtContactPerson;
@property(strong ,nonatomic)IBOutlet UITextField *txtAnimals;
@property(strong ,nonatomic)IBOutlet UITextField *txtAddress;

@property(strong ,nonatomic)IBOutlet UITextField *txtPhone;
@property(strong ,nonatomic)IBOutlet UITextField *txtEmail;
@property(strong ,nonatomic)IBOutlet UITextField *txtComment;
@end

