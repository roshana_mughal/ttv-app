//
//  MenuView.h
//  Bovilisplan
//
//  Created by Admin on 5/12/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuView : UIView

@property(strong ,nonatomic)IBOutlet UIView *innerView;
@property(strong ,nonatomic)IBOutlet  UIButton * btnClose;
@property(nonatomic, strong)IBOutlet  UITableView *tableView;
@property(strong ,nonatomic)IBOutlet  UIButton * btnSetting;
@property(strong ,nonatomic)IBOutlet  UIButton * btnDisclaimer;
@property(strong ,nonatomic)IBOutlet  UIButton * btnPrivacy;
@property(strong ,nonatomic)IBOutlet  UIButton * btnTerms;
@property(strong ,nonatomic)IBOutlet  UIButton * btnSignout;
@property(strong ,nonatomic)IBOutlet  UIButton * btnListing;
@property(strong ,nonatomic)IBOutlet  UIButton * btnCalendar;
@property(strong ,nonatomic)IBOutlet  UIButton * btnNotification;
@property(strong ,nonatomic)IBOutlet  UIButton * btnLibrary;
@property(strong ,nonatomic)IBOutlet  UIButton * btnAboutUS;
@property(strong ,nonatomic)IBOutlet  UIButton * btnAnimals;


@end
