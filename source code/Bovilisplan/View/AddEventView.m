//
//  AddEventView.m
//  Bovilisplan
//
//  Created by Admin on 4/25/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "AddEventView.h"
#import "ProductMaster.h"
#import "AddEventCell.h"
#import "UserFarmMasterModel.h"
#import "AppDelegate.h"

@interface AddEventView ()<UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>
@property(strong,nonatomic)IBOutlet UIView *contentView;
@property(strong,nonatomic)IBOutlet UIView *bottomView;
@property(strong,nonatomic)IBOutlet UIPickerView *pickerView;
@property(strong,nonatomic)IBOutlet  UIToolbar *toolBar;
@property(strong,nonatomic)IBOutlet UIButton *btnFarm;
@property(strong,nonatomic)IBOutlet UIButton *btnAnimals;
@end

@implementation AddEventView
{
    int variabla;
    NSMutableArray* pickerArray1;
    NSMutableArray* pickerArray2;
    NSIndexPath *rowIndex;
    NSInteger row1;
    NSString *isPicker;
    UIPickerView *myPickerView;
    UIPickerView *myPickerViewFarm;
    AppDelegate *appDelegate;
    NSString*isSwitchOn;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initializeSubviews];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    
    if(self)
    {
        [self initializeSubviews];
    }
    return self;
}

-(void)initializeSubviews
{
    [[NSBundle mainBundle] loadNibNamed:@"AddEventView" owner:self options:nil];
    
    self.contentView.frame =self.bounds;
    appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    [self showPickview];
    isSwitchOn =@"No";
    [_outerScrollView setContentSize:CGSizeMake( _outerScrollView.frame.size.width, 700)];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row1 inSection:0];
    appDelegate.selectdVaccineName=@"";
    appDelegate.selectedVaccineType=@"";
    [_btnDone setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnDone.layer.borderWidth = 1.0f;
    [_btnDone.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnCancel setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnCancel.layer.borderWidth = 1.0f;
    
    [_btnCancel.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    AddEventCell *cell1 = (AddEventCell*)[_tableView cellForRowAtIndexPath:indexPath];
    cell1.btnDD2.hidden=TRUE;
    cell1.lblDD2.hidden=TRUE;
    NSString * reVac =[appDelegate.dictCulture valueForKey:@"Revaccination"];
    if (reVac.length ==0)
    {
    pickerArray2 = [[NSMutableArray alloc] initWithObjects:@"Primary Vaccination",@"Secondary Vaccination ",@"Revaccination", nil];
       
    }
    else{
 
        pickerArray2 = [NSMutableArray new];
        [pickerArray2 addObject:[appDelegate.dictCulture valueForKey:@"PrimaryVaccination"]];
        [pickerArray2 addObject:[appDelegate.dictCulture valueForKey:@"SecondaryVaccination"]];
        [pickerArray2 addObject:reVac];
 
    }  
    
    arrayForBool=[[NSMutableArray alloc]init];
    _dataArray = [[NSMutableArray alloc]init];
    
    [ _dataArray addObject:@"Primary Vaccination"];
    [_pickerView setHidden:YES];
    [_pickerView setHidden:YES];
    [_toolBar setHidden:YES];
    
    [self changeLanguage];
    [self configureTableViewCell];
    
    if(appDelegate.getUserFarmsFromServer.count==1)
    {
        UserFarmMasterModel * obj  = [[UserFarmMasterModel alloc] init];
        obj =[appDelegate.getUserFarmsFromServer objectAtIndex:0];
        
        NSString * titleForRow = obj.farmName;
        appDelegate.uniqueFarmID = obj.farmID;
        _txtFarmName.text =  titleForRow;
    }
    
    [self addSubview:self.contentView];
    self.contentView.frame =self.bounds;
}

-(void)changeLanguage
{
    NSString * btnCacel =[appDelegate.dictCulture valueForKey:@"Cancel"];
    if (btnCacel.length==0)
    {
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    else
    {
        [_btnCancel setTitle:[appDelegate.dictCulture valueForKey:@"Cancel"] forState:UIControlStateNormal];
    }
    
    NSString * btnDone=[appDelegate.dictCulture valueForKey:@"Save"];
    if (btnDone.length==0)
    {
        [_btnDone setTitle:@"SAVE" forState:UIControlStateNormal];
    }
    else
    {
        [_btnDone setTitle:[appDelegate.dictCulture valueForKey:@"Save"] forState:UIControlStateNormal];
    }
    
    NSString * lblTop=[appDelegate.dictCulture valueForKey:@"AddToCalendar"];
    if (btnDone.length==0)
    {
        _lblTop.text=@"Add To Calendar";
    }
    else
    {
        [_lblTop setText:[appDelegate.dictCulture valueForKey:@"AddToCalendar"]];
        
    }
    
    NSString * txtDate=[appDelegate.dictCulture valueForKey:@"Date"];
    
    if (txtDate.length==0)
    {
        _txtDate.placeholder = [NSString stringWithFormat:@"%@%@",@"Date", @"*"];
    }
    else
    {
        _txtDate.placeholder = [NSString stringWithFormat:@"%@%@",[appDelegate.dictCulture valueForKey:@"Date"], @"*"];
    }
    
    NSString * txtFarmName=[appDelegate.dictCulture valueForKey:@"FarmName"];
    
    if (txtFarmName.length==0)
    {
        _txtFarmName.placeholder = [NSString stringWithFormat:@"%@%@",@"Farm Name", @"*"];
    }
    else
    {
        _txtFarmName.placeholder = [NSString stringWithFormat:@"%@%@", [appDelegate.dictCulture valueForKey:@"FarmName"], @"*"];
    }
    
    
    NSString * txtVaccineName=[appDelegate.dictCulture valueForKey:@"Title"];
    if (txtVaccineName.length==0)
    {
        _txtVaccineName.placeholder = [NSString stringWithFormat:@"%@",@"Title*"];
    }
    else
    {
        _txtVaccineName.placeholder = [NSString stringWithFormat:@"%@%@",[appDelegate.dictCulture valueForKey:@"Title"],@"*"];
    }
    
    NSString * txtAnimalNo=[appDelegate.dictCulture valueForKey:@"AnimalID"];
    
    if (txtAnimalNo.length==0)
    {
        _txtAnimalNo.placeholder = [NSString stringWithFormat:@"%@%@",@"Animal ID",@"*"];
    }
    else
    {
        _txtAnimalNo.placeholder = [NSString stringWithFormat:@"%@%@",[appDelegate.dictCulture valueForKey:@"AnimalID"],@"*"];
        
        
    }
    
    NSString * txtAddVaccine=[appDelegate.dictCulture valueForKey:@"AddVaccine"];
    
    if (txtAddVaccine.length==0)
    {
        _txtAddvaccine.placeholder = [NSString stringWithFormat:@"%@",@"Add Vaccine"];
    }
    else
    {
        _txtAddvaccine.placeholder = [appDelegate.dictCulture valueForKey:@"AddVaccine"];
    }
    
    NSString * txtNoOfVaccine=[appDelegate.dictCulture valueForKey:@"NumberOfVaccinatedCattle"];
    
    if (txtNoOfVaccine.length==0)
    {
        _txtNoOfVaccine.placeholder = [NSString stringWithFormat:@"%@",@"Number Of Vaccinated Cattle"];
    }
    else
    {
        _txtNoOfVaccine.placeholder = [appDelegate.dictCulture valueForKey:@"NumberOfVaccinatedCattle"];
    }
    
    NSString * txtNotes=[appDelegate.dictCulture valueForKey:@"Notes"];
    if (txtNotes.length==0)
    {
        _txtNotes.placeholder = [NSString stringWithFormat:@"%@",@"Notes"];
    }
    else
    {
        _txtNotes.placeholder =[appDelegate.dictCulture valueForKey:@"Notes"];
    }
    
    NSString * txtPhotos=[appDelegate.dictCulture valueForKey:@"Photo"];
    
    if (txtPhotos.length==0)
    {
        _txtPhotos.placeholder = [NSString stringWithFormat:@"%@",@"Photo"];
    }
    else
    {
        _txtPhotos.placeholder =[appDelegate.dictCulture valueForKey:@"Photo"];
    }
    
    NSString * txtSelectedSchedule=[appDelegate.dictCulture valueForKey:@"SelectASchedule"];
    
    if (txtSelectedSchedule.length==0)
    {
        _txtSelectSchedule.placeholder = [NSString stringWithFormat:@"%@",@"Select A Schedule"];
    }
    
    else
    {
        _txtSelectSchedule.placeholder =[appDelegate.dictCulture valueForKey:@"SelectASchedule"];
    }
}

-(void)setTextHeader:(NSString *)texteader
{
    lblHeader.text =texteader;
}

#pragma mark === Configure TableView Cell===
-(void)configureTableViewCell
{
    [_tableView registerNib:[UINib nibWithNibName:@"AddEventCell"
                                           bundle:nil] forCellReuseIdentifier:@"AddEventCell"];
}

#pragma mark === TableView Delegates/Datasource===
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"AddEventCell";
    AddEventCell *cell = (AddEventCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSString * ScheduleAVaccine = [appDelegate.dictCulture valueForKey:@"ScheduleAVaccine"];
    
    if (ScheduleAVaccine.length==0)
    {
        cell.lblDD1.text = @"Select Vaccine";
    }
    else
    {
        cell.lblDD1.text =[appDelegate.dictCulture valueForKey:@"ScheduleAVaccine"];
    }
    cell.lblDD1.font = [UIFont fontWithName:@"UNIVERS" size:13.0] ;
    [cell.btnDD1 addTarget:self action:@selector(dropdownOnePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString * SelectASchedule = [appDelegate.dictCulture valueForKey:@"SelectASchedule"];
    
    if (SelectASchedule.length==0)
    {
        cell.lblDD2.text = @"Select A Schedule";
    }
    else
    {
        cell.lblDD2.text = [appDelegate.dictCulture valueForKey:@"SelectASchedule"];
    }
    
    cell.lblDD2.font = [UIFont fontWithName:@"UNIVERS" size:13.0] ;
    [cell.btnDD2 addTarget:self action:@selector(dropdownTwoPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)textFieldShouldEndEditingOneCI:(NSNotification *)notification
{
    [self myMethod];
}

-(void)myMethod
{
    for (int a=0; a<_dataArray.count; a++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:a inSection:0];
        AddEventCell *cell1 = (AddEventCell*)[_tableView cellForRowAtIndexPath:indexPath];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    rowIndex = indexPath;
}

-(IBAction)selectFarmName
{
    [_pickerView setHidden:NO];
}

-(void)showPickview
{
    _pickerView = [[UIPickerView alloc]initWithFrame:
                   CGRectMake(0,  self.contentView.frame.size.height-200, self.contentView.frame.size.width
                              , 200)];
    
    _pickerView.dataSource = self;
    _pickerView.delegate = self;
    _pickerView.showsSelectionIndicator = YES;
    NSString *doneText =[appDelegate.dictCulture valueForKey:@"Done"];
    if(doneText.length==0)
    {
        doneText =@"Done";
    }

    _pickerView.backgroundColor = [UIColor lightGrayColor];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:doneText style:UIBarButtonItemStyleDone
                                   target:self action:@selector(done:)];
    
    _toolBar = [[UIToolbar alloc]initWithFrame:
                CGRectMake(0, self.contentView.frame.size.height-250, self.contentView.frame.size.width, 50)];
    [_toolBar setBarStyle:UIBarStyleBlackOpaque];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [_toolBar setItems:toolbarItems];
    
    [self.contentView addSubview:_toolBar];
    [self.contentView addSubview:_pickerView];
}

-(IBAction)showFarmPicker:(id)sender
{
    [_pickerView setHidden:NO];
    [_toolBar setHidden:NO];
    
    isPicker =@"Farm";
    row1=[sender tag];
    [_pickerView reloadAllComponents];
}

-(IBAction)showFarmPicker2:(id)sender
{
    [_pickerView setHidden:NO];
    [_toolBar setHidden:NO];
    
    isPicker =@"One";
    row1=[sender tag];
    [_pickerView reloadAllComponents];
}

-(void)dropdownOnePressed:(id)sender
{
    [_pickerView setHidden:NO];
    [_toolBar setHidden:NO];
    
    isPicker =@"One";
    row1=[sender tag];
    [_pickerView reloadAllComponents];
}

-(void)dropdownTwoPressed:(id)sender
{
    [_pickerView setHidden:NO];
    [_toolBar setHidden:NO];
    
    row1=[sender tag];
    isPicker =@"Two";
    [_pickerView reloadAllComponents];
    [_pickerView setHidden:NO];
    
}

- (IBAction)changeSwitch:(UISwitch*)sender
{
    if([sender isOn])
    {
        isSwitchOn =@"Yes";
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row1 inSection:0];
        AddEventCell *cell1 = (AddEventCell*)[_tableView cellForRowAtIndexPath:indexPath];
        cell1.btnDD2.hidden=FALSE;
        cell1.lblDD2.hidden=FALSE;
    }
    else
    {
        isSwitchOn =@"No";
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row1 inSection:0];
        AddEventCell *cell1 = (AddEventCell*)[_tableView cellForRowAtIndexPath:indexPath];
        cell1.btnDD2.hidden=TRUE;
        cell1.lblDD2.hidden=TRUE;
    }
}

-(IBAction)done:(id)sender
{
    AppDelegate *appDelegate1 =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appDelegate1.dropdownSelectionArray  removeAllObjects];
    appDelegate1.numberOfVaccineCattleTotoal = @"";
    [_pickerView setHidden:YES];
    [_toolBar setHidden:YES];
}

-(IBAction)addVaccine:(id)sender
{
    [_tableView beginUpdates];
    [_dataArray addObject:@"Primary Vaccination"];
    
    if(_dataArray.count<4)
    {
        NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:[_dataArray count]-1 inSection:0]];
        [[self tableView] insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationTop];
        
        CGRect tFrame = _tableView.frame;
        tFrame.size.height = MIN(_tableView.contentSize.height+80
                                 ,
                                 80*_dataArray.count);
        _tableView.frame = tFrame;
        [_tableView endUpdates];
        
        [UIView animateWithDuration:0.3
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             
                             [_bottomView setFrame:CGRectMake(0,280+_tableView.contentSize.height, self.bottomView.frame.size.width, self.bottomView.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             
                         }];
        
        [_outerScrollView setContentSize:CGSizeMake( _outerScrollView.frame.size.width, _outerScrollView.frame.size.height + _tableView.contentSize.height)];
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _txtAnimalNo ||  textField == _txtFarmName)
    {
        return  NO;
    }
    return  YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField == _txtAnimalNo ||  textField == _txtFarmName)
    {
        [textField resignFirstResponder];
    }
    if (textField == _txtSelectSchedule)
    {
        [textField resignFirstResponder];
        [_txtNoOfVaccine becomeFirstResponder];
    }
    else if (textField == _txtNoOfVaccine)
    {
        [textField resignFirstResponder];
        [_txtSelectSchedule becomeFirstResponder];
    }
    
    else if (textField == _txtNotes)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark === PickerView Delegates/Datasource===
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    if ( [isPicker  isEqualToString:@"One"])
    {
        return [appDelegate.productsArray count];
    }
    
    else if ( [isPicker  isEqualToString:@"Farm"])
    {
        return [appDelegate.getUserFarmsFromServer count];
    }
    
    else
    {
        return [pickerArray2 count];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row1 inSection:0];
    AddEventCell *cell1 = (AddEventCell*)[_tableView cellForRowAtIndexPath:indexPath];
    NSString * testValue = cell1.lblNoOfCattles.text;
    
    if ( [isPicker  isEqualToString:@"One"])
    {
        ProductMaster * obj =  [ProductMaster new];
        obj = [appDelegate.productsArray objectAtIndex:row];
        _txtSelectSchedule.text = obj.vaccineName;
        appDelegate.vaccineID = obj.vaccineID;
        cell1.lblDD1.text = obj.vaccineName;
        appDelegate.selectdVaccineName = cell1.lblDD1.text;
    }
    else if ( [isPicker  isEqualToString:@"Two"])
    {
        if(cell1.switch1.isOn){
            cell1.lblDD2.text =  [pickerArray2 objectAtIndex:row];
          //  appDelegate.selectedVaccineType =  [pickerArray2 objectAtIndex:row];
            
            if (row == 0) {
                appDelegate.selectedVaccineType =@"PrimaryVaccination";
            }
           else if (row == 1) {
                appDelegate.selectedVaccineType =@"SecondaryVaccination";
            }
            if (row == 2) {
                appDelegate.selectedVaccineType =@"Revaccination";
            }
        }
    }
    else if ( [isPicker  isEqualToString:@"Farm"])
    {
        UserFarmMasterModel * obj  = [[UserFarmMasterModel alloc] init];
        obj =[appDelegate.getUserFarmsFromServer objectAtIndex:row];
        NSString * titleForRow = obj.farmName;
        appDelegate.uniqueFarmID = obj.farmID;
        _txtFarmName.text =  titleForRow;
        self.txtAnimalNo.text =@"";
        [appDelegate.animalsSelectedArray removeAllObjects];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    CGRect frame = CGRectMake(0,0,265,40);
    pickerLabel = [[UILabel alloc] initWithFrame:frame];
    pickerLabel.textAlignment = NSTextAlignmentCenter;;
    [pickerLabel setBackgroundColor:[UIColor clearColor]];
    [pickerLabel setFont:[UIFont fontWithName:@"Univers" size:14]];
    [pickerLabel setNumberOfLines:0];
    
    if ( [isPicker  isEqualToString:@"One"])
    {
        ProductMaster * obj =  [ProductMaster new];
        obj = [appDelegate.productsArray objectAtIndex:row];
        NSString * titleName = obj.vaccineName;
        [pickerLabel setText:titleName];
        return pickerLabel;
    }
    
    else  if ( [isPicker  isEqualToString:@"Two"])
    {
        NSString * pickerV = [pickerArray2 objectAtIndex:row];
        [pickerLabel setText:pickerV];
        return pickerLabel;
    }
    
    else  if ( [isPicker  isEqualToString:@"Farm"])
    {
        UserFarmMasterModel * obj  = [[UserFarmMasterModel alloc] init];
        obj =[appDelegate.getUserFarmsFromServer objectAtIndex:row];
        NSString * titleForRow = obj.farmName;
        [pickerLabel setText:titleForRow];
        return pickerLabel;
        
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ( [isPicker  isEqualToString:@"One"])
    {
        ProductMaster * obj =  [ProductMaster new];
        obj = [appDelegate.productsArray objectAtIndex:row];
        NSString * titleName = obj.vaccineName;
        return titleName;
    }
    else  if ( [isPicker  isEqualToString:@"Two"])
    {
        return [pickerArray2 objectAtIndex:row];
    }
    else  if ( [isPicker  isEqualToString:@"Farm"])
    {
        UserFarmMasterModel * obj  = [[UserFarmMasterModel alloc] init];
        obj =[appDelegate.getUserFarmsFromServer objectAtIndex:row];
        NSString * titleForRow = obj.farmName;
        return titleForRow ;
    }
    return 0;
}

-(void)doneFarm:(id)sender
{
    [_txtFarmName resignFirstResponder];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
