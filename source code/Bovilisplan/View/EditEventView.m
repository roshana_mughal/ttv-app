//
//  EditEventView.m
//  TTVApp
//
//  Created by Admin on 8/16/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "EditEventView.h"
#import "AppDelegate.h"

@interface EditEventView ()

@property(strong,nonatomic) IBOutlet UIView *contentView;

@end

@implementation EditEventView
{
    AppDelegate *appDelegate;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initializeSubviews];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if(self)
    {
        [self initializeSubviews];
    }
    return self;
}

-(void)initializeSubviews
{
    [[NSBundle mainBundle] loadNibNamed:@"EditEventView" owner:self options:nil];
    appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.contentView.frame =self.bounds;
    
   NSString *notes =[appDelegate.dictCulture valueForKey:@"Notes"];
    if ([notes isEqualToString:@"NA"]  || (notes.length == 0))
    {
        _lblNotes.text=@"Notes";
       // self.txtNotes.placeholder =@"Notes";
    }
    else{
        _lblNotes.text =notes;
       // self.txtNotes.placeholder =notes;

    }
    
    [_btnCancel setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnCancel.layer.borderWidth = 1.0f;
    [_btnCancel.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnSave setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnSave.layer.borderWidth = 1.0f;
    
    [_btnSave.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    [self changeLanguage];
    
    [self addSubview:self.contentView];
    self.contentView.frame =self.bounds;
}

-(void)changeLanguage
{
    NSString * btnCacel =[appDelegate.dictCulture valueForKey:@"Cancel"];
    if (btnCacel.length==0)
    {
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    else
    {
        [_btnCancel setTitle:[appDelegate.dictCulture valueForKey:@"Cancel"] forState:UIControlStateNormal];
    }
    
    NSString * btnSave=[appDelegate.dictCulture valueForKey:@"Save"];
    if (btnSave.length==0)
    {
        [_btnSave setTitle:@"Save" forState:UIControlStateNormal];
    }
    else
    {
        [_btnSave setTitle:[appDelegate.dictCulture valueForKey:@"Save"] forState:UIControlStateNormal];
    }
}

@end
