//
//  MenuView.m
//  Bovilisplan
//
//  Created by Admin on 5/12/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "ListingViewController.h"
#import "MenuCell.h"
#import "MenuView.h"
#import "AppDelegate.h"

@interface MenuView ()<UITableViewDelegate,UITableViewDataSource>
{
    AppDelegate * appDelegate;
    NSMutableArray *menuArray;
    NSMutableArray * menuImagesArray;
}

@property(strong,nonatomic) IBOutlet UIView *contentView;
@property(nonatomic,strong) IBOutlet UILabel*lblMenu;
@property(nonatomic,strong) IBOutlet UILabel*lblSettings;
@property(nonatomic,strong) IBOutlet UILabel*lblDisclaimer;
@property(nonatomic,strong) IBOutlet UILabel*lblPrivacy;
@property(nonatomic,strong) IBOutlet UILabel*lblTerms;
@property(nonatomic,strong) IBOutlet UILabel*lblSignout;
@property(nonatomic,strong) IBOutlet UILabel*lblCalendar;
@property(nonatomic,strong) IBOutlet UILabel*lblListing;
@property(nonatomic,strong) IBOutlet UILabel*lblLibrary;
@property(nonatomic,strong) IBOutlet UILabel*lblAboutUs;
@property(nonatomic,strong) IBOutlet UILabel*lblMyAnimals;


@property(nonatomic,strong) IBOutlet UILabel*lblNotification;

@end

@implementation MenuView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if(self)
    {
        [self initializeSubviews];
    }
    return self;
}

-(void)initializeSubviews
{
    [[NSBundle mainBundle] loadNibNamed:@"MenuView" owner:self options:nil];
    
    self.contentView.frame =self.bounds;
    self.contentView.backgroundColor =[UIColor redColor];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    
   // [self configureTableViewCell];
    
    NSString * menuText =[appDelegate.dictCulture valueForKey:@"Menu"];
    self.lblMenu.text = menuText.length == 0 ? @"MENU" : [menuText uppercaseString];

    
    NSString * settingText =[appDelegate.dictCulture valueForKey:@"AccountSettings"];
    self.lblSettings.text = settingText.length == 0 ? @"Account Settings" : settingText;

    NSString * privacyText =[appDelegate.dictCulture valueForKey:@"PrivacyPolicy"];
    self.lblPrivacy.text = privacyText.length == 0 ? @"Privacy Policy" : privacyText;

    
    NSString * disclaimerText =[appDelegate.dictCulture valueForKey:@"Disclaimer"];
    self.lblDisclaimer.text = disclaimerText.length == 0 ? @"Disclaimer" : disclaimerText;

    
    NSString * termsAndCondition =[appDelegate.dictCulture valueForKey:@"TermsAndConditions"];
    self.lblTerms.text = termsAndCondition.length == 0 ? @"Terms and Conditions" : termsAndCondition;


    NSString * calendarText =[appDelegate.dictCulture valueForKey:@"Calendar"];
    self.lblCalendar.text = calendarText.length == 0 ? @"Calendar" : calendarText;

    
    NSString * myListingText =[appDelegate.dictCulture valueForKey:@"MyListing"];
    self.lblListing.text = myListingText.length == 0 ? @"My Farms" : myListingText;


    NSString * noticAndNotifications =[appDelegate.dictCulture valueForKey:@"NoticeAndNotifications"];
    self.lblNotification.text = noticAndNotifications.length == 0 ? @"Reminders" : noticAndNotifications;


    NSString * libraryText =[appDelegate.dictCulture valueForKey:@"Library"];
    self.lblLibrary.text = libraryText.length == 0 ? @"Library" : libraryText;

    
    NSString *about =[appDelegate.dictCulture valueForKey:@"AboutUs"];
    self.lblAboutUs.text = about.length == 0 ? @"About Us" : about;
    
    NSString * signOut =[appDelegate.dictCulture valueForKey:@"SignOut"];
    self.lblSignout.text = signOut.length == 0 ? @"Sign Out" : signOut;
    
    
    NSString * myAnimal =[appDelegate.dictCulture valueForKey:@"MyAnimals"];
    self.lblMyAnimals.text = myAnimal.length == 0 ? @"My Animals" : signOut;


    [self addSubview:self.contentView];
}

#pragma mark === Configure TableView Cell===
-(void)configureTableViewCell{
    [_tableView registerNib:[UINib nibWithNibName:@"MenuCell"
                                           bundle:nil] forCellReuseIdentifier:@"MenuCell"];
}

#pragma mark === TableView Delegates/Datasource===

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"MenuCell";
    
    MenuCell *cell = (MenuCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSString * title = [menuArray objectAtIndex:indexPath.row];
    NSString *menuImage = [menuImagesArray objectAtIndex:indexPath.row];
    
    cell.lblTitle.text = title;
    cell.menuImage.image = cell.menuImage.image = [UIImage imageNamed:menuImage];
    cell.lblTitle.font = [UIFont fontWithName:@"UNIVERS" size:15.0] ;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString*title = [menuArray objectAtIndex:indexPath.row];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
