//
//  AddNewFarmView.m
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "AddNewFarmView.h"

@interface AddNewFarmView()<UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>


@property(strong ,nonatomic)IBOutlet UIView *contentView;

@property(strong ,nonatomic) UIPickerView *pickerView;

@end

@implementation AddNewFarmView

{

    int variabla;
    NSMutableArray* countryNameArray;
    
    
    NSString *selectedValue;
    
    UIPickerView *myPickerView;

}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // CUSTOM INITIALIZATION HERE
        //We are in the storyboard code path. Initialize from the xib.
        [self initializeSubviews];
        
    }
    
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    
    self =[super initWithFrame:frame];
    
    if(self)
        
    {
        
        [self initializeSubviews];
        
    }
    
    return self;
}
-(void)initializeSubviews {
    
    //  NSString * nibName = @"AddEventView";
    // id view =   [[[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil] firstObject];
    
    

    

    [[NSBundle mainBundle] loadNibNamed:@"AddNewFarmView" owner:self options:nil];
    
    
    [_btnCancel setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnCancel.layer.borderWidth = 1.0f;
    _btnCancel.layer.cornerRadius= 5.0;
    [_btnCancel.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    
    
    [_btnSave setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnSave.layer.cornerRadius= 5.0;
    _btnSave.layer.borderWidth = 1.0f;
    
    [_btnSave.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [self changeLanguage];

    [self addPickerView];
    
    
    [self addSubview:self.contentView];
    self.contentView.frame =self.bounds;
  
    
    
}

-(void)changeLanguage{
    
    
    [_btnCancel setTitle:NSLocalizedString(@"Cancel",nil) forState:UIControlStateNormal];
    
    [_btnSave setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    [_lblTop setText:NSLocalizedString(@"New_Farm",nil)];
    

    _txtFarmName.placeholder = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Farm_Name", nil), @"*"];
    
    
    _txtRegistration.placeholder =
    [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Registration_Number",nil), @"*"];
    _txtReigon.placeholder =[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Select_Region",nil), @"*"];
    _txtContactPerson.placeholder =NSLocalizedString(@"Contact_Person", nil);
    _txtAnimals.placeholder =
    [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Animal",nil), @"*"];
   _txtAddress.placeholder =
    [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Address",nil), @"*"];
    
    _txtPhone.placeholder =NSLocalizedString(@"Phone", @"Time_To_Vaccinate");
    _txtEmail.placeholder =
    [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Email_Address",nil), @"*"];
    _txtComment.placeholder =
    [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"Notes",nil), @"*"];
    
    
    
    
}


-(IBAction)selectReigon

{

    [self addPickerView];
}

-(void)addPickerView{
 
    countryNameArray = [[NSMutableArray alloc]init];
    
    [countryNameArray addObject:@"Albania"];
    [countryNameArray addObject:@"Algeria"];
    [countryNameArray addObject:@"Angola"];
    [countryNameArray addObject:@"Antigua and Barbuda"];
    [countryNameArray addObject:@"Argentina"];
    [countryNameArray addObject:@"Armenia"];
    [countryNameArray addObject:@"Aruba"];
    [countryNameArray addObject:@"Australia"];
    [countryNameArray addObject:@"Austria"];
    [countryNameArray addObject:@"Azerbaijan"];
    
    
    myPickerView = [[UIPickerView alloc]init];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                   target:self action:@selector(done:)];
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, self.frame.size.height-
                                     myPickerView.frame.size.height-50, 320, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    _txtReigon.inputView = myPickerView;
    _txtReigon.inputAccessoryView = toolBar;
    
}

-(void)done:(id)sender
{
    
    [_txtReigon resignFirstResponder];
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    return [countryNameArray count];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    [_txtReigon setText:[countryNameArray objectAtIndex:row]];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
    return [countryNameArray objectAtIndex:row];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
