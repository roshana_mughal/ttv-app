//
//  AddNewFarmView.m
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "AddNewFarmView.h"
@interface AddNewFarmView()<UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>
@property(strong ,nonatomic) IBOutlet UIView *contentView;
@property(strong ,nonatomic) UIPickerView *pickerView;
@end
@implementation AddNewFarmView
{
    int variabla;
    NSMutableArray* countryArray;
    NSString *selectedValue;
    UIPickerView *myPickerView;
    AppDelegate*appDelegate;
    
    NSMutableArray* countyIDArray;
    NSString *countryKey;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initializeSubviews];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    
    if(self)
    {
        [self initializeSubviews];
    }
    
    return self;
}

-(void)initializeSubviews
{
    [[NSBundle mainBundle] loadNibNamed:@"AddNewFarmView" owner:self options:nil];
    
    appDelegate=(AppDelegate*) [[UIApplication sharedApplication]delegate];
    
    [_scrollView setContentSize:CGSizeMake( _scrollView.frame.size.width, 780)];
    [_btnCancel setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnCancel.layer.borderWidth = 1.0f;
    [_btnCancel.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    [_btnSave setTitleColor:[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1]forState:UIControlStateNormal];
    _btnSave.layer.borderWidth = 1.0f;
    
    [_btnSave.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:174/255.0 blue:25/255.0 alpha:1] CGColor]];
    appDelegate.switchValue1 =@"False";
    appDelegate.switchValue2 =@"False";
    appDelegate.switchValue3 =@"False";
    appDelegate.switchValue4 =@"False";

    [self changeLanguage];
    [self addPickerView];
    
    [self addSubview:self.contentView];
    self.contentView.frame =self.bounds;
}

-(void)changeLanguage
{
    NSString * btnCacel =[appDelegate.dictCulture valueForKey:@"Cancel"];
    if (btnCacel.length==0)
    {
        [_btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    }
    else
    {
        [_btnCancel setTitle:[appDelegate.dictCulture valueForKey:@"Cancel"] forState:UIControlStateNormal];
    }
    
    NSString * btnSave=[appDelegate.dictCulture valueForKey:@"Save"];
    if (btnSave.length==0)
    {
        [_btnSave setTitle:@"Save" forState:UIControlStateNormal];
    }
    else
    {
        [_btnSave setTitle:[appDelegate.dictCulture valueForKey:@"Save"] forState:UIControlStateNormal];
    }
    
    NSString * lblTop=[appDelegate.dictCulture valueForKey:@"NewFarm"];
    if (btnSave.length==0)
    {
        [_lblTop setText:@"NEW FARM" ];
    }
    else
    {
        [_lblTop setText:[appDelegate.dictCulture valueForKey:@"NewFarm"]];
    }
    
    NSString * lblFarmName=[appDelegate.dictCulture valueForKey:@"FarmName"];
    if (lblFarmName.length==0)
    {
        _lblFarmName.text = [NSString stringWithFormat:@"%@%2@",@"Farm Name", @"*"];
    }
    
    else
    {
        _lblFarmName.text = [NSString stringWithFormat:@"%@%@",[appDelegate.dictCulture valueForKey:@"FarmName"], @"*"];
    }
    
    NSString * lblRegistration=[appDelegate.dictCulture valueForKey:@"RegistrationNr"];
    if (lblRegistration.length==0)
    {
        _lblRegistration.text = [NSString stringWithFormat:@"%@",@"Registration Nr"];
    }
    
    else
    {
        _lblRegistration.text =[appDelegate.dictCulture valueForKey:@"RegistrationNr"];
    }
    NSString * lblRegion=[appDelegate.dictCulture valueForKey:@"SelectRegion"];
    if (lblRegion.length==0)
    {
        _lblReigon.text = [NSString stringWithFormat:@"%@",@"Select Region"];
    }
    else
    {
        _lblReigon.text =[appDelegate.dictCulture valueForKey:@"SelectRegion"];
    }
    
    NSString * txtContactPerson=[appDelegate.dictCulture valueForKey:@"ContactPerson"];
    if (txtContactPerson.length==0)
    {
        _lblContactPerson.text = [NSString stringWithFormat:@"%@",@"Contact Person"];
    }
    else
    {
        _lblContactPerson.text = [appDelegate.dictCulture valueForKey:@"ContactPerson"];
    }
    
    NSString * txtAnimal=[appDelegate.dictCulture valueForKey:@"AnimalNumber"];
    
    if (txtAnimal.length==0) {
        _lblAnimals.text = [NSString stringWithFormat:@"%@",@"Animal Number"];
    }
    else
    {
        _lblAnimals.text = [appDelegate.dictCulture valueForKey:@"AnimalNumber"];;
    }
    
    NSString * txtAddress=[appDelegate.dictCulture valueForKey:@"Address"];
    if (txtAddress.length==0)
    {
        _lblAddress.text = [NSString stringWithFormat:@"%@",@"Address"];
    }
    else
    {
        _lblAddress.text =[appDelegate.dictCulture valueForKey:@"Address"];
    }
    
    NSString * txtphone=[appDelegate.dictCulture valueForKey:@"Phone"];
    if (txtphone.length==0)
    {
        _lblPhone.text = [NSString stringWithFormat:@"%@",@"Phone"];
    }
    else
    {
        _lblPhone.text = [appDelegate.dictCulture valueForKey:@"Phone"];
    }
    
    NSString * txtEmail=[appDelegate.dictCulture valueForKey:@"EmailAddress"];
    if (txtEmail.length==0)
    {
        _lblEmail.text = [NSString stringWithFormat:@"%@",@"Email Address"];
    }
    else
    {
        _lblEmail.text = [appDelegate.dictCulture valueForKey:@"EmailAddress"];
    }
    
    NSString * txtComment=[appDelegate.dictCulture valueForKey:@"Notes"];
    if (txtComment.length==0)
    {
        _lblComment.text = [NSString stringWithFormat:@"%@",@"Notes"];
    }
    else
    {
        _lblComment.text = [appDelegate.dictCulture valueForKey:@"Notes"];
    }
    
    NSString * txtFarmType=[appDelegate.dictCulture valueForKey:@"FarmType"];
    if (txtFarmType.length==0)
    {
        _farmType.text = [NSString stringWithFormat:@"%@",@"Farm Type"];
    }
    else
    {
        _farmType.text = [appDelegate.dictCulture valueForKey:@"FarmType"];
    }
    
    NSString * lblDairy=[appDelegate.dictCulture valueForKey:@"Dairy"];
    if (lblDairy.length==0)
    {
        _lblDairy.text = [NSString stringWithFormat:@"%@",@"Dairy"];
    }
    else
    {
        _lblDairy.text = [appDelegate.dictCulture valueForKey:@"Dairy"];
    }
    
    NSString * lblBeef=[appDelegate.dictCulture valueForKey:@"BeefSlashVeal"];
    if (lblBeef.length==0)
    {
        _lblBeff.text = [NSString stringWithFormat:@"%@",@"Beef/Veal"];
    }
    else
    {
        _lblBeff.text = [appDelegate.dictCulture valueForKey:@"BeefSlashVeal"];
    }
    
    NSString * lblCow=[appDelegate.dictCulture valueForKey:@"CowCalf"];
    if (lblCow.length==0)
    {
        _lblCow.text = [NSString stringWithFormat:@"%@",@"Cow Calf"];
    }
    else
    {
        _lblCow.text = [appDelegate.dictCulture valueForKey:@"CowCalf"];
    }
    
    NSString * lblMixed=[appDelegate.dictCulture valueForKey:@"Mixed"];
    if (lblMixed.length==0)
    {
        _mixed.text = [NSString stringWithFormat:@"%@",@"Mixed"];
    }
    else
    {
        _mixed.text = [appDelegate.dictCulture valueForKey:@"Mixed"];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtFarmName)
    {
        [textField resignFirstResponder];
        [_txtRegistration becomeFirstResponder];
    }
    else if (textField == _txtRegistration)
    {
        [textField resignFirstResponder];
        [_txtReigon becomeFirstResponder];
    }
    else if (textField == _txtReigon)
    {
        [textField resignFirstResponder];
        [_txtContactPerson becomeFirstResponder];
    }
    else if (textField == _txtContactPerson)
    {
        [textField resignFirstResponder];
        [_txtAnimals becomeFirstResponder];
    }
    else if (textField == _txtAnimals)
    {
        [textField resignFirstResponder];
        [_txtAddress becomeFirstResponder];
    }
    
    else if (textField == _txtAddress)
    {
        [textField resignFirstResponder];
        [_txtPhone becomeFirstResponder];
    }
    
    else if (textField == _txtPhone)
    {
        [textField resignFirstResponder];
        [_txtEmail becomeFirstResponder];
    }
    
    else if (textField == _txtEmail)
    {
        [textField resignFirstResponder];
        [_txtComment becomeFirstResponder];
    }
    else if (textField == _txtComment)
    {
        [textField resignFirstResponder];
        
    }
    return YES;
}

- (IBAction)changeSwitch:(UISwitch*)sender
{
    if (sender.tag==1)
    {
        if([sender isOn])
        {
            appDelegate.switchValue1 =@"True";
        }
        else
        {
            appDelegate.switchValue1 =@"False";
        }
    }
    
    if (sender.tag==2)
    {
        if([sender isOn])
        {
            appDelegate.switchValue2 =@"True";
        }
        else{
            appDelegate.switchValue2 =@"False";
        }
    }
    
    if (sender.tag==3)
    {
        if([sender isOn])
        {
            appDelegate.switchValue3 =@"True";
        }
        else{
            appDelegate.switchValue3 =@"False";
        }
    }
    
    if (sender.tag==4)
    {
        if([sender isOn])
        {
            appDelegate.switchValue4 =@"True";
        } else
        {
            appDelegate.switchValue4 =@"False";
        }
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _txtReigon)
    {
        return  NO;
    }
    if ( self.txtFarmName.text.length == 0 && [string isEqualToString: @" " ]) {
        return NO;
        
    }
    return YES;
}

-(void)addPickerView{
    
    countryArray = [[NSMutableArray alloc]init];
    
    [ countryArray addObject:@"Afghanistan"];
    [ countryArray addObject:@"Aland Islands"];
    [ countryArray addObject:@"Albania"];
    [ countryArray addObject:@"Algeria"];
    [ countryArray addObject:@"American Samoa"];
    [ countryArray addObject:@"Andorra"];
    [ countryArray addObject:@"Angola"];
    [ countryArray addObject:@"Anguilla"];
    [ countryArray addObject:@"Antarctica"];
    [ countryArray addObject:@"Antigua and Barbud"];
    [ countryArray addObject:@"Argentina"];
    [ countryArray addObject:@"Armenia"];
    [ countryArray addObject:@"Aruba"];
    [ countryArray addObject:@"Australia"];
    [ countryArray addObject:@"Austria"];
    [ countryArray addObject:@"Azerbaijan"];
    [ countryArray addObject:@"Bahamas"];
    [ countryArray addObject:@"Bahrain"];
    [ countryArray addObject:@"Bangladesh"];
    [ countryArray addObject:@"Barbados"];
    [ countryArray addObject:@"Belarus"];
    [ countryArray addObject:@"Belgium"];
    [ countryArray addObject:@"Belize"];
    [ countryArray addObject:@"Benin"];
    [ countryArray addObject:@"Bermuda"];
    [ countryArray addObject:@"Bhutan"];
    [ countryArray addObject:@"Bolivia"];
    [ countryArray addObject:@"Bosnia and Herzegovina"];
    [ countryArray addObject:@"Botswana"];
    [ countryArray addObject:@"Bouvet Island"];
    [ countryArray addObject:@"Brazil"];
    [ countryArray addObject:@"British Indian Ocean Territory"];
    [ countryArray addObject:@"Brunei Darussalam"];
    [ countryArray addObject:@"Bulgaria"];
    [ countryArray addObject:@"Burkina Faso"];
    [ countryArray addObject:@"Burundi"];
    [ countryArray addObject:@"Cambodia"];
    [ countryArray addObject:@"Cameroon"];
    [ countryArray addObject:@"Canada"];
    [ countryArray addObject:@"Cape Verde"];
    [ countryArray addObject:@"Cayman Islands"];
    [ countryArray addObject:@"Central African Republic"];
    [ countryArray addObject:@"Chad"];
    [ countryArray addObject:@"Chile"];
    [ countryArray addObject:@"China"];
    [ countryArray addObject:@"Christmas Island"];
    [ countryArray addObject:@"Cocos (keeling) Islands"];
    [ countryArray addObject:@"Colombia"];
    [ countryArray addObject:@"Comoros"];
    [ countryArray addObject:@"Congo"];
    [ countryArray addObject:@"Congo, The Democratic Republic of the"];
    [ countryArray addObject:@"Cook Islands"];
    [ countryArray addObject:@"Costa Rica"];
    [ countryArray addObject:@"Côte d'Ivoire"];
    [ countryArray addObject:@"Croatia"];
    [ countryArray addObject:@"Cuba"];
    [ countryArray addObject:@"Cyprus"];
    [ countryArray addObject:@"Czech Republic"];
    [ countryArray addObject:@"Denmark"];
    [ countryArray addObject:@"Djibouti"];
    [ countryArray addObject:@"Dominica"];
    [ countryArray addObject:@"Dominican Republic"];
    [ countryArray addObject:@"Ecuador"];
    [ countryArray addObject:@"Egypt"];
    [ countryArray addObject:@"El Salvador"];
    [ countryArray addObject:@"Equatorial Guinea"];
    [ countryArray addObject:@"Eritrea"];
    [ countryArray addObject:@"Estonia"];
    [ countryArray addObject:@"Ethiopia"];
    [ countryArray addObject:@"Falkland Islands (Malvinas)"];
    [ countryArray addObject:@"Faroe Islands"];
    [ countryArray addObject:@"Fiji"];
    [ countryArray addObject:@"Finland"];
    [ countryArray addObject:@"France"];
    [ countryArray addObject:@"French Guiana"];
    [ countryArray addObject:@"French Polynesia"];
    [ countryArray addObject:@"French Southern Territories"];
    [ countryArray addObject:@"Gabon"];
    [ countryArray addObject:@"Gambia"];
    [ countryArray addObject:@"Georgia"];
    [ countryArray addObject:@"Germany"];
    [ countryArray addObject:@"Ghana"];
    [ countryArray addObject:@"Gibraltar"];
    [ countryArray addObject:@"Greece"];
    [ countryArray addObject:@"Greenland"];
    [ countryArray addObject:@"Grenada"];
    [ countryArray addObject:@"Guadeloupe"];
    [ countryArray addObject:@"Guam"];
    [ countryArray addObject:@"Guatemala"];
    [ countryArray addObject:@"Guinea"];
    [ countryArray addObject:@"Guinea-Bissau"];
    [ countryArray addObject:@"Guyana"];
    [ countryArray addObject:@"Haiti"];
    [ countryArray addObject:@"Heard Island and Mcdonald Islands"];
    [ countryArray addObject:@"Holy See (Vatican City State)"];
    [ countryArray addObject:@"Honduras"];
    [ countryArray addObject:@"Hong kong"];
    [ countryArray addObject:@"Hungary"];
    [ countryArray addObject:@"Iceland"];
    [ countryArray addObject:@"India"];
    [ countryArray addObject:@"Indonesia"];
    [ countryArray addObject:@"Iran, Islamic Republic of"];
    [ countryArray addObject:@"Iraq"];
    [ countryArray addObject:@"Ireland"];
    [ countryArray addObject:@"Israel"];
    [ countryArray addObject:@"Italy"];
    [ countryArray addObject:@"Jamaica"];
    [ countryArray addObject:@"Japan"];
    [ countryArray addObject:@"Jordan"];
    [ countryArray addObject:@"Kazakhstan"];
    [ countryArray addObject:@"Kenya"];
    [ countryArray addObject:@"Kiribati"];
    [ countryArray addObject:@"Korea, Democratic People's Republic of"];
    [ countryArray addObject:@"Korea, Republic of"];
    [ countryArray addObject:@"Kuwait"];
    [ countryArray addObject:@"Kyrgyzstan"];
    [ countryArray addObject:@"Lao People's Democratic Republic"];
    [ countryArray addObject:@"Letvia"];
    [ countryArray addObject:@"Lebanon"];
    [ countryArray addObject:@"Lesotho"];
    [ countryArray addObject:@"Liberia"];
    [ countryArray addObject:@"Libyan Arab Jamahiriya"];
    [ countryArray addObject:@"Liechtenstein"];
    [ countryArray addObject:@"Lithuania"];
    [ countryArray addObject:@"Luxembourg"];
    [ countryArray addObject:@"Macao"];
    [ countryArray addObject:@"Macedonia, Former Yugoslav Republic of"];
    [ countryArray addObject:@"Madagascar"];
    [ countryArray addObject:@"Malawi"];
    [ countryArray addObject:@"Malaysia"];
    [ countryArray addObject:@"Maldives"];
    [ countryArray addObject:@"Mali"];
    [ countryArray addObject:@"Malta"];
    [ countryArray addObject:@"Marshall Islands"];
    [ countryArray addObject:@"Martinique"];
    [ countryArray addObject:@"Mauritania"];
    [ countryArray addObject:@"Mauritius"];
    [ countryArray addObject:@"Mayotte"];
    [ countryArray addObject:@"Mexico"];
    [ countryArray addObject:@"Micronesia, Federated States of"];
    [ countryArray addObject:@"Moldova"];
    [ countryArray addObject:@"Monaco"];
    [ countryArray addObject:@"Mongolia"];
    [ countryArray addObject:@"Montserrat"];
    [ countryArray addObject:@"Morocco"];
    [ countryArray addObject:@"Mozambique"];
    [ countryArray addObject:@"Myanmar"];
    [ countryArray addObject:@"Namibia"];
    [ countryArray addObject:@"Nauru"];
    [ countryArray addObject:@"Nepal"];
    [ countryArray addObject:@"Netherlands"];
    [ countryArray addObject:@"Netherlands Antilles"];
    [ countryArray addObject:@"New Caledonia"];
    [ countryArray addObject:@"New Zealand"];
    [ countryArray addObject:@"Nicaragua"];
    [ countryArray addObject:@"Niger"];
    [ countryArray addObject:@"Nigeria"];
    [ countryArray addObject:@"Niue"];
    [ countryArray addObject:@"Norfolk Island"];
    [ countryArray addObject:@"Northern Mariana Islands"];
    [ countryArray addObject:@"Norway"];
    [ countryArray addObject:@"Oman"];
    [ countryArray addObject:@"Pakistan"];
    [ countryArray addObject:@"Palau"];
    [ countryArray addObject:@"Palestinian Territory"];
    [ countryArray addObject:@"Panama"];
    [ countryArray addObject:@"Papua New Guinea"];
    [ countryArray addObject:@"Paraguay"];
    [ countryArray addObject:@"Peru"];
    [ countryArray addObject:@"Philippines"];
    [ countryArray addObject:@"Pitcairn"];
    [ countryArray addObject:@"Poland"];
    [ countryArray addObject:@"Portugal"];
    [ countryArray addObject:@"Puerto Rico"];
    [ countryArray addObject:@"Qatar"];
    [ countryArray addObject:@"Réunion"];
    [ countryArray addObject:@"Romania"];
    [ countryArray addObject:@"Russian Federation"];
    [ countryArray addObject:@"Rwanda"];
    [ countryArray addObject:@"Saint Helena"];
    [ countryArray addObject:@"Saint Kitts and Nevis"];
    [ countryArray addObject:@"Saint Lucia"];
    [ countryArray addObject:@"Saint Pierre and Miquelon"];
    [ countryArray addObject:@"Saint Vincent and the Grenadines"];
    [ countryArray addObject:@"Samoa"];
    [ countryArray addObject:@"San Marino"];
    [ countryArray addObject:@"Sao Tome and Principe"];
    [ countryArray addObject:@"Saudi Arabia"];
    [ countryArray addObject:@"Senegal"];
    [ countryArray addObject:@"Serbia"];
    [ countryArray addObject:@"Seychelles"];
    [ countryArray addObject:@"Sierra Leone"];
    [ countryArray addObject:@"Singapore"];
    [ countryArray addObject:@"Slovakia"];
    [ countryArray addObject:@"Slovenia"];
    [ countryArray addObject:@"Solomon Islands"];
    [ countryArray addObject:@"Somalia"];
    [ countryArray addObject:@"South Africa"];
    [ countryArray addObject:@"South Georgia and the South Sandwich Islands"];
    [ countryArray addObject:@"Spain"];
    [ countryArray addObject:@"Sri Lanka"];
    [ countryArray addObject:@"Sudan"];
    [ countryArray addObject:@"Suriname"];
    [ countryArray addObject:@"Svalbard and Jan Mayen"];
    [ countryArray addObject:@"Swaziland"];
    [ countryArray addObject:@"Sweden"];
    [ countryArray addObject:@"Switzerland"];
    [ countryArray addObject:@"Syrian Arab Republic"];
    [ countryArray addObject:@"Taiwan"];
    [ countryArray addObject:@"Tajikistan"];
    [ countryArray addObject:@"Tanzania, United Republic of"];
    [ countryArray addObject:@"Thailand"];
    [ countryArray addObject:@"Timor-Leste"];
    [ countryArray addObject:@"Togo"];
    [ countryArray addObject:@"Tokelau"];
    [ countryArray addObject:@"Tonga"];
    [ countryArray addObject:@"Trinidad and Tobago"];
    [ countryArray addObject:@"Tunisia"];
    [ countryArray addObject:@"Turkey"];
    [ countryArray addObject:@"Turkmenistan"];
    [ countryArray addObject:@"Turks and Caicos Islands"];
    [ countryArray addObject:@"Tuvalu"];
    [ countryArray addObject:@"Uganda"];
    [ countryArray addObject:@"Ukraine"];
    [ countryArray addObject:@"United Arab Emirates"];
    [ countryArray addObject:@"United Kingdom"];
    [ countryArray addObject:@"United States of America"];
    [ countryArray addObject:@"United States minor outlying islands"];
    [ countryArray addObject:@"Uruguay"];
    [ countryArray addObject:@"Uzbekistan"];
    [ countryArray addObject:@"Vanuatu"];
    [ countryArray addObject:@"Venezuela"];
    [ countryArray addObject:@"Vietnam"];
    [ countryArray addObject:@"Virgin Islands, British"];
    [ countryArray addObject:@"Virgin Islands, U.S."];
    [ countryArray addObject:@"Wallis and Futuna"];
    [ countryArray addObject:@"Western Sahara"];
    [ countryArray addObject:@"Yemen"];
    [ countryArray addObject:@"Zambia"];
    [ countryArray addObject:@"Zimbabwe"];
    
    
    countyIDArray =[[NSMutableArray alloc]init];
    [countyIDArray addObject:@"AF"];
    [countyIDArray addObject:@"AX"];
    [countyIDArray addObject:@"AL"];
    [countyIDArray addObject:@"DZ"];
    [countyIDArray addObject:@"AS"];
    [countyIDArray addObject:@"AD"];
    [countyIDArray addObject:@"AO"];
    [countyIDArray addObject:@"AI"];
    [countyIDArray addObject:@"AQ"];
    [countyIDArray addObject:@"AG"];
    [countyIDArray addObject:@"AR"];
    [countyIDArray addObject:@"AM"];
    [countyIDArray addObject:@"AW"];
    [countyIDArray addObject:@"AU"];
    [countyIDArray addObject:@"AT"];
    [countyIDArray addObject:@"AZ"];
    [countyIDArray addObject:@"BS"];
    [countyIDArray addObject:@"BH"];
    [countyIDArray addObject:@"BD"];
    [countyIDArray addObject:@"BB"];
    [countyIDArray addObject:@"BY"];
    [countyIDArray addObject:@"BE"];
    [countyIDArray addObject:@"BZ"];
    [countyIDArray addObject:@"BJ"];
    [countyIDArray addObject:@"BM"];
    [countyIDArray addObject:@"BT"];
    [countyIDArray addObject:@"BO"];
    [countyIDArray addObject:@"BA"];
    [countyIDArray addObject:@"BW"];
    [countyIDArray addObject:@"BV"];
    [countyIDArray addObject:@"BR"];
    [countyIDArray addObject:@"IO"];
    [countyIDArray addObject:@"BN"];
    [countyIDArray addObject:@"BG"];
    [countyIDArray addObject:@"BF"];
    [countyIDArray addObject:@"BI"];
    [countyIDArray addObject:@"KH"];
    [countyIDArray addObject:@"CM"];
    [countyIDArray addObject:@"CA"];
    [countyIDArray addObject:@"CV"];
    [countyIDArray addObject:@"KY"];
    [countyIDArray addObject:@"CF"];
    [countyIDArray addObject:@"TD"];
    [countyIDArray addObject:@"CL"];
    [countyIDArray addObject:@"CN"];
    [countyIDArray addObject:@"CX"];
    [countyIDArray addObject:@"CC"];
    [countyIDArray addObject:@"CO"];
    [countyIDArray addObject:@"KM"];
    [countyIDArray addObject:@"CG"];
    [countyIDArray addObject:@"CD"];
    [countyIDArray addObject:@"CK"];
    [countyIDArray addObject:@"CR"];
    [countyIDArray addObject:@"CI"];
    [countyIDArray addObject:@"HR"];
    [countyIDArray addObject:@"CU"];
    [countyIDArray addObject:@"CY"];
    [countyIDArray addObject:@"CZ"];
    [countyIDArray addObject:@"DK"];
    [countyIDArray addObject:@"DJ"];
    [countyIDArray addObject:@"DM"];
    [countyIDArray addObject:@"DO"];
    [countyIDArray addObject:@"EC"];
    [countyIDArray addObject:@"EG"];
    [countyIDArray addObject:@"SV"];
    [countyIDArray addObject:@"GQ"];
    [countyIDArray addObject:@"ER"];
    [countyIDArray addObject:@"EE"];
    [countyIDArray addObject:@"ET"];
    [countyIDArray addObject:@"FK"];
    [countyIDArray addObject:@"FO"];
    [countyIDArray addObject:@"FJ"];
    [countyIDArray addObject:@"FI"];
    [countyIDArray addObject:@"FR"];
    [countyIDArray addObject:@"GF"];
    [countyIDArray addObject:@"PF"];
    [countyIDArray addObject:@"TF"];
    [countyIDArray addObject:@"GA"];
    [countyIDArray addObject:@"GM"];
    [countyIDArray addObject:@"GE"];
    [countyIDArray addObject:@"DE"];
    [countyIDArray addObject:@"GH"];
    [countyIDArray addObject:@"GI"];
    [countyIDArray addObject:@"GR"];
    [countyIDArray addObject:@"GL"];
    [countyIDArray addObject:@"GD"];
    [countyIDArray addObject:@"GP"];
    [countyIDArray addObject:@"GU"];
    [countyIDArray addObject:@"GT"];
    [countyIDArray addObject:@"GN"];
    [countyIDArray addObject:@"GW"];
    [countyIDArray addObject:@"GY"];
    [countyIDArray addObject:@"HT"];
    [countyIDArray addObject:@"HM"];
    [countyIDArray addObject:@"VA"];
    [countyIDArray addObject:@"HN"];
    [countyIDArray addObject:@"HK"];
    [countyIDArray addObject:@"HU"];
    [countyIDArray addObject:@"IS"];
    [countyIDArray addObject:@"IN"];
    [countyIDArray addObject:@"ID"];
    [countyIDArray addObject:@"IR"];
    [countyIDArray addObject:@"IQ"];
    [countyIDArray addObject:@"IE"];
    [countyIDArray addObject:@"IL"];
    [countyIDArray addObject:@"IT"];
    [countyIDArray addObject:@"JM"];
    [countyIDArray addObject:@"JP"];
    [countyIDArray addObject:@"JO"];
    [countyIDArray addObject:@"KZ"];
    [countyIDArray addObject:@"KE"];
    [countyIDArray addObject:@"KI"];
    [countyIDArray addObject:@"KP"];
    [countyIDArray addObject:@"KR"];
    [countyIDArray addObject:@"KW"];
    [countyIDArray addObject:@"KG"];
    [countyIDArray addObject:@"LA"];
    [countyIDArray addObject:@"LV"];
    [countyIDArray addObject:@"LB"];
    [countyIDArray addObject:@"LS"];
    [countyIDArray addObject:@"LR"];
    [countyIDArray addObject:@"LY"];
    [countyIDArray addObject:@"LI"];
    [countyIDArray addObject:@"LT"];
    [countyIDArray addObject:@"LU"];
    [countyIDArray addObject:@"MO"];
    [countyIDArray addObject:@"MK"];
    [countyIDArray addObject:@"MG"];
    [countyIDArray addObject:@"MW"];
    [countyIDArray addObject:@"MY"];
    [countyIDArray addObject:@"MV"];
    [countyIDArray addObject:@"ML"];
    [countyIDArray addObject:@"MT"];
    [countyIDArray addObject:@"MH"];
    [countyIDArray addObject:@"MQ"];
    [countyIDArray addObject:@"MR"];
    [countyIDArray addObject:@"MU"];
    [countyIDArray addObject:@"YT"];
    [countyIDArray addObject:@"MX"];
    [countyIDArray addObject:@"FM"];
    [countyIDArray addObject:@"MD"];
    [countyIDArray addObject:@"MC"];
    [countyIDArray addObject:@"MN"];
    [countyIDArray addObject:@"MS"];
    [countyIDArray addObject:@"MA"];
    [countyIDArray addObject:@"MZ"];
    [countyIDArray addObject:@"MM"];
    [countyIDArray addObject:@"NA"];
    [countyIDArray addObject:@"NR"];
    [countyIDArray addObject:@"NP"];
    [countyIDArray addObject:@"NL"];
    [countyIDArray addObject:@"AN"];
    [countyIDArray addObject:@"NC"];
    [countyIDArray addObject:@"NZ"];
    [countyIDArray addObject:@"NI"];
    [countyIDArray addObject:@"NE"];
    [countyIDArray addObject:@"NG"];
    [countyIDArray addObject:@"NU"];
    [countyIDArray addObject:@"NF"];
    [countyIDArray addObject:@"MP"];
    [countyIDArray addObject:@"NO"];
    [countyIDArray addObject:@"OM"];
    [countyIDArray addObject:@"PK"];
    [countyIDArray addObject:@"PW"];
    [countyIDArray addObject:@"PS"];
    [countyIDArray addObject:@"PA"];
    [countyIDArray addObject:@"PG"];
    [countyIDArray addObject:@"PY"];
    [countyIDArray addObject:@"PE"];
    [countyIDArray addObject:@"PH"];
    [countyIDArray addObject:@"PN"];
    [countyIDArray addObject:@"PL"];
    [countyIDArray addObject:@"PT"];
    [countyIDArray addObject:@"PR"];
    [countyIDArray addObject:@"QA"];
    [countyIDArray addObject:@"RE"];
    [countyIDArray addObject:@"RO"];
    [countyIDArray addObject:@"RU"];
    [countyIDArray addObject:@"RW"];
    [countyIDArray addObject:@"SH"];
    [countyIDArray addObject:@"KN"];
    [countyIDArray addObject:@"LC"];
    [countyIDArray addObject:@"PM"];
    [countyIDArray addObject:@"VC"];
    [countyIDArray addObject:@"WS"];
    [countyIDArray addObject:@"SM"];
    [countyIDArray addObject:@"ST"];
    [countyIDArray addObject:@"SA"];
    [countyIDArray addObject:@"SN"];
    [countyIDArray addObject:@"CS"];
    [countyIDArray addObject:@"SC"];
    [countyIDArray addObject:@"SL"];
    [countyIDArray addObject:@"SG"];
    [countyIDArray addObject:@"SK"];
    [countyIDArray addObject:@"SI"];
    [countyIDArray addObject:@"SB"];
    [countyIDArray addObject:@"SO"];
    [countyIDArray addObject:@"ZA"];
    [countyIDArray addObject:@"GS"];
    [countyIDArray addObject:@"ES"];
    [countyIDArray addObject:@"LK"];
    [countyIDArray addObject:@"SD"];
    [countyIDArray addObject:@"SR"];
    [countyIDArray addObject:@"SJ"];
    [countyIDArray addObject:@"SZ"];
    [countyIDArray addObject:@"SE"];
    [countyIDArray addObject:@"CH"];
    [countyIDArray addObject:@"SY"];
    [countyIDArray addObject:@"TW"];
    [countyIDArray addObject:@"TJ"];
    [countyIDArray addObject:@"TZ"];
    [countyIDArray addObject:@"TH"];
    [countyIDArray addObject:@"TL"];
    [countyIDArray addObject:@"TG"];
    [countyIDArray addObject:@"TK"];
    [countyIDArray addObject:@"TO"];
    [countyIDArray addObject:@"TT"];
    [countyIDArray addObject:@"TN"];
    [countyIDArray addObject:@"TR"];
    [countyIDArray addObject:@"TM"];
    [countyIDArray addObject:@"TC"];
    [countyIDArray addObject:@"TV"];
    [countyIDArray addObject:@"UG"];
    [countyIDArray addObject:@"UA"];
    [countyIDArray addObject:@"AE"];
    [countyIDArray addObject:@"GB"];
    [countyIDArray addObject:@"US"];
    [countyIDArray addObject:@"UM"];
    [countyIDArray addObject:@"UY"];
    [countyIDArray addObject:@"UZ"];
    [countyIDArray addObject:@"VU"];
    [countyIDArray addObject:@"VE"];
    [countyIDArray addObject:@"VN"];
    [countyIDArray addObject:@"VG"];
    [countyIDArray addObject:@"VI"];
    [countyIDArray addObject:@"WF"];
    [countyIDArray addObject:@"EH"];
    [countyIDArray addObject:@"YE"];
    [countyIDArray addObject:@"ZM"];
    [countyIDArray addObject:@"ZW"];
    
 
    myPickerView = [[UIPickerView alloc]init];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                   target:self action:@selector(done:)];
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, self.frame.size.height-
                                     myPickerView.frame.size.height-50, 320, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    _txtReigon.inputView = myPickerView;
    _txtReigon.inputAccessoryView = toolBar;
    
}
-(void)done:(id)sender
{
    [_txtReigon resignFirstResponder];
    
    [_txtContactPerson becomeFirstResponder];
}
#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return [countryArray count];
}

#pragma mark- Picker View Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component
{
    [_txtReigon setText:[countryArray objectAtIndex:row]];
    appDelegate.regionKey =[countyIDArray objectAtIndex:row];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component
{
    return [countryArray objectAtIndex:row];
}
-(IBAction)selectReigon
{
    [self addPickerView];
}

@end
