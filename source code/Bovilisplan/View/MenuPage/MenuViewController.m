//
//  MenuViewController.m
//  CUNY
//
//  Created by Mac on 10/11/2015.
//  Copyright (c) 2015 Celeritas. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuCell.h"
#import "HomeViewController.h"

@interface MenuViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSString *menuTitle ;
}

@property(nonatomic,strong) NSMutableArray *menuArray;
@property(nonatomic,strong) NSMutableArray *menuImagesArray;
@property(nonatomic,strong) IBOutlet UITableView *tableView;
@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    menuTitle=@"HOME";
    [_tableView registerNib:[UINib nibWithNibName:@"MenuCell"
                                           bundle:nil] forCellReuseIdentifier:@"MenuCell"];
    
    self.tableView.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:140.0/255.0 blue:236.0/255.0 alpha:1.0];
    self.menuArray = [[NSMutableArray alloc]initWithObjects:@"HOME",@"QUIZZES",@"RESOURCESS",@"FEEDBACK", nil];
    
    self.menuImagesArray = [[NSMutableArray alloc]initWithObjects:@"icon-Event",@"icon-ListingSelected",@"icon-NotificationSelected",@"icon-LibrarySelected", nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)filterTabBtnPressed:(id)sender
{
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"MenuCell";
    MenuCell *cell = (MenuCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    menuTitle = [self.menuArray objectAtIndex:indexPath.row];
}

- (void)tableView: (UITableView*)tableView willDisplayCell: (MenuCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    
}


@end
