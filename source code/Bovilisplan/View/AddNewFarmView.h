//
//  AddNewFarmView.h
//  Bovilisplan
//
//  Created by Admin on 5/2/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewFarmView : UIView

@property(strong,nonatomic) IBOutlet UIButton *btnCancel;
@property(strong,nonatomic) IBOutlet UIButton *btnSave;
@property(strong,nonatomic) IBOutlet UIView *innerView;
@property(nonatomic,strong) IBOutlet UILabel *lblTop;
@property(nonatomic,strong) IBOutlet UILabel *farmType;
@property(nonatomic,strong) IBOutlet UILabel *lblDairy;
@property(nonatomic,strong) IBOutlet UILabel *lblBeff;
@property(nonatomic,strong) IBOutlet UILabel *lblCow;
@property(nonatomic,strong) IBOutlet UILabel *mixed;
@property(strong,nonatomic) IBOutlet UITextField *txtFarmName;
@property(strong,nonatomic) IBOutlet UITextField *txtRegistration;
@property(strong,nonatomic) IBOutlet UITextField *txtReigon;
@property(strong,nonatomic) IBOutlet UITextField *txtContactPerson;
@property(strong,nonatomic) IBOutlet UITextField *txtAnimals;
@property(strong,nonatomic) IBOutlet UITextField *txtAddress;
@property(strong,nonatomic) IBOutlet UITextField *txtPhone;
@property(strong,nonatomic) IBOutlet UITextField *txtEmail;
@property(strong,nonatomic) IBOutlet UITextField *txtComment;
@property(strong,nonatomic) IBOutlet UIScrollView *scrollView;
@property(strong,nonatomic) IBOutlet UISwitch *switch1;
@property(strong,nonatomic) IBOutlet UISwitch *switch2;
@property(strong,nonatomic) IBOutlet UISwitch *switch3;
@property(strong,nonatomic) IBOutlet UISwitch *switch4;
@property(strong,nonatomic) IBOutlet UITextField *lblFarmName;
@property(strong,nonatomic) IBOutlet UITextField *lblRegistration;
@property(strong,nonatomic) IBOutlet UITextField *lblReigon;
@property(strong,nonatomic) IBOutlet UITextField *lblContactPerson;
@property(strong,nonatomic) IBOutlet UITextField *lblAnimals;
@property(strong,nonatomic) IBOutlet UITextField *lblAddress;
@property(strong,nonatomic) IBOutlet UITextField *lblPhone;
@property(strong,nonatomic) IBOutlet UITextField *lblEmail;
@property(strong,nonatomic) IBOutlet UITextField *lblComment;

@end

