//
//  AnimalMaster.h
//  TTVApp
//
//  Created by Admin on 9/12/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnimalMaster : NSObject

@property(strong,nonatomic) AnimalMaster *animalModel;

@property(strong,nonatomic) NSString *animalID;
@property(strong,nonatomic) NSString *animalNumber;
@property(strong,nonatomic) NSString *birthDate;
@property(strong,nonatomic) NSString *userName;
@property(strong,nonatomic) NSString *gender;
@property(strong,nonatomic) NSString *farmName;

@property(strong,nonatomic) NSString *scheduleDate;
@property(strong,nonatomic) NSString *scheduleVaccine;
@property(strong,nonatomic) NSString *status;

-(void)getAnimals;
-(NSMutableArray*)getAnimalsVaccines:(NSDictionary*)dict;

@end
