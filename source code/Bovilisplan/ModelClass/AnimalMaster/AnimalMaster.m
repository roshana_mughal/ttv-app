//
//  AnimalMaster.m
//  TTVApp
//
//  Created by Admin on 9/12/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "AnimalMaster.h"
#import "AppDelegate.h"

@implementation AnimalMaster
{
    AppDelegate * appDelegate;
}

-(void)getAnimals {
 
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *result1 = [userDefaults   objectForKey:@"AnimalsList"];
    [userDefaults synchronize];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.animalsArray =[[NSMutableArray alloc]init];
    
    NSArray *resultDic = [result1 objectForKey:@"results"];
    
    if ([result1 count] != 0){
        for (int i = 0; i<[resultDic count]; i++)
        {
            id item = [resultDic objectAtIndex:i];
            
            NSDictionary *jsonDict = (NSDictionary *) item;
            AnimalMaster  *theObject =[[AnimalMaster alloc] init];
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])
            {
                [theObject setAnimalID:[jsonDict valueForKey:@"ID"]];
                [theObject setAnimalNumber:[jsonDict valueForKey:@"AnimalNumber"]];
                [theObject setBirthDate:[jsonDict valueForKey:@"BirthDate"]];
                [theObject setGender:[jsonDict valueForKey:@"Gender"]];
                [theObject setFarmName:[jsonDict valueForKey:@"FarmName"]];
                [appDelegate.animalsArray addObject:theObject];
            }
        }
    }
}

-(NSMutableArray*)getAnimalsVaccines:(NSDictionary*)dict  {
     
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSArray *resultDic = [dict objectForKey:@"results"];
    NSMutableArray *localArray = [NSMutableArray new];
    if ([dict count] != 0){
        for (int i = 0; i<[resultDic count]; i++) {
            id item = [resultDic objectAtIndex:i];
            
            NSDictionary *jsonDict = (NSDictionary *) item;
            AnimalMaster  *theObject =[[AnimalMaster alloc] init];
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])  {
                
                [theObject setScheduleDate:[jsonDict valueForKey:@"ScheduleDate"]];
                [theObject setScheduleVaccine:[jsonDict valueForKey:@"ScheduleVaccine"]];
                [theObject setStatus:[jsonDict valueForKey:@"ScheduleStatus"]];
                [localArray addObject:theObject];
                
            }
        }
    }
    return localArray;
}

@end
