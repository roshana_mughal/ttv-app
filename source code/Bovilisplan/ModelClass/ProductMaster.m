//
//  ProductMaster.m
//  TTVApp
//
//  Created by Apple Macbook on 30/05/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "ProductMaster.h"
#import "AppDelegate.h"

@implementation ProductMaster
{
    AppDelegate * appDelegate;
}
-(void)getProductsVaccineData:(NSDictionary*)result
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *result1 = [userDefaults   objectForKey:@"VaccineList"];
    [userDefaults synchronize];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.productsArray =[[NSMutableArray alloc]init];
    
    NSArray *resultDic = [result1 objectForKey:@"results"];
    if ([result1 count] != 0) {
        for (int i = 0; i<[resultDic count]; i++)
        {
            id item = [resultDic objectAtIndex:i];
            
            NSDictionary *jsonDict = (NSDictionary *) item;
            ProductMaster  *theObject =[[ProductMaster alloc] init];
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])
            {
                [theObject setVaccineID:[jsonDict valueForKey:@"VaccineID"]];
                [theObject setVaccineName:[jsonDict valueForKey:@"VaccineName"]];
                [appDelegate.productsArray addObject:theObject];
            }
        }
    }
}

-(void)getProductsListData:(NSDictionary*)result
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *result1 = [userDefaults   objectForKey:@"ProductList"];
    [userDefaults synchronize];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.productsArrayList =[[NSMutableArray alloc]init];
    
    NSArray *resultDic = [result1 objectForKey:@"results"];
    if ([result1 count] != 0) {
        for (int i = 0; i<[resultDic count]; i++)
        {
            id item = [resultDic objectAtIndex:i];
            NSDictionary *jsonDict = (NSDictionary *) item;
            ProductMaster  *theObject =[[ProductMaster alloc] init];
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])
            {
                [theObject setProductIDL:[jsonDict valueForKey:@"ProductID"]];
                [theObject setVaccineNameL:[jsonDict valueForKey:@"VaccineName"]];
                [theObject setDosageL:[jsonDict valueForKey:@"Dosage"]];
                [theObject setAddministrationL:[jsonDict valueForKey:@"Administration"]];
                [theObject setLinksAndDownloadsL:[jsonDict valueForKey:@"LinksAndDownloads"]];
                [theObject setMoreInformationL:[jsonDict valueForKey:@"MoreInformation"]];
                [theObject setPrimaryL:[jsonDict valueForKey:@"PrimaryOne"]];
                [theObject setSecondaryOneL:[jsonDict valueForKey:@"SecondaryOne"]];
                [theObject setSecondaryTwoL:[jsonDict valueForKey:@"SecondaryTwo"]];
                [theObject setSecondaryThreeL:[jsonDict valueForKey:@"SecondaryThree"]];
                [appDelegate.productsArrayList addObject:theObject];
            }
        }
    }
}

@end
