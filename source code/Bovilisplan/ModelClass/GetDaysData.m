////
//  GetDaysData.m
//  TTVApp
//
//  Created by Apple Macbook on 01/06/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "GetDaysData.h"
#import "AppDelegate.h"
#import "HomeViewController.h"

@implementation GetDaysData

-(void)getUserVaccineDataWithDate:(NSString*)date
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *result1 = [userDefaults   objectForKey:@"ScheduleVaccinesList"];
    [userDefaults synchronize];
    
    AppDelegate*appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.getVaccinesWithDate =[[NSMutableArray alloc]init];
    
    NSArray *resultDic = [result1 objectForKey:@"results"];
    if ([result1 count] != 0){
        for (int i = 0; i<[resultDic count]; i++)
        {
            id item = [resultDic objectAtIndex:i];
            NSDictionary *jsonDict = (NSDictionary *) item;
            GetDaysData  *theObject =[[GetDaysData alloc] init];
            
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])
            {
                NSString *selectedDate =[jsonDict valueForKey:@"AddedDateTime"];
                NSDateFormatter *dateFormatC = [[NSDateFormatter alloc] init];
                [dateFormatC setDateFormat:@"yyyy-MM-dd"];
                NSDate * serverDate = [dateFormatC dateFromString:selectedDate];
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd"];
                
                NSString * newvaccineDate= [dateFormat stringFromDate:serverDate];
                
                if ([newvaccineDate isEqualToString:date])
                {
                    [theObject setVaccineIDforEdit:[jsonDict valueForKey:@"ID"]];
                    [theObject setScheduleType1:[jsonDict valueForKey:@"ScheduleVaccine"]];
                    [theObject setNotes1:[jsonDict valueForKey:@"Notes"]];
                    [theObject setVaccinationType1:[jsonDict valueForKey:@"VaccineType"]];
                    [theObject setVaccineName1:[jsonDict valueForKey:@"VaccineName"]];
                    [theObject setFarmName1:[jsonDict valueForKey:@"FarmName"]];
                    [theObject setDay1:[jsonDict valueForKey:@"Day"]];
                    [theObject setAddedDateTime1:[jsonDict valueForKey:@"AddedDateTime"]];
                    [theObject setRegion1:[jsonDict valueForKey:@"Region"]];
                    [theObject setImage1:[jsonDict valueForKey:@"Image1"]];
                    [theObject setNumberOfAnimals1:[jsonDict valueForKey:@"NumberOfAnimals"]];
                    [appDelegate.getVaccinesWithDate addObject:theObject];
                }
            }
        }
    }
}

@end
