//
//  UserVacineMaster.m
//  Bovilisplan
//
//  Created by Apple Macbook on 24/05/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "UserVacineMaster.h"
#import "AppDelegate.h"

@implementation UserVacineMaster

-(void)getUserVaccinesDataWithSelectedDate:(NSString*)date
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *result1 = [userDefaults   objectForKey:@"VaccineData"];
    [userDefaults synchronize];
    
    AppDelegate * appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.getUserVaccinesFromServer =[[NSMutableArray alloc]init];
    NSArray *resultDic = [result1 objectForKey:@"results"];
    
    if ([result1 count] != 0){
        for (int i = 0; i<[resultDic count]; i++)
        {
            id item = [resultDic objectAtIndex:i];
            NSDictionary *jsonDict = (NSDictionary *) item;
            UserVacineMaster  *theObject =[[UserVacineMaster alloc] init];
            
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])
            {
                NSString *selectedDate =[jsonDict valueForKey:@"Date"];
                if ([date isEqualToString:selectedDate])
                {
                    [theObject setDate:[jsonDict valueForKey:@"Date"]];
                    [theObject setFarmName:[jsonDict valueForKey:@"FarmName"]];
                    [theObject setVaccineName:[jsonDict valueForKey:@"VaccineName"]];
                    [theObject setAnimalNumber:[jsonDict valueForKey:@"AnimalNumber"]];
                    [theObject setScheduleVaccine:[jsonDict valueForKey:@"ScheduleVaccine"]];
                    [theObject setVaccineType:[jsonDict valueForKey:@"VaccineType"]];
                    [theObject setNotes:[jsonDict valueForKey:@"Notes"]];
                    [theObject setImage1:[jsonDict valueForKey:@"Image1"]];
                    [theObject setImage2:[jsonDict valueForKey:@"Image2"]];
                    [theObject setImage3:[jsonDict valueForKey:@"Image3"]];
                    [theObject setImage4:[jsonDict valueForKey:@"Image4"]];
                    [theObject setImage5:[jsonDict valueForKey:@"Image5"]];
                    [theObject setAddedDateTime:[jsonDict valueForKey:@"AddedDateTime"]];
                    [appDelegate.getUserVaccinesFromServer addObject:theObject];
                }
            }
        }
    }
}
-(void)getUserVaccinesDataWithFarmName:(NSString*)vName
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *result1 = [userDefaults   objectForKey:@"VaccineData"];
    [userDefaults synchronize];
    
    AppDelegate * appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.getUserVaccineWithFarmName =[[NSMutableArray alloc]init];
    
    NSArray *resultDic = [result1 objectForKey:@"results"];
    
    if ([result1 count] != 0){
        for (int i = 0; i<[resultDic count]; i++)
        {
            id item = [resultDic objectAtIndex:i];
            NSDictionary *jsonDict = (NSDictionary *) item;
            UserVacineMaster  *theObject =[[UserVacineMaster alloc] init];
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])
            {
                NSString *fName =[jsonDict valueForKey:@"FarmName"];
                if ([vName isEqualToString:fName])
                {
                    [theObject setIdForEdit:[jsonDict valueForKey:@"ID"]];
                    [theObject setDate1:[jsonDict valueForKey:@"Date"]];
                    [theObject setFarmName1:[jsonDict valueForKey:@"FarmName"]];
                    [theObject setVaccineName1:[jsonDict valueForKey:@"VaccineName"]];
                    [theObject setAnimalNumber1:[jsonDict valueForKey:@"AnimalNumber"]];
                    [theObject setScheduleVaccine1:[jsonDict valueForKey:@"ScheduleVaccine"]];
                    [theObject setVaccineType1:[jsonDict valueForKey:@"VaccineType"]];
                    [theObject setNotes1:[jsonDict valueForKey:@"Notes"]];
                    [theObject setImage11:[jsonDict valueForKey:@"Image1"]];
                    [theObject setImage21:[jsonDict valueForKey:@"Image2"]];
                    [theObject setImage31:[jsonDict valueForKey:@"Image3"]];
                    [theObject setImage41:[jsonDict valueForKey:@"Image4"]];
                    [theObject setImage51:[jsonDict valueForKey:@"Image5"]];
                    [theObject setAddedDateTime1:[jsonDict valueForKey:@"AddedDateTime"]];
                    
                    NSDateFormatter *dateFormater= [[NSDateFormatter alloc] init];
                    [dateFormater setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
                    
                    NSDate *newDate = [dateFormater dateFromString:[jsonDict valueForKey:@"AddedDateTime"]];
                    
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                    [dateFormat setDateFormat:@"dd-MMM-yyyy"];
                    NSString *dateFinal = [dateFormat stringFromDate:newDate];
                    [theObject setAddedDateTimeFormatted:dateFinal];
                    [appDelegate.getUserVaccineWithFarmName addObject:theObject];
                }
            }
        }
    }
}

@end
