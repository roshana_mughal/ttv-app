//
//  ContentMasterModel.m
//  Capalino
//
//  Created by Admin on 5/23/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ContentMasterModel.h"
#import "AppDelegate.h"



@implementation ContentMasterModel{
    AppDelegate *appDelegate;
    
    
}
@synthesize contentPostedDate,contentTitle,contentID,contentType,contentStatusCode,contentPostedByUser,contentReferenceURL,contentExpirationDate,contentLastUpdate,contentRelevantDateTime,ID,eventCost,eventLocation,eventEndDateTime,eventStartDateTime,deletedTitleArray;

/*

-(void)getContentMasterData:(NSString *)dbPath{
    
    appDelegate = [[UIApplication sharedApplication]delegate];
    [appDelegate.contentMasterArray removeAllObjects];
    
   NSString *query=  [NSString stringWithFormat:@"SELECT * From ContentMasterUpdated"];

    
   // NSString *query=  [NSString stringWithFormat:@"SELECT * From ContentMasterUpdated ORDER BY date(ContentPostedDate) DESC "];

    
    const char *sql =[query UTF8String];
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW) {
                
                NSInteger primaryKey = sqlite3_column_int(selectstmt, 0);
                ContentMasterModel *contentObj = [[ContentMasterModel alloc] initWithPrimaryKey:primaryKey];
                
                contentObj.contentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                contentObj.contentTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                contentObj.contentDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                contentObj.eventStartDateTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,3)];
                contentObj.eventEndDateTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,4)];
               
                contentObj.eventLocation = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,5)];
                
                contentObj.eventCost = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,6)];
                contentObj.contentReferenceURL = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                contentObj.contentPostedDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,8)];
                
                contentObj.contentLastUpdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,9)];
                
                
                contentObj.contentType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,10)];

          //      NSLog(@"post Date : %@",contentObj.contentPostedDate);
                [appDelegate.contentMasterArray addObject:contentObj];
                
            }
        }
        
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
}

-(void)deleteDataFromContentMaster:(NSString*)dbPath withContent:(NSString*)content;
{
    //delete selected row
    
    
     sqlite3_open([dbPath UTF8String], &database);
    
    NSString *query = [NSString stringWithFormat:@"DELETE from ContentMasterUpdated where ContentTitle='%@'",content];
    
    const char *sqlStatement = [query UTF8String];
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
      
        
        }
        
        // Release the compiled statement from memory
        
        sqlite3_finalize(compiledStatement);

        sqlite3_close(database);
    
    }
    
    
    
}
+(void)deleteContentDataFromLocal:(NSString*)dbPath
{
    sqlite3_open([dbPath UTF8String], &database);
    
        NSString *query = @"DELETE from ContentMasterUpdated";
        const char *sqlStatement = [query UTF8String];
           sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                
            }
            
            // Release the compiled statement from memory
              sqlite3_finalize(compiledStatement);
            sqlite3_close(database);
           
        }

}

-(void)setContentDataToLocalDB:(NSString *)dbPath{

    
    if(addStmt == nil) {
        
        
        
        const char *sql = "insert into ContentMasterUpdated(ContentTitle,ContentDescription,EventStartDateTime,EventEndDateTime,EventLocation,EventCost,ReferenceURL,ContentPostedDate,LASTUPDATE,ContentType) Values(?,?,?,?,?,?,?,?,?,?)";
        
        if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
            
            if(sqlite3_prepare_v2(database, sql, -1, &addStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        
        
    }
    
    // sqlite3_bind_text(addStmt, 0, [contentID UTF8String], -1, SQLITE_TRANSIENT);

       sqlite3_bind_text(addStmt, 1, [contentTitle UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(addStmt, 2, [_contentDescription UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(addStmt, 3, [eventStartDateTime UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(addStmt, 4, [eventEndDateTime UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(addStmt, 5, [eventLocation UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(addStmt, 6, [eventCost UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(addStmt, 7, [contentReferenceURL UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(addStmt, 8, [contentPostedDate UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(addStmt, 9, [contentLastUpdate UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(addStmt, 10, [contentType UTF8String], -1, SQLITE_TRANSIENT);
    
    
        NSLog(@"post Date : %@",contentPostedDate);
    
    if(SQLITE_DONE != sqlite3_step(addStmt))
        NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
    else
        
    {  
        ID = (NSInteger)sqlite3_last_insert_rowid(database);
    }
    //Reset the add statement.
    sqlite3_reset(addStmt);
    // sqlite3_finalize(addStmt);
    sqlite3_close(database);
    
    
}

- (id) initWithPrimaryKey:(NSInteger) pk {
    
    ID = pk;
    
    //	NSLog(@"primary key %d ", pk);
    return self;
}
*/

@end
