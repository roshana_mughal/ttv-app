//
//  ContentMasterModel.h
//  Capalino
//
//  Created by Admin on 5/23/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface ContentMasterModel : NSObject


{


    

}

@property (strong,nonatomic) NSMutableArray *deletedTitleArray;
@property (strong,nonatomic) NSString *contentID;
@property (strong,nonatomic) NSString *contentType;
@property (strong,nonatomic) NSString *contentTitle;
@property (strong,nonatomic) NSString *contentDescription;
@property (strong,nonatomic) NSString *contentReferenceURL;
@property (strong,nonatomic) NSString *eventStartDateTime;
@property (strong,nonatomic) NSString *eventEndDateTime;
@property (strong,nonatomic) NSString *eventLocation;
@property (strong,nonatomic) NSString *eventCost;

@property (strong,nonatomic) NSString *contentPostedByUser;
@property (strong,nonatomic) NSString *contentPostedDate;
@property (strong,nonatomic) NSString *contentStatusCode;
@property (strong,nonatomic) NSString *contentExpirationDate;

@property (strong,nonatomic)NSString *contentRelevantDateTime;


@property (strong,nonatomic)NSString *contentLastUpdate;




@property (nonatomic) NSInteger ID;

-(void)getContentMasterData:(NSString *)dbPath;


-(void)deleteDataFromContentMaster:(NSString*)dbPath withContent:(NSString*)content;


+(void)deleteContentDataFromLocal:(NSString*)dbPath;

-(void)setContentDataToLocalDB:(NSString *)dbPath;
@end
