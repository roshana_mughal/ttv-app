//
//  GetDaysData.h
//  TTVApp
//
//  Created by Apple Macbook on 01/06/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetDaysData : NSObject

@property(nonatomic) NSString *vaccineID;
@property(nonatomic) NSString *vaccineName;
@property(nonatomic) NSString *vaccinationType;
@property(nonatomic) NSString *farmName;
@property(nonatomic) NSString *addedDateTime;
@property(nonatomic) NSString *vaccineType;
@property(nonatomic) NSString *vaccineNameFrom;
@property(nonatomic) NSString *vaccineTypeFrom;
@property(nonatomic) NSString *day;
@property(nonatomic) NSString *vaccineName1;
@property(nonatomic) NSString *vaccinationType1;
@property(nonatomic) NSString *farmName1;
@property(nonatomic) NSString *addedDateTime1;
@property(nonatomic) NSString *scheduleType1;
@property(nonatomic) NSString *day1;
@property(nonatomic) NSString *notes1;
@property(nonatomic) NSString *region1;
@property(nonatomic) NSString *image1;
@property(nonatomic) NSString *numberOfAnimals1;
@property(nonatomic) NSString *vaccineIDforEdit;
@property(nonatomic) NSString *uniqueID;
@property(nonatomic) NSString *vaccineName2;
@property(nonatomic) NSString *vaccineFarmID2;
@property(nonatomic) NSString *vaccinationType2;
@property(nonatomic) NSString *vaccineType2;
@property(nonatomic) NSString *farmName2;
@property(nonatomic) NSString *addedDateTime2;
@property(nonatomic) NSString *addedDateTimeWithFormat2;
@property(nonatomic) NSString *addedDateForCheck2;
@property(nonatomic) NSString *scheduleType2;
@property(nonatomic) NSString *day2;
@property(nonatomic) NSString *uniqueID2;
@property(nonatomic) NSString *notes2;
@property(nonatomic) NSString *region2;
@property(nonatomic) NSString *numberOfAnimals2;

-(void)getUserVaccineDataWithDate:(NSString*)date;

@end
