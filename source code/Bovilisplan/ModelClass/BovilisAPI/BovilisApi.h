//
//  BovilisApi.h
//  Bovilisplan
//
//  Created by Admin on 5/17/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "LibraryViewController.h"
#import "AFNetworking.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface BovilisApi : NSObject

@property(nonatomic,strong) AFHTTPSessionManager *sessionManager;

+(instancetype)sharedInstance;

-(void)sendToken:(NSString*)tID  WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)getVaccineMappingDaysWithVaccineID:(NSString*)vID ViewController:(UIViewController*)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)getVaccineSchedule:(NSString*)email WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)getAnimalsDataWithID:(NSString*)fID WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)addAnimalsDataWithID:(NSMutableDictionary*)animDict WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)requestForGetUserFarmDataWithUserEmail:(NSString *)userEmail;

-(void)requestGetProducts;

-(void)requestGetProductsList;

-(void)deleteFarmFromServerWithFarmID:(NSString *)farmID ViewController:(UIViewController*)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)deleteScheduleVaccineFromServerWithID:(NSString *)vID ;

-(void)requestImagedataFromServer:(NSString *)vaccineID WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;


-(void)deleteScheduleVaccineFromServerWithID:(NSString *)vID  WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)sentNewUserVaccine:(NSString*)fName withDate:(NSString*)date withAnimalNumber:(NSString*)animalNumber withScheduleVaccine:(NSString*)scheduleVaccine withVaccineType:(NSString*)vaccineType withNumberOfVaccineCattle:(NSString*)vaccineCattle withNotes:(NSString*)notes withImage1:(NSString*)image1 withImage2:(NSString*)image2 withImage3:(NSString*)image3 withImage4:(NSString*)image4 withImage5:(NSString*)image5 withAddedDateTime:(NSString*)addedDateTime withUserName:(NSString*)userName withVaccineName:(NSString *)vaccineName withVaccineID:(NSString *)vaccineID withFarmID:(NSString*)farmID;

-(void)sentNewFarmDataToServerWithFarmName:(NSString*)fName withRegNO:(NSString*)regNo withRegion:(NSString*)region withContact:(NSString*)contact withPhone:(NSString*)phone withNotes:(NSString*)notes withEmail:(NSString*)email withAnimalNumber:(NSString *)animalNumber withAddress:(NSString*)address withFarmType1:(NSString *)farmType1 withUserName:(NSString *)userName withAddedDateTime:(NSString *)addedDateTime withFarmType2:(NSString *)farmType2 withFarmType3:(NSString *)farmType3 withFarmType4:(NSString *)farmType4  WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;


-(void)updateEventWithID:(NSString *)eID withNotes:(NSString*)notes WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)editNewFarmDataToServerWithFarmName:(NSString*)fName withRegNO:(NSString*)regNo withRegion:(NSString*)region withContact:(NSString*)contact withPhone:(NSString*)phone withNotes:(NSString*)notes withEmail:(NSString*)email withAnimalNumber:(NSString *)animalNumber withAddress:(NSString*)address withFarmType1:(NSString *)farmType1 withUserName:(NSString *)userName withAddedDateTime:(NSString *)addedDateTime withFarmType2:(NSString *)farmType2 withFarmType3:(NSString *)farmType3 withFarmType4:(NSString *)farmType4 withFarmID:(NSString *)farmID  WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)updateScheduleVaccine:(NSString*)vID  withNumberOfAnimals:(NSString*)animals withAddedTime:(NSString*)time WithSuccessBlock:(void (^)(BOOL successVaccine, NSDictionary* resultVaccine))success failure:(void (^)(NSError *error, NSString *message))failure;
-(void)deletenimalFromServerWithAnimalID:(NSString *)animalID ViewController:(UIViewController*)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;
-(void)updateAnimalsDataWithID:(NSMutableDictionary*)animDict WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;
-(void)getAnimalsVaccinesDataWithID:(NSString*)animalNumber WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure ;
-(void)getVaccineCountDataWithID:(NSString*)vID WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;
-(void)getAnimalsDataWithVaccineID:(NSString*)vaccineID WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

@end
