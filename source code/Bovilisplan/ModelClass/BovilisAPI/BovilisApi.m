//
//  BovilisApi.m
//  Bovilisplan
//
//  Created by Admin on 5/17/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "BovilisApi.h"
#import "Utility.h"
#import "UserFarmMasterModel.h"
#import "LibraryViewController.h"
#import "AFNetworking.h"
#import "UserVacineMaster.h"
#import "ProductMaster.h"
#import "AppDelegate.h"
#import "GetDaysData.h"
#import "DataController.h"
#import "AnimalMaster.h"
#import "GetMappingDays.h"
#import "SBJSON.h"

@implementation BovilisApi

-(id) init
{
    self = [super init];
    
    self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    NSSet *set = [NSSet setWithObjects:KAcceptContentTypeJSON,kAcceptContentTypeText,nil];
    self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [self.sessionManager.responseSerializer     setAcceptableContentTypes:set];
    self.sessionManager.requestSerializer =     [AFJSONRequestSerializer serializer];
    self.sessionManager.responseSerializer =    [AFJSONResponseSerializer serializer];
     [self.sessionManager.requestSerializer setValue:[NSString stringWithFormat:@"596a9fe0-368f-4eea-aaae-faa429733aa0"] forHTTPHeaderField:@"X-App-Token"];
 
    return self;
}

#pragma mark ==== shared instance  ======
+(instancetype)sharedInstance
{
    static BovilisApi *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[BovilisApi alloc] init];
    });
    return sharedClient;
}

-(void)getVaccineMappingDaysWithVaccineID:(NSString*)vID ViewController:(UIViewController *)currentViewController WithSuccessBlock:(void (^)(BOOL, NSDictionary *))success failure:(void (^)(NSError *, NSString *))failure
{
    [MBProgressHUD showHUDAddedTo:currentViewController.view animated:YES];

    NSDictionary *parameters = @{@"ID":vID};
    [self.sessionManager POST:KgetVaccineMappingDaysURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
             [userDefaults setObject:responseObject  forKey:@"DaysForScheduleVaccine"];
             [userDefaults synchronize];
             GetMappingDays * daysD = [[GetMappingDays alloc] init];
             [daysD getDaysForVaccineSchedule];
             [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
     }];
}

-(void)requestImagedataFromServer:(NSString *)vaccineID WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure{
    
    NSDictionary *parameters = @{@"ID":vaccineID};
    [self.sessionManager POST:kGetImageDataFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success)
         {
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         
     }];
}

/******Get Vaccine *********************/
-(void)getVaccineSchedule:(NSString*)email WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure{
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{
                                 @"userName":userEmail,
                                 };
    
    [self.sessionManager POST:KgetVaccineScheduleURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
             [userDefaults setObject:responseDict  forKey:@"ScheduleVaccinesList"];
             [userDefaults synchronize];
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         
     }];
}


/******Update  Vaccine *********************/
-(void)updateScheduleVaccine:(NSString*)vID  withNumberOfAnimals:(NSString*)animals withAddedTime:(NSString*)time WithSuccessBlock:(void (^)(BOOL successVaccine, NSDictionary* resultVaccine))success failure:(void (^)(NSError *error, NSString *message))failure{
    
    NSDictionary *parameters = @{
                                 @"ID":vID,
                                 @"AnimalNumber":animals,
                                 @"AddedDateTime":time
                                 };
    
    [self.sessionManager POST:KUpdateScheduledVaccineURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

/******Get Animals *********************/

-(void)addAnimalsDataWithID:(NSMutableDictionary*)animDict WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure{
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{
                                 @"UserName":userEmail,
                                 @"AnimalNumber" :[animDict valueForKey:@"animalNumber"],
                                 @"Gender" :[animDict valueForKey:@"gender"],
                                 @"FarmName" :[animDict valueForKey:@"farmName"],
                                 @"BirthDate" :[animDict valueForKey:@"birthDate"],
                                 };
    
    [self.sessionManager POST:KAddAnimalsFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

-(void)updateAnimalsDataWithID:(NSMutableDictionary*)animDict WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
    
    
    NSDictionary *parameters = @{
                                 @"ID":[animDict valueForKey:@"ID"],
                                 @"BirthDate" :[animDict valueForKey:@"birthDate"],
                                 @"Gender" :[animDict valueForKey:@"gender"],
                              
                                 };
    
    [self.sessionManager POST:KUpdateAnimalsFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

-(void)getAnimalsVaccinesDataWithID:(NSString*)animalNumber WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
      NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
      [userDefaults1 synchronize];
      
    
    NSDictionary *parameters = @{
                                 @"userEmail":userEmail,
                                 @"AnimalID" : animalNumber
                              
                                 };
    
    [self.sessionManager POST:KgetAnimalsVaccinesFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

-(void)getAnimalsDataWithID:(NSString*)fID WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure{
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{
                                 @"userEmail":userEmail,
                                 @"FarmID" :fID,
                                 };
    
    [self.sessionManager POST:KgetAnimalsFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
             [userDefaults setObject:responseDict  forKey:@"AnimalsList"];
             [userDefaults synchronize];
             success(YES,responseDict);
             
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

-(void)getAnimalsDataWithVaccineID:(NSString*)vaccineID WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{
                                 @"AnimalID":vaccineID,
                                 @"userEmail" :userEmail,
                                 };
    
    [self.sessionManager POST:KGetVaccineAnimalsFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
             [userDefaults setObject:responseDict  forKey:@"AnimalsList"];
             [userDefaults synchronize];
             success(YES,responseDict);
             
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}


-(void)updateEventWithID:(NSString *)eID withNotes:(NSString*)notes WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure{
    
    NSDictionary *parameters = @{
                                 @"ID":eID,
                                 @"Notes":notes,
                                 };
    
    [self.sessionManager POST:KUpdateEvent parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         failure(error,error.localizedDescription);
     }];
}

-(void)requestGetProducts
{
    [self.sessionManager POST:KGetProductsFromServer parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
         [userDefaults setObject:responseObject  forKey:@"VaccineList"];
         [userDefaults synchronize];
         
         ProductMaster *obj=[ProductMaster new];
         [obj getProductsVaccineData:responseObject];
         
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

-(void)requestGetProductsList
{
    
    
 /* AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://ec2-34-228-197-182.compute-1.amazonaws.com/NewApis/api/"]];
    NSSet *set = [NSSet setWithObjects:KAcceptContentTypeJSON,kAcceptContentTypeText,nil];
     sessionManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [sessionManager.responseSerializer     setAcceptableContentTypes:set];
    sessionManager.requestSerializer =     [AFJSONRequestSerializer serializer];
    sessionManager.responseSerializer =    [AFJSONResponseSerializer serializer];
    NSString*testURL = @"ProductListLibrary/GetProductList";*/
    //KGetProductsListFromServer
    [self.sessionManager POST:KGetProductsListFromServer parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
         [userDefaults setObject:responseObject  forKey:@"ProductList"];
         [userDefaults synchronize];
         
         ProductMaster *obj=[ProductMaster new];
         [obj getProductsListData:responseObject];
         
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

/******Schedule Vaccine *********************/
-(void)sentNewUserVaccine:(NSString*)fName withDate:(NSString*)date withAnimalNumber:(NSString*)animalNumber withScheduleVaccine:(NSString*)scheduleVaccine withVaccineType:(NSString*)vaccineType withNumberOfVaccineCattle:(NSString*)vaccineCattle withNotes:(NSString*)notes withImage1:(NSString*)image1 withImage2:(NSString*)image2 withImage3:(NSString*)image3 withImage4:(NSString*)image4 withImage5:(NSString*)image5 withAddedDateTime:(NSString*)addedDateTime withUserName:(NSString*)userName withVaccineName:(NSString *)vaccineName withVaccineID:(NSString *)vaccineID withFarmID:(NSString*)farmID
{
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    NSString *firstName = [userDefaults1   valueForKey:@"UserFirstName"];
    NSString *lastName = [userDefaults1   valueForKey:@"UserLastName"];

    
    

    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{
                                 @"Date":date,
                                 @"FarmName":fName,
                                 @"AnimalNumber":animalNumber,
                                 @"ScheduleVaccine":scheduleVaccine,
                                 @"VaccineType":vaccineType,
                                 @"NumberOfVaccinatedCattle":vaccineCattle,
                                 @"Notes":notes,
                                 @"Image1":image1,
                                 @"AddedDateTime":addedDateTime,
                                 @"UserName":userEmail,
                                 @"VaccineName":vaccineName,
                                 @"VaccineID":vaccineID,
                                 @"FarmID":farmID,
                                 @"UserFirstName" :firstName ,
                                 @"UserLastName":lastName
                                 
                                 };
    
    NSMutableArray *array =[[NSMutableArray alloc]init];
    [array addObject:parameters];
    
    [self.sessionManager POST:kSendNewUserVaccinesDataToServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
     AppDelegate * appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
     appDelegate.methodComplete = @"Yes";
     }
     failure:^(NSURLSessionTask *operation, NSError *error)
     {
     
     }];
    
 }

/******Edit Form Data *********************/
-(void)editNewFarmDataToServerWithFarmName:(NSString*)fName withRegNO:(NSString*)regNo withRegion:(NSString*)region withContact:(NSString*)contact withPhone:(NSString*)phone withNotes:(NSString*)notes withEmail:(NSString*)email withAnimalNumber:(NSString *)animalNumber withAddress:(NSString*)address withFarmType1:(NSString *)farmType1 withUserName:(NSString *)userName withAddedDateTime:(NSString *)addedDateTime withFarmType2:(NSString *)farmType2 withFarmType3:(NSString *)farmType3 withFarmType4:(NSString *)farmType4 withFarmID:(NSString *)farmID  WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure
{
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{
                                 @"FarmName":fName,
                                 @"RegistrationNumber":regNo,
                                 @"Region":region,
                                 @"ContactPerson":contact,
                                 @"Address":address,
                                 @"NumberOfAnimals":animalNumber,
                                 @"Phone":phone,
                                 @"Email":email,
                                 @"Comments":notes,
                                 @"FarmType1":farmType1,
                                 @"UserName":userEmail,
                                 @"AddedDateTime":addedDateTime,
                                 @"FarmType2":farmType2,
                                 @"FarmType3":farmType3,
                                 @"FarmType4":farmType4,
                                 @"FarmID":farmID,
                                 };
    
    NSMutableArray *array =[[NSMutableArray alloc]init];
    [array addObject:parameters];
    
    [self.sessionManager POST:kUpdateNewFormDataToServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

-(void)requestForGetUserFarmDataWithUserEmail:(NSString *)userEmail
{
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail1 = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{@"userName":userEmail1};
    
    [self.sessionManager POST:kGetDataFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         UserFarmMasterModel *obj=[UserFarmMasterModel new];
         [obj getUserFarmData:responseObject];
         
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         
     }];
}

-(void)deleteScheduleVaccineFromServerWithID:(NSString *)vID  WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure{
    
    
    NSDictionary *parameters = @{
                                 @"ID": vID
                                 };
    
    [self.sessionManager POST:kDeleteScheduleVaccineFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
         }
         
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

-(void)deleteScheduleVaccineFromServerWithID:(NSString *)vID
{
    NSDictionary *parameters = @{
                                 @"ID": vID
                                 };
    
    [self.sessionManager POST:kDeleteScheduleVaccineFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}


-(void)deleteFarmFromServerWithFarmID:(NSString *)farmID ViewController:(UIViewController*)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure
{
    

      [MBProgressHUD showHUDAddedTo:currentViewController.view animated:YES];
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{@"userEmail": userEmail,
                                 @"FarmID": farmID
                                 };
    
    [self.sessionManager POST:KDeleteFarmFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
         }
                
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
           [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
     }];
}

-(void)sentNewFarmDataToServerWithFarmName:(NSString*)fName withRegNO:(NSString*)regNo withRegion:(NSString*)region withContact:(NSString*)contact withPhone:(NSString*)phone withNotes:(NSString*)notes withEmail:(NSString*)email withAnimalNumber:(NSString *)animalNumber withAddress:(NSString*)address withFarmType1:(NSString *)farmType1 withUserName:(NSString *)userName withAddedDateTime:(NSString *)addedDateTime withFarmType2:(NSString *)farmType2 withFarmType3:(NSString *)farmType3 withFarmType4:(NSString *)farmType4  WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure{
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{
                                 @"FarmName":fName,
                                 @"RegistrationNumber":regNo,
                                 @"Region":region,
                                 @"ContactPerson":contact,
                                 @"Address":address,
                                 @"NumberOfAnimals":animalNumber,
                                 @"Phone":phone,
                                 @"Email":email,
                                 @"Comments":notes,
                                 @"FarmType1":farmType1,
                                 @"UserName":userEmail,
                                 @"AddedDateTime":addedDateTime,
                                 @"FarmType2":farmType2,
                                 @"FarmType3":farmType3,
                                 @"FarmType4":farmType4,
                                 };
    
    NSMutableArray *array =[[NSMutableArray alloc]init];
    [array addObject:parameters];
    
    [self.sessionManager POST:kSendNewFormDataToServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
             
         }
         
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

-(void)deletenimalFromServerWithAnimalID:(NSString *)animalID ViewController:(UIViewController*)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure
{
    
    
    [MBProgressHUD showHUDAddedTo:currentViewController.view animated:YES];
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{
                                 @"AnimalID": animalID,
                                 @"userEmail": userEmail

                                 
                                 };
    
    [self.sessionManager POST:KDeleteAnimalsFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             success(YES,responseDict);
         }
         
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
     }];
}


-(void)getVaccineCountDataWithID:(NSString*)vID WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
    [userDefaults1 synchronize];
    
    NSDictionary *parameters = @{
                                 @"userEmail":userEmail,
                                 @"AnimalID" :vID,
                                 };
    
    [self.sessionManager POST:kGetVaccineCountDataFromServerURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         if (success) {
             //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            // [userDefaults setObject:responseDict  forKey:@"AnimalsList"];
            // [userDefaults synchronize];
             success(YES,responseDict);
             
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
     }];
}

-(void)sendToken:(NSString*)tID  WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
       NSString *userEmail = [userDefaults1   valueForKey:@"UserEmail"];
       [userDefaults1 synchronize];
    userEmail = userEmail.length == 0 ? @"" : userEmail;
       NSDictionary *parameters = @{
                                    @"userEmail":userEmail,
                                    @"Token" :tID,
                                    };
       
       [self.sessionManager POST:kAddTokenURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject)
        {
            NSDictionary *responseDict = (NSDictionary *)responseObject;
            if (success) {
             
                success(YES,responseDict);
                
            }
            else {
                success(NO,responseDict);

            }
        } failure:^(NSURLSessionTask *operation, NSError *error)
        {
        }];
    
}

@end
