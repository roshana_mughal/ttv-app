//
//  UserVacineMaster.h
//  Bovilisplan
//
//  Created by Apple Macbook on 24/05/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserVacineMaster : NSObject

@property(nonatomic) NSString *date;
@property(nonatomic) NSString *farmName;
@property(nonatomic) NSString *animalNumber;
@property(nonatomic) NSString *scheduleVaccine;
@property(nonatomic) NSString *vaccineType;
@property(nonatomic) NSString *numberOfVaccinatedCattle;
@property(nonatomic) NSString *notes;
@property(nonatomic) NSString *image1;
@property(nonatomic) NSString *image2;
@property(nonatomic) NSString *image3;
@property(nonatomic) NSString *image4;
@property(nonatomic) NSString *image5;
@property(nonatomic) NSString *addedDateTime;
@property(nonatomic) NSString *vaccineName;
@property(nonatomic) NSString *date1;
@property(nonatomic) NSString *farmName1;
@property(nonatomic) NSString *animalNumber1;
@property(nonatomic) NSString *scheduleVaccine1;
@property(nonatomic) NSString *vaccineType1;
@property(nonatomic) NSString *numberOfVaccinatedCattle1;
@property(nonatomic) NSString *notes1;
@property(nonatomic) NSString *image11;
@property(nonatomic) NSString *image21;
@property(nonatomic) NSString *image31;
@property(nonatomic) NSString *image41;
@property(nonatomic) NSString *image51;
@property(nonatomic) NSString *addedDateTime1;
@property(nonatomic) NSString *addedDateTimeFormatted;
@property(nonatomic) NSString *vaccineName1;
@property(nonatomic) NSString *idForEdit;
-(void)getUserVaccinesDataWithSelectedDate:(NSString*)date;
-(void)getUserVaccinesDataWithFarmName:(NSString*)fName;

@end
