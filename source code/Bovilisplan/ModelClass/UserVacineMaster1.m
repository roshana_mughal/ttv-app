//
//  UserVacineMaster.m
//  Bovilisplan
//
//  Created by Apple Macbook on 24/05/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "UserVacineMaster.h"
#import "AppDelegate.h"
@implementation UserVacineMaster



-(void)getUserVaccinesDataWithSelectedDate:(NSString*)date
{

    
    
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *result1 = [userDefaults   objectForKey:@"VaccineData"];
    [userDefaults synchronize];
    
    AppDelegate * appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    appDelegate.getUserVaccinesFromServer =[[NSMutableArray alloc]init];
 // appDelegate.allVaccinesWithDate =[[NSMutableArray alloc]init];
    NSArray *resultDic = [result1 objectForKey:@"results"];
    
    
       
    for (int i = 0; i<[resultDic count]; i++) {
        id item = [resultDic objectAtIndex:i];
        
        NSDictionary *jsonDict = (NSDictionary *) item;
        UserVacineMaster  *theObject =[[UserVacineMaster alloc] init];
        
        
        NSString *selectedDate =[jsonDict valueForKey:@"Date"];
        //appDelegate.selectdDate
  
      if ([date isEqualToString:selectedDate]) {
         
   
        [theObject setDate:[jsonDict valueForKey:@"Date"]];
        [theObject setFarmName:[jsonDict valueForKey:@"FarmName"]];
        [theObject setAnimalNumber:[jsonDict valueForKey:@"AnimalNumber"]];
        [theObject setScheduleVaccine:[jsonDict valueForKey:@"ScheduleVaccines"]];
        [theObject setVaccineType:[jsonDict valueForKey:@"VaccineType"]];
        [theObject setNotes:[jsonDict valueForKey:@"Notes"]];
        [theObject setImage1:[jsonDict valueForKey:@"Image1"]];
        [theObject setImage2:[jsonDict valueForKey:@"Image2"]];
        [theObject setImage3:[jsonDict valueForKey:@"Image3"]];
        [theObject setImage4:[jsonDict valueForKey:@"Image4"]];
        [theObject setImage5:[jsonDict valueForKey:@"Image5"]];
        [theObject setAddedDateTime:[jsonDict valueForKey:@"AddedDateTime"]];
        [theObject setUserName:[jsonDict valueForKey:@"UserName"]];
        [theObject setVaccineName:[jsonDict valueForKey:@"VaccineName"]];
        [theObject setVaccineID:[jsonDict valueForKey:@"VaccineID"]];
        [appDelegate.getUserVaccinesFromServer addObject:theObject];
    }
        
      //  [appDelegate.allVaccinesWithDate addObject:theObject];
       
    }
   
    NSLog(@"count : %lu",(unsigned long)appDelegate.allVaccinesWithDate.count);
    
}

@end
