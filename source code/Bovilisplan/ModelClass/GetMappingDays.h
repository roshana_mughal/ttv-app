//
//  GetMappingDays.h
//  TTVApp
//
//  Created by Admin on 9/14/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetMappingDays : NSObject

@property(nonatomic) NSString *vaccineDayID;
@property(nonatomic) NSString *scheduleIDPK;
@property(nonatomic) NSString * vaccineType;
@property(nonatomic) NSString * image1;
@property(nonatomic) NSString *vaccineName;
@property(nonatomic) NSString *scheduleVaccine;
@property(nonatomic) NSString *notes;
@property(nonatomic) NSString *vaccinationType;
@property(nonatomic) NSString *farmName;
@property(nonatomic) NSString *addedDateTime;
@property(nonatomic) NSString *addedDateTime2;
@property(nonatomic) NSString *Region;
@property(nonatomic) NSString *FarmID;
@property(nonatomic) NSString *NumberOfAnimals;
@property(nonatomic) NSString *day;
@property(nonatomic) NSString *VaccinationType;
@property(nonatomic) NSString *VaccineID;
@property(nonatomic) NSString * imageData;
-(void)getDaysForVaccineSchedule;
-(void)getScheduleVaccineFromServer;


@end

