//
//  GetMappingDays.m
//  TTVApp
//
//  Created by Admin on 9/14/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "GetMappingDays.h"
#import "AppDelegate.h"

@implementation GetMappingDays

-(void)getDaysForVaccineSchedule
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *result1 = [userDefaults   objectForKey:@"DaysForScheduleVaccine"];
    [userDefaults synchronize];
    
    AppDelegate*appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.getMappingDaysArray =[[NSMutableArray alloc]init];
    
    NSArray *resultDic = [result1 objectForKey:@"results"];
    
    if ([result1 count] != 0) {
        for (int i = 0; i<[resultDic count]; i++)
        {
            id item = [resultDic objectAtIndex:i];
            NSDictionary *jsonDict = (NSDictionary *) item;
            
            GetMappingDays  *theObject =[[GetMappingDays alloc] init];
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])
            {
                [theObject setVaccineDayID:[jsonDict valueForKey:@"VaccineID"]];
                [theObject setDay:[jsonDict valueForKey:@"Day"]];
                [theObject setVaccinationType:[jsonDict valueForKey:@"VaccinationType"]];
                [appDelegate.getMappingDaysArray addObject:theObject];
            }
        }
    }
}

-(void)getScheduleVaccineFromServer
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *result1 = [userDefaults   objectForKey:@"ScheduleVaccinesList"];
    [userDefaults synchronize];
    
    AppDelegate*appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.getScheduleVaccineArray =[[NSMutableArray alloc]init];
    
    NSArray *resultDic = [result1 objectForKey:@"results"];
    
    if ([result1 count] != 0) {
 
        
        for (int i = 0; i<[resultDic count]; i++)
        {
            id item = [resultDic objectAtIndex:i];
            NSDictionary *jsonDict = (NSDictionary *) item;
            
            GetMappingDays  *theObject =[[GetMappingDays alloc] init];
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])
            {
                [theObject setVaccineID:[jsonDict valueForKey:@"VaccineID"]];
                [theObject setScheduleIDPK:[jsonDict valueForKey:@"ID"]];
                [theObject setVaccineType:[jsonDict valueForKey:@"VaccineType"]];
                [theObject setVaccineName:[jsonDict valueForKey:@"VaccineName"]];
                [theObject setScheduleVaccine:[jsonDict valueForKey:@"ScheduleVaccine"]];
                [theObject setNotes:[jsonDict valueForKey:@"Notes"]];
                [theObject setFarmName:[jsonDict valueForKey:@"FarmName"]];
                [theObject setAddedDateTime:[jsonDict valueForKey:@"AddedDateTime"]];
                [theObject setRegion:[jsonDict valueForKey:@"Region"]];
                [theObject setFarmID:[jsonDict valueForKey:@"FarmID"]];
                [theObject setNumberOfAnimals:[jsonDict valueForKey:@"NumberOfAnimals"]];
                
                NSDateFormatter *dateFormater= [[NSDateFormatter alloc] init];
                //[dateFormater setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
                [dateFormater setDateFormat:@"yyyy-MM-dd"];

                NSString *str = [jsonDict valueForKey:@"AddedDateTime"];
                str = [str substringToIndex:str.length-(str.length-10)];
                 NSDate *newDate = [dateFormater dateFromString:str];
//                NSString *dateFinal = [dateFormat stringFromDate:newDate];
                
                [theObject setAddedDateTime2:str];
                [appDelegate.getScheduleVaccineArray addObject:theObject];
            }
        }
    }
   }

@end
