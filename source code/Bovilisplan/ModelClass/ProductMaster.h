//
//  ProductMaster.h
//  TTVApp
//
//  Created by Apple Macbook on 30/05/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductMaster : NSObject

@property(nonatomic) NSString *produtID;
@property(nonatomic) NSString *primaryVaccination;
@property(nonatomic) NSString *recorderVaccination;
@property(nonatomic) NSString *reVaccination;
@property(nonatomic) NSString *userName;
@property(nonatomic) NSString *addInformation;
@property(nonatomic) NSString *refLink;
@property(nonatomic) NSString *culture;
@property(nonatomic) NSString *vaccineID;
@property(nonatomic) NSString *vaccineName;
@property(nonatomic) NSString *dose;
@property(nonatomic) NSString *addministration;
@property(nonatomic) NSString *day;
@property(nonatomic) NSString *vaccinationType;
@property(nonatomic) NSString *vaccineIDPro;
@property(nonatomic) NSString * productIDL;
@property(nonatomic) NSString * vaccineNameL;
@property(nonatomic) NSString * addministrationL;
@property(nonatomic) NSString * dosageL;
@property(nonatomic) NSString * moreInformationL;
@property(nonatomic) NSString * linksAndDownloadsL;
@property(nonatomic) NSString * primaryL;
@property(nonatomic) NSString * secondaryOneL;
@property(nonatomic) NSString * secondaryTwoL;
@property(nonatomic) NSString * secondaryThreeL;

-(void)getProductsListData:(NSDictionary*)result;
-(void)getProductsVaccineData:(NSDictionary*)result;

@end
