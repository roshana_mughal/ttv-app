//
//  UserVacineMaster.h
//  Bovilisplan
//
//  Created by Apple Macbook on 24/05/2017.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserVacineMaster : NSObject
@property (nonatomic) NSString *date;
@property (nonatomic)NSString *farmName;
@property (nonatomic)NSString *animalNumber;
@property (nonatomic)NSString *scheduleVaccine;
@property (nonatomic)NSString *vaccineType;
@property (nonatomic)NSString *numberOfVaccinatedCattle;
@property (nonatomic)NSString *notes;
@property (nonatomic)NSString *image1;
@property (nonatomic)NSString *image2;
@property (nonatomic)NSString *image3;
@property (nonatomic)NSString *image4;
@property (nonatomic)NSString *image5;
@property (nonatomic)NSString *addedDateTime;
@property (nonatomic)NSString *vaccineID;
@property (nonatomic)NSString *userName;
@property (nonatomic)NSString *vaccineName;


-(void)getUserVaccinesDataWithSelectedDate:(NSString*)date;



//-(void)getUserVaccinesData:(NSDictionary*)result withDate:(NSString*)date;
@end
