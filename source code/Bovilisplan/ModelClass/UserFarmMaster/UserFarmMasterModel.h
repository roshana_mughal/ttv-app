//
//  UserFarmMasterModel.h
//  Bovilisplan
//
//  Created by Admin on 5/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserFarmMasterModel : NSObject

@property(nonatomic) NSInteger ID;
@property(nonatomic) NSString *farmID;
@property(nonatomic) NSString *farmName;
@property(nonatomic) NSString *registrationNumber;
@property(nonatomic) NSString *region;
@property(nonatomic) NSString *contactPerson;
@property(nonatomic) NSString *numberOfAnimals;
@property(nonatomic) NSString *address;
@property(nonatomic) NSString *phone;
@property(nonatomic) NSString *email;
@property(nonatomic) NSString *Comments;
@property(nonatomic) NSString *userName;
@property(nonatomic) NSString *addedDateTime;
@property(nonatomic) NSString *farmType1;
@property(nonatomic) NSString *farmType2;
@property(nonatomic) NSString *farmType3;
@property(nonatomic) NSString *farmType4;

-(void)getUserFarmData:(NSDictionary*)result;

@end
