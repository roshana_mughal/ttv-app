//
//  UserFarmMasterModel.m
//  Bovilisplan
//
//  Created by Admin on 5/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "UserFarmMasterModel.h"
#import "AppDelegate.h"

@implementation UserFarmMasterModel
{
}

-(void)getUserFarmData:(NSDictionary*)result
{
    AppDelegate * appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.getUserFarmsFromServer =[[NSMutableArray alloc]init];
    NSArray *resultDic1 = [result objectForKey:@"results"];
    if ([result count] != 0) {
        for (int i = 0; i<[resultDic1 count]; i++)
        {
            id item = [resultDic1 objectAtIndex:i];
            NSDictionary *jsonDict = (NSDictionary *) item;
            UserFarmMasterModel  *theObject =[[UserFarmMasterModel alloc] init];
            NSString*error = [jsonDict valueForKey:@"error"];
            if(![error isEqualToString:@"No Record Found."])
            {
                [theObject setFarmID:[jsonDict valueForKey:@"FarmID"]];
              //[theObject setFarmName:@"Testing Farm"];

             [theObject setFarmName:[jsonDict valueForKey:@"FarmName"]];
                [theObject setRegion:[jsonDict valueForKey:@"Region"]];
                [theObject setRegistrationNumber:[jsonDict valueForKey:@"RegistrationNumber"]];
                [theObject setContactPerson:[jsonDict valueForKey:@"ContactPerson"]];
                [theObject setNumberOfAnimals:[jsonDict valueForKey:@"NumberOfAnimals"]];
                [theObject setAddress:[jsonDict valueForKey:@"Address"]];
                [theObject setPhone:[jsonDict valueForKey:@"Phone"]];
                [theObject setEmail:[jsonDict valueForKey:@"Email"]];
                [theObject setComments:[jsonDict valueForKey:@"Comments"]];
                [theObject setUserName:[jsonDict valueForKey:@"UserName"]];
                [theObject setAddedDateTime:[jsonDict valueForKey:@"AddedDateTime"]];
                [theObject setFarmType1:[jsonDict valueForKey:@"FarmType1"]];
                [theObject setFarmType2:[jsonDict valueForKey:@"FarmType2"]];
                [theObject setFarmType3:[jsonDict valueForKey:@"FarmType3"]];
                [theObject setFarmType4:[jsonDict valueForKey:@"FarmType4"]];
                [appDelegate.getUserFarmsFromServer addObject:theObject];
            }
        }
    }
}

@end
