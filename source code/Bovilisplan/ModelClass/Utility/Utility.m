//
//  Utility.m
//  Gather
//
//  Created by O16 Labs on 18/08/2015.
//  Copyright (c) 2015 O16 Labs. All rights reserved.
//

#import "Utility.h"
#import "AppDelegate.h"
#import <NHNetworkTime/NHNetworkTime.h>


@implementation Utility {
    AppDelegate *appDelegate;
    NSString * language;
}

NSString *const kAcceptContentTypeText = @"text/html";
NSString *const KAcceptContentTypeJSON = @"application/json";

NSString *const kBaseURL = @"http://35.168.116.207:8888/api";
//@"http://35.168.116.207:7777/api/";


NSString *const kAddTokenURL = @"Tokens/AddToken";
NSString *const KDeleteFarmFromServerURL=@"FarmMaster/DeleteFarm";
NSString *const kGetDataFromServerURL= @"FarmMaster/CurrentUserFarms";
NSString *const kUpdateNewFormDataToServerURL=@"FarmMaster/UpdateFarm";
NSString *const kSendNewFormDataToServerURL=@"FarmMaster/InsertFarm";
NSString *const KGetProductsFromServer=@"ProductVaccineMaster/GetVaccineList";
NSString *const KGetProductsListFromServer=@"ProductListLibrary/GetProductList";

NSString *const KgetAnimalsFromServerURL=@"Animals/GetAnimals";
NSString *const KAddAnimalsFromServerURL=@"Animals/InsertAnimal";
NSString *const KUpdateAnimalsFromServerURL=@"Animals/UpdateAnimal";
NSString *const KGetVaccineAnimalsFromServerURL=@"UserVaccinationSchedules/VaccineAnimals";




NSString *const KDeleteAnimalsFromServerURL=@"Animals/DeleteAnimal";
NSString *const KgetAnimalsVaccinesFromServerURL=@"Animals/AnimalVaccines";


NSString *const KgetVaccineMappingDaysURL=@"VaccineMapping/GetMappingDays";
NSString *const kDeleteScheduleVaccineFromServerURL=@"UserVaccinationSchedules/DeleteNotification";
NSString *const kSendNewUserVaccinesDataToServerURL=@"UserVaccinationSchedules/InsertUserVaccines";
NSString *const KUpdateEvent=@"UserVaccinationSchedules/UpdateEvent";
NSString *const KUpdateScheduledVaccineURL=@"UserVaccinationSchedules/UpdateVaccination";
NSString *const KgetVaccineScheduleURL=@"UserVaccinationSchedules/GetVaccineScheduleID";
NSString *const kGetImageDataFromServerURL=@"UserVaccinationSchedules/VaccineImageData";
NSString *const kGetVaccineCountDataFromServerURL=@"UserVaccinationSchedules/AnimalInVaccine";


-(id)init {
    
    self = [super init];
    self.region= [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([self.region isEqualToString:@"en"])
    {
        self.region=@"en-US";
    }
    return self;
    
    
}

+(NetworkStatus )checkNetworkStatus {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    return [reachability currentReachabilityStatus];
}
+(NSString *)getTranslationkey:(NSString*)key witchLanguageDict:(NSDictionary *)dict {
    
    return  [dict valueForKey:key];
}

+ (BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(NSUInteger)getAnimalCountWithAnimal:(NSString*)animals {
    
    NSArray *items = [animals componentsSeparatedByString:@","];
    return items.count;
}

+(UIImage*)getNormalizedImage:(UIImage *)rawImage {
    
    if(rawImage.imageOrientation == UIImageOrientationUp)
        return rawImage;
    
    CGRect rect = CGRectMake(0,0,110,110);
    UIGraphicsBeginImageContext( rect.size);
    [rawImage drawInRect:rect];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return normalizedImage;
}

+(NSString *)setLocalDateFormatter:(NSString *)dateStr {
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"en"])
    {
        language=@"en-US";
    }
    
    NSDateFormatter *dateFormatC = [[NSDateFormatter alloc] init];
    [dateFormatC setDateFormat:@"yyyy-MM-dd"];
    
    NSDate * serverDate = [dateFormatC dateFromString:dateStr];
    NSLocale *locale = [[NSLocale alloc]
                        initWithLocaleIdentifier:language];
    [dateFormatC setLocale:locale];
    
    NSString * newvaccineDate= [dateFormatC stringFromDate:serverDate];
    
    
    return newvaccineDate;
}


+(NSString*)MonthNameString:(int)monthNumber {
    
    NSDateFormatter *formate = [NSDateFormatter new];

    NSArray *monthNames = [formate standaloneMonthSymbols];

    NSString *monthName = [monthNames objectAtIndex:(monthNumber - 1)];

    return monthName;
}

+(NSString *)setListViewDateFormatter:(NSString *)dateStr {
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"en"])
    {
        language=@"en-US";
    }
    
    
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [self getDateFromString:dateStr formaterString:@"yyyy-MM-dd"];
    //now you have the date, you can output the bits you want

    [dateFormatter setDateFormat:@"yyyy"];
    NSString *year = [dateFormatter stringFromDate:date];

    [dateFormatter setDateFormat:@"MM"];
    NSString *month = [dateFormatter stringFromDate:date];

    
    NSLocale *locale = [[NSLocale alloc]
                        initWithLocaleIdentifier:language];
    [dateFormatter setLocale:locale];
    
    NSString * monthDate = [self MonthNameString:[month intValue]];
    NSString * yeadMonth = [NSString stringWithFormat:@"%@ %@",monthDate,year];

    
    NSString * newvaccineDate= [dateFormatter stringFromDate:date];
    
    
    return yeadMonth;
}

+(NSDate *)getDateFromString:(NSString *)dateStr formaterString:(NSString*)fomaterStr {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
      [dateFormatter setDateFormat:fomaterStr];
    
    return  [dateFormatter dateFromString:dateStr];
    
}

+(NSString *)getDayFromDate:(NSString *)dateStr {
    
    NSDate *date = [self getDateFromString:dateStr formaterString:@"yyyy-MM-dd"];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
     
      [dateFormatter setDateFormat:@"dd"];
      NSString *day = [dateFormatter stringFromDate:date];

    
    return [dateFormatter stringFromDate:date];
    
}

+(NSString *)getDayNameFromDate:(NSString *)dateStr {
    
    NSDate *date = [self getDateFromString:dateStr formaterString:@"yyyy-MM-dd"];

    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar components: NSCalendarUnitDay | NSCalendarUnitWeekday fromDate:date];

  //  NSLog(@"Day in week: %ld", (long)dateComponents.weekday); // Day in week: 3
   // NSLog(@"Day in month: %ld", (long)dateComponents.day);    // Day in month: 25
    NSLog(@"Day name: %@", calendar.shortWeekdaySymbols[dateComponents.weekday - 1]);
    // Day nam
    
    return  calendar.shortWeekdaySymbols[dateComponents.weekday - 1];
    
}

+(NSString *)setLocalizeBackground {
    
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"en"])
    {
        language=@"en-US";
    }
    NSString *backgroundImagename;
    
    if ([language containsString:@"en"]) {
        backgroundImagename =@"background";
    }
    else  if ([language containsString:@"fr"]) {
        backgroundImagename =@"background-FR";
        
    }
    else  if ([language containsString:@"es"]) {
        backgroundImagename =@"background-ES";
        
    }
    else  if ([language containsString:@"pt"]) {
        backgroundImagename =@"background-PT";
        
    }
    else  if ([language containsString:@"de"]) {
        backgroundImagename =@"background-DE";
        
    }
    else  if ([language containsString:@"it"]) {
        backgroundImagename =@"background-IT";
        
    }
    else  if ([language containsString:@"nl"]) {
        backgroundImagename =@"background-NL";
        
    }
    return backgroundImagename;
}

+(NSString *)setLocalizeLogo:(NSString*)pre {
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"en"])
    {
        language=@"en-US";
    }
    NSString *imageName;
    
    if ([language containsString:@"en"]) {
        imageName =[NSString stringWithFormat:@"%@-EN",pre];
    }
    else  if ([language containsString:@"fr"]) {
        imageName =[NSString stringWithFormat:@"%@-FR",pre];
        
        
    }
    else  if ([language containsString:@"es"]) {
        imageName =[NSString stringWithFormat:@"%@-ES",pre];
        
        
    }
    else  if ([language containsString:@"pt"]) {
        imageName =[NSString stringWithFormat:@"%@-PT",pre];
        
    }
    else  if ([language containsString:@"de"]) {
        imageName =[NSString stringWithFormat:@"%@-DE",pre];
        
    }
    else  if ([language containsString:@"it"]) {
        imageName =[NSString stringWithFormat:@"%@-IT",pre];
        
        
    }
    else  if ([language containsString:@"nl"]) {
        imageName =[NSString stringWithFormat:@"%@-NL",pre];
        
    }
    return imageName;
}


+(NSString *)setLocalizeString:(NSString *)text {
    AppDelegate *appDelegate =(AppDelegate*) [[UIApplication sharedApplication]delegate];
    
    NSString * Title=[appDelegate.dictCulture valueForKey:text];
    
    return Title;
}

+(BOOL)isOldeDate:(NSString*)strDate {
    
    NSDateFormatter *df= [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *dt1 = [[NSDate alloc] init];
    dt1=[df dateFromString:strDate];

    
    NSDate *dt2 = [[NSDate alloc] init];
     NSDate * dateCUrrent = [NSDate networkDate];
    NSString * currentDateStr=[df stringFromDate:dateCUrrent];
    dt2=[df dateFromString:currentDateStr];
    
    NSComparisonResult result = [dt1 compare:dt2];
    
   
    
    if( [dt2 compare:dt1]== NSOrderedDescending)
    {
        return  FALSE;
    }
    return TRUE;
}
@end
