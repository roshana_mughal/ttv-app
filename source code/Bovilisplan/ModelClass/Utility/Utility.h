//
//  Utility.h
//  Gather
//
//  Created by O16 Labs on 18/08/2015.
//  Copyright (c) 2015 O16 Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Reachability.h"

@class LoginUserModel;



@interface Utility : NSObject

typedef enum {
    kDate,
    kFarm,
    kAnimal,
    kVaccine
} ListViewSelectedType;




extern NSString *const kDeleteScheduleVaccineFromServerURL;
extern NSString *const kAcceptContentTypeText;
extern NSString *const KAcceptContentTypeJSON;
extern NSString *const kBaseURL;
extern NSString *const kGetDataFromServerURL;
extern NSString *const kUpdateNewFormDataToServerURL;
extern NSString *const kSendNewFormDataToServerURL;
extern NSString *const KGetProductsFromServer;
extern NSString *const KGetProductsListFromServer;
extern NSString *const KGetNotifications;
extern NSString *const KInsertNotification;
extern NSString *const KUpdateEvent;
extern NSString *const KDeleteFarmFromServerURL;
extern NSString *const KgetAnimalsFromServerURL;
extern NSString *const KAddAnimalsFromServerURL;
extern NSString *const KDeleteAnimalsFromServerURL;
extern NSString *const KUpdateAnimalsFromServerURL;
extern NSString *const KgetAnimalsVaccinesFromServerURL;

extern NSString *const KUpdateScheduledVaccineURL;
extern NSString *const KgetVaccineMappingDaysURL;
extern NSString *const KgetVaccineScheduleURL;
extern NSString *const kSendNewUserVaccinesDataToServerURL;
extern NSString *const kGetUserVaccinesDataFromServerURL;
extern NSString *const kGetImageDataFromServerURL;
extern NSString *const kLanguage;
extern NSString *const kGetVaccineCountDataFromServerURL;
extern NSString *const kAddTokenURL;
extern NSString *const KGetVaccineAnimalsFromServerURL;




+(NetworkStatus )checkNetworkStatus;
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString;
+(UIImage*)getNormalizedImage:(UIImage *)rawImage;
+(NSString *)setLocalDateFormatter:(NSString *)dateStr ;
+(NSString *)setLocalizeString:(NSString *)text;
@property (strong,nonatomic)NSString *region;
+(NSString *)setLocalizeBackground;
+(NSString *)setLocalizeLogo:(NSString*)pre;
+(BOOL)isOldeDate:(NSString*)strDate;
+(NSString *)setListViewDateFormatter:(NSString *)dateStr;
+(NSString *)getDayFromDate:(NSString *)dateStr;
+(NSString *)getDayNameFromDate:(NSString *)dateStr;

+(NSUInteger)getAnimalCountWithAnimal:(NSString*)animals;
+(NSString *)getTranslationkey:(NSString*)key witchLanguageDict:(NSDictionary *)dict;

@end
