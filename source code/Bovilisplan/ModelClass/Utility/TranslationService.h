//
//  TranslationService.h
//  TTVApp
//
//  Created by Mac on 3/19/19.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFHTTPSessionManager.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface TranslationService : NSObject
+(NSString *)getDateForTermsAndConditions;
+(NSString *)getDateForPrivacy ;
+(NSString *)getDateForDisclaimer;

@property(nonatomic,strong) AFHTTPSessionManager *sessionManager;
-(void)getCultures:(UIViewController*)viewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;
@end
