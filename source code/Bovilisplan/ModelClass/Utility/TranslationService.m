//
//  TranslationService.m
//  TTVApp
//
//  Created by Mac on 3/19/19.
//  Copyright © 2019 celeritas-solutions. All rights reserved.
//

#import "TranslationService.h"
#import "Utility.h"
#import "AppDelegate.h"

@implementation TranslationService {
    AppDelegate *appDelegate;
}

-(void)getCultures:(UIViewController*)viewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
    [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    
    NSSet *set = [NSSet setWithObjects:KAcceptContentTypeJSON,kAcceptContentTypeText,nil];
    [self.sessionManager.responseSerializer setAcceptableContentTypes:set];
    
    self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    

    NSString *culture = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([culture isEqualToString:@"en"])
    {
        culture=@"en-US";
    }
    else if ([culture containsString:@"it"])
      {
          culture=@"it-IT";
      }
      else if ([culture containsString:@"de"])
      {
          culture=@"de-DE";
      }
      else if ([culture containsString:@"nl"])
      {
          culture=@"nl-NL";
      }
      else if ([culture containsString:@"fr"])
      {
          culture=@"fr-FR";
      }
      else if ([culture containsString:@"es"])
      {
          culture=@"es-ES";
      }
      else if ([culture containsString:@"pt"])
      {
          culture=@"pt-PT";
      }
    
    NSString *urlString = [NSString stringWithFormat:@"https://services.merck-animal-health.com/translationservice/v2/GetTranslatedItems?appID=58&culture=%@&resourceKey=&resourceKeyMatchOperator=&timestamp=&subscriber=ccac8637-35e2-4fed-90c6-4e49397e80d4",culture];
    [self.sessionManager.requestSerializer setValue:[NSString stringWithFormat:@"1639fd95-a81a-4a14-9c2a-b30aec7560fb"] forHTTPHeaderField:@"X-App-Token"];
    
    if (urlString == NULL || [urlString  isEqual: @"\0"]) {
        urlString = [NSString stringWithFormat:@"https://services.merck-animal-health.com/translationservice/v2/GetTranslatedItems?appID=58&culture=en-US&resourceKey=&resourceKeyMatchOperator=&timestamp=&subscriber=ccac8637-35e2-4fed-90c6-4e49397e80d4"];
    }
    
    if (urlString != NULL) {
     
    [self.sessionManager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         
         NSError *jsonError;
         NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]initWithDictionary:responseDict];
         
         NSArray *resultArray = [jsonDict objectForKey:@"Result"];
         NSString *resultStatus = [jsonDict valueForKey:@"ResultStatus"];
         appDelegate.dictCulture= [NSMutableDictionary dictionary];
        // languageAlert= resultStatus;
         if([resultStatus isEqualToString:@"Succeeded"]){
             for (int i = 0; i<[resultArray count]; i++)
             {
                 id item = [resultArray objectAtIndex:i];
                 NSDictionary *jsonDict1 = (NSDictionary *) item;
                 NSString *key = [jsonDict1 valueForKey:@"Key"];
                 NSString *value = [jsonDict1 valueForKey:@"Value"];
                 [appDelegate.dictCulture setValue:value forKey:key];
             }
         }

         [MBProgressHUD hideHUDForView:viewController.view animated:YES];
         success(YES,responseDict);

         
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         [MBProgressHUD hideHUDForView:viewController.view animated:YES];
         
     }];
    }
}

+(NSString *)getDateForPrivacy {
    
    NSString *translateString;
    
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"en"])
    {
        language=@"en-US";
    }
    NSString *imageName;
    
    if ([language containsString:@"en"]) {
        translateString = [self getPrivacyEN];
        
    }
    else  if ([language containsString:@"fr"]) {
        translateString = [self getPrivacyFR];
        
    }
    else  if ([language containsString:@"es"]) {
        translateString = [self getPrivacyES];
        
    }
    else  if ([language containsString:@"pt"]) {
        
        translateString = [self getPrivacyPT];
        
    }
    else  if ([language containsString:@"de"]) {
        translateString = [self getPrivacyDE];
        
    }
    else  if ([language containsString:@"it"]) {
        translateString = [self getPrivacyIT];
        
    }
    else  if ([language containsString:@"nl"]) {
        translateString = [self getPrivacyNL];
        
    }
    
    return  translateString ;

}

+(NSString *)getPrivacyEN {
    
    NSString *Privacy = @"<p>As described in our <a href ='http://www.msd.com/privacy/internet-privacy-policy/'>Internet Privacy Policy</a>, one type of online resource that we provide is mobile computing applications ('Apps') for mobile devices, such as smartphones and tablets. Many of these can be purchased or downloaded from App Stores that are offered by the provider of the device operating system, such as Apple and Google. Some of these Apps allow you to record information within the App and store it on your device, to create reports, and to email content from the App. In some cases, that information may be 'personal information' that identifies or is used to identify you, such as your name. Where the personal information you record is collected by us, or others working for us, it will be used in accordance with our Internet Privacy Policy.</p><p>If you use an older version of one of our Apps, such as an App that we offered prior to 2014, the App may have functionality for evaluation of how it is used by enabling data about your use of the App on your device, such as which features you used the most, to be transmitted to Flurry, an analytics service provider. If you do not want Flurry to track your use of the App on your device, you can opt-out. If you have questions, please contact the Privacy Office.</p>";
    
    return Privacy;
}

+(NSString *)getPrivacyFR{
    
    NSString *Privacy = @"<p> Zoals beschreven in ons <a href='http://www.msd.com/privacy/internet-privacy-policy/'> internetprivacybeleid</a>, is één type online bron die we bieden mobiel computerapplicaties ('Apps') voor mobiele apparaten, zoals smartphones en tablets. Veel van deze kunnen worden gekocht of gedownload vanuit App Stores die worden aangeboden door de provider van het besturingssysteem van het apparaat, zoals Apple en Google. Met sommige van deze apps kunt u informatie in de app opnemen en op uw apparaat opslaan, rapporten maken en inhoud van de app e-mailen. In sommige gevallen kan die informatie 'persoonlijke informatie' zijn die identificeert of wordt gebruikt om u te identificeren, zoals uw naam. Wanneer de persoonlijke informatie die u vastlegt door ons wordt verzameld of door anderen die voor ons werken, wordt deze gebruikt in overeenstemming met ons internetprivacybeleid. </p> <p> Als u een oudere versie van een van onze apps gebruikt, zoals een app die we vóór 2014 hebben aangeboden, de app kan functionaliteit hebben voor de evaluatie van hoe deze wordt gebruikt door gegevens over uw gebruik van de app op uw apparaat, zoals welke functies u het meest gebruikt, mogelijk te maken aan Flurry, een analytics-serviceprovider. Als u niet wilt dat Flurry uw gebruik van de app op uw apparaat bijhoudt, kunt u zich afmelden. Neem voor vragen contact op met het Privacy Office. </p>";
    
    return Privacy;
}

+(NSString *)getPrivacyES{
    
    NSString *Privacy = @"<p> Como se describe en nuestra <a href ='http://www.msd.com/privacy/internet-privacy-policy/'> Política de privacidad de Internet</a>, un tipo de recurso en línea que ofrecemos es móvil aplicaciones informáticas ('Aplicaciones') para dispositivos móviles, como teléfonos inteligentes y tabletas. Muchos de estos pueden comprarse o descargarse en las tiendas de aplicaciones que ofrece el proveedor del sistema operativo del dispositivo, como Apple y Google. Algunas de estas aplicaciones le permiten registrar información dentro de la aplicación y almacenarla en su dispositivo, crear informes y enviar contenido por correo electrónico desde la aplicación. En algunos casos, esa información puede ser 'información personal' que lo identifica o se usa para identificarlo, como su nombre. Cuando recopilemos la información personal que registre, u otras personas que trabajen para nosotros, se utilizará de acuerdo con nuestra Política de privacidad en Internet. </p> <p> Si usa una versión anterior de una de nuestras Aplicaciones, como una aplicación que ofrecimos antes de 2014, la aplicación puede tener una funcionalidad para evaluar cómo se usa al permitir que los datos sobre su uso de la aplicación en su dispositivo, como las funciones que más usó, se transmitan a Flurry, un Proveedor de servicios analíticos. Si no desea que Flurry haga un seguimiento de su uso de la aplicación en su dispositivo, puede optar por no participar. Si tiene alguna pregunta, comuníquese con la Oficina de Privacidad. </p>";
    
    return Privacy;
}

+(NSString *)getPrivacyPT{
    
    NSString *Privacy = @"<p> Conforme descrito em nossa <a href ='http://www.msd.com/privacy/internet-privacy-policy/'> Política de Privacidade da Internet</a>, um tipo de recurso on-line que fornecemos é móvel aplicativos de computação ('Apps') para dispositivos móveis, como smartphones e tablets. Muitos deles podem ser comprados ou baixados nas lojas de aplicativos oferecidas pelo provedor do sistema operacional do dispositivo, como Apple e Google. Alguns desses aplicativos permitem que você grave informações dentro do aplicativo e as armazene no dispositivo, crie relatórios e envie conteúdo por e-mail a partir do aplicativo. Em alguns casos, essas informações podem ser \"informações pessoais\" que identificam ou são usadas para identificá-lo, como seu nome. Quando as informações pessoais que você registra são coletadas por nós ou por outras pessoas trabalhando para nós, elas serão usadas de acordo com nossa Política de Privacidade na Internet. </p> <p> Se você usar uma versão mais antiga de um de nossos aplicativos, como um aplicativo que oferecemos antes de 2014, o aplicativo pode ter funcionalidade para avaliação de como ele é usado, permitindo que os dados sobre o uso do aplicativo no dispositivo, como os recursos mais usados, sejam transmitidos para o Flurry, provedor de serviços de análise. Se você não quiser que a Flurry rastreie o uso do aplicativo no seu dispositivo, será possível desativá-lo. Se você tiver dúvidas, entre em contato com o Privacy Office. </p>";
    
    return Privacy;
}

+(NSString *)getPrivacyDE{
    
    NSString *Privacy = @"<p> Wie in unseren <a href ='http://www.msd.com/privacy/internet-privacy-policy/'> Internet-Datenschutzbestimmungen</a> beschrieben, ist eine Art von Online-Ressource, die wir zur Verfügung stellen, mobil Computeranwendungen (\"Apps\") für mobile Geräte wie Smartphones und Tablets. Viele davon können in App Stores erworben oder heruntergeladen werden, die vom Anbieter des Gerätebetriebssystems wie Apple und Google angeboten werden. Mit einigen dieser Apps können Sie Informationen in der App aufzeichnen und auf Ihrem Gerät speichern, Berichte erstellen und E-Mails aus der App per E-Mail versenden. In einigen Fällen kann es sich bei diesen Informationen um \"persönliche Informationen\" handeln, die Sie identifizieren, wie beispielsweise Ihr Name. Wenn die personenbezogenen Daten, die Sie erfassen, von uns oder anderen für uns arbeitenden Personen erfasst werden, werden sie gemäß unserer Internet-Datenschutzrichtlinie verwendet. </p> <p> Wenn Sie eine ältere Version einer unserer Apps verwenden, wie z Bei einer App, die wir vor 2014 angeboten haben, verfügt die App möglicherweise über Funktionen zur Bewertung der Verwendung, indem Daten über die Verwendung der App auf Ihrem Gerät, z. B. welche Funktionen Sie am häufigsten verwendet haben, an Flurry übermittelt werden Analysedienstanbieter. Wenn Sie nicht möchten, dass Flurry Ihre Verwendung der App auf Ihrem Gerät verfolgt, können Sie dies ablehnen. Wenn Sie Fragen haben, wenden Sie sich bitte an das Privacy Office. </p>";
    
    return Privacy;
}

+(NSString *)getPrivacyIT{
    
    NSString *Privacy = @"<p> Come descritto nella nostra <a href ='http://www.msd.com/privacy/internet-privacy-policy/'> Politica sulla privacy di Internet</a>, un tipo di risorsa online che forniamo è mobile applicazioni di calcolo ('App') per dispositivi mobili, come smartphone e tablet. Molti di questi possono essere acquistati o scaricati da App Store offerti dal fornitore del sistema operativo del dispositivo, come Apple e Google. Alcune di queste app consentono di registrare informazioni all'interno dell'App e memorizzarle sul dispositivo, creare report e inviare via email i contenuti dall'App. In alcuni casi, tali informazioni potrebbero essere \"informazioni personali\" che identificano o sono utilizzate per identificarti, come il tuo nome. Laddove le informazioni personali registrate siano raccolte da noi o da altri che lavorano per noi, verranno utilizzate in conformità con la nostra Informativa sulla privacy di Internet. </p> <p> Se utilizzi una versione precedente di una delle nostre app, ad esempio Un'app che abbiamo offerto prima del 2014, l'App può avere funzionalità per la valutazione di come viene utilizzata abilitando i dati sull'utilizzo dell'App sul tuo dispositivo, come ad esempio le funzionalità che hai usato di più, da trasmettere a Flurry, un fornitore di servizi di analisi. Se non desideri che Flurry tenga traccia del tuo utilizzo dell'App sul tuo dispositivo, puoi disattivarlo. In caso di domande, contattare l'Ufficio privacy. </p>";
    
    return Privacy;
}

+(NSString *)getPrivacyNL{
    
    NSString *Privacy = @"<p> Zoals beschreven in ons <a href='http://www.msd.com/privacy/internet-privacy-policy/'> internetprivacybeleid</a>, is één type online bron die we bieden mobiel computerapplicaties ('Apps') voor mobiele apparaten, zoals smartphones en tablets. Veel van deze kunnen worden gekocht of gedownload vanuit App Stores die worden aangeboden door de provider van het besturingssysteem van het apparaat, zoals Apple en Google. Met sommige van deze apps kunt u informatie in de app opnemen en op uw apparaat opslaan, rapporten maken en inhoud van de app e-mailen. In sommige gevallen kan die informatie 'persoonlijke informatie' zijn die identificeert of wordt gebruikt om u te identificeren, zoals uw naam. Wanneer de persoonlijke informatie die u vastlegt door ons wordt verzameld of door anderen die voor ons werken, wordt deze gebruikt in overeenstemming met ons internetprivacybeleid. </p> <p> Als u een oudere versie van een van onze apps gebruikt, zoals een app die we vóór 2014 hebben aangeboden, de app kan functionaliteit hebben voor de evaluatie van hoe deze wordt gebruikt door gegevens over uw gebruik van de app op uw apparaat, zoals welke functies u het meest gebruikt, mogelijk te maken aan Flurry, een analytics-serviceprovider. Als u niet wilt dat Flurry uw gebruik van de app op uw apparaat bijhoudt, kunt u zich afmelden. Neem voor vragen contact op met het Privacy Office. </p>";
    
    return Privacy;
}

+(NSString *)getDateForDisclaimer {
    
    NSString *translateString;
    
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"en"])
    {
        language=@"en-US";
    }
    NSString *imageName;
    
    if ([language containsString:@"en"]) {
        translateString = [self getDisclaimerEN];

    }
    else  if ([language containsString:@"fr"]) {
        translateString = [self getDisclaimerFR];

    }
    else  if ([language containsString:@"es"]) {
        translateString = [self getDisclaimerES];

    }
    else  if ([language containsString:@"pt"]) {
        
        translateString = [self getDisclaimerPT];

    }
    else  if ([language containsString:@"de"]) {
        translateString = [self getDisclaimerDE];

    }
    else  if ([language containsString:@"it"]) {
        translateString = [self getDisclaimerIT];

    }
    else  if ([language containsString:@"nl"]) {
        translateString = [self getDisclaimerNL];

    }
    
    return  translateString ;
}

+(NSString *)getDisclaimerEN {
    
    NSString *disclaimer = @"<p>This Web Site and the information, names, images, pictures, logos and icons regarding or relating to Intervet Inc. and its subsidiaries, affiliates, products and services is provided as is and on an as available basis without any representation or warranty of any kind, whether expressed or implied, including, but not limited to the implied warranties of mercantability, fitness for a particular purpose or non infringement</p><p>Although the information on this Web Site has been compiled in good faith to provide information on Intervet Inc. and its products and services, Intervet Inc., its parents, subsidiaries, affiliates or employees shall in no event be liable for any damages of any kind or nature whatsoever, including, without limitation, direct, indirect, special, consequential or incidental damages resulting from or in connection with the use or access of this Web Site or resulting from or in connection with any materials, information, qualifications or recommendations on this Web Site.</p><p>Intervet Inc. may at any time make modifications, improvements and/or changes to these Terms and conditions, the information, names, images, pictures, logos and icons displayed on this Web Site or the products and services referred to in this Web Site without notice.</p><p>This Web Site may contain links to or may contain references to other web sites which are not under the control of Intervet Inc.. Intervet Inc. shall have no responsibility whatsoever for the contents of any such linked web site or any link contained in such a linked site. You agree not to add, alter, substitute or amend partially or in whole any information on this Web Site.</p>";
    
    
    
    return disclaimer;
    
}

+(NSString *)getDisclaimerFR {
    
    NSString *disclaimer = @"<p>Ce site Web et les informations, noms, images, images, logos et icônes concernant ou concernant Intervet Inc. et ses filiales, sociétés affiliées, produits et services sont fournis tels quels et selon leur disponibilité, sans déclaration ni garantie. de toute nature, implicite ou explicite, y compris, sans toutefois s'y limiter, les garanties implicites de qualité marchande, d'adéquation à un usage particulier ou de non-violation. </p> <p>Bien que les informations de ce site web aient été compilées de bonne foi pour fournir des informations sur Intervet Inc. et ses produits et services, Intervet Inc., ses sociétés mères, ses filiales, ses sociétés affiliées ou ses employés ne pourront en aucun cas être tenus responsables des dommages de quelque nature que ce soit, y compris, sans limitation, directe, indirecte, particulière ou indirecte. , dommages indirects ou accessoires résultant de ou liés à l'utilisation ou à l'accès de ce site Web ou résultant de ou liés à tout matériel, information, qualification ou recommandations de ce W eb Site. </p> <p>Intervet Inc. peut à tout moment apporter des modifications, des améliorations et / ou des modifications aux présentes conditions générales, aux informations, noms, images, images, logos et icônes figurant sur ce site Web ou dans les autres sites. Les produits et services auxquels il est fait référence dans ce site Web sans préavis. </p> <p>Ce site Web peut contenir des liens vers ou peut contenir des références à d'autres sites Web qui ne sont pas sous le contrôle d'Intervet Inc.. Intervet Inc. doit aucune responsabilité que ce soit pour le contenu de tout site Web lié ou de tout lien contenu dans un tel site lié. Vous acceptez de ne pas ajouter, altérer, remplacer ou modifier partiellement ou totalement les informations de ce site Web. </p>";
    
    
    return disclaimer;
    
}
+(NSString *)getDisclaimerES {
    
    NSString *disclaimer = @"<p> Este sitio web y la información, nombres, imágenes, imágenes, logotipos e íconos relacionados con Intervet Inc. y sus subsidiarias, afiliados, productos y servicios se proporcionan tal como están y sin disponibilidad alguna, sin ninguna representación o garantía. de cualquier tipo, ya sea expresa o implícita, incluidas, entre otras, las garantías implícitas de comercialización, idoneidad para un propósito particular o no infracción </p> <p> Aunque la información en este sitio web se ha compilado de buena fe para proporcionar información sobre Intervet Inc. y sus productos y servicios, Intervet Inc., sus matrices, subsidiarias, afiliadas o empleados no serán responsables en ningún caso por daños de ningún tipo o naturaleza, incluyendo, sin limitación, directa, indirecta, especial , daños consecuentes o incidentales que resulten de o en conexión con el uso o acceso de este sitio web o resulten de o en conexión con cualquier material, información, calificaciones o recomendaciones sobre este tema. eb Sitio. </p> <p> Intervet Inc. puede realizar en cualquier momento modificaciones, mejoras y / o cambios a estos Términos y condiciones, la información, nombres, imágenes, imágenes, logotipos e iconos que se muestran en este Sitio web o en productos y servicios mencionados en este sitio web sin previo aviso. </p> <p> Este sitio web puede contener enlaces o puede contener referencias a otros sitios web que no están bajo el control de Intervet Inc .. Intervet Inc. tendrá no se responsabiliza de ningún modo por el contenido de cualquier sitio web vinculado o cualquier enlace contenido en dicho sitio vinculado. Acepta no agregar, alterar, sustituir o enmendar de manera parcial o total ninguna información en este sitio web. </p>";
    
    
    return disclaimer;
    
}

+(NSString *)getDisclaimerPT {
    
    NSString *disclaimer = @"<p> Este Site e as informações, nomes, imagens, imagens, logotipos e ícones relacionados ou relacionados à Intervet Inc. e suas subsidiárias, afiliadas, produtos e serviços são fornecidos no estado em que se encontram e na base disponível, sem qualquer representação ou garantia. de qualquer tipo, seja expressa ou implícita, incluindo, mas não se limitando às garantias implícitas de mercantilidade, adequação a uma finalidade específica ou não infração </p> <p> Embora as informações contidas neste Site tenham sido compiladas de boa fé para fornecer informações sobre a Intervet Inc. e seus produtos e serviços, a Intervet Inc., seus pais, subsidiárias, afiliadas ou funcionários não serão, em hipótese alguma, responsáveis ​​por quaisquer danos de qualquer espécie ou natureza, incluindo, sem limitação, direta, indireta, especial danos conseqüentes ou incidentais resultantes de ou em conexão com o uso ou acesso a este Site ou resultantes de ou relacionados a quaisquer materiais, informações, qualificações ou recomendações sobre este W eb Site. </p> <p> A Intervet Inc. pode a qualquer momento fazer modificações, melhorias e / ou alterações nestes Termos e Condições, nas informações, nomes, imagens, imagens, logotipos e ícones exibidos neste site ou no site. produtos e serviços mencionados neste Site sem aviso prévio. </p> <p> Este Site pode conter links para ou conter referências a outros sites que não estejam sob o controle da Intervet Inc. A Intervet Inc. terá nenhuma responsabilidade pelo conteúdo de qualquer site vinculado ou qualquer link contido em tal site vinculado. Você concorda em não adicionar, alterar, substituir ou emendar parcial ou integralmente qualquer informação neste site. </p>";
    
    
    return disclaimer;
}

+(NSString *)getDisclaimerDE {
    
    NSString *disclaimer = @"<p> Diese Website und die Informationen, Namen, Bilder, Bilder, Logos und Symbole in Bezug auf oder in Bezug auf Intervet Inc. und seine Tochtergesellschaften, verbundenen Unternehmen, Produkte und Dienstleistungen werden in der vorliegenden Form und ohne jegliche Zusicherung oder Gewährleistung zur Verfügung gestellt jeglicher Art, ob ausdrücklich oder stillschweigend, einschließlich, jedoch nicht beschränkt auf die implizierten Garantien der Marktgängigkeit, der Eignung für einen bestimmten Zweck oder der Nichtverletzung. </p> <p> Die Informationen auf dieser Website wurden in gutem Glauben erstellt Wenn Sie Informationen über Intervet Inc. und seine Produkte und Dienstleistungen bereitstellen, haften Intervet Inc., ihre Eltern, Tochterunternehmen, verbundenen Unternehmen oder Mitarbeiter in keinem Fall für Schäden jeglicher Art oder Art, einschließlich, jedoch nicht beschränkt auf direkte, indirekte oder besondere Schäden , Folgeschäden oder zufällige Schäden, die sich aus oder in Verbindung mit der Nutzung oder dem Zugriff auf diese Website oder aus oder in Verbindung mit Materialien, Informationen, Qualifikationen oder Empfehlungen zu W ergeben eb Site. </p> <p> Intervet Inc. kann jederzeit Änderungen, Verbesserungen und / oder Änderungen an diesen Allgemeinen Geschäftsbedingungen, den Informationen, Namen, Bildern, Bildern, Logos und Symbolen vornehmen, die auf dieser Website oder im Internet angezeigt werden Produkte und Dienstleistungen, auf die in dieser Website ohne Ankündigung Bezug genommen wird. </p> <p> Diese Website kann Links zu anderen Websites enthalten oder enthalten, die nicht unter der Kontrolle von Intervet Inc. stehen. Intervet Inc. hat dies Es wird keinerlei Verantwortung für den Inhalt einer solchen verlinkten Website oder eines Links übernommen, der auf einer solchen verlinkten Site enthalten ist. Sie erklären sich damit einverstanden, Informationen auf dieser Website nicht oder nur teilweise hinzuzufügen, zu ändern, zu ersetzen oder zu ergänzen. </p>";
    
    
    return disclaimer;
    
}

+(NSString *)getDisclaimerIT {
    
    NSString *disclaimer = @"<p> Questo sito Web e le informazioni, i nomi, le immagini, le immagini, i loghi e le icone relative a Intervet Inc. e alle sue sussidiarie, affiliate, prodotti e servizi sono forniti così come sono e in base alla disponibilità senza alcuna dichiarazione o garanzia di qualsiasi tipo, espressa o implicita, incluse, ma non limitate alle garanzie implicite di commerciabilità, idoneità per uno scopo particolare o non violazione </p> <p> Sebbene le informazioni di questo sito Web siano state compilate in buona fede per fornire informazioni su Intervet Inc. e i suoi prodotti e servizi, Intervet Inc., i suoi genitori, sussidiarie, affiliate o dipendenti non saranno in alcun caso responsabili per danni di qualsiasi tipo o natura di alcun tipo, inclusi, a titolo esemplificativo, diretto, indiretto, speciale , danni consequenziali o incidentali derivanti da o in connessione con l'uso o l'accesso a questo sito Web o derivanti da o in relazione a qualsiasi materiale, informazione, qualifica o raccomandazione su questo W Sito eb. </p> <p> Intervet Inc. può in qualsiasi momento apportare modifiche, miglioramenti e / o modifiche a questi Termini e condizioni, informazioni, nomi, immagini, immagini, loghi e icone visualizzati su questo sito Web o sul prodotti e servizi a cui si fa riferimento in questo sito Web senza preavviso. </p> <p> Questo sito Web può contenere collegamenti a o contenere riferimenti ad altri siti Web che non sono sotto il controllo di Intervet Inc .. Intervet Inc. deve avere nessuna responsabilità per il contenuto di tali siti Web collegati o di qualsiasi collegamento contenuto in tale sito collegato. L'utente accetta di non aggiungere, alterare, sostituire o modificare parzialmente o interamente alcuna informazione su questo sito Web. </p>";
    
    
    return disclaimer;
    
}

+(NSString *)getDisclaimerNL {
    
    NSString *disclaimer = @"<p> Deze Website en de informatie, namen, afbeeldingen, afbeeldingen, logo's en pictogrammen met betrekking tot of met betrekking tot Intervet Inc. en haar dochterondernemingen, gelieerde ondernemingen, producten en diensten worden geleverd zoals ze zijn en op een beschikbare basis zonder enige verklaring of garantie. van welke aard dan ook, expliciet of impliciet, inclusief maar niet beperkt tot impliciete garanties van verkoopbaarheid, geschiktheid voor een bepaald doel of niet-inbreuk. Hoewel de informatie op deze website te goeder trouw is samengesteld, informatie verstrekken over Intervet Inc. en haar producten en diensten, Intervet Inc., haar ouders, dochterondernemingen, gelieerde ondernemingen of werknemers kunnen in geen geval aansprakelijk worden gesteld voor schade van welke aard of aard dan ook, inclusief, maar niet beperkt tot, directe, indirecte, speciale , gevolgschade of incidentele schade die voortvloeit uit of verband houdt met het gebruik of de toegang tot deze website of die voortvloeit uit of in verband staat met materialen, informatie, kwalificaties of aanbevelingen op deze W eb Site. </p> <p> Intervet Inc. kan te allen tijde wijzigingen, verbeteringen en / of wijzigingen aanbrengen in deze Algemene voorwaarden, de informatie, namen, afbeeldingen, afbeeldingen, logo's en pictogrammen weergegeven op deze Website of de producten en diensten waarnaar deze Website verwijst zonder voorafgaande kennisgeving. </p> <p> Deze Website kan links naar of verwijzingen bevatten naar andere websites die niet onder de controle van Intervet Inc. staan. Intervet Inc. geen enkele verantwoordelijkheid voor de inhoud van dergelijke gekoppelde websites of koppelingen op dergelijke gelinkte sites. U stemt ermee in om geen enkele informatie op deze website geheel of gedeeltelijk toe te voegen, te wijzigen, te vervangen of te wijzigen. </p>";
    
    
    return disclaimer;
    
}

+(NSString *)getDateForTermsAndConditions {
    
    NSString *translateString;
    
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"en"])
    {
        language=@"en-US";
    }
    NSString *imageName;
    
    if ([language containsString:@"en"]) {
        
        translateString = [self getTermsAndConditionsEN];
    }
    else  if ([language containsString:@"fr"]) {
        translateString = [self getTermsAndConditionsFR];

    }
    else  if ([language containsString:@"es"]) {
        translateString = [self getTermsAndConditionsES];

    }
    else  if ([language containsString:@"pt"]) {
        translateString = [self getTermsAndConditionsPT];

    }
    else  if ([language containsString:@"de"]) {
        translateString = [self getTermsAndConditionsDE];

    }
    else  if ([language containsString:@"it"]) {
        translateString = [self getTermsAndConditionsIT];

    }
    else  if ([language containsString:@"nl"]) {
        translateString = [self getTermsAndConditionsNL];

    }
    
    return  translateString ;
}

+(NSString *)getTermsAndConditionsEN {
    
    NSString *terms = @"<p>You may not access this Web Site or use the information on this Web Site without prior acceptance of these terms and conditions. Access of this Web Site or use of the information on this Web Site constitutes your acceptance of these terms and conditions and your waiver of any and all claims against Intervet Inc., its subsidiaries, affiliates or employees arising out of your use of this Web Site or any material, information, opinions or recommendations contained therein.</p><p>These terms and conditions further consist of the following clauses:</p><p><b>Information and disclaimer</b></p><p>This Web Site and the information, names, images, pictures, logos and icons regarding or relating to Intervet Inc. and its subsidiaries, affiliates, products and services is provided 'as is' and on an 'as available' basis without any representation or warranty of any kind, whether expressed or implied, including, but not limited to the implied warranties of mercantability, fitness for a particular purpose or non infringement.</p><p>Although the information on this Web Site has been compiled in good faith to provide information on Intervet Inc. and its products and services, Intervet Inc., its parents, subsidiaries, affiliates or employees shall in no event be liable for any damages of any kind or nature whatsoever, including, without limitation, direct, indirect, special, consequential or incidental damages resulting from or in connection with the use or access of this Web Site or resulting from or in connection with any materials, information, qualifications or recommendations on this Web Site.</p><p>Intervet Inc. may at any time make modifications, improvements and/or changes to these Terms and conditions, the information, names, images, pictures, logos and icons displayed on this Web Site or the products and services referred to in this Web Site without notice.</p><p>This Web Site may contain links to or may contain references to other web sites which are not under the control of Intervet Inc.. Intervet Inc. shall have no responsibility whatsoever for the contents of any such linked web site or any link contained in such a linked site. You agree not to add, alter, substitute or amend partially or in whole any information on this Web Site.</p><p><b>Intellectual property</b></p><p>Please note that the materials displayed on this Web Site, including, without limitation, all editorial materials, photographs, illustrations and other graphic materials, and names, logos, trademarks and service marks, are the property of Intervet Inc. or any of its subsidiaries, affiliates or licensors and are protected by copyright, trademark, and other intellectual property laws. Other products or company names mentioned on this Web Site may be the trademarks of their respective owners. You are not allowed to reproduce, retransmit, distribute, disseminate, sell, publish, broadcast or circulate any such material or any part thereof save that you may:</p><p>print or down-load extracts of the material on this Web Site for your non-commercial, informational and personal use; or copy the material on this Web Site for the purpose of sending it, without any commercial objective, to individual parties for their personal information provided that you acknowledge Intervet Inc. as the source of the material and that you inform the third party that these terms and conditions apply to them and that they must comply with them. Nothing contained herein shall be construed as conferring by implication, estoppel or otherwise any licence or right under any patent or trademark of Intervet Inc. or any third party.</p><p>Except as expressly provided above nothing contained herein shall be construed as conferring any licence or right to any copyright or other intellectual property rights in relation to any such information, data, products or services.</p><p><b>Applicable law and jurisdiction</b></p><p>These terms and conditions shall be governed by, construed and enforced in accordance with the laws of Delaware, without respect to Delaware's laws concerning conflicts of law. Any and all disputes arising herefrom or in connection herewith shall be exclusively submitted to and finally resolved by the Delaware courts.</p>";
    
    
    return terms;
    
}
+(NSString *)getTermsAndConditionsFR {
    
    NSString *terms = @"<p> Vous ne pouvez pas accéder à ce site Web ou utiliser les informations de ce site Web sans l'acceptation préalable de ces termes et conditions. L'accès à ce site Web ou l'utilisation des informations de ce site Web constitue votre acceptation de ces termes et conditions et votre renonciation à toute réclamation contre Intervet Inc., ses filiales, ses sociétés affiliées ou ses employés découlant de votre utilisation de ce site Web. ou tout élément, information, opinion ou recommandation y figurant. </p> <p> Les présentes conditions générales comprennent en outre les clauses suivantes: </p> <p> <b> Informations et non-responsabilité </b> </p> <p> Ce site Web et les informations, noms, images, images, logos et icônes concernant ou concernant Intervet Inc. et ses filiales, sociétés affiliées, produits et services sont fournis \"en l'état\" et sur une base \"tel que disponible\". sans aucune représentation ni garantie d'aucune sorte, expresse ou implicite, y compris, sans toutefois s'y limiter, les garanties implicites de qualité marchande, d'adéquation à un usage particulier ou de non-violation. </p> <p> Bien que les informations de ce site Web aient été compilé de bonne foi pour fournir des informations En ce qui concerne Intervet Inc. et ses produits et services, Intervet Inc., ses sociétés mères, ses filiales, ses sociétés affiliées ou ses employés ne pourront en aucun cas être tenus responsables des dommages de quelque nature que ce soit, y compris, sans limitation, directs, indirects, spéciaux, consécutifs et indirects. ou des dommages fortuits résultant de ou liés à l'utilisation ou à l'accès de ce site Web ou résultant de ou en relation avec tout matériel, information, qualification ou recommandations de ce site Web. </p> <p> Intervet Inc. peut à tout moment temps, modifications, améliorations et / ou modifications apportées aux présentes Conditions générales, aux informations, noms, images, images, logos et icônes affichés sur ce site Web ou aux produits et services mentionnés dans ce site Web sans préavis. </p> <p> Ce site Web peut contenir des liens vers ou peut contenir des références à d'autres sites Web qui ne sont pas sous le contrôle d'Intervet Inc .. Intervet Inc. n'assume aucune responsabilité pour le contenu de ces sites Web liés ni de liens c. ont contenu dans un tel site lié. Vous vous engagez à ne pas ajouter, altérer, substituer ou modifier tout ou partie des informations de ce site Web. </p> <p> <b> Propriété intellectuelle </b> </p> <p> Veuillez noter que les éléments affichés sur ce site Web, y compris, sans limitation, tous les documents éditoriaux, photographies, illustrations et autres éléments graphiques, ainsi que les noms, logos, marques de commerce et marques de service, sont la propriété d’Intervet Inc. ou de l’une de ses filiales, sociétés affiliées ou concédants de licence et sont protégés par le droit d'auteur, les marques de commerce et d'autres lois sur la propriété intellectuelle. Les autres produits ou noms de société mentionnés sur ce site Web peuvent être des marques de commerce de leurs propriétaires respectifs. Vous n'êtes pas autorisé à reproduire, retransmettre, distribuer, diffuser, vendre, publier, diffuser ou diffuser tout ou partie de ce matériel, sauf que vous pouvez: </p> <p> imprimer ou télécharger des extraits du contenu de ce document. Site Web pour votre utilisation non commerciale, informative et personnelle; ou copier le contenu de ce site Web dans le but de l’envoyer, sans objectif commercial, à des tiers pour leur information personnelle, à condition que vous reconnaissiez la source du matériel à Intervet Inc. et que vous informiez le tiers que les présentes conditions et des conditions s’appliquent à eux et qu’ils doivent les respecter. Aucune information contenue dans ce document ne doit être interprétée comme conférant implicitement, par estoppel ou autrement, une licence ou un droit en vertu d’un brevet ou d’une marque déposée d’Intervet Inc. ou de tiers. </p> <p> Sauf disposition expresse contraire ci-dessus, rien dans le présent document ne doit être interprété comme conférant une licence ou un droit à un droit d'auteur ou à un autre droit de propriété intellectuelle en relation avec de telles informations, données, produits ou services. </p> <p> <b> Loi applicable et juridiction </b> </p> <p> Les présentes conditions générales sont régies, interprétées et appliquées conformément aux lois du Delaware, sans égard aux lois du Delaware sur les conflits de lois. Tout litige découlant de la présente ou liée à celle-ci sera exclusivement soumis aux tribunaux du Delaware, qui le résoudront en dernier ressort. </p>";
    
    
    return terms;
    
}
+(NSString *)getTermsAndConditionsES {
    
    NSString *terms = @"<p> No puede acceder a este sitio web ni utilizar la información de este sitio web sin la aceptación previa de estos términos y condiciones. El acceso a este sitio web o el uso de la información en este sitio web constituye su aceptación de estos términos y condiciones y su renuncia a todos y cada uno de los reclamos contra Intervet Inc., sus subsidiarias, afiliadas o empleados que surjan de su uso de este sitio web. o cualquier material, información, opiniones o recomendaciones contenidas en el mismo. </p> <p> Estos términos y condiciones incluyen además las siguientes cláusulas: </p> <p> <b> Información y exención de responsabilidad </b> </ p > <p> Este sitio web y la información, nombres, imágenes, imágenes, logotipos e íconos relacionados con Intervet Inc. y sus subsidiarias, afiliadas, productos y servicios se proporcionan \"tal cual\" y en una base \"según esté disponible\" sin ninguna representación o garantía de ningún tipo, ya sea expresa o implícita, incluidas, entre otras, las garantías implícitas de comercialización, idoneidad para un propósito particular o no infracción. </p> <p> Aunque la información en este sitio web Ha sido compilado de buena fe para proporcionar información. En Intervet Inc. y sus productos y servicios, Intervet Inc., sus matrices, subsidiarias, afiliadas o empleados no serán responsables en ningún caso por daños de ningún tipo o naturaleza, incluyendo, sin limitación, directa, indirecta, especial, consecuente o daños incidentales resultantes de o en conexión con el uso o acceso de este sitio web o como resultado de o en conexión con cualquier material, información, calificaciones o recomendaciones en este sitio web. </p> <p> Intervet Inc. puede en cualquier haga modificaciones, mejoras y / o cambios a estos Términos y condiciones, la información, nombres, imágenes, imágenes, logotipos e íconos que se muestran en este sitio web o los productos y servicios mencionados en este sitio web sin previo aviso. </p> <p> Este sitio web puede contener enlaces o puede contener referencias a otros sitios web que no están bajo el control de Intervet Inc .. Intervet Inc. no tendrá responsabilidad alguna por el contenido de dichos sitios web vinculados o de cualquier enlace c Mantenido en un sitio vinculado. Acepta no agregar, alterar, sustituir o enmendar parcial o totalmente cualquier información en este Sitio web. </p> <p> <b> Propiedad intelectual </b> </p> <p> Tenga en cuenta que los materiales mostrados en este sitio web, incluidos, entre otros, todos los materiales editoriales, fotografías, ilustraciones y otros materiales gráficos, así como los nombres, logotipos, marcas comerciales y marcas de servicio, son propiedad de Intervet Inc. o de cualquiera de sus subsidiarias, afiliadas o licenciantes. están protegidos por derechos de autor, marcas registradas y otras leyes de propiedad intelectual. Otros productos o nombres de compañías mencionados en este sitio web pueden ser marcas comerciales de sus respectivos propietarios. No se le permite reproducir, retransmitir, distribuir, difundir, vender, publicar, difundir o distribuir ningún material de este tipo o parte del mismo, salvo que: </p> <p> imprima o descargue extractos del material en este Sitio web para su uso no comercial, informativo y personal; o copie el material en este sitio web con el fin de enviarlo, sin ningún objetivo comercial, a las partes individuales para su información personal, siempre que usted reconozca a Intervet Inc. como la fuente del material y que informe a la tercera parte que estos términos y condiciones que se les aplican y que deben cumplir con ellas. Nada de lo contenido en este documento se interpretará como un otorgamiento por implicación, impedimento o de otro modo cualquier licencia o derecho bajo cualquier patente o marca registrada de Intervet Inc. o cualquier tercero. </p> <p> Excepto que se indique expresamente arriba, nada de lo contenido en este documento se interpretará como confiere cualquier licencia o derecho a cualquier derecho de autor u otros derechos de propiedad intelectual en relación con dicha información, datos, productos o servicios. </p> <p> <b> Ley aplicable y jurisdicción </b> </p> <p> Estos términos y condiciones se regirán, interpretarán y aplicarán de conformidad con las leyes de Delaware, sin respetar las leyes de Delaware relativas a conflictos de leyes. Todas y cada una de las disputas que surjan aquí o en relación con el presente serán exclusivamente sometidas y finalmente resueltas por los tribunales de Delaware. </p>";
    
    
    return terms;
    
}
+(NSString *)getTermsAndConditionsPT {
    
    NSString *terms = @"<p> Você não pode acessar este site ou usar as informações deste site sem a aceitação prévia destes termos e condições. O acesso deste site ou o uso das informações contidas neste site constitui sua aceitação destes termos e condições e sua renúncia a todas e quaisquer reclamações contra a Intervet Inc., suas subsidiárias, afiliadas ou funcionários decorrentes do uso deste site ou qualquer material, informação, opiniões ou recomendações nele contidas. </p> <p> Estes termos e condições consistem adicionalmente nas seguintes cláusulas: </p> <p> <b> Informação e isenção de responsabilidade </b> </p> <p> Este Site e as informações, nomes, imagens, imagens, logotipos e ícones relacionados ou relacionados à Intervet Inc. e suas subsidiárias, afiliadas, produtos e serviços são fornecidos 'como estão' e 'conforme disponíveis' sem qualquer declaração ou garantia de qualquer tipo, expressa ou implícita, incluindo, mas não limitado às garantias implícitas de mercantilidade, adequação a uma finalidade específica ou não violação. </p> <p> Embora as informações contidas neste site tenham foi compilado de boa fé para fornecer informações na Intervet Inc. e seus produtos e serviços, a Intervet Inc., seus pais, subsidiárias, afiliadas ou funcionários não serão, em hipótese alguma, responsáveis ​​por quaisquer danos de qualquer espécie ou natureza, incluindo, sem limitação, direta, indireta, especial, consequencial ou danos incidentais resultantes de ou em conexão com o uso ou acesso deste Site da Web ou resultantes de ou em conexão com quaisquer materiais, informações, qualificações ou recomendações neste Site. </p> <p> A Intervet Inc. pode, a qualquer momento fazer modificações, melhorias e / ou alterações nestes Termos e Condições, nas informações, nomes, imagens, imagens, logotipos e ícones exibidos neste site ou nos produtos e serviços mencionados neste site sem aviso prévio. </p> <p> Este Site pode conter links para ou pode conter referências a outros sites que não estão sob o controle da Intervet Inc. A Intervet Inc. não terá nenhuma responsabilidade pelo conteúdo de qualquer site vinculado ou qualquer link ontained em tal site vinculado. Você concorda em não adicionar, alterar, substituir ou emendar parcial ou integralmente qualquer informação neste Site. </p> <p> <b> Propriedade intelectual </b> </p> <p> Por favor, note que os materiais exibidos neste Site, incluindo, sem limitação, todos os materiais editoriais, fotografias, ilustrações e outros materiais gráficos e nomes, logotipos, marcas registradas e marcas de serviço, são de propriedade da Intervet Inc. ou de qualquer de suas subsidiárias, afiliadas ou licenciadas e estão protegidos por direitos autorais, marcas registradas e outras leis de propriedade intelectual. Outros produtos ou nomes de empresas mencionados neste site podem ser marcas comerciais de seus respectivos proprietários. Você não tem permissão para reproduzir, retransmitir, distribuir, disseminar, vender, publicar, difundir ou circular qualquer material ou parte dele, a não ser que você possa: </p> <p> imprimir ou baixar os extratos do material neste documento Web Site para seu uso não comercial, informativo e pessoal; ou copie o material deste site com o objetivo de enviá-lo, sem qualquer objetivo comercial, a terceiros para suas informações pessoais, desde que você reconheça a Intervet Inc. como a fonte do material e informe ao terceiro que esses termos e condições se aplicam a eles e que eles devem cumpri-los. Nada contido aqui deve ser interpretado como conferindo por implicação, preclusão ou de outra forma qualquer licença ou direito sob qualquer patente ou marca registrada da Intervet Inc. ou qualquer terceiro. Exceto como expressamente fornecido acima, nada aqui contido deve ser interpretado. conferir qualquer licença ou direito a qualquer direito autoral ou outros direitos de propriedade intelectual em relação a qualquer informação, dados, produtos ou serviços. </p> <p> <b> Legislação e jurisdição aplicáveis ​​</b> </p> <p> Estes termos e condições serão regidos, interpretados e executados de acordo com as leis de Delaware, sem respeito às leis de Delaware relativas a conflitos de lei. Todas e quaisquer controvérsias oriundas daqui ou em conexão com este serão exclusivamente submetidas e finalmente resolvidas pelos tribunais de Delaware. </p>";
    
    
    return terms;
}
+(NSString *)getTermsAndConditionsIT {
    
    NSString *terms = @"<p> È vietato accedere a questo sito Web o utilizzare le informazioni contenute in questo sito Web senza previa accettazione di questi termini e condizioni. L'accesso a questo sito Web o l'uso delle informazioni su questo sito Web implica l'accettazione di questi termini e condizioni e la rinuncia a qualsiasi rivendicazione nei confronti di Intervet Inc., delle sue sussidiarie, affiliate o dipendenti derivanti dall'utilizzo di questo sito Web. o qualsiasi materiale, informazione, opinione o raccomandazione in esso contenuta. </p> <p> Questi termini e condizioni comprendono inoltre le seguenti clausole: </p> <p> <b> Informazioni e dichiarazione di non responsabilità </b> </p> <p> Questo sito Web e le informazioni, i nomi, le immagini, le immagini, i loghi e le icone relativi a o connessi a Intervet Inc. e alle sue filiali, affiliate, prodotti e servizi sono forniti \"così come sono\" e su base \"disponibile\" senza alcuna dichiarazione o garanzia di alcun tipo, espressa o implicita, incluse, ma non limitate alle garanzie implicite di commerciabilità, idoneità per uno scopo particolare o non violazione. </p> <p> Sebbene le informazioni su questo sito Web siano stato compilato in buona fede per fornire informazioni su Intervet Inc. e i suoi prodotti e servizi, Intervet Inc., i suoi genitori, sussidiarie, affiliate o dipendenti non saranno in alcun caso responsabili per danni di qualsiasi tipo o natura di alcun tipo, inclusi, a titolo esemplificativo, diretto, indiretto, speciale, consequenziale o danni accidentali derivanti da o in connessione con l'uso o l'accesso a questo sito Web o derivanti da o in connessione con qualsiasi materiale, informazioni, qualifiche o raccomandazioni su questo sito Web. </p> <p> Intervet Inc. può in qualsiasi tempo apportare modifiche, miglioramenti e / o modifiche a questi termini e condizioni, le informazioni, nomi, immagini, immagini, loghi e icone visualizzati su questo sito Web oi prodotti e servizi cui si fa riferimento in questo sito Web senza preavviso. </p> <p> Questo sito Web può contenere collegamenti a o può contenere riferimenti ad altri siti Web che non sono sotto il controllo di Intervet Inc.. Intervet Inc. non avrà alcuna responsabilità per il contenuto di tali siti Web collegati o di qualsiasi collegamento c raggiunto in un sito collegato. Accetti di non aggiungere, alterare, sostituire o modificare parzialmente o interamente alcuna informazione su questo sito Web. </p> <p> <b> Proprietà intellettuale </b> </p> <p> Ti preghiamo di notare che i materiali visualizzati su questo sito Web, compresi, a titolo esemplificativo, tutti i materiali editoriali, fotografie, illustrazioni e altri materiali grafici e nomi, loghi, marchi e marchi di servizio, sono di proprietà di Intervet Inc. o delle sue sussidiarie, affiliate o licenzianti e sono protetti da copyright, marchio commerciale e altre leggi sulla proprietà intellettuale. Altri prodotti o nomi di società citati in questo sito Web possono essere marchi dei rispettivi proprietari. Non sei autorizzato a riprodurre, ritrasmettere, distribuire, divulgare, vendere, pubblicare, trasmettere o diffondere alcun materiale o parte di esso salvo che tu possa: </p> <p> stampare o scaricare gli estratti del materiale su questo Sito Web per uso non commerciale, informativo e personale; o copiare il materiale su questo sito Web allo scopo di inviarlo, senza alcun obiettivo commerciale, a singole parti per le loro informazioni personali a condizione che tu riconosca Intervet Inc. come fonte del materiale e che informi la terza parte che questi termini e le condizioni si applicano a loro e che devono rispettarli. Nulla di quanto contenuto nel presente documento può essere interpretato nel senso di conferire implicitamente, preclusione o altra licenza o diritto in base a qualsiasi brevetto o marchio di Intervet Inc. o di terze parti. </p> <p> Ad eccezione di quanto espressamente previsto sopra, nulla di quanto contenuto nel presente documento deve essere interpretato come conferire qualsiasi licenza o diritto a qualsiasi diritto d'autore o altri diritti di proprietà intellettuale in relazione a tali informazioni, dati, prodotti o servizi. </p> <p> <b> Legge applicabile e giurisdizione </b> </p> <p> Questi termini e condizioni sono regolati, interpretati e applicati in conformità con le leggi del Delaware, senza rispettare le leggi del Delaware relative ai conflitti di legge. Qualsiasi e tutte le controversie derivanti qui o in connessione con questo saranno esclusivamente sottoposte e risolte dai tribunali del Delaware. </p>";
    
    
    return terms;
    
}
+(NSString *)getTermsAndConditionsNL {
    
    NSString *terms = @"<p> U mag deze Website niet openen of de informatie op deze Website gebruiken zonder voorafgaande aanvaarding van deze algemene voorwaarden. Toegang tot deze website of gebruik van de informatie op deze website geeft aan dat u akkoord gaat met deze voorwaarden en bepalingen en dat u afstand doet van alle claims tegen Intervet Inc., haar dochterondernemingen, gelieerde ondernemingen of werknemers die voortvloeien uit uw gebruik van deze website of enig materiaal, informatie, meningen of aanbevelingen daarin. </p> <p> Deze algemene voorwaarden omvatten verder de volgende clausules: </p> <p> <b> Informatie en disclaimer </b> </p > <p> Deze Website en de informatie, namen, afbeeldingen, afbeeldingen, logo's en pictogrammen met betrekking tot of verband houdend met Intervet Inc. en haar dochterondernemingen, gelieerde bedrijven, producten en diensten worden aangeboden 'as is' en op een 'as available' basis zonder enige verklaring of garantie van welke aard dan ook, expliciet of impliciet, inclusief, maar niet beperkt tot, de impliciete garanties van verkoopbaarheid, geschiktheid voor een bepaald doel of niet-inbreuk. </p> <p> Hoewel de informatie op deze website heeft te goeder trouw samengesteld om informatie te verstrekken op Intervet Inc. en haar producten en diensten, zijn Intervet Inc., haar ouders, dochterondernemingen, gelieerde ondernemingen of werknemers in geen geval aansprakelijk voor enige vorm van schade, van welke aard dan ook, inclusief, zonder beperking, directe, indirecte, speciale, gevolg of incidentele schade die voortvloeit uit of verband houdt met het gebruik of de toegang tot deze website of voortvloeit uit of verband houdt met materiaal, informatie, kwalificaties of aanbevelingen op deze website. </p> <p> Intervet Inc. kan op elke tijd om zonder voorafgaande kennisgeving wijzigingen, verbeteringen en / of wijzigingen aan te brengen in deze Algemene voorwaarden, de informatie, namen, afbeeldingen, afbeeldingen, logo's en pictogrammen die op deze Website of de producten en services op deze Website worden vermeld. </p> <p> Deze website kan koppelingen bevatten naar of verwijzingen bevatten naar andere websites die niet onder de controle van Intervet Inc. staan. Intervet Inc. is in geen enkel opzicht verantwoordelijk voor de inhoud van dergelijke gekoppelde websites of voor links c op een dergelijke gekoppelde site. U stemt ermee in om geen informatie op deze Website geheel of gedeeltelijk toe te voegen, te wijzigen, te vervangen of aan te passen. </p> <p> <b> Intellectuele eigendom </b> </p> <p> Houd er rekening mee dat de materialen weergegeven op deze website, inclusief maar niet beperkt tot alle redactionele materialen, foto's, illustraties en ander grafisch materiaal, en namen, logo's, handelsmerken en servicemerken, zijn het eigendom van Intervet Inc. of een van haar dochterondernemingen, gelieerde ondernemingen of licentiegevers en zijn beschermd door auteursrecht, handelsmerk en andere wetgeving met betrekking tot intellectueel eigendom. Andere producten of bedrijfsnamen die op deze website worden vermeld, kunnen handelsmerken zijn van hun respectieve eigenaars. Het is u niet toegestaan ​​om dergelijk materiaal of een deel daarvan te reproduceren, opnieuw te verzenden, distribueren, verspreiden, verkopen, publiceren, uitzenden of verspreiden, behalve dat u: </p> <p> fragmenten van dit materiaal kunt afdrukken of downloaden Website voor uw niet-commerciële, informatieve en persoonlijke gebruik; of kopieer het materiaal op deze website met als doel om het zonder enige commerciële doelstelling naar individuele partijen te sturen voor hun persoonlijke gegevens, op voorwaarde dat u Intervet Inc. erkent als de bron van het materiaal en dat u de derde informeert dat deze voorwaarden en voorwaarden zijn van toepassing op hen en dat ze hieraan moeten voldoen. Niets hierin kan worden geïnterpreteerd als het verlenen door implicatie, uitsluiting of anderszins van enige licentie of recht onder enig patent of handelsmerk van Intervet Inc. of een derde partij. </p> <p> Behalve zoals uitdrukkelijk hierboven is bepaald, kan niets hierin worden geïnterpreteerd als het verlenen van licenties of rechten op auteursrechten of andere intellectuele eigendomsrechten met betrekking tot dergelijke informatie, gegevens, producten of services. </p> <p> <b> Toepasselijk recht en jurisdictie </b> </p> <p> Deze voorwaarden en bepalingen worden beheerst door, geïnterpreteerd en gehandhaafd in overeenstemming met de wetten van Delaware, zonder inachtneming van de wetten van Delaware met betrekking tot wetsconflicten. Alle geschillen die hieruit voortvloeien of in verband hiermee worden uitsluitend voorgelegd aan en definitief opgelost door de rechtbanken in Delaware. </p>";
    
    
    return terms;
    
}

+(NSString *)getTermsAndConditionsDE {
    
    NSString *terms = @"<p> Sie dürfen nicht ohne vorherige Zustimmung zu diesen Bedingungen auf diese Website zugreifen oder die Informationen auf dieser Website verwenden. Durch den Zugriff auf diese Website oder die Verwendung der Informationen auf dieser Website erklären Sie sich mit diesen Bedingungen einverstanden und verzichten auf alle Ansprüche gegen Intervet Inc., ihre Tochtergesellschaften, verbundenen Unternehmen oder Mitarbeiter, die sich aus Ihrer Nutzung dieser Website ergeben oder jegliches Material, Informationen, Meinungen oder Empfehlungen, die darin enthalten sind. </p> <p> Diese Allgemeinen Geschäftsbedingungen enthalten ferner die folgenden Klauseln: </p> <p> <b> Informationen und Haftungsausschluss </b> </p> <p> Diese Website und die Informationen, Namen, Bilder, Bilder, Logos und Symbole in Bezug auf oder in Bezug auf Intervet Inc. und seine Tochtergesellschaften, verbundenen Unternehmen, Produkte und Dienstleistungen werden \"wie besehen\" und \"soweit verfügbar\" bereitgestellt ohne jegliche Zusicherung oder Gewährleistung jeglicher Art, ob ausdrücklich oder stillschweigend, einschließlich, jedoch nicht beschränkt auf die implizierten Garantien der Marktgängigkeit, der Eignung für einen bestimmten Zweck oder der Nichtverletzung in gutem Glauben zusammengestellt, um Informationen zu geben In Bezug auf Intervet Inc. und seine Produkte und Dienstleistungen haften Intervet Inc., ihre Eltern, Tochtergesellschaften, verbundenen Unternehmen oder Mitarbeiter in keinem Fall für Schäden jeglicher Art oder Art, einschließlich, jedoch nicht beschränkt auf direkte, indirekte, spezielle Folgeschäden oder zufällige Schäden, die aus oder im Zusammenhang mit der Nutzung oder dem Zugriff auf diese Website oder aus oder in Verbindung mit Materialien, Informationen, Qualifikationen oder Empfehlungen auf dieser Website entstehen. </p> <p> Intervet Inc. kann dies jederzeit tun Nehmen Sie rechtzeitig Änderungen, Verbesserungen und / oder Änderungen an diesen Allgemeinen Geschäftsbedingungen, den Informationen, Namen, Bildern, Bildern, Logos und Symbolen vor, die auf dieser Website oder den auf dieser Website genannten Produkten und Dienstleistungen ohne vorherige Ankündigung angezeigt werden. </p> <p> Diese Website kann Links zu anderen Websites enthalten oder enthält Verweise auf andere Websites, die nicht unter der Kontrolle von Intervet Inc. stehen. Intervet Inc. übernimmt keinerlei Verantwortung für den Inhalt einer solchen verlinkten Website oder einen Link. c in einer solchen verlinkten Seite erhalten. Sie stimmen zu, die Informationen auf dieser Website nicht ganz oder teilweise hinzuzufügen, zu ändern, zu ersetzen oder zu ergänzen. </p> <p> <b> Geistiges Eigentum </b> </p> <p> Bitte beachten Sie die Materialien Die auf dieser Website dargestellten Informationen, einschließlich aller redaktionellen Materialien, Fotos, Illustrationen und anderen grafischen Materialien sowie Namen, Logos, Marken und Dienstleistungsmarken, sind Eigentum von Intervet Inc. oder einer ihrer Tochtergesellschaften, verbundenen Unternehmen oder Lizenzgebern und sind urheberrechtlich, markenrechtlich und durch andere Gesetze zum Schutz geistigen Eigentums geschützt. Andere auf dieser Website erwähnte Produkt- oder Firmennamen können Marken der jeweiligen Eigentümer sein. Es ist Ihnen nicht gestattet, solches Material oder Teile davon zu reproduzieren, erneut zu übertragen, zu verteilen, zu verbreiten, zu veröffentlichen, zu senden oder zu verbreiten, es sei denn, Sie dürfen Auszüge des Materials hierüber ausdrucken oder herunterladen Website für Ihre nichtkommerzielle, informative und persönliche Nutzung; oder das Material auf dieser Website zu kommerziellen Zwecken an Dritte senden, um persönliche Informationen zu erhalten, sofern Sie Intervet Inc. als Quelle des Materials anerkennen und den Dritten darüber informieren, dass diese Bedingungen gelten und Bedingungen gelten für sie und dass sie diese einhalten müssen. Nichts, das in diesem Dokument enthalten ist, ist so auszulegen, als impliziere es, impliziere oder anderweitig irgendwelche Lizenzen oder Rechte unter einem Patent oder einer Marke von Intervet Inc. oder einem Dritten. </p> <p> Sofern nicht ausdrücklich anders angegeben, ist nichts hierin enthalten B. eine Lizenz oder ein Recht auf ein Urheberrecht oder ein anderes geistiges Eigentum in Bezug auf solche Informationen, Daten, Produkte oder Dienstleistungen übertragen. </p> <p> <b> Anwendbares Recht und Gerichtsbarkeit </b> </p> Diese Bedingungen und Bedingungen unterliegen den Gesetzen von Delaware, sind auszulegen und durchzusetzen, ohne die Gesetze von Delaware bezüglich Gesetzeskonflikten zu beachten. Alle Streitigkeiten, die sich hieraus oder in Verbindung damit ergeben, werden ausschließlich den Gerichten in Delaware vorgelegt und endgültig beigelegt. </p>";
    
    
    return terms;
    
}

@end
