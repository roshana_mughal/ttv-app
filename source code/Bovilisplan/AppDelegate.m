//
//  AppDelegate.m
//  Bovilisplan
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "WelcomeViewController.h"
#import "DataController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "IQKeyboardManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "NHNTLog.h"
#import <NHNetworkTime/NHNetworkTime.h>
#import "BovilisApi.h"
#import "EventDetailViewController.h"


@import Firebase;
@import AppCenter;

@import FirebaseMessaging;
@import UserNotifications;
@import FirebaseInstanceID;


@interface AppDelegate ()<NSURLSessionDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate>
{
    NSString * language;
    NSString * fileName;
}
@end

@implementation AppDelegate

@synthesize vaccineName,vaccineDate,vaccineRegion,vaccineComments,selectdDate,numberOfVaccineCattleTotoal,selectdVaccineName,vaccineID,farmName,vaccineType,switchValue1,switchValue2,switchValue3,switchValue4,scheduleVaccine,region,dictCulture,testString,removeView,methodComplete,selectedVaccineType,cutomVaccineName,farmDeleted,languageAlert,reloadCalendar,isSwitchOn,vaccineIDforEdit,vaccineNumberOfAnimals;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [UIWindow new];
    self.window.frame = [UIScreen mainScreen].bounds;
    dispatch_async(dispatch_get_main_queue(), ^{
        
      [self getCultures];

    });
    //firbase tracking
    [FIRApp configure];
   //[MSAppCenter start:@"" withServices:[MSAppCenter self]];

    if ([UNUserNotificationCenter class] != nil) {
      // iOS 10 or later
      // For iOS 10 display notification (sent via APNS)
      [UNUserNotificationCenter currentNotificationCenter].delegate = self;
      UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
          UNAuthorizationOptionSound;
      [[UNUserNotificationCenter currentNotificationCenter]
          requestAuthorizationWithOptions:authOptions
          completionHandler:^(BOOL granted, NSError * _Nullable error) {
            // ...
          }];
    } else {
      // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
      UIUserNotificationType allNotificationTypes =
      (UIUserNotificationTypeSound | UIUserNotificationTypeAlert);
      UIUserNotificationSettings *settings =
      [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
      [application registerUserNotificationSettings:settings];
    }

    [application registerForRemoteNotifications];
    [FIRMessaging messaging].delegate = self;

    //Facebook tracking
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    // Google analytics
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithTrackingId:@"UA-62446063-38"];
    
    gai.trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval=2.0;
    
    _eventAddedDateArray =[NSMutableArray new];
    _dropdownSelectionArray = [[NSMutableArray alloc]init];
    _vaccineListingArray = [[NSMutableArray alloc]init];
    _selectedDateImagesarray =[[NSMutableArray alloc]init];
    _selectedImageArrayRed =[[NSMutableArray alloc]init];
    _selectedImageArrayGreen =[[NSMutableArray alloc]init];
    _selectedImageArrayBrown =[[NSMutableArray alloc]init];
    _selectedImageArrayRedAndGreen =[[NSMutableArray alloc]init];
    _selectedImageArrayAll =[[NSMutableArray alloc]init];
    _userSevedCalenderEventArray =[[NSMutableArray alloc]init];
    _getUserVaccinesFromServer =[[NSMutableArray alloc]init];
    _productsArray =[[NSMutableArray alloc]init];
    _userListingArray =[[NSMutableArray alloc]init];
    _imageDataFromServer=[[NSMutableArray alloc]init];
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    [[NHNetworkClock sharedNetworkClock] synchronize];
    
    reloadCalendar=@"No";
    self.isLoadedNotifications =@"No";
    methodComplete = @"No";
    
    switchValue1 = @"False";
    switchValue2 = @"False";
    switchValue3 = @"False";
    switchValue4 = @"False";
    
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
    NSString *savedUserEmail = [userDefaults1   valueForKey:@"IsLogin"];
    [userDefaults1 synchronize];
    

   if(savedUserEmail.length>0) {
        HomeViewController*welcomeVC = [HomeViewController new];
         self.navigationController = [[UINavigationController alloc]initWithRootViewController:welcomeVC];
    }
    else {
        
        WelcomeViewController*welcomeVC = [WelcomeViewController new];
         self.navigationController = [[UINavigationController alloc]initWithRootViewController:welcomeVC];
    }
    
   // [NSThread sleepForTimeInterval:2.0];

     [self.window setRootViewController: self.navigationController];
     self.navigationController.navigationBar.hidden = YES;

    [self.window makeKeyAndVisible];
    
   
    return YES;
}
#pragma mark === Firebase Code ===

-(void)tokenRefreshCallback:(NSNotification *)notification
{
    NSString *refreshToken = [[FIRInstanceID  instanceID] token];
   [self connectToFirebase];
}

-(void)connectToFirebase
{
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error)
     {
         if(error !=nil)
         {
             NSLog(@"Unable to connect FCM %@",error);
             
         }
         else
         {
             NSLog(@"Connected with FCM");
         }
     }];
}

- (void)tokenWithAuthorizedEntity:(nonnull NSString *)authorizedEntity
                            scope:(nonnull NSString *)scope
                          options:(nullable NSDictionary *)options
                          handler:(nonnull FIRInstanceIDTokenHandler)handler {
    

}

- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
        // Note that this callback will be fired everytime a new token is generated, including the first
        // time. So if you need to retrieve the token as soon as it is available this is where that
        // should be done.
       // NSLog(@"FCM registration token: %@", fcmToken);
        
        //[AppUtilities savePushDeviceToken:fcmToken];
        
        //        NSData* data = [fcmToken dataUsingEncoding:NSUTF8StringEncoding];
        //        [FIRMessaging messaging].APNSToken = data;
        
        // TODO: If necessary send token to application server.
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  // If you are receiving a notification message while your app is in the background,
  // this callback will not be fired till the user taps on the notification launching the application.
  // TODO: Handle data of notification

  // With swizzling disabled you must let Messaging know about the message, for Analytics
  // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

  // Print message ID.
  /*if (userInfo[kGCMMessageIDKey]) {
    NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
  }*/

  // Print full message.
   // NSLog(@"receive 2:%@", userInfo);



}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
    fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
   NSDictionary *dic =[[NSDictionary alloc]initWithObjectsAndKeys: userInfo ,@"aps", nil];

    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];

      NSString *savedUserEmail = [userDefaults1   valueForKey:@"IsLogin"];

      [userDefaults1 synchronize];
    
    if(savedUserEmail.length == 0) {
         
        self.navigationController.navigationBar.hidden = YES;

        WelcomeViewController*welcomeVC = [WelcomeViewController new];
        [self.navigationController pushViewController:welcomeVC animated:NO];
     }
    

    else
    {

        [self showOfferNotification:userInfo.mutableCopy];

   }

 //completionHandler(UIBackgroundFetchResultNewData);
}
- (void)showAlarmWithTitle:(NSString *)text withSubtitle:(NSString*)subTitle{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:text
                                                        message:text delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
 completionHandler(UNNotificationPresentationOptionAlert+UNNotificationPresentationOptionSound + UNNotificationPresentationOptionBadge);
}

-(void)showOfferNotification:(NSMutableDictionary *)userInfo {
    

    self.navigationController.navigationBar.hidden = YES;

          EventDetailViewController*eventVc = [EventDetailViewController new];
           eventVc.farmName =  userInfo[@"aps"][@"FarmName"];
           eventVc.region =  userInfo[@"aps"][@"Region"];
           eventVc.vaccineName =  userInfo[@"aps"][@"VaccineName"];
           eventVc.vaccineType =  userInfo[@"aps"][@"VaccineType"];
           eventVc.vaccineSchedule =  userInfo[@"aps"][@"ScheduleVaccine"];
           eventVc.date =  userInfo[@"aps"][@"ScheduleDate"];
           eventVc.comments =  userInfo[@"aps"][@"Notes"];
           eventVc.image1 =  userInfo[@"aps"][@"Image1"];
        eventVc.isFrom = @"Notification";
    [self.navigationController pushViewController:eventVc animated:NO];

}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(nonnull NSData *)deviceToken {
    
    //for real testing with server
    [FIRMessaging messaging].APNSToken = deviceToken;
    [[FIRMessaging messaging] setAPNSToken:deviceToken type:FIRMessagingAPNSTokenTypeSandbox];
    
    NSString * deviceTokenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""] stringByReplacingOccurrencesOfString: @">" withString: @""]   stringByReplacingOccurrencesOfString: @" " withString: @""];
    self.fcmToken = deviceTokenString;
    //_tokenString = deviceTokenString;
    NSUserDefaults *userDefaults1 = [NSUserDefaults standardUserDefaults];
          NSString *userEmail = [userDefaults1   valueForKey:@"IsLogin"];
          [userDefaults1 synchronize];
  
    //NSLog(@"token11:%@",deviceTokenString);
    [self connectToFirebase];
    if (userEmail.length > 0) {
          [self sendTokenAtSever];
      }
}

-(void)sendTokenAtSever {
    
    BovilisApi *obj = [BovilisApi new];
    [obj sendToken: self.fcmToken WithSuccessBlock:^(BOOL success, NSDictionary *result) {
      
     
    } failure:^(NSError *error, NSString *message) {
    }];
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    
     //for firebase testing
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
   
    NSString * deviceTokenString = [[[fcmToken stringByReplacingOccurrencesOfString: @"<" withString: @""] stringByReplacingOccurrencesOfString: @">" withString: @""]   stringByReplacingOccurrencesOfString: @" " withString: @""];
   // _tokenString = deviceTokenString;
   // NSLog(@"token12:%@",deviceTokenString);

    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}


- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    
    // Add any custom logic here.
    return handled;
}



-(void)getCultures {
    
    [MBProgressHUD showHUDAddedTo:self.window.rootViewController.view animated:YES];

    self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    NSSet *set = [NSSet setWithObjects:KAcceptContentTypeJSON,kAcceptContentTypeText,nil];
    [self.sessionManager.responseSerializer setAcceptableContentTypes:set];

    self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];

    NSString  *culture =language;
    if ([culture containsString:@"en"])
    {
        
        culture=@"en-US";
    }
    else if ([culture containsString:@"it"])
    {
        culture=@"it-IT";
    }
    else if ([culture containsString:@"de"])
    {
        culture=@"de-DE";
    }
    else if ([culture containsString:@"nl"])
    {
        culture=@"nl-NL";
    }
    else if ([culture containsString:@"fr"])
    {
        culture=@"fr-FR";
    }
    else if ([culture containsString:@"es"])
    {
        culture=@"es-ES";
    }
    else if ([culture containsString:@"pt"])
    {
        culture=@"pt-PT";
    }
   
    //com.Hivelet.TTVApp
 
   NSString *urlString = [NSString stringWithFormat:@"https://services.merck-animal-health.com/translationservice/v2/GetTranslatedItems?appID=58&culture=%@&resourceKey=&resourceKeyMatchOperator=&timestamp=&subscriber=ccac8637-35e2-4fed-90c6-4e49397e80d4",culture];
    [self.sessionManager.requestSerializer setValue:[NSString stringWithFormat:@"1639fd95-a81a-4a14-9c2a-b30aec7560fb"] forHTTPHeaderField:@"X-App-Token"];

    [self.sessionManager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         
         NSError *jsonError;
         NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]initWithDictionary:responseDict];
         
         NSArray *resultArray = [jsonDict objectForKey:@"Result"];
         NSString *resultStatus = [jsonDict valueForKey:@"ResultStatus"];
         dictCulture= [NSMutableDictionary dictionary];
         languageAlert= resultStatus;
        NSLog(@"Dict:%@",jsonDict);
         if([resultStatus isEqualToString:@"Succeeded"]){
             for (int i = 0; i<[resultArray count]; i++)
             {
                 id item = [resultArray objectAtIndex:i];
                 NSDictionary *jsonDict1 = (NSDictionary *) item;
                 NSString *key = [jsonDict1 valueForKey:@"Key"];
                 NSString *value = [jsonDict1 valueForKey:@"Value"];
                 [dictCulture setValue:value forKey:key];
             }
         }
         [MBProgressHUD hideHUDForView:self.window.rootViewController.view animated:YES];
         
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.window.rootViewController.view animated:YES];

     }];
    
}


- (BOOL)application:(UIApplication *)application shouldAllowExtensionPointIdentifier:(NSString *)extensionPointIdentifier
{
    if ([extensionPointIdentifier isEqualToString:UIApplicationKeyboardExtensionPointIdentifier])
    {
        return NO;
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [FBSDKAppEvents activateApp];
    [self connectToFirebase];
    [FIRMessaging messaging].shouldEstablishDirectChannel = YES;

}

- (void)applicationWillTerminate:(UIApplication *)application{
}

@end
