
//
//  main.m
//  Bovilisplan
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//s

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
