//
//  AppDelegate.h
//  Bovilisplan
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 celeritas-solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "GAI.h"
#import "AFHTTPSessionManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong,nonatomic) UIWindow *window;
@property(nonatomic,strong) id<GAITracker> tracker;
@property(nonatomic) NSString *reloadCalendar;
@property(nonatomic,strong) NSMutableArray*imageDataFromServer;
@property(strong,nonatomic) NSMutableArray *eventAddedDateArray;
@property(nonatomic,strong) NSMutableArray *dropdownSelectionArray;
@property(nonatomic,strong) NSMutableArray *vaccineListingArray;
@property(nonatomic,strong) NSMutableArray *userListingArray;
@property(nonatomic,strong) NSMutableArray*selectedDateImagesarray;
@property(nonatomic,strong) NSMutableArray*userSevedCalenderEventArray;
@property(nonatomic,strong) NSMutableArray*getUserVaccinesFromServer;
@property(nonatomic,strong) NSMutableArray*productsArray;
@property(nonatomic,strong) NSMutableArray*daysArray;
@property(nonatomic,strong) NSMutableArray*getUserFarmsFromServer;
@property(nonatomic) NSString *vaccineName;
@property(nonatomic) NSString *vaccineDate;
@property(nonatomic) NSString *vaccineComments;
@property(nonatomic) NSString *vaccineRegion;
@property(nonatomic) NSString *numberOfVaccineCattleTotoal;
@property(nonatomic) NSMutableArray *daysArrayForCalander;
@property(nonatomic) NSString *selectdDate;
@property(nonatomic) NSString *vaccineID;
@property(nonatomic) NSString *vaccineType;
@property(nonatomic) NSString *farmName;
@property(nonatomic) NSString *scheduleVaccine;
@property(nonatomic) NSString *uniqueFarmID;
@property(nonatomic) NSString *region;
@property(nonatomic) NSString *image1;
@property(nonatomic) NSString *languageAlert;
@property(nonatomic,strong) NSMutableArray*selectedImageArrayRed;
@property(nonatomic,strong) NSMutableArray*selectedImageArrayGreen;
@property(nonatomic,strong) NSMutableArray*selectedImageArrayBrown;
@property(nonatomic,strong) NSMutableArray*selectedImageArrayRedAndGreen;
@property(nonatomic,strong) NSMutableArray*selectedImageArrayAll;
@property(nonatomic,strong) NSMutableArray*allVaccinesWithDate;
@property(nonatomic,strong) NSMutableArray*cutomVaccineName;
@property(nonatomic,strong) NSMutableArray*getVaccinesWithDate;
@property(nonatomic,strong) NSMutableArray*productsArrayList;
@property(nonatomic) NSString *farmDeleted;
@property(nonatomic) NSString *selectdVaccineName;
@property(nonatomic) NSString *removeView;
@property(nonatomic) NSDate * testString;
@property(nonatomic) NSString *switchValue1;
@property(nonatomic) NSString *switchValue2;
@property(nonatomic) NSString *switchValue3;
@property(nonatomic) NSString *switchValue4;
@property(nonatomic) NSString *methodComplete;
@property(nonatomic) NSString *selectedVaccineType;
@property(nonatomic,strong) NSDictionary * dictCulture;
@property(nonatomic,strong) NSMutableArray*getUserVaccineWithFarmName;
@property(nonatomic,strong) NSMutableArray*getAllVaccineNotificationArray;
@property(nonatomic,strong) NSMutableArray*serverAllVaccineNotificationArray;
@property(nonatomic,strong) NSMutableArray *allVaccinesWithType;
@property(nonatomic) NSString *isSwitchOn;
@property(nonatomic) NSString *vaccineIDforEdit;
@property(nonatomic) NSString *vaccineNumberOfAnimals;
@property(nonatomic,strong) NSMutableArray*animalsArray;
@property(nonatomic,strong) NSMutableArray*getMappingDaysArray;
@property(nonatomic,strong) NSMutableArray*getScheduleVaccineArray;
@property(nonatomic,strong) NSMutableArray*animalsSelectedArray;
@property(nonatomic) NSString *isLoadedNotifications;

@property(nonatomic) NSString *regionKey , *fcmToken;
@property (strong, nonatomic) UINavigationController *navigationController;

#pragma -mark AFNetworking
@property(nonatomic,strong) AFHTTPSessionManager *sessionManager;



@end

